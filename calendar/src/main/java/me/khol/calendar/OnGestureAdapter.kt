package me.khol.calendar

import android.view.GestureDetector
import android.view.MotionEvent

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 10.04.2018
 **/
abstract class OnGestureAdapter : GestureDetector.OnGestureListener {
	override fun onShowPress(e: MotionEvent?) = Unit
	override fun onDown(e: MotionEvent) = false
	override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float) = false
	override fun onScroll(e1: MotionEvent, e2: MotionEvent, distanceX: Float, distanceY: Float) = false
	override fun onLongPress(e: MotionEvent) = Unit
	override fun onSingleTapUp(e: MotionEvent) = false
}
