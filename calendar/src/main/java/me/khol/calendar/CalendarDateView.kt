package me.khol.calendar

import android.annotation.SuppressLint
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.support.v4.content.res.ResourcesCompat
import android.text.TextPaint
import android.view.View
import me.khol.extensions.dp
import me.khol.extensions.dpf
import me.khol.extensions.spf
import org.threeten.bp.LocalDate
import org.threeten.bp.format.TextStyle
import java.util.*

/**
 * TODO: 17. 3. 2018 david.khol: add class description
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 17.03.2018
 **/
@SuppressLint("ViewConstructor")
internal class CalendarDateView(
		private val calendarContext: CalendarContext
) : View(calendarContext.context) {

	private lateinit var dayOfMonthText: String
	private lateinit var dayOfWeekText: String

	var date: LocalDate = LocalDate.now()
		set(value) {
			field = value

			dayOfMonthText = date.dayOfMonth.toString()
			dayOfWeekText = date.dayOfWeek.getDisplayName(TextStyle.SHORT, Locale.getDefault())
			invalidate()
		}

	private val dateTextPaint: TextPaint = TextPaint().apply {
		color = Color.BLACK
		style = Paint.Style.FILL
		textSize = context.spf(24)
		typeface = ResourcesCompat.getFont(context, R.font.noto_sans)
		isAntiAlias = true
	}

	private val dayTextPaint: TextPaint = TextPaint().apply {
		color = Color.BLACK
		style = Paint.Style.FILL
		textSize = context.spf(14)
		typeface = ResourcesCompat.getFont(context, R.font.noto_sans)
		isAntiAlias = true
	}

	private val dateWidth: Float = dateTextPaint.measureText("31")
	private val dayWidth: Float = dayTextPaint.measureText("Mon")

	override fun onDraw(canvas: Canvas) {
		canvas.drawText(
				dayOfMonthText,
				context.dpf(2),
				context.dpf(24 + 8),
				dateTextPaint
		)
		canvas.drawText(
				dayOfWeekText,
				context.dpf(2),
				context.dpf(24 + 8 + 14),
				dayTextPaint
		)
		canvas.drawLine(
				0f,
				0f,
				0f,
				(canvas.height).toFloat(),
				calendarContext.backgroundPaint
		)
	}

	override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
		val desiredWidth = if (dateWidth > dayWidth) dateWidth else dayWidth
		val desiredHeight = context.dp(64)

		setMeasuredDimension(
				reconcileSize(desiredWidth.toInt(), widthMeasureSpec),
				reconcileSize(desiredHeight, heightMeasureSpec)
		)
	}

	private fun reconcileSize(contentSize: Int, measureSpec: Int): Int {
		val mode = View.MeasureSpec.getMode(measureSpec)
		val specSize = View.MeasureSpec.getSize(measureSpec)
		return when (mode) {
			View.MeasureSpec.EXACTLY -> specSize
			View.MeasureSpec.AT_MOST -> if (contentSize < specSize) {
				contentSize
			} else {
				specSize
			}
			View.MeasureSpec.UNSPECIFIED -> contentSize
			else -> contentSize
		}
	}

}
