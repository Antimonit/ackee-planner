package me.khol.calendar

import org.threeten.bp.Duration
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZonedDateTime
import org.threeten.bp.temporal.ChronoUnit

/**
 * Data class containing information about single event in a calendar such as
 * start time and end time, event title, event color, etc.
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 17.03.2018
 **/
data class Event(
		val id: String,
		val title: String,
		val location: String?,
		val start: ZonedDateTime,
		val end: ZonedDateTime,
		val allDay: Boolean,
		val color: Int,
		val status: Status,
		val layer: Int
) {

	enum class Status {
		CONFIRMED,
		TENTATIVE,
		CANCELLED
	}


	private fun isSameDay(start: ZonedDateTime, end: ZonedDateTime): Boolean {
		return Duration.between(start.truncatedTo(ChronoUnit.DAYS), end.truncatedTo(ChronoUnit.DAYS)).toDays() == 0L
	}

	private fun isSameDay(start: LocalDateTime, end: LocalDateTime): Boolean {
		return Duration.between(start.truncatedTo(ChronoUnit.DAYS), end.truncatedTo(ChronoUnit.DAYS)).toDays() == 0L
	}

	fun spansMultipleDays(): Boolean {
		return !isSameDay(start, end)
	}

}
