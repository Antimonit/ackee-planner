package me.khol.calendar

import android.annotation.SuppressLint
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.ViewGroup
import me.khol.extensions.beginDelayedTransition
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import org.threeten.bp.LocalDate

/**
 * TODO: 17. 3. 2018 david.khol: add class description
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 17.03.2018
 **/
@SuppressLint("ViewConstructor")
internal class CalendarHeaderLayout(
		private val calendarContext: CalendarContext,
		private val startingDate: LocalDate
) : ViewGroup(calendarContext.context) {

	private var numberOfDays: Int = 1

	private val viewCache: MutableMap<String, CalendarEventView> = mutableMapOf()

	private fun eventLayoutParams(event: Event): EventLayoutParams {
		val start = event.start.toLocalDate().toEpochDay()
		val end = event.end.toLocalDate().toEpochDay()

		return EventLayoutParams(start, end)
	}

	init {
		setWillNotDraw(false)
	}

	private lateinit var compositeDisposable: CompositeDisposable
	override fun onAttachedToWindow() {
		super.onAttachedToWindow()
		compositeDisposable = CompositeDisposable()
		compositeDisposable += calendarContext.observeNumberOfDays()
				.subscribe { numberOfDays ->
					this.numberOfDays = numberOfDays
					beginDelayedTransition()
					requestLayout()
				}

		compositeDisposable += calendarContext.observeAllDayEvents()
				.subscribe { unsortedEvents ->
					val events = unsortedEvents.sortedBy { it.start }

					beginDelayedTransition()

					val visibleEvents: MutableSet<String> = mutableSetOf()

					for (event in events) {
						visibleEvents.add(event.id)
						val calendarEventView: CalendarEventView? = viewCache[event.id]
						if (calendarEventView == null) {
							val eventView = CalendarEventView(calendarContext).apply {
								this.event = event
							}
							viewCache[event.id] = eventView
							addViewInLayout(eventView, -1, eventLayoutParams(event), true)
						} else {
							calendarEventView.event = event
							calendarEventView.layoutParams = eventLayoutParams(event)
						}
					}

					viewCache
							.map { it.key }
							.minus(visibleEvents)
							.forEach { invisibleEvent ->
								val removedView = viewCache.remove(invisibleEvent)
								removeViewInLayout(removedView)
							}

					requestLayout()
				}
	}

	override fun onDetachedFromWindow() {
		super.onDetachedFromWindow()
		compositeDisposable.dispose()
	}




	override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
		val spaceForChildren: Int = (right - left)
		val childWidth: Float = spaceForChildren / numberOfDays.toFloat()
		val daysSinceEpoch = startingDate.toEpochDay()

		// contains end times of the latest event in the column
		val columns = LongArray(3) { 0 }

		(0 until childCount)
				.map { index ->
					val child = getChildAt(index) as CalendarEventView
					val lp = child.layoutParams as EventLayoutParams

					val emptyColumnIndex = columns.indexOfFirst { occupiedUntil ->
						occupiedUntil <= lp.startDate
					}

					if (emptyColumnIndex == -1)
						// we are displaying more than 3 events, hide this one
						child.layout(0, 0, 0, 0)
 					else {
						columns[emptyColumnIndex] = lp.endDate

						val t = emptyColumnIndex * calendarContext.headerEventHeight
						val b = t + calendarContext.headerEventHeight
						val l = (lp.startDate - daysSinceEpoch) * childWidth
						val r = (lp.endDate - daysSinceEpoch) * childWidth

						child.layout(l.toInt(), t, r.toInt(), b)
					}
				}

	}

	override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
		val count = childCount

		val totalWidth = MeasureSpec.getSize(widthMeasureSpec)
		val totalHeight = MeasureSpec.getSize(heightMeasureSpec)

		if (count > 0) {
			val remainingWidth = totalWidth
			val remainingHeight = totalHeight

			val verticalWidthMeasureSpec = MeasureSpec.makeMeasureSpec(remainingWidth / count, MeasureSpec.getMode(widthMeasureSpec))
			val verticalHeightMeasureSpec = MeasureSpec.makeMeasureSpec(remainingHeight, MeasureSpec.getMode(heightMeasureSpec))

			for (i in 0 until count) {
				val child = getChildAt(i)

				measureChild(child,
						verticalWidthMeasureSpec,
						verticalHeightMeasureSpec
				)
			}
		}

		setMeasuredDimension(
				resolveSize(totalWidth, widthMeasureSpec),
				resolveSize(totalHeight, heightMeasureSpec))
	}

	override fun onDraw(canvas: Canvas) {
		super.onDraw(canvas)

		canvas.drawLine(
				0f,
				(canvas.height - 1).toFloat(),
				(canvas.width).toFloat(),
				(canvas.height - 1).toFloat(),
				calendarContext.backgroundPaint
		)
	}


	override fun generateLayoutParams(attrs: AttributeSet): EventLayoutParams {
		throw IllegalStateException("Children cannot be added from xml")
	}

	override fun generateDefaultLayoutParams(): EventLayoutParams {
		throw IllegalStateException("You must provide EventLayoutParams as child's layout params")
	}

	override fun generateLayoutParams(p: ViewGroup.LayoutParams): EventLayoutParams {
		throw IllegalStateException("You must provide EventLayoutParams as child's layout params")
	}

	override fun checkLayoutParams(p: ViewGroup.LayoutParams): Boolean {
		return p is EventLayoutParams
	}


	/**
	 * Special LayoutParams that ignore layout.width and layout.height attributes as these
	 * attributes are not used to layout the child. [startDate] and [endDate] are used instead
	 * to determine width of the child. Height is fixed.
	 */
	data class EventLayoutParams(
			var startDate: Long,
			var endDate: Long
	) : ViewGroup.LayoutParams(WRAP_CONTENT, WRAP_CONTENT) {
		init {
			if (endDate < startDate) {
				throw IllegalArgumentException("startDate cannot be after the endDate")
			}
		}
	}

}
