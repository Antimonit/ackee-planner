package me.khol.calendar

import android.content.Context
import android.support.v4.view.ViewCompat
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import android.widget.ScrollView
import me.khol.extensions.dp

/**
 * ScrollView with added functionality to scale the contents with a pinch gesture.
 *
 * It seems like we cannot scale the direct child of [ScalableScrollView] as it does nothing.
 * Workaround is to wrap the contents in another ViewGroup such as FrameLayout. In such case,
 * scale the content, not the FrameLayout.
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 18.03.2018
 **/
open class ScalableScrollView @JvmOverloads constructor(
		context: Context,
		attrs: AttributeSet? = null,
		defStyleAttr: Int = 0
) : ScrollView(context, attrs, defStyleAttr) {

	companion object {
		private const val DEAD_SCALE_HEIGHT = 16 // in dp
		private const val MIN_SCALE_SIZE_Y: Float = 0.7f
		private const val MAX_SCALE_SIZE_Y: Float = 3f
	}

	interface ScaleListener {
		fun onScale(scale: Float)
	}

	interface ScrollListener {
		fun onScroll(scroll: Int)
	}

	private val scaleDetector: ScaleGestureDetector
	private var scaleSizeY: Float = 1f

	var scaleListener: ScaleListener? = null
	var scrollListener: ScrollListener? = null

	fun setScale(scale: Float) {
		scaleSizeY = scale
	}

	protected fun onScaleChanged(oldY: Float, newY: Float) {
		scaleListener?.onScale(newY)
	}

	override fun onScrollChanged(l: Int, t: Int, oldl: Int, oldt: Int) {
		super.onScrollChanged(l, t, oldl, oldt)
		scrollListener?.onScroll(t)
	}

	init {
		isHorizontalScrollBarEnabled = false
		isVerticalFadingEdgeEnabled = false

		/* scaleDetector allows us to intercept pinch gesture */
		scaleDetector = ScaleGestureDetector(context, object : ScaleGestureDetector.SimpleOnScaleGestureListener() {
			override fun onScale(detector: ScaleGestureDetector): Boolean {
				val spanFactorY = detector.currentSpanY / detector.previousSpanY
				if (spanFactorY != 1f && detector.currentSpanY > context.dp(DEAD_SCALE_HEIGHT)) {
					val scaleFactorY: Float = spanFactorY
					val focusY: Float = detector.focusY

					val oldScaleSizeY = scaleSizeY
					val newScaleSizeY = Math.max(MIN_SCALE_SIZE_Y, Math.min(oldScaleSizeY * scaleFactorY, MAX_SCALE_SIZE_Y))
					if (oldScaleSizeY == newScaleSizeY) {
						return true
					} else {
						scaleSizeY = newScaleSizeY
					}
					onScaleChanged(oldScaleSizeY, newScaleSizeY)

					val height: Int = bottom - top
					val touchY: Float = focusY - top
					val touchPercent = touchY / height
					val touch = height * touchPercent
					scrollY = ((scrollY + touch) * scaleFactorY - touch).toInt()
//					onScrollChanged() already handled by the framework

					ViewCompat.postInvalidateOnAnimation(this@ScalableScrollView)
				}
				return true
			}
		}).apply {
			isQuickScaleEnabled = false
		}
	}

	override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
		scaleDetector.onTouchEvent(ev)
		if (!scaleDetector.isInProgress) {
			// don't allow scrolling when we are zooming
			super.dispatchTouchEvent(ev)
		}
		return true
	}

}
