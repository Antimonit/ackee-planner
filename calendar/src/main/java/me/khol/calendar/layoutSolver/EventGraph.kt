package me.khol.calendar.layoutSolver

/**
 * STEP TWO
 */
@Suppress("MemberVisibilityCanBePrivate")
internal data class EventGraph(
		val timeline: List<EventTimeline.TimelineStep>
) {

	val alpha = EventGroup()
	val omega = EventGroup()

	private val nodeToGroupMap: MutableMap<Int, EventGroup> = mutableMapOf()

	private var EventNode.group: EventGroup
		set(value) {
			nodeToGroupMap[this.index] = value
		}
		get() {
			return nodeToGroupMap[this.index]!!
		}

	init {
		// When we are adding a group of events, the alpha and omega must be part of the same
		// graph, but also must be always separated by inactive groups.
		// Add an empty EventGroup in between alpha and omega to make them connected, but still
		// separated by inactive group.
		EventGroup().apply {
			this.next += omega; omega.prev += this
			this.prev += alpha; alpha.next += this
			// remove active flag after connecting it to alpha and omega
			isActive = false
		}

		timeline.forEach { (events, type) ->
			when (type) {
				EventTimeline.TimelineStep.Type.ADD -> insert(events)
				EventTimeline.TimelineStep.Type.REMOVE -> remove(events)
			}
		}
	}

	private fun isBottomLineInvalid(): EventGroup? {
		var current = alpha
		while (current != omega) {
			if (current.isActive && !current.areNextActive && !current.arePrevActive) {
				return current
			}
			current = current.lastNext
		}
		return null
	}

	private fun getBottomLine(): List<EventGroup> {
		val groups = mutableListOf<EventGroup>()
		var current = alpha
		while (current != omega) {
			groups += current
			current = current.lastNext
		}
		groups += omega
		return groups
	}

	private fun getBottomSpaces(): List<Pair<EventGroup, EventGroup>> {
		val groups = mutableListOf<Pair<EventGroup, EventGroup>>()

		var lastActive = alpha
		var isSpace = false
		var current = alpha
		while (true) {
			if (current.isActive) {
				if (isSpace) {
					groups += lastActive to current
				}
				isSpace = false
				lastActive = current
			} else {
				isSpace = true
			}
			if (current == omega)
				break
			current = current.lastNext
		}

		return groups
	}

	private fun getInvalidGroups(bottomLine: List<EventGroup>): List<EventGroup> {
		return bottomLine.filter {
			it.isActive && !it.areNextActive && !it.arePrevActive
		}
	}
	private fun getRemovedGroups(bottomLine: List<EventGroup>): List<EventGroup> {
		return bottomLine.filter {
			!it.isActive
		}
	}

	/**
	 * Inserting will process correctly only when the bottom line is valid.
	 * Because [remove] always happens prior to this, we can fix the line there.
	 */
	private fun insert(nodes: Set<EventNode>) {
		val events = EventGroup(nodes)

		val bottomLine = getBottomLine()
		val invalidGroups = getInvalidGroups(bottomLine)
		val removedGroups = getRemovedGroups(bottomLine)
		val bottomSpaces = getBottomSpaces()

		events.nodes.forEach { node -> node.group = events }

		var left = alpha
		var right = omega

		while (left.arePrevActive && left.lastNext.isActive) {
			left = left.lastNext
		}
		while (right.areNextActive && right.lastPrev.isActive) {
			right = right.lastPrev
		}

		left = bottomSpaces[0].first
		right = bottomSpaces[0].second

		left.next += events; events.prev += left
		right.prev += events; events.next += right
	}

	private fun rotate(groupsToRotate: List<EventGroup>, alpha: EventGroup, omega: EventGroup) {
		/** leftmost groups are directly connected to the [alpha] group */
		val leftmostGroups = alpha.next.toMutableSet().apply { retainAll(groupsToRotate) }
		/** rightmost groups are directly connected to the [omega] group */
		val rightmostGroups = omega.prev.toMutableSet().apply { retainAll(groupsToRotate) }

		leftmostGroups.forEach { it.prev.remove(alpha); it.prev.add(omega) }
		rightmostGroups.forEach { it.next.remove(omega); it.next.add(alpha) }

		alpha.next.removeAll(leftmostGroups)
		alpha.next.addAll(rightmostGroups)

		omega.prev.removeAll(rightmostGroups)
		omega.prev.addAll(leftmostGroups)

		groupsToRotate.forEach { group ->
			group.isRotated = !group.isRotated
		}
	}

	private fun remove(nodes: Set<EventNode>) {
		val eventGroupsSet = nodes.map { it.group }.toSet()
		eventGroupsSet.forEach {
			val retained = it.nodes.toMutableSet().apply { retainAll(nodes) }

			val eventGroup: EventGroup = if (!it.nodes.containsAll(retained)) {
				throw IllegalArgumentException("Trying to remove events that are not part of this EventGroup")
			} else if (it.nodes.size == retained.size) {
				it
			} else if (it.areNextActive) {
				it.extractEventsToLeft(retained)
			} else {
				it.extractEventsToRight(retained)
			}
			eventGroup.let { group ->
				group.nodes.forEach { node -> node.group = group }
				group.isActive = false
			}
		}

		// check the consistency of the graph
		if (eventGroupsSet.size == 1) {
			// no problem, one removed group can never break the graph
		} else {
			// we might have broken the graph
			while (isBottomLineInvalid() != null) {
				// yep, we broke it... fix it!
				println("Bottom line is invalid")

				break

//				val invalidGroup = isBottomLineInvalid()
//				if (invalidGroup != null) {
//					if (invalidGroup.next.size == 1) {
//						val next = invalidGroup.lastNext
//						val nextPrevNext = next.prev.flatMap { it.next }.toSet()
//						if (nextPrevNext.size == 1) {
//							// this side is okay to rotate
//							// collect all groups to the left of [next]
//							var allPrev: Set<EventGroup> = next.prev.toSet()
//							var currentSize = 0
//							while (currentSize != allPrev.size) {
//								currentSize = allPrev.size
//								allPrev += allPrev.flatMap { it.prev }.toSet()
//							}
//							val groupsToRotate = allPrev.filter { it != alpha }
//							rotate(groupsToRotate, alpha, next)
//							invalidGroup.arePrevActive = true
//							continue
//						}
//					}
//					if (invalidGroup.prev.size == 1) {
//						val prev = invalidGroup.lastPrev
//						val prevNextPrev = prev.next.flatMap { it.prev }.toSet()
//						if (prevNextPrev.size == 1) {
//							// this side is okay to rotate
//							var allNext: Set<EventGroup> = prev.next.toSet()
//							var currentSize = 0
//							while (currentSize != allNext.size) {
//								currentSize = allNext.size
//								allNext += allNext.flatMap { it.next }.toSet()
//							}
//							val groupsToRotate = allNext.filter { it != omega }
//							rotate(groupsToRotate, prev, omega)
//							invalidGroup.areNextActive = true
//							continue
//						}
//					}
//
//					throw IllegalStateException("BOTTOM LINE IS STILL INVALID")
//				}
			}
		}
	}
}
