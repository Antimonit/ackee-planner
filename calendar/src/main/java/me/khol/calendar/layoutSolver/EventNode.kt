package me.khol.calendar.layoutSolver

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 18.04.2018
 **/
internal data class EventNode(
		val index: Int,
		val start: Int,
		val end: Int
) {

	var offset = Fraction(0)
	var width = Fraction(1)

}
