package me.khol.calendar.layoutSolver

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 18.04.2018
 **/
internal class EventGroup(
		val nodes: MutableSet<EventNode> = mutableSetOf()
) {

	constructor(nodes: Collection<EventNode>) : this(nodes.toMutableSet())

	private val _prev = arrayListOf<EventGroup>()
	private val _next = arrayListOf<EventGroup>()

	var isRotated: Boolean = false

	var isActive: Boolean = true
		set(value) {
			if (field != value) {
				field = value
				if (value == false) {
					arePrevActive = false
					areNextActive = false
				}
			}
		}

	var arePrevActive: Boolean = true
		set(value) {
			if (field != value) {
				field = value
				if (!value) {
					next.forEach { it.arePrevActive = false }
				}
			}
		}

	var areNextActive: Boolean = true
		set(value) {
			if (field != value) {
				field = value
				if (!value) {
					prev.forEach { it.areNextActive = false }
				}
			}
		}

	val prev: ArrayList<EventGroup>
		get() = if (isRotated) _next else _prev
	val next: ArrayList<EventGroup>
		get() = if (isRotated) _prev else _next

	val lastPrev: EventGroup
		get() = prev.last()
	val lastNext: EventGroup
		get() = next.last()

	/**
	 * @return extracted group with [events]
	 */
	fun extractEventsToLeft(events: Set<EventNode>): EventGroup {
		nodes.removeAll(events)

		val newEventGroup = EventGroup(nodes = events)
		newEventGroup.next.add(this)

		this.prev.forEach { it ->
			val index = it.next.indexOf(this)
			it.next.removeAt(index)
			it.next.add(index, newEventGroup)
			newEventGroup.prev.add(it)
		}
		this.prev.clear()
		this.prev.add(newEventGroup)
		return newEventGroup
	}

	fun extractEventsToRight(events: Set<EventNode>): EventGroup {
		nodes.removeAll(events)

		val newEventGroup = EventGroup(nodes = events)
		newEventGroup.prev.add(this)

		this.next.forEach { it ->
			val index = it.prev.indexOf(this)
			it.prev.removeAt(index)
			it.prev.add(index, newEventGroup)
			newEventGroup.next.add(it)
		}
		this.next.clear()
		this.next.add(newEventGroup)
		return newEventGroup

	}

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (javaClass != other?.javaClass) return false

		other as EventGroup

		if (nodes != other.nodes) return false
		if (prev != other.prev) return false
		if (next != other.next) return false
		if (isActive != other.isActive) return false
		if (arePrevActive != other.arePrevActive) return false
		if (areNextActive != other.areNextActive) return false

		return true
	}

	override fun toString(): String {
		return StringBuilder()
				.append("Nodes: ")
				.append(nodes.joinToString(prefix = "[", postfix = "]") { it.index.toString() })
				.append(" Prev: ${prev.size}")
				.append(" Next: ${next.size}")
				.append(" | ")
				.append("isActive $isActive")
				.append(" arePrevActive $arePrevActive")
				.append(" areNextActive $areNextActive")
				.toString()
	}

	override fun hashCode(): Int {
		var result = nodes.hashCode()
		result = 31 * result + prev.size
		result = 31 * result + next.size
		result = 31 * result + isActive.hashCode()
		result = 31 * result + arePrevActive.hashCode()
		result = 31 * result + areNextActive.hashCode()
		return result
	}

}
