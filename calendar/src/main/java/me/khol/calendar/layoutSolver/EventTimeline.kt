package me.khol.calendar.layoutSolver

/**
 * STEP ONE
 *
 * Transforms a list of singular events into groups. Each group defines a concurrency between
 * its members. A single event can be a part of multiple groups.
 * Each created group has its preceding and following neighbors empty.
 *
 * We create two sorted lists of events based on their starting time and their ending time.
 *
 * Then we traverse both of these lists at the same time, processing the first remaining element
 * from one of the lists based on their starting / ending time time.
 *
 * TODO: write better description
 *
 * @param events List of single events
 * @return eventGroups
 */
internal data class EventTimeline(
		var events: List<EventNode>
) {

	data class TimelineStep(
			val events: Set<EventNode>,
			val type: Type
	) {
		constructor(eventGroup: Collection<EventNode>, type: Type) : this(eventGroup.toSet(), type)

		enum class Type {
			ADD,
			REMOVE
		}

		override fun toString(): String {
			return (if (type == Type.ADD) "+ " else "- ") + events.toString()
		}
	}

	/**
	 * + (ABCDE)
	 * - (AB)
	 * + (FG)
	 * - (E)
	 * + (HI)
	 * - (FG)
	 * + (J)
	 * - (HI)
	 * + (KL)
	 * - (CJ)
	 * + (M)
	 * - (DKL)
	 * + (N)
	 * - (MN)
	 */
	val timelineSteps: List<TimelineStep>

	/**
	 * (ABCDE)
	 * (CDE)
	 * (CDEFG)
	 * (CDFG)
	 * (CDFGHI)
	 * (CDHI)
	 * (CDHIJ)
	 * (CDJ)
	 * (CDJKL)
	 * (DKL)
	 * (DKLM)
	 * (M)
	 * (MN)
	 * ()
	 */
	val concurrentEventGroups: List<Set<EventNode>>

	init {
		val count = events.size

		val eventsByStart = events.sortedBy { it.start }
		val eventsByEnd = events.sortedBy { it.end }

		val currentHistoryEvents = mutableListOf<EventNode>()
		val timelineGroups = mutableListOf<TimelineStep>()

		// instead of removing events from the list, just remember the index of the 'first'
		// remaining event in [eventsByStart] and [eventsByEnd]
		var start = 0
		var end = 0
		var isGrowing = true

		while (end < count) {
			if (start < count && eventsByStart[start].start < eventsByEnd[end].end) {
				// adding events
				if (!isGrowing) {
					isGrowing = true
					timelineGroups += TimelineStep(currentHistoryEvents.toList(), TimelineStep.Type.REMOVE)
					currentHistoryEvents.clear()
				}
				currentHistoryEvents.add(eventsByStart[start])
				start++
			} else {
				// removing events
				if (isGrowing) {
					isGrowing = false
					timelineGroups += TimelineStep(currentHistoryEvents.toList(), TimelineStep.Type.ADD)
					currentHistoryEvents.clear()
				}
				currentHistoryEvents.add(eventsByEnd[end])
				end++
			}
		}
		timelineGroups += TimelineStep(currentHistoryEvents.toList(), TimelineStep.Type.REMOVE)
		currentHistoryEvents.clear()

		timelineSteps = timelineGroups.toList()


		// build concurrencyGroups
		val currentConcurrentEvents = mutableSetOf<EventNode>()
		val concurrencyGroups = mutableListOf<Set<EventNode>>()

		timelineSteps.forEach { (events, type) ->
			if (type == TimelineStep.Type.ADD) {
				currentConcurrentEvents += events
				concurrencyGroups += currentConcurrentEvents.toSet()
			} else if (type == TimelineStep.Type.REMOVE) {
				currentConcurrentEvents -= events
			}
		}

		concurrentEventGroups = concurrencyGroups
	}


}
