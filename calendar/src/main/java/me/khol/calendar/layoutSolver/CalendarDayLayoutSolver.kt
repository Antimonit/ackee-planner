package me.khol.calendar.layoutSolver

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 25.03.2018
 **/
internal object CalendarDayLayoutSolver {

	private fun calculateWidths(eventGroups: List<Set<EventNode>>) {
		var unresolvedGroups = eventGroups
				.filter { it.isNotEmpty() }
				.map { group: Set<EventNode> ->
					val nodes = group.toSet()
					val remainingWidth = Fraction(1)
					Pair(nodes, remainingWidth)
				}

		while (unresolvedGroups.isNotEmpty()) {
			val (oppressedNodes, oppressedRemainingWidth) = unresolvedGroups.minBy { (nodes, remainingWidth) ->
				remainingWidth / nodes.size
			}!!

			oppressedNodes.forEach { node ->
				node.width = oppressedRemainingWidth / oppressedNodes.size
			}

			unresolvedGroups = unresolvedGroups
					.map { (nodes, remainingWidth) ->
						val retainedNodes = nodes.toMutableList().apply { removeAll(oppressedNodes) }
						val removedNodes = nodes.toMutableList().apply { retainAll(oppressedNodes) }
						val retainedRemainingWidth = removedNodes.fold(remainingWidth) { acc, node -> acc - node.width }

						Pair(retainedNodes.toSet(), retainedRemainingWidth)
					}
					.filter { (nodes, _) -> nodes.isNotEmpty() }
		}
	}

	private fun calculateOffsets(graph: EventGraph) {
		fun recursiveOffset(root: EventGroup, usedSpace: Fraction) {
			if (root == graph.omega) {
				// do nothing
			} else {
				var offset = usedSpace
				for (event: EventNode in root.nodes) {
					event.offset = max(event.offset, offset)
					offset += event.width
				}
				for (eventGroup: EventGroup in root.next) {
					recursiveOffset(eventGroup, offset)
				}
			}
		}
		recursiveOffset(graph.alpha, Fraction(0))
	}

	private fun getAllConcurrentEventGroups(graph: EventGraph): List<Set<EventNode>> {
		fun recursiveGroup(root: EventGroup): List<Set<EventNode>> {
			val followingGroups: List<Set<EventNode>> = root.next.flatMap { eventGroup ->
				recursiveGroup(eventGroup)
			}
			return if (followingGroups.isEmpty()) {
				listOf(root.nodes)
			} else {
				followingGroups.map { it: Set<EventNode> ->
					root.nodes + it
				}
			}
		}
		return recursiveGroup(graph.alpha)
	}

	fun solve(events: List<EventNode>) {
		val timeline = EventTimeline(events)
		val timelineSteps = timeline.timelineSteps
		val idealConcurrentEventGroups = timeline.concurrentEventGroups

		val graph = EventGraph(timelineSteps)
		calculateWidths(idealConcurrentEventGroups)

		val allConcurrentEventGroups = getAllConcurrentEventGroups(graph)
		calculateWidths(allConcurrentEventGroups) // recalculate widths with all the concurrencyGroups

		calculateOffsets(graph)
	}

}
