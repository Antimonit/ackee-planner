@file:Suppress("unused")

package me.khol.calendar.layoutSolver

/**
 * Fraction helps us to keep a precision that float or double cannot achieve.
 * For example 1/3f + 1/3f + 1/3f doesn't give us exact 1f, but 0.99999...
 * This is useful when adding and subtracting fractions with absolute precision is needed.
 *
 * [Fraction] is immutable.
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 26.03.2018
 **/
data class Fraction(
		private val num: Int = 0,
		private val den: Int = 1
) : Comparable<Fraction> {

	init {
		if (den == 0) {
			throw IllegalArgumentException("Denominator cannot be zero")
		}
	}

	private fun gcd(a: Int, b: Int): Int {
		var x = a
		var y = b
		while (x != y) {
			if (x > y) {
				x -= y
			} else {
				y -= x
			}
		}
		return x
	}

	private fun reduce(n: Int, d: Int): Fraction {
		if (n == 0) {
			return Fraction(0)
		}
		val gcdNum = gcd(n, d)
		return Fraction(n / gcdNum, d / gcdNum)
	}

	operator fun plus(b: Fraction): Fraction {
		val num1 = this.num * b.den + b.num * this.den
		val num2 = this.den * b.den
		return reduce(num1, num2)
	}

	operator fun minus(b: Fraction): Fraction {
		val num1 = this.num * b.den - b.num * this.den
		val num2 = this.den * b.den
		return reduce(num1, num2)
	}

	operator fun times(b: Fraction): Fraction {
		val num1 = this.num * b.num
		val num2 = this.den * b.den
		return reduce(num1, num2)
	}

	operator fun times(b: Int): Fraction {
		val num1 = this.num * b
		val num2 = this.den
		return reduce(num1, num2)
	}

	operator fun times(b: Float): Float {
		return toFloat() * b
	}

	operator fun div(b: Fraction): Fraction {
		val num1 = this.num * b.den
		val num2 = this.den * b.num
		return reduce(num1, num2)
	}

	operator fun div(b: Int): Fraction {
		val num1 = this.num
		val num2 = this.den * b
		return reduce(num1, num2)
	}

	operator fun div(b: Float): Float {
		return toFloat() / b
	}

	fun toDouble(): Double {
		return num.toDouble() / den.toDouble()
	}

	fun toFloat(): Float {
		return num.toFloat() / den.toFloat()
	}

	fun toInt(): Int {
		return (num.toFloat() / den).toInt()
	}

	override operator fun compareTo(other: Fraction): Int {
		return num * other.den - other.num * den
	}

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (other !is Fraction) return false
		return this.compareTo(other) == 0
	}

	override fun hashCode(): Int {
		var result = num
		result = 31 * result + den
		return result
	}

}

fun min(a: Fraction, b: Fraction): Fraction {
	return if (a < b) a else b
}

fun max(a: Fraction, b: Fraction): Fraction {
	return if (a < b) b else a
}
