package me.khol.calendar

import android.annotation.SuppressLint
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.View
import android.view.ViewGroup
import io.reactivex.disposables.Disposable
import org.threeten.bp.LocalDate
import org.threeten.bp.temporal.ChronoUnit

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 24.04.2018
 **/
@SuppressLint("ViewConstructor")
internal class CalendarPager(
		private val calendarContext: CalendarContext
) : ViewPager(calendarContext.context) {

	companion object {
		// I don't expect anyone to still use this application by 2070...
		private val EPOCH_DAY = LocalDate.ofYearDay(1970, 5) // make it Monday
		private val DOOMS_DAY = LocalDate.ofYearDay(2070, 1)
		private val DAY_COUNT = ChronoUnit.DAYS.between(EPOCH_DAY, DOOMS_DAY).toInt()
	}


	inner class CalendarPagerAdapter(val calendarContext: CalendarContext) : PagerAdapter() {

		private var numberOfDays: Int = 1

		/**
		 * ATTEMPT TO KEEP CURRENT ITEM ON THE SAME VIEW WHEN WE UPDATE NUMBER OF DAYS
		 * keeping the currentItem on the same view would enable smooth animations when events
		 * change their sizes
		 */

//		private var middleDate: LocalDate = EPOCH_DAY
//		private var middlePosition: Int = dateToPosition(middleDate)
//
//		fun setNumberOfDays(number: Int) {
//			middleDate = positionToDate(middlePosition)
//			val originalPosition = dateToPosition(middleDate)
//			numberOfDays = number
//			val newPosition = dateToPosition(middleDate)
//			middlePosition = originalPosition - newPosition
//			currentItem = currentItem / number * number // reset to monday
//			notifyDataSetChanged()
//		}
//
//		fun positionToDate(position: Int): LocalDate {
//			val positionDelta = position - middlePosition
//			val epochToMiddle: LocalDate = EPOCH_DAY.plusDays(middlePosition.toLong())
//			val middleToPosition: LocalDate = epochToMiddle.plusDays(positionDelta.toLong() * numberOfDays)
//			return middleToPosition
//		}
//
//		fun dateToPosition(date: LocalDate): Int {
//			val dateDelta = ChronoUnit.DAYS.between(middleDate, date).toInt()
//			val epochToMiddle = ChronoUnit.DAYS.between(EPOCH_DAY, middleDate).toInt()
//			return epochToMiddle + dateDelta / numberOfDays
//		}


		fun setNumberOfDays(number: Int) {
			currentItem = currentItem * numberOfDays / number
			numberOfDays = number
//			currentItem = currentItem / number * number // reset to monday
			notifyDataSetChanged()
			currentItem += 1
			currentItem -= 1	// hack to update current view
		}

		fun positionToDate(position: Int): LocalDate {
			return EPOCH_DAY.plusDays(position.toLong() * numberOfDays)
		}

		fun dateToPosition(date: LocalDate): Int {
			return ChronoUnit.DAYS.between(EPOCH_DAY, date).toInt() / numberOfDays
		}

		override fun getItemPosition(`object`: Any): Int {
			val currentDate = positionToDate(currentItem)
			val itemDate = (`object` as CalendarPage).startingDate
			return if (currentDate == itemDate) {
				POSITION_UNCHANGED    // the view is fine
			} else {
				POSITION_NONE        // re-initialize the view
			}
		}

		override fun instantiateItem(container: ViewGroup, position: Int): Any {
			val startingDate = positionToDate(position)
			return CalendarPage(calendarContext, startingDate).also {
				container.addView(it)
			}
		}

		override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
			container.removeView(`object` as View)
		}

		override fun getPageWidth(position: Int): Float {
			// change this if you want more pages displayed together, possibly
			// for the 3-day view
			return 1f
		}

		override fun getCount(): Int {
			return DAY_COUNT
		}

		override fun isViewFromObject(view: View, `object`: Any): Boolean {
			return view === `object`
		}

	}


	init {
		adapter = CalendarPagerAdapter(calendarContext)

		addOnPageChangeListener(object : SimpleOnPageChangeListener() {
			override fun onPageSelected(position: Int) {
				val date = (adapter as CalendarPagerAdapter).positionToDate(position)
				calendarContext.listener?.onDateChanged(date)
			}
		})
	}

	private lateinit var currentDateDisposable: Disposable
	override fun onAttachedToWindow() {
		super.onAttachedToWindow()

		calendarContext.observeNumberOfDays()
				.subscribe { numberOfDays ->
					(adapter as CalendarPagerAdapter).setNumberOfDays(numberOfDays)
				}

		currentDateDisposable = calendarContext.observeCurrentDate()
				.subscribe { currentDate ->
					currentItem = (adapter as CalendarPagerAdapter).dateToPosition(currentDate)
				}
	}

	override fun onDetachedFromWindow() {
		super.onDetachedFromWindow()
		currentDateDisposable.dispose()
	}

}
