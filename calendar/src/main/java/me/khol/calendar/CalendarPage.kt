package me.khol.calendar

import android.annotation.SuppressLint
import android.view.View
import android.view.ViewGroup.LayoutParams.*
import android.widget.FrameLayout
import android.widget.LinearLayout
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import org.threeten.bp.LocalDate

/**
 * TODO: 18. 3. 2018 david.khol: add class description
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 18.03.2018
 **/
@SuppressLint("ViewConstructor")
internal class CalendarPage(
		private val calendarContext: CalendarContext,
		val startingDate: LocalDate
) : LinearLayout(calendarContext.context) {

	private val headerDates = CalendarDateLayout(calendarContext, startingDate)
	private val headerEvents = CalendarHeaderLayout(calendarContext, startingDate)
	private val scalableScrollView: ScalableScrollView
	private val calendarLayout = CalendarLayout(calendarContext, startingDate)


	init {
		orientation = VERTICAL

		val internalView = FrameLayout(context)

		scalableScrollView = ScalableScrollView(context).apply {
			addView(calendarLayout, MATCH_PARENT, MATCH_PARENT)
			scaleListener = object : ScalableScrollView.ScaleListener {
				override fun onScale(scale: Float) {
					calendarContext.setScale(scale)
					internalView.requestLayout()
				}
			}
			scrollListener = object : ScalableScrollView.ScrollListener {
				override fun onScroll(scroll: Int) {
					calendarContext.setScroll(scroll)
				}
			}
			addOnLayoutChangeListener(object: OnLayoutChangeListener {
				override fun onLayoutChange(v: View?, left: Int, top: Int, right: Int, bottom: Int, oldLeft: Int, oldTop: Int, oldRight: Int, oldBottom: Int) {
					removeOnLayoutChangeListener(this)
					// scrolls and scales pages that are not yet created
					scrollTo(0, calendarContext.observeScroll().blockingFirst())
					setScale(calendarContext.observeScale().blockingFirst())
				}
			})
		}

		internalView.addView(scalableScrollView, MATCH_PARENT, MATCH_PARENT)

		this.addView(headerDates, MATCH_PARENT, calendarContext.headerDateHeight)
		this.addView(headerEvents, MATCH_PARENT, calendarContext.headerEventHeight * calendarContext.maxHeaderEvents)
		this.addView(internalView, MATCH_PARENT, MATCH_PARENT)
	}

	private lateinit var compositeDisposable: CompositeDisposable
	override fun onAttachedToWindow() {
		super.onAttachedToWindow()
		compositeDisposable = CompositeDisposable()
		compositeDisposable += calendarContext.observeScroll()
				.subscribe { scroll ->
					// scrolls already existing pages
					scalableScrollView.scrollTo(0, scroll)
				}
		compositeDisposable += calendarContext.observeScale()
				.subscribe { scale ->
					// scales already existing pages
					scalableScrollView.setScale(scale)
				}
	}

	override fun onDetachedFromWindow() {
		super.onDetachedFromWindow()
		compositeDisposable.dispose()
	}

}
