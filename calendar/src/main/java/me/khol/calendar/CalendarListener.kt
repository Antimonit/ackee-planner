package me.khol.calendar

import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 09.04.2018
 **/
interface CalendarListener {

	fun onEventClick(event: Event)

	fun onEventLongClick(event: Event)

	fun onEmptySpaceClick(dateTime: LocalDateTime)

	fun onEmptySpaceLongClick(dateTime: LocalDateTime)

	fun onDateChanged(date: LocalDate)

}
