@file:Suppress("MemberVisibilityCanBePrivate")

package me.khol.calendar

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.graphics.*
import android.os.Build
import android.support.annotation.ColorInt
import android.support.v4.content.res.ResourcesCompat
import android.support.v4.view.ViewCompat
import android.text.TextPaint
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.view.ViewOutlineProvider
import me.khol.extensions.*

/**
 * View displaying a rectangle with name of an event
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 15.12.2017
 **/
@SuppressLint("ViewConstructor")
internal class CalendarEventView(
		val calendarContext: CalendarContext
) : ViewWithForeground(calendarContext.context) {

	var event: Event? = null
		set(value) {
			field = value

			if (value == null)
				return

			val alpha = if (value.layer > 0) 0xFF else 0x5F
			text = value.title
			when (value.status) {
				Event.Status.CONFIRMED -> {
					setBackgroundBorderColor(value.color.withAlpha(alpha))
					setTextColor(Color.WHITE)
					setBackgroundInnerColor(value.color.withAlpha(alpha))
				}
				Event.Status.TENTATIVE -> {
					setBackgroundBorderColor(value.color)
					setTextColor(value.color)
					setBackgroundInnerColor(Color.WHITE.withAlpha(alpha))
				}
				Event.Status.CANCELLED -> {
					setBackgroundBorderColor(Color.GRAY.withAlpha(alpha))
					setTextColor(Color.WHITE)
					setBackgroundInnerColor(Color.GRAY.withAlpha(alpha))
				}
			}
			isClickable = value.layer == 1
			isInteractive = value.layer == 1
			ViewCompat.setElevation(this, value.layer * context.dpf(4))
		}


	private val tapDetector: GestureDetector

	private val boundsF = RectF()
	private val bounds = Rect()
	private val innerBoundsF = RectF()
	private val backgroundBorderPaint: Paint
	private val backgroundInnerPaint: Paint
	private val textPaint: TextPaint

	private val textOffsetTop: Float
	private val textOffsetLeft: Float
	private var isInteractive: Boolean = false

	private fun setBackgroundBorderColor(@ColorInt color: Int) {
		backgroundBorderPaint.color = color
		postInvalidate()
	}

	private fun setBackgroundInnerColor(@ColorInt color: Int) {
		backgroundInnerPaint.color = color
		postInvalidate()
	}

	private fun setTextColor(@ColorInt color: Int) {
		textPaint.color = color
		postInvalidate()
	}

	private var text: String

	private var cornerRadius: Float

	private var textSize: Float
		get() {
			return textPaint.textSize
		}
		set(value) {
			textPaint.textSize = value
		}

	init {
		foregroundDrawable = getDrawableAttr(android.R.attr.selectableItemBackground)

		setPadding(
				context.dp(2),
				context.dp(0),
				context.dp(0),
				context.dp(2)
		)

		textPaint = TextPaint().apply {
			style = Paint.Style.FILL
			typeface = ResourcesCompat.getFont(context, R.font.noto_sans)
			isAntiAlias = true
		}

		backgroundBorderPaint = Paint().apply {
			style = Paint.Style.FILL
		}

		backgroundInnerPaint = Paint().apply {
			style = Paint.Style.FILL
		}

		setTextColor(Color.WHITE)
		setBackgroundInnerColor(Color.GRAY)
		setBackgroundBorderColor(Color.WHITE)

		text = ""
		cornerRadius = context.dpf(2)
		textSize = context.spf(12)

		textOffsetTop = -textPaint.fontMetrics.top
		textOffsetLeft = context.dpf(2)

		tapDetector = GestureDetector(context, object : OnGestureAdapter() {
			override fun onSingleTapUp(e: MotionEvent): Boolean {
				event?.let {
					calendarContext.listener?.onEventClick(it)
				}
				return true
			}

			override fun onLongPress(e: MotionEvent) {
				event?.let {
					calendarContext.listener?.onEventLongClick(it)
				}
			}
		})
	}

	private fun refreshBounds(width: Int = this.width, height: Int = this.height) {
		bounds.set(
				paddingLeft,
				paddingTop,
				width - paddingRight,
				height - paddingBottom
		)
		boundsF.set(bounds)
		innerBoundsF.set(
				boundsF.left + context.dpf(1),
				boundsF.top + context.dpf(1),
				boundsF.right - context.dpf(1),
				boundsF.bottom - context.dpf(1)
		)
	}


	override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
		// Measure maximum possible width of text.
		val maxTextWidth = textPaint.measureText(text)
		// Estimate maximum possible height of text.
		val fontMetrics = textPaint.fontMetrics
		val maxTextHeight = -fontMetrics.top + fontMetrics.bottom

		// Add padding to maximum width calculation.
		val desiredWidth = Math.round(maxTextWidth + paddingLeft.toFloat() + paddingRight.toFloat())

		// Add padding to maximum height calculation.
		val desiredHeight = Math.round(maxTextHeight * 2f + paddingTop.toFloat() + paddingBottom.toFloat())

		setMeasuredDimension(
				reconcileSize(desiredWidth, widthMeasureSpec),
				reconcileSize(desiredHeight, heightMeasureSpec)
		)
	}

	private fun reconcileSize(contentSize: Int, measureSpec: Int): Int {
		val mode = View.MeasureSpec.getMode(measureSpec)
		val specSize = View.MeasureSpec.getSize(measureSpec)
		return when (mode) {
			View.MeasureSpec.EXACTLY -> specSize
			View.MeasureSpec.AT_MOST -> if (contentSize < specSize) {
				contentSize
			} else {
				specSize
			}
			View.MeasureSpec.UNSPECIFIED -> contentSize
			else -> contentSize
		}
	}

	override fun onTouchEvent(event: MotionEvent): Boolean {
		super.onTouchEvent(event)    // needed to show ripple
		if (isInteractive) {
			tapDetector.onTouchEvent(event)
			return true    // consume the event so the tapDetector correctly handles single click
		}
		// TODO: notify the parent that the event was long pressed
		// currently we need to return true to correctly handle single click and at the same time
		// false to let the parent handle the long click
		return false    // don't consume the event, parent might want to do something on long click
	}

	override fun onDraw(canvas: Canvas) {
		super.onDraw(canvas)
		refreshBounds(canvas.width, canvas.height)

		canvas.drawRoundRect(boundsF, cornerRadius, cornerRadius, backgroundBorderPaint)
		canvas.drawRoundRect(innerBoundsF, cornerRadius, cornerRadius, backgroundInnerPaint)
		canvas.drawText(text, paddingLeft + textOffsetLeft, paddingTop + textOffsetTop, textPaint)
	}

	override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
		super.onSizeChanged(w, h, oldw, oldh)
		if (targetApi(Build.VERSION_CODES.LOLLIPOP)) {
			outlineProvider = EventOutline(w, h)
		}
	}

	/**
	 * Defines an outline of the view so the framework can correctly display shadows for the view
	 */
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	private inner class EventOutline(var width: Int, var height: Int) : ViewOutlineProvider() {
		override fun getOutline(view: View, outline: Outline) {
			refreshBounds(width, height)
			outline.setRoundRect(bounds, cornerRadius)
		}
	}

}
