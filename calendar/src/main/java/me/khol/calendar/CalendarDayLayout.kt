package me.khol.calendar

import android.annotation.SuppressLint
import android.graphics.Canvas
import android.support.v4.view.ViewCompat
import android.util.AttributeSet
import android.view.GestureDetector
import android.view.View
import android.view.ViewGroup
import org.threeten.bp.temporal.ChronoField
import org.threeten.bp.temporal.ChronoUnit
import android.view.MotionEvent
import me.khol.extensions.beginDelayedTransition
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import me.khol.calendar.layoutSolver.CalendarDayLayoutSolver
import me.khol.calendar.layoutSolver.EventNode
import org.threeten.bp.*

/**
 * Single column of [CalendarEventView]s inside of a [CalendarLayout].
 * See [onLayout] for the logic behind laying out the [CalendarEventView]s.
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 24.02.2018
 * @see [onLayout]
 **/
@SuppressLint("ViewConstructor")
internal class CalendarDayLayout(
		private val calendarContext: CalendarContext
) : ViewGroup(calendarContext.context) {

	private val tapDetector: GestureDetector
	private var isFloatingMode: Boolean = false
	private var floatingEvent: Event = Event(
			"floating",
			"",
			null,
			ZonedDateTime.now(),
			ZonedDateTime.now().plus(1, ChronoUnit.HOURS),
			false,
			-2818048,
			Event.Status.CONFIRMED,
			2
	)
	private val floatingEventView: CalendarEventView

	lateinit var date: LocalDate

	private var scaledHourSize: Float = calendarContext.observeScaledHourSize().blockingFirst()

	private fun eventLayoutParams(event: Event): EventLayoutParams {
		val start = event.start.get(ChronoField.MINUTE_OF_DAY)
		val end = event.end.get(ChronoField.MINUTE_OF_DAY) +
				// This ensures that events that last until 0:00 are computed correctly
				Duration.between(
						event.start.truncatedTo(ChronoUnit.DAYS),
						event.end.truncatedTo(ChronoUnit.DAYS)
				).toDays().toInt() * ChronoUnit.DAYS.duration.toMinutes().toInt()
		val depth = event.layer

		return EventLayoutParams(start, end, depth)
	}


	private val viewCache: MutableMap<String, CalendarEventView> = mutableMapOf()

	fun setDailyEvents(dayEvents: List<Event>) {
		beginDelayedTransition()

		val visibleEvents: MutableSet<String> = mutableSetOf()

		for (event in dayEvents) {
			visibleEvents.add(event.id)
			val calendarEventView: CalendarEventView? = viewCache[event.id]
			if (calendarEventView == null) {
				val eventView = CalendarEventView(calendarContext).apply {
					this.event = event
				}
				viewCache[event.id] = eventView
				addViewInLayout(eventView, -1, eventLayoutParams(event), true)
			} else {
				calendarEventView.event = event
				calendarEventView.layoutParams = eventLayoutParams(event)
			}
		}

		viewCache.map { it.key }
				.minus(visibleEvents)
				.forEach { invisibleEvent ->
					val removedView = viewCache.remove(invisibleEvent)
					removeViewInLayout(removedView)
				}

		floatingEventView.visibility = View.GONE
		requestLayout()
	}

	init {
		setWillNotDraw(false)

		tapDetector = GestureDetector(context, object : OnGestureAdapter() {
			override fun onSingleTapUp(e: MotionEvent): Boolean {
				calendarContext.listener?.onEmptySpaceClick(getDateTimeFromMotionEvent(e))
				return true
			}

			override fun onLongPress(e: MotionEvent) {
				onFloatingEventDown(e)
			}
		})

		floatingEventView = CalendarEventView(calendarContext).apply {
			this.event = floatingEvent
			this.visibility = View.GONE
		}
		if (calendarContext.observeAllowInsertionOfEventsSubject().blockingFirst()) {
			addViewInLayout(floatingEventView, -1, eventLayoutParams(floatingEvent), true)
		}
	}

	private lateinit var compositeDisposable: CompositeDisposable
	override fun onAttachedToWindow() {
		super.onAttachedToWindow()
		compositeDisposable = CompositeDisposable()
		compositeDisposable += calendarContext.observeScaledHourSize()
				.subscribe { scaledHourSize ->
					this.scaledHourSize = scaledHourSize
					requestLayout()
					ViewCompat.postInvalidateOnAnimation(this)
				}
	}

	override fun onDetachedFromWindow() {
		super.onDetachedFromWindow()
		compositeDisposable.dispose()
	}


	private fun updateFloatingEvent(e: MotionEvent) {
		val duration = calendarContext.observeDuration().blockingFirst().toLong()
		val startDateTime = getDateTimeFromMotionEvent(e)
		floatingEvent = floatingEvent.copy(
				start = startDateTime.atZone(ZoneId.systemDefault()),
				end = startDateTime.plus(duration, ChronoUnit.MINUTES).atZone(ZoneId.systemDefault())
		)
		floatingEventView.layoutParams = eventLayoutParams(floatingEvent)
	}

	private fun getDateTimeFromMotionEvent(e: MotionEvent): LocalDateTime {
		val clickedHours = e.y / scaledHourSize
		val hours = Math.min(Math.max(0f, clickedHours - 0.5f), 23.75f)
		val time = LocalTime.of(hours.toInt(), ((hours % 1) * 60).toInt())
		val snappedTime = time.withMinute(time.minute / 15 * 15)
		return LocalDateTime.of(date, snappedTime)
	}

	private fun onFloatingEventDown(event: MotionEvent) {
		isFloatingMode = true
		requestDisallowInterceptTouchEvent(true)
		updateFloatingEvent(event)
		floatingEventView.visibility = View.VISIBLE
	}

	private fun onFloatingEventMove(event: MotionEvent) {
		if (isFloatingMode) {
			(floatingEventView.layoutParams as (EventLayoutParams)).apply {
				updateFloatingEvent(event)
				invalidate()
			}
			floatingEventView.visibility = View.VISIBLE
		}
	}

	private fun onFloatingEventUp(event: MotionEvent) {
		if (isFloatingMode) {
			isFloatingMode = false
			requestDisallowInterceptTouchEvent(false)
			val newDate = getDateTimeFromMotionEvent(event)
			floatingEventView.event = floatingEventView.event?.copy(id = newDate.toString())
			calendarContext.listener?.onEmptySpaceLongClick(newDate)
		}
	}

	override fun onTouchEvent(event: MotionEvent): Boolean {
		if (event.action == MotionEvent.ACTION_MOVE) {
			onFloatingEventMove(event)
		}
		if (event.action == MotionEvent.ACTION_UP) {
			onFloatingEventUp(event)
		}
		tapDetector.onTouchEvent(event)
		return true
	}

	/**
	 * We receive start and end time of each event through [EventLayoutParams] which we use
	 * to compute events' Y-axis positions and heights.
	 *
	 * It is also our obligation to find out children's X-axis position and width so that events
	 * don't overlap each other and that each event receives as much space available. This
	 * algorithm is encapsulated in [CalendarDayLayoutSolver].
	 *
	 * Y-axis position and height is represented in number of minutes since midnight.
	 * X-axis position and width is represented in a float value between (0.0, 1.0)
	 */
	@SuppressLint("DrawAllocation")
	override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
		(0 until childCount)
				.map { index ->
					val child = getChildAt(index) as CalendarEventView
					val lp = child.layoutParams as EventLayoutParams
					index to lp
				}
				.groupBy({ (_, lp) ->
					lp.depth
				}, { (index, lp) ->
					EventNode(index, lp.startTime, lp.endTime)
				})
				.map { (_, events) ->
					events
				}
				.flatMap { events: List<EventNode> ->
					CalendarDayLayoutSolver.solve(events)
					events
				}
				.forEach { event ->
					val index = event.index
					val t = event.start * scaledHourSize / 60
					val b = event.end * scaledHourSize / 60
					val l = event.offset * width
					val r = (event.offset + event.width) * width

					getChildAt(index).layout(l.toInt(), t.toInt(), r.toInt(), b.toInt())
				}
	}

	override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
		val count = childCount

		var maxHeight = 0
		var maxWidth = 0
		var childState = 0

		for (i in 0 until count) {
			val child = getChildAt(i)

			if (child.visibility == View.GONE)
				continue

			measureChild(child, widthMeasureSpec, heightMeasureSpec)

			maxWidth = Math.max(maxWidth, child.measuredWidth)
			maxHeight = Math.max(maxHeight, child.measuredHeight)
			childState = View.combineMeasuredStates(childState, child.measuredState)
		}

		// Check against our minimum height and width
		maxHeight = Math.max(maxHeight, suggestedMinimumHeight)
		maxWidth = Math.max(maxWidth, suggestedMinimumWidth)

//		super.onMeasure(widthMeasureSpec, heightMeasureSpec)
		setMeasuredDimension(
				View.resolveSizeAndState(maxWidth, widthMeasureSpec, childState),
				View.resolveSizeAndState(maxHeight, heightMeasureSpec, childState shl View.MEASURED_HEIGHT_STATE_SHIFT)
		)
	}

	override fun onDraw(canvas: Canvas) {
		super.onDraw(canvas)
		// right border
		canvas.drawLine(
				(canvas.width - 1).toFloat(),
				0f,
				(canvas.width - 1).toFloat(),
				(canvas.height - 1).toFloat(),
				calendarContext.backgroundPaint
		)
	}


	override fun generateLayoutParams(attrs: AttributeSet): EventLayoutParams {
		throw IllegalStateException("Children cannot be added from xml")
	}

	override fun generateDefaultLayoutParams(): EventLayoutParams {
		throw IllegalStateException("You must provide EventLayoutParams as child's layout params")
	}

	override fun generateLayoutParams(p: ViewGroup.LayoutParams): EventLayoutParams {
		throw IllegalStateException("You must provide EventLayoutParams as child's layout params")
	}

	override fun checkLayoutParams(p: ViewGroup.LayoutParams): Boolean {
		return p is EventLayoutParams
	}


	/**
	 * Special LayoutParams that ignore layout.width and layout.height attributes as these
	 * attributes are not used to layout the child. [startTime] and [endTime] are used instead
	 * to determine height of the child. Width is determined based on a complex algorithm and
	 * width of the parent.
	 */
	data class EventLayoutParams(
			var startTime: Int,
			var endTime: Int,
			var depth: Int
	) : ViewGroup.LayoutParams(WRAP_CONTENT, WRAP_CONTENT) {
		init {
			if (endTime < startTime) {
				throw IllegalArgumentException("startTime cannot be after the endTime")
			}
		}
	}

}
