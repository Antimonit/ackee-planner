package me.khol.calendar

import android.annotation.SuppressLint
import android.graphics.Canvas
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.LinearLayout
import me.khol.extensions.beginDelayedTransition
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import org.threeten.bp.LocalDate

/**
 * A [ViewGroup] containing a [CalendarTimesView] on the left side of the calendar and
 * one or more [CalendarDayLayout]s displaying events for the specified days
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 14.03.2018
 **/
@SuppressLint("ViewConstructor")
internal class CalendarLayout(
		private val calendarContext: CalendarContext,
		private val startingDate: LocalDate
) : LinearLayout(calendarContext.context) {

	private var timesView: CalendarTimesView
	private var dayLayouts: Array<CalendarDayLayout>

	init {
		orientation = HORIZONTAL

		setWillNotDraw(false)

		/*
		 * Add a static [CalendarTimesView] with 0 width.
		 * It fixes the height of the rest of day columns.
		 */
		timesView = CalendarTimesView(calendarContext).also {
			val timesLP = LinearLayout.LayoutParams(0, WRAP_CONTENT)
			addViewInLayout(it, -1, timesLP, true)
		}

		/* Add 7 static [CalendarDayLayout]s, hide and reveal them as necessary */
		dayLayouts = Array(calendarContext.maxDays) {
			CalendarDayLayout(calendarContext).also {
				val eventLP = LinearLayout.LayoutParams(0, MATCH_PARENT, 1f)
				addViewInLayout(it, -1, eventLP, true)
			}
		}

		requestLayout()
	}

	private lateinit var compositeDisposable: CompositeDisposable
	override fun onAttachedToWindow() {
		super.onAttachedToWindow()
		compositeDisposable = CompositeDisposable()
		compositeDisposable += calendarContext.observeNumberOfDays()
				.subscribe { numberOfDays ->
					beginDelayedTransition()
					for (i in dayLayouts.indices) {
						if (i < numberOfDays) {
							dayLayouts[i].visibility = View.VISIBLE
						} else {
							dayLayouts[i].visibility = View.GONE
						}
					}
					requestLayout()
				}

		compositeDisposable += calendarContext.observeNormalEvents()
				.subscribe { events ->
					// group events by starting date
					val dayEvents = events.groupBy({ event ->
						event.start.toLocalDate()
					})

					for (i in 0 until calendarContext.observeNumberOfDays().blockingFirst()) {
						val date: LocalDate = startingDate.plusDays(i.toLong())
						dayLayouts[i].date = date
						dayLayouts[i].setDailyEvents(dayEvents[date] ?: emptyList())
					}

					requestLayout()
					invalidate()
				}
	}

	override fun onDetachedFromWindow() {
		super.onDetachedFromWindow()
		compositeDisposable.dispose()
	}


	override fun onDraw(canvas: Canvas) {
		super.onDraw(canvas)

		val scaledHourSize = calendarContext.observeScaledHourSize().blockingFirst()

		(1 until 24).forEach { hours ->
			canvas.drawLine(
					0f,
					scaledHourSize * hours,
					canvas.width.toFloat(),
					scaledHourSize * hours,
					calendarContext.backgroundPaint
			)
		}
	}

}
