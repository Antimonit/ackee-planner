package me.khol.calendar

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.support.v4.graphics.drawable.DrawableCompat
import android.util.AttributeSet
import android.view.View

/**
 * Adds a [foregroundDrawable] functionality to [View].
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 10.04.2018
 **/
open class ViewWithForeground @JvmOverloads constructor(
		context: Context,
		attrs: AttributeSet? = null,
		defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

	var foregroundDrawable: Drawable? = null
		set(value) {
			field = value
			field?.callback = this
		}

	override fun onDrawForeground(canvas: Canvas) {
		super.onDrawForeground(canvas)
		foregroundDrawable?.draw(canvas)
	}

	override fun verifyDrawable(who: Drawable): Boolean {
		return super.verifyDrawable(who) || who === foregroundDrawable
	}

	@TargetApi(11)
	override fun jumpDrawablesToCurrentState() {
		super.jumpDrawablesToCurrentState()
		foregroundDrawable?.jumpToCurrentState()
	}

	override fun drawableStateChanged() {
		super.drawableStateChanged()
		foregroundDrawable?.state = drawableState
		invalidate()
	}

	override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
		super.onSizeChanged(w, h, oldw, oldh)
		foregroundDrawable?.setBounds(
				paddingLeft,
				paddingTop,
				w - paddingRight,
				h - paddingBottom
		)
	}

	override fun drawableHotspotChanged(x: Float, y: Float) {
		super.drawableHotspotChanged(x, y)
		foregroundDrawable?.let {
			DrawableCompat.setHotspot(it, x, y)
		}
	}

}
