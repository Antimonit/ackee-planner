package me.khol.calendar

import android.annotation.SuppressLint
import android.graphics.Canvas
import android.view.View
import android.view.ViewGroup
import me.khol.extensions.beginDelayedTransition
import me.khol.extensions.dp
import io.reactivex.disposables.Disposable
import org.threeten.bp.LocalDate

/**
 * TODO: 17. 3. 2018 david.khol: add class description
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 17.03.2018
 **/
@SuppressLint("ViewConstructor")
internal class CalendarDateLayout(
		private val calendarContext: CalendarContext,
		private val startingDate: LocalDate
) : ViewGroup(calendarContext.context) {

	private val dateLayouts: Array<CalendarDateView>

	init {
		setWillNotDraw(false)

		/* Add 7 static [CalendarDateView]s, hide and reveal them as necessary */
		dateLayouts = Array(calendarContext.maxDays) { i ->
			CalendarDateView(calendarContext).apply {
				date = startingDate.plusDays(i.toLong())
			}.also {
				val headerLP = LayoutParams(LayoutParams.MATCH_PARENT, context.dp(48))
				addViewInLayout(it, -1, headerLP, true)
			}
		}
	}

	override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
		val count = childCount

		var leftSpace = 0f
		val spaceForChildren = (right - left)
		val childWidth = spaceForChildren / count.toFloat()

		for (i in 0 until count) {
			val child = getChildAt(i)
			child.layout(
					leftSpace.toInt(),
					0,
					(leftSpace + childWidth).toInt(),
					bottom - top
			)
			leftSpace += childWidth
		}
	}

	override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
		val count = childCount

		val totalWidth = MeasureSpec.getSize(widthMeasureSpec)
		val totalHeight = MeasureSpec.getSize(heightMeasureSpec)

		if (count > 0) {
			val remainingWidth = totalWidth
			val remainingHeight = totalHeight

			val verticalWidthMeasureSpec = MeasureSpec.makeMeasureSpec(remainingWidth / count, MeasureSpec.getMode(widthMeasureSpec))
			val verticalHeightMeasureSpec = MeasureSpec.makeMeasureSpec(remainingHeight, MeasureSpec.getMode(heightMeasureSpec))

			for (i in 0 until count) {
				val child = getChildAt(i)

				measureChild(child,
						verticalWidthMeasureSpec,
						verticalHeightMeasureSpec
				)
			}
		}

		setMeasuredDimension(
				resolveSize(totalWidth, widthMeasureSpec),
				resolveSize(totalHeight, heightMeasureSpec))
	}

	override fun onDraw(canvas: Canvas) {
		super.onDraw(canvas)

		canvas.drawLine(
				0f,
				(canvas.height - 1).toFloat(),
				(canvas.width).toFloat(),
				(canvas.height - 1).toFloat(),
				calendarContext.backgroundPaint
		)
	}

	private lateinit var numberOfDaysDisposable: Disposable
	override fun onAttachedToWindow() {
		super.onAttachedToWindow()
		numberOfDaysDisposable = calendarContext.observeNumberOfDays()
				.subscribe { numberOfDays ->
					beginDelayedTransition()
					for (i in dateLayouts.indices) {
						if (i < numberOfDays) {
							dateLayouts[i].visibility = View.VISIBLE
						} else {
							dateLayouts[i].visibility = View.GONE
						}
					}
					requestLayout()
				}
	}

	override fun onDetachedFromWindow() {
		super.onDetachedFromWindow()
		numberOfDaysDisposable.dispose()
	}

}
