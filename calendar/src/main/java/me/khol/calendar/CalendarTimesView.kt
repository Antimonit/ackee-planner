package me.khol.calendar

import android.annotation.SuppressLint
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.support.v4.content.res.ResourcesCompat
import android.support.v4.view.ViewCompat
import android.text.TextPaint
import android.view.View
import me.khol.extensions.spf

/**
 * Displays a narrow list of times from 01:00 to 23:00.
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 17.03.2018
 **/
@SuppressLint("ViewConstructor")
internal class CalendarTimesView(
		calendarContext: CalendarContext
) : View(calendarContext.context) {

	private val textPaint = TextPaint().apply {
		color = Color.BLACK
		style = Paint.Style.FILL
		textSize = context.spf(12)
		typeface = ResourcesCompat.getFont(context, R.font.noto_sans)
		isAntiAlias = true
	}
	private val textHeight: Float
	private val textWidth: Float

	private var scaledHourSize: Float

	init {
		val bounds = Rect()
		textPaint.getTextBounds("00:00", 0, 5, bounds)
		textHeight = bounds.height().toFloat()
		textWidth = bounds.width().toFloat()

		scaledHourSize = 0f
		calendarContext.observeScaledHourSize()
				.subscribe { scaledHourSize ->
					this.scaledHourSize = scaledHourSize
					requestLayout()
					ViewCompat.postInvalidateOnAnimation(this)
				}
	}

	override fun onDraw(canvas: Canvas) {
		super.onDraw(canvas)

		(1 until 24).forEach { hours ->
			val text = if (hours < 10) "0$hours:00" else "$hours:00"
			canvas.drawText(
					text,
					(canvas.width - textWidth) / 2,
					scaledHourSize * hours + textHeight / 2,
					textPaint
			)
		}
	}

	override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
		val height: Int = (24 * scaledHourSize).toInt()

		setMeasuredDimension(
				getDefaultSize(suggestedMinimumWidth, widthMeasureSpec),
				getDefaultSize(height, heightMeasureSpec)
		)
	}

}
