package me.khol.calendar

import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import me.khol.extensions.dp
import org.threeten.bp.Duration
import org.threeten.bp.LocalDate
import org.threeten.bp.temporal.ChronoUnit

/**
 * A class that is passed to every view in the Calendar view hierarchy. Contains some global values
 * such as widths and height of some components and current state of the Calendar View, such as
 * events to display, current scroll amount
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 29.04.2018
 **/
internal class CalendarContext(val context: Context) {

	val maxDays = 7

	val headerEventHeight = context.dp(24)
	val headerDateHeight = context.dp(56)
	val maxHeaderEvents = 1
	val headerHeight = headerDateHeight + headerEventHeight * maxHeaderEvents

	val timesWidth = context.dp(40)

	private val defaultHourSize = context.dp(32)

	val backgroundPaint: Paint = Paint().apply {
		color = Color.LTGRAY
		style = Paint.Style.STROKE
		strokeWidth = 1f // very thin
	}

	private val allowInsertionOfEventsSubject: Subject<Boolean> = BehaviorSubject.createDefault(false)
	fun setAllowInsertionOfEvents(value: Boolean) {
		allowInsertionOfEventsSubject.onNext(value)
	}
	fun observeAllowInsertionOfEventsSubject(): Observable<Boolean> = allowInsertionOfEventsSubject


	/**
	 * Callbacks for clicking/long-clicking on an event/empty space. Also notifies that
	 * a date has changed.
	 */
	var listener: CalendarListener? = null


	/**
	 * Number of days to display next to each other.
	 */
	private val numberOfDaysSubject: Subject<Int> = BehaviorSubject.createDefault(1)
	fun setNumberOfDays(value: Int) {
		numberOfDaysSubject.onNext(value)
	}
	fun observeNumberOfDays(): Observable<Int> = numberOfDaysSubject


	/**
	 * The date of the left-most visible day.
	 */
	private val currentDateSubject: Subject<LocalDate> = BehaviorSubject.createDefault(LocalDate.now())
	fun setCurrentDate(value: LocalDate) {
		listener?.onDateChanged(value)
		currentDateSubject.onNext(value)
	}
	fun observeCurrentDate(): Observable<LocalDate> = currentDateSubject


	private val eventsSubject: Subject<List<Event>> = BehaviorSubject.createDefault(emptyList())
	private val allDayEventsSubject: Subject<List<Event>> = BehaviorSubject.createDefault(emptyList())
	private val normalEventsSubject: Subject<List<Event>> = BehaviorSubject.createDefault(emptyList())
	fun setEvents(events: List<Event>) {
		eventsSubject.onNext(events)

		val groupBy: Map<Boolean, List<Event>> = events.groupBy { it.allDay }
		val allDayEvents = groupBy[true] ?: emptyList()
		val normalEventsRaw = groupBy[false] ?: emptyList()
		val normalEvents = normalEventsRaw.flatMap { originalEvent ->
			val splitEvents = mutableListOf<Event>()

			var current = originalEvent
			while (current.spansMultipleDays()) {
				splitEvents.add(current.copy(start = current.start, end = current.start.truncatedTo(ChronoUnit.DAYS).plusDays(1)))
				current = current.copy(start = current.start.truncatedTo(ChronoUnit.DAYS).plusDays(1))
			}
			if (Duration.between(current.start, current.end).toMillis() != 0L) {
				splitEvents.add(current)
			}
			splitEvents
		}

		allDayEventsSubject.onNext(allDayEvents)
		normalEventsSubject.onNext(normalEvents)
	}
	fun observeEvents(): Observable<List<Event>> = eventsSubject
	fun observeAllDayEvents(): Observable<List<Event>> = allDayEventsSubject
	fun observeNormalEvents(): Observable<List<Event>> = normalEventsSubject


	private val scaleSubject: Subject<Float> = BehaviorSubject.createDefault(1f)
	fun setScale(value: Float) {
		scaleSubject.onNext(value)
	}
	fun observeScale(): Observable<Float> = scaleSubject
	fun observeScaledHourSize(): Observable<Float> = scaleSubject.map {
		defaultHourSize * it
	}


	private val scrollSubject: Subject<Int> = BehaviorSubject.createDefault((7.5 * defaultHourSize).toInt())
	fun setScroll(value: Int) {
		scrollSubject.onNext(value)
	}
	fun observeScroll(): Observable<Int> = scrollSubject


	private val durationSubject: Subject<Int> = BehaviorSubject.createDefault(0)
	fun setDuration(value: Int) {
		durationSubject.onNext(value)
	}
	fun observeDuration(): Observable<Int> = durationSubject


}
