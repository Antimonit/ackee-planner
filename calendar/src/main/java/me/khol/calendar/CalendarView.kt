package me.khol.calendar

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.*
import android.widget.FrameLayout
import android.widget.LinearLayout
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import org.threeten.bp.LocalDate

/**
 * Contains a complex View hierarchy.
 *
 *   - [CalendarView][CalendarView]
 *     - [ScalableScrollView][ScalableScrollView] allows to scroll and scale content vertically
 *       - [CalendarTimesView][CalendarTimesView] shows list of times from 01:00 to 23:00
 *     - [CalendarPager][CalendarPager] to be able to change between days (or weeks) with a swipe gesture
 *       - [CalendarPage][CalendarPage] displays all information about a single day (or a week)
 *         - [CalendarDateLayout][CalendarDateLayout]
 *           - [CalendarDateView][CalendarDateView] shows date for a single day
 *         - [CalendarHeaderLayout][CalendarHeaderLayout] shows dates for a time period specified by CalendarPage
 *           - [CalendarEventView][CalendarEventView] shows an event
 *         - [ScalableScrollView][ScalableScrollView] allows to scroll and scale content vertically
 *           - [CalendarLayout][CalendarLayout] shows events for a time period specified by CalendarPage
 *             - [CalendarDayLayout][CalendarDayLayout] shows events for a single day
 *               - [CalendarEventView][CalendarEventView] shows an event
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 18.03.2018
 **/
class CalendarView @JvmOverloads constructor(
		context: Context,
		attrs: AttributeSet? = null,
		defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

	private val calendarContext = CalendarContext(context)

	private val timesView: CalendarTimesView
	private val calendarPager: CalendarPager
	private val scalableScrollView: ScalableScrollView

	/**
	 * Callbacks for clicking/long-clicking on an event/empty space. Also notifies that
	 * a date has changed.
	 *
	 * Delegates the property to [calendarContext].
	 */
	var calendarListener: CalendarListener?
		get() = calendarContext.listener
		set(value) {
			calendarContext.listener = value
		}

	/**
	 * Number of days to display next to each other.
	 *
	 * Delegates the property to [calendarContext]
	 */
	var numberOfDays: Int
		get() = calendarContext.observeNumberOfDays().blockingFirst()
		set(value) = calendarContext.setNumberOfDays(value)

	/**
	 * The date of the left-most visible day.
	 *
	 * Delegates the property to [calendarContext]
	 */
	var currentDate: LocalDate
		get() = calendarContext.observeCurrentDate().blockingFirst()
		set(value) = calendarContext.setCurrentDate(value)

	/**
	 * All events to be displayed.
	 * Does not distinguish between normal and all-day long events.
	 * Does not distinguish between non-interactive and interactive events (layers).
	 *
	 * Delegates the property to [calendarContext]
	 */
	var events: List<Event>
		get() = calendarContext.observeEvents().blockingFirst()
		set(value) = calendarContext.setEvents(value)


	/**
	 * Duration of overlaid events.
	 *
	 * Delegates the property to [calendarContext]
	 */
	var eventDuration: Int
		get() = calendarContext.observeDuration().blockingFirst()
		set(value) = calendarContext.setDuration(value)

	/**
	 * Enables or disables long click gesture to add new events.
	 *
	 * Delegates the property to [calendarContext]
	 */
	var allowInsertionOfEvents: Boolean
		get() = calendarContext.observeAllowInsertionOfEventsSubject().blockingFirst()
		set(value) = calendarContext.setAllowInsertionOfEvents(value)


	init {
		setWillNotDraw(false)

		orientation = HORIZONTAL

		/* Add a static [CalendarTimesView] */
		timesView = CalendarTimesView(calendarContext)

		val internalView = FrameLayout(context)
		scalableScrollView = ScalableScrollView(context).apply {
			addView(timesView, MATCH_PARENT, WRAP_CONTENT)
			scaleListener = object : ScalableScrollView.ScaleListener {
				override fun onScale(scale: Float) {
					calendarContext.setScale(scale)
					internalView.requestLayout()
				}
			}
			scrollListener = object : ScalableScrollView.ScrollListener {
				override fun onScroll(scroll: Int) {
					calendarContext.setScroll(scroll)
				}
			}
		}
		internalView.addView(scalableScrollView, MATCH_PARENT, MATCH_PARENT)

		addView(internalView, ViewGroup.MarginLayoutParams(calendarContext.timesWidth, MATCH_PARENT).apply {
			topMargin = calendarContext.headerHeight
		})

		calendarPager = CalendarPager(calendarContext).also {
			addView(it, MATCH_PARENT, MATCH_PARENT)
		}
	}

	private lateinit var compositeDisposable: CompositeDisposable
	override fun onAttachedToWindow() {
		super.onAttachedToWindow()
		compositeDisposable = CompositeDisposable()
		compositeDisposable += calendarContext.observeScroll()
				.subscribe { scroll ->
					// scrolls timesView
					scalableScrollView.scrollTo(0, scroll)
				}
		compositeDisposable += calendarContext.observeScale()
				.subscribe { scale ->
					// scales timesView
					scalableScrollView.setScale(scale)
				}
	}

	override fun onDetachedFromWindow() {
		super.onDetachedFromWindow()
		compositeDisposable.dispose()
	}

	override fun onDraw(canvas: Canvas) {
		super.onDraw(canvas)

		// border between nothing and times
		canvas.drawLine(
				0f,
				calendarContext.headerHeight.toFloat(),
				calendarContext.timesWidth.toFloat(),
				calendarContext.headerHeight.toFloat(),
				calendarContext.backgroundPaint
		)

		// border between times and view pager
		canvas.drawLine(
				calendarContext.timesWidth.toFloat(),
				0f,
				calendarContext.timesWidth.toFloat(),
				(canvas.height - 1).toFloat(),
				calendarContext.backgroundPaint
		)
	}


}
