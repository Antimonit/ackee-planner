package me.khol.calendar

import me.khol.calendar.layoutSolver.CalendarDayLayoutSolver
import me.khol.calendar.layoutSolver.EventNode
import me.khol.calendar.layoutSolver.Fraction
import org.junit.Test
import org.junit.Assert.*

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 26.03.2018
 **/
class CalendarUnitTest {

	@Test
	fun fractionCompare() {
		assertEquals(Fraction(2, 3) < Fraction(3, 4), true)
		assertEquals(Fraction(1, 2) === Fraction(2, 4), false)
		assertEquals(Fraction(1, 2) == Fraction(2, 4), true)
		assertEquals(Fraction(2, 2) > Fraction(2, 4), true)
	}

	@Test
	fun calendarSolver() {
		val events = listOf(
				EventNode(index=0, start=540, end=630),
				EventNode(index=1, start=555, end=645),
				EventNode(index=2, start=660, end=750),
				EventNode(index=3, start=660, end=830),
				EventNode(index=4, start=660, end=830),
				EventNode(index=5, start=660, end=830),
				EventNode(index=6, start=795, end=885),
				EventNode(index=7, start=795, end=885),
				EventNode(index=8, start=840, end=930),
				EventNode(index=9, start=840, end=960),
				EventNode(index=10, start=900, end=990),
				EventNode(index=11, start=1080, end=1170),
				EventNode(index=12, start=1170, end=1260),
				EventNode(index=13, start=1200, end=1290)
		)

		/*
			0 1
				2 3 4 5
					3 4 5 6 7
						  6 7 8 9
							  8 9 10
									 11
										12 13
		 */

		CalendarDayLayoutSolver.solve(events)

		assertEquals(events[0].width >= Fraction(1, 2), true)
		assertEquals(events[1].width >= Fraction(1, 2), true)

		assertEquals(events[2].width >= Fraction(2, 5), true)

		assertEquals(events[3].width >= Fraction(1, 5), true)
		assertEquals(events[4].width >= Fraction(1, 5), true)
		assertEquals(events[5].width >= Fraction(1, 5), true)

		assertEquals(events[6].width >= Fraction(1, 5), true)
		assertEquals(events[7].width >= Fraction(1, 5), true)

		assertEquals(events[8].width >= Fraction(3, 10), true)
		assertEquals(events[9].width >= Fraction(3, 10), true)

		assertEquals(events[10].width >= Fraction(4, 10), true)

		assertEquals(events[11].width == Fraction(1), true)

		assertEquals(events[12].width >= Fraction(1, 2), true)
		assertEquals(events[13].width >= Fraction(1, 2), true)
	}

}