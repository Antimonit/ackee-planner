# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

-printconfiguration proguard-configuration.txt

# Uncomment this to preserve the line number information for
# debugging stack traces.
-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
-renamesourcefileattribute SourceFile

-keepattributes Signature
-keepattributes Exceptions

-keep class kotlin.jvm.functions.**
-keep class me.khol.ackeeplanner.model.contacts.AvatarLoader
-keep class org.threeten.bp.LocalDate

-dontnote com.airbnb.epoxy.**
-dontnote com.facebook.stetho.**

-dontwarn okhttp3.**
-dontwarn okio.**
-dontwarn retrofit2.Platform$Java8

