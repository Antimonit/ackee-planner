package me.khol.ackeeplanner

import me.khol.ackeeplanner.extensions.*
import org.junit.Test

import org.junit.Assert.*
import org.junit.Rule
import org.junit.rules.ExpectedException
import org.threeten.bp.Instant
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZonedDateTime
import org.threeten.bp.format.DateTimeParseException
import org.threeten.bp.zone.ZoneRulesException


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

	@get:Rule
	val exception = ExpectedException.none()

	companion object {
		private const val API_TIME = "2018-04-11T18:48:51.112Z"
		private val INSTANT_TIME = Instant.ofEpochMilli(1523472531112)
	}

	@Test
	fun parsingInstant() {
		assertEquals(parseInstant(API_TIME), INSTANT_TIME)
	}

	@Test
	fun formattingInstant() {
		assertEquals(formatInstant(INSTANT_TIME), API_TIME)
	}

	@Test
	fun parsingZonedDateTime() {
		assertEquals(parseZonedDateTime(API_TIME), INSTANT_TIME)
	}

}
