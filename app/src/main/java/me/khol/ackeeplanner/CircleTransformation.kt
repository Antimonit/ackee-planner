package me.khol.ackeeplanner

import android.graphics.*
import com.squareup.picasso.Transformation


/**
 * Transforms a [Bitmap] into a circular [Bitmap], cropped in the center of the original one.
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 25.04.2018
 **/
object CircleTransform : Transformation {

	override fun transform(source: Bitmap): Bitmap {
		val size = Math.min(source.width, source.height)

		val x = (source.width - size) / 2
		val y = (source.height - size) / 2

		val squaredBitmap = Bitmap.createBitmap(source, x, y, size, size)

		return Bitmap.createBitmap(size, size, source.config).apply {
			val canvas = Canvas(this)
			val r = size / 2f
			val paint = Paint().apply {
				shader = BitmapShader(squaredBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
				isAntiAlias = true
			}
			canvas.drawCircle(r, r, r, paint)
		}.also {
			squaredBitmap.recycle()
		}
	}

	override fun key(): String {
		return "circle"
	}

}
