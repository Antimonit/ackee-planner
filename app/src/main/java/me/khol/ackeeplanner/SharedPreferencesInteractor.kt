package me.khol.ackeeplanner

import android.content.Context
import android.content.SharedPreferences
import com.f2prateek.rx.preferences2.RxSharedPreferences
import io.reactivex.Observable
import me.khol.extensions.*
import org.jetbrains.anko.defaultSharedPreferences

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 05.10.2017
 **/
class SharedPreferencesInteractor(context: Context) {

	companion object {
		private const val PREF_SELECTED_ACCOUNT_NAME = "selected_account_name"
		private const val PREF_SELECTED_CALENDAR_ID = "selected_calendar_id"
		private const val PREF_ACKEE_USER_ID = "ackee_user_id"

		private const val PREF_CALENDAR_COLORS_SYNC_TIMESTAMP = "calendarColorsSyncTimestamp"
	}

	private val sp: SharedPreferences = context.defaultSharedPreferences
	private val rxSp: RxSharedPreferences = RxSharedPreferences.create(sp)

	var ackeeUserId: Long?
		get() = sp.getLongPref(PREF_ACKEE_USER_ID)
		set(value) {
			if (value == null) {
				sp.removePref(PREF_ACKEE_USER_ID)
			} else {
				sp.setLongPref(PREF_ACKEE_USER_ID, value)
			}
		}

	var selectedCalendarId: Long?
		get() = sp.getLongPref(PREF_SELECTED_CALENDAR_ID)
		set(value) {
			if (value == null) {
				sp.removePref(PREF_SELECTED_CALENDAR_ID)
			} else {
				sp.setLongPref(PREF_SELECTED_CALENDAR_ID, value)
			}
		}

	var selectedAccountName: String?
		get() = sp.getStringPref(PREF_SELECTED_ACCOUNT_NAME)
		set(value) {
			if (value == null) {
				sp.removePref(PREF_SELECTED_ACCOUNT_NAME)
			} else {
				sp.setStringPref(PREF_SELECTED_ACCOUNT_NAME, value)
			}
		}

	val selectedCalendarIdObservable: Observable<Long> = rxSp.getLong(PREF_SELECTED_CALENDAR_ID).asObservable()
	val selectedAccountNameObservable: Observable<String?> = rxSp.getString(PREF_SELECTED_ACCOUNT_NAME).asObservable()

	val hasCalendarColorsSyncTimestamp: Boolean
		get() = sp.contains(PREF_CALENDAR_COLORS_SYNC_TIMESTAMP)

	fun clearRx() = rxSp.clear()
	fun clear() = sp.clear()
}
