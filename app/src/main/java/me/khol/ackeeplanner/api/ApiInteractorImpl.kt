package me.khol.ackeeplanner.api

import io.reactivex.Completable
import io.reactivex.Single
import me.khol.ackeeplanner.extensions.formatInstant
import me.khol.ackeeplanner.extensions.subscribeOnNewThread
import me.khol.ackeeplanner.model.api.CalendarEntity
import me.khol.ackeeplanner.model.api.CalendarEventsEntity
import me.khol.ackeeplanner.model.api.RegistrationEntity
import me.khol.ackeeplanner.model.api.meeting.ConfirmMeetingRequest
import me.khol.ackeeplanner.model.api.meeting.EmailsRequest
import me.khol.ackeeplanner.model.api.meeting.MeetingRequest
import me.khol.ackeeplanner.model.api.meeting.Meeting
import me.khol.ackeeplanner.model.db.dao.UserDao
import me.khol.ackeeplanner.model.toDBUser
import me.khol.ackeeplanner.oauth.OAuthManager
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneOffset
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 07.03.2018
 **/
@Singleton
class ApiInteractorImpl @Inject constructor(
		private val apiDescription: ApiDescription,
		private val userDao: UserDao,
		private val oAuthManager: OAuthManager
) : ApiInteractor {

	override fun loginWithGoogleAccessToken(accessToken: String): Single<RegistrationEntity> {
		return apiDescription.loginWithGoogleAccessToken(accessToken)
				.doOnSuccess { response: RegistrationEntity ->
					userDao.insertUser(response.user.toDBUser())
					oAuthManager.saveCredentials(response.credentials)
				}
				.subscribeOnNewThread()
	}

	override fun loginWithGoogleAuthCode(authCode: String): Single<RegistrationEntity> {
		return apiDescription.loginWithGoogleAuthCode(authCode)
				.doOnSuccess { response: RegistrationEntity ->
					userDao.insertUser(response.user.toDBUser())
					oAuthManager.saveCredentials(response.credentials)
				}
				.subscribeOnNewThread()
	}

	override fun loginAnonymous(): Single<RegistrationEntity> {
		return apiDescription.anonymousLogin()
				.doOnSuccess { response: RegistrationEntity ->
					userDao.insertUser(response.user.toDBUser())
					oAuthManager.saveCredentials(response.credentials)
				}
				.subscribeOnNewThread()
	}

	override fun refreshToken(refreshToken: String): Single<RegistrationEntity> {
		return apiDescription.refreshToken(refreshToken)
				.doOnSuccess { response: RegistrationEntity ->
					userDao.insertUser(response.user.toDBUser())
					oAuthManager.saveCredentials(response.credentials)
				}
				.subscribeOnNewThread()
	}

	override fun logout(): Completable {
		return Completable.fromAction {
			oAuthManager.clearCredentials()
		}
	}


	override fun createMeeting(
			type: String,
			description: String,
			durationMinutes: Int,
			calendarGoogleId: String,
			location: String,
			dates: List<String>,
			emails: List<String>
	): Single<Meeting> {
		return createMeeting(
				MeetingRequest.Meeting(type, description, durationMinutes, calendarGoogleId, location),
				dates,
				emails
		)
	}

	override fun createMeeting(
			meeting: MeetingRequest.Meeting,
			dates: List<String>,
			emails: List<String>
	): Single<Meeting> {
		return apiDescription.createMeeting(MeetingRequest(meeting, dates, emails))
				.subscribeOnNewThread()
	}


	override fun meetingDetail(meetingId: Int, meetingToken: String): Single<Meeting> {
		return apiDescription.meetingDetail(meetingId, meetingToken)
				.subscribeOnNewThread()
	}


	override fun confirmMeeting(
			calendarGoogleId: String?,
			name: String?,
			email: String?,
			meetingId: Int,
			dateId: Int,
			meetingToken: String
	): Single<Meeting> {
		return confirmMeeting(ConfirmMeetingRequest(
				calendarGoogleId, name, email
		), meetingId, dateId, meetingToken)
	}

	override fun confirmMeeting(
			meeting: ConfirmMeetingRequest,
			meetingId: Int,
			dateId: Int,
			meetingToken: String
	): Single<Meeting> {
		return apiDescription.confirmMeeting(meeting, meetingId, dateId, meetingToken)
				.subscribeOnNewThread()
	}

	override fun sendInvitations(emails: List<String>, meetingId: Int, meetingToken: String): Completable {
		return apiDescription.sendInvitations(EmailsRequest(emails), meetingId, meetingToken)
	}


	@Deprecated("Retrieve meetings from local database instead")
	override fun getAllEvents(timeMin: LocalDateTime, timeMax: LocalDateTime): Single<List<CalendarEventsEntity>> {
		return apiDescription.getAllEvents(
				formatInstant(timeMin.atZone(ZoneOffset.systemDefault()).toInstant()),
				formatInstant(timeMax.atZone(ZoneOffset.systemDefault()).toInstant())
		)
				.subscribeOnNewThread()
	}

	@Deprecated("Retrieve calendars from local database instead")
	override fun getAllCalendars(): Single<List<CalendarEntity>> {
		return apiDescription.getAllCalendars()
				.subscribeOnNewThread()
	}

}
