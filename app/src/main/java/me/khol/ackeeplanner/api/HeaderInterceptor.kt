package me.khol.ackeeplanner.api


import me.khol.ackeeplanner.BuildConfig
import me.khol.ackeeplanner.constants.NetworkConstants
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

/**
 * Interceptor that inserts headers that ackee backends uses for logging - application version, Ackee User Agent and Device id
 *
 * @author David Bilik [david.bilik@ackee.cz]
 * @since 31/10/16
 */
class HeaderInterceptor(private val deviceId: String, private val userAgent: String) : Interceptor {

	@Throws(IOException::class)
	override fun intercept(chain: Interceptor.Chain): Response {
		val originalRequest = chain.request()
		return chain.proceed(originalRequest.newBuilder()
				.addHeader(NetworkConstants.Header.X_VERSION, BuildConfig.VERSION_CODE.toString())
				.addHeader(NetworkConstants.Header.USER_AGENT, userAgent)
				.addHeader(NetworkConstants.Header.X_DEVICE_ID, deviceId)
				.build())
	}

}
