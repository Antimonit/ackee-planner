package me.khol.ackeeplanner.api

import io.reactivex.Completable
import io.reactivex.Single
import me.khol.ackeeplanner.model.api.CalendarEntity
import me.khol.ackeeplanner.model.api.CalendarEventsEntity
import me.khol.ackeeplanner.model.api.RegistrationEntity
import me.khol.ackeeplanner.model.api.User
import me.khol.ackeeplanner.model.api.meeting.ConfirmMeetingRequest
import me.khol.ackeeplanner.model.api.meeting.EmailsRequest
import me.khol.ackeeplanner.model.api.meeting.MeetingRequest
import me.khol.ackeeplanner.model.api.meeting.Meeting
import retrofit2.http.*

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 22.11.2017
 **/
interface ApiDescription {

	@GET("auth/google/sign-in")
	fun loginWithGoogleAuthCode(@Query("token") authCode: String): Single<RegistrationEntity>

	@GET("auth/google/sign-in")
	fun loginWithGoogleAccessToken(@Query("accessToken") accessToken: String): Single<RegistrationEntity>

	@GET("auth/anonym/sign-in")
	fun anonymousLogin(): Single<RegistrationEntity>

	@GET("auth/token/refresh")
	fun refreshToken(@Query("token") refreshToken: String): Single<RegistrationEntity>


	@GET("users/me")
	fun userDetail(): Single<User>


	@POST("meetings")
	fun createMeeting(@Body meeting: MeetingRequest): Single<Meeting>

	@GET("meetings/{meetingId}")
	fun meetingDetail(@Path("meetingId") meetingId: Int, @Query("token") meetingToken: String): Single<Meeting>

	@POST("meetings/{meetingId}/dates/{dateId}")
	fun confirmMeeting(@Body meeting: ConfirmMeetingRequest, @Path("meetingId") meetingId: Int, @Path("dateId") dateId: Int, @Query("token") meetingToken: String): Single<Meeting>

	@POST("meetings/{meetingId}/emails")
	fun sendInvitations(@Body emails: EmailsRequest, @Path("meetingId") meetingId: Int, @Query("token") meetingToken: String): Completable


	@GET("calendars")
	fun getAllCalendars(): Single<List<CalendarEntity>>

	@GET("meetings")
	fun getAllEvents(@Query("timeMin") timeMin: String,
					 @Query("timeMax") timeMax: String): Single<List<CalendarEventsEntity>>

}
