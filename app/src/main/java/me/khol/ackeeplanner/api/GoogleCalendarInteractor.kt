package me.khol.ackeeplanner.api

import com.google.api.client.extensions.android.http.AndroidHttp
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.client.util.DateTime
import com.google.api.services.calendar.model.*
import me.khol.ackeeplanner.extensions.Permission
import org.threeten.bp.ZoneOffset
import org.threeten.bp.ZonedDateTime
import javax.inject.Inject
import javax.inject.Singleton

/**
 * TODO: 23. 3. 2018 david.khol: add class description
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 23.03.2018
 **/
interface GoogleCalendarInteractor {
	fun getColors(accountName: String): Colors

	fun getEvents(year: Int, month: Int, accountName: String, calendarId: String): Events

	fun getCalendar(calendarId: String): Calendar

	fun getCalendarList(accountName: String): CalendarList
}

@Singleton
class GoogleCalendarInteractorImpl @Inject constructor(
		private val credential: GoogleAccountCredential
) : GoogleCalendarInteractor {

	companion object {
		private val HTTP_TRANSPORT = AndroidHttp.newCompatibleTransport()
		private val JSON_FACTORY = JacksonFactory.getDefaultInstance()
	}

	private val calendarService = com.google.api.services.calendar.Calendar.Builder(
			HTTP_TRANSPORT,
			JSON_FACTORY,
			credential)
			.setApplicationName("Ackee Planner")
			.build()


	override fun getColors(accountName: String): Colors {
		credential.selectedAccountName = accountName
		return calendarService
				.colors()
				.get()
				.execute()
	}

	override fun getCalendar(calendarId: String): Calendar {
		return calendarService
				.calendars()
				.get(calendarId)
				.execute()
	}

	override fun getCalendarList(accountName: String): CalendarList {
		credential.selectedAccountName = accountName
		return calendarService
				.calendarList()
				.list()
				.setShowHidden(true)
				.setShowDeleted(true)
				.execute()
	}

	/**
	 * User doesn't have to be logged in to retrieve meetings directly from google services,
	 * but valid [accountName][GoogleAccountCredential.accountName] must be set
	 * in [credentials][GoogleAccountCredential].
	 */
	override fun getEvents(year: Int, month: Int, accountName: String, calendarId: String): Events {
		credential.selectedAccountName = accountName

		val startOfTheMonth = ZonedDateTime.of(year, month, 1, 0, 0, 0, 0, ZoneOffset.UTC)
		val endOfTheMonth = startOfTheMonth.plusMonths(1)

		return calendarService
				.events()
				.list(calendarId)
//                .setMaxResults(10)
				.setTimeMin(DateTime(startOfTheMonth.toInstant().toEpochMilli()))
				.setTimeMax(DateTime(endOfTheMonth.toInstant().toEpochMilli()))
				.setOrderBy("startTime")
				.setSingleEvents(true)
				.execute()
	}

	fun createEvent(calendarId: String, event: Event): Event? {
		return calendarService
				.events()
				.insert(calendarId, event)
				.execute()
				.also {
					System.out.println("Event created: ${event.htmlLink}")
				}
	}

}
