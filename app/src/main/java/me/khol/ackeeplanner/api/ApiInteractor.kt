package me.khol.ackeeplanner.api

import io.reactivex.Completable
import io.reactivex.Single
import me.khol.ackeeplanner.model.api.CalendarEntity
import me.khol.ackeeplanner.model.api.CalendarEventsEntity
import me.khol.ackeeplanner.model.api.RegistrationEntity
import me.khol.ackeeplanner.model.api.meeting.ConfirmMeetingRequest
import me.khol.ackeeplanner.model.api.meeting.MeetingRequest
import me.khol.ackeeplanner.model.api.meeting.Meeting
import org.threeten.bp.LocalDateTime

/**
 * TODO: 7. 3. 2018 david.khol: add class description
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 07.03.2018
 **/
interface ApiInteractor {

	fun loginWithGoogleAccessToken(accessToken: String): Single<RegistrationEntity>

	fun loginWithGoogleAuthCode(authCode: String): Single<RegistrationEntity>

	fun loginAnonymous(): Single<RegistrationEntity>

	fun refreshToken(refreshToken: String): Single<RegistrationEntity>

	fun logout(): Completable


	fun createMeeting(type: String, description: String, durationMinutes: Int, calendarGoogleId: String, location: String, dates: List<String>, emails: List<String>): Single<Meeting>

	fun createMeeting(meeting: MeetingRequest.Meeting, dates: List<String>, emails: List<String>): Single<Meeting>

	fun meetingDetail(meetingId: Int, meetingToken: String): Single<Meeting>

	fun confirmMeeting(calendarGoogleId: String?, name: String?, email: String?, meetingId: Int, dateId: Int, meetingToken: String): Single<Meeting>

	fun confirmMeeting(meeting: ConfirmMeetingRequest, meetingId: Int, dateId: Int, meetingToken: String): Single<Meeting>

	fun sendInvitations(emails: List<String>, meetingId: Int, meetingToken: String): Completable


	@Deprecated("Retrieve meetings from local database")
	fun getAllEvents(timeMin: LocalDateTime, timeMax: LocalDateTime): Single<List<CalendarEventsEntity>>

	@Deprecated("Retrieve calendars from local database")
	fun getAllCalendars(): Single<List<CalendarEntity>>

}
