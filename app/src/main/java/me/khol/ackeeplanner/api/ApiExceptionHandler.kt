package me.khol.ackeeplanner.api

import com.google.android.gms.auth.api.signin.GoogleSignInStatusCodes
import com.google.android.gms.common.api.ApiException

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 13.04.2018
 **/
object ApiExceptionHandler {

	fun resolve(exception: ApiException): String {
		return when {
			exception.statusCode == GoogleSignInStatusCodes.SIGN_IN_CANCELLED -> "Sign in cancelled."
			exception.statusCode == GoogleSignInStatusCodes.SIGN_IN_CURRENTLY_IN_PROGRESS -> "Sign in currently in progress."
			exception.statusCode == GoogleSignInStatusCodes.SIGN_IN_FAILED -> "Sign in failed."
			exception.statusCode == GoogleSignInStatusCodes.SIGN_IN_REQUIRED -> "Sign in required."
			exception.statusCode == GoogleSignInStatusCodes.API_NOT_CONNECTED -> "API not connected."
			exception.statusCode == GoogleSignInStatusCodes.CANCELED -> "Request cancelled."
			exception.statusCode == GoogleSignInStatusCodes.TIMEOUT -> "Timed out."
			exception.statusCode == GoogleSignInStatusCodes.INTERNAL_ERROR -> "An internal error occurred. Retrying should resolve the problem."
			exception.statusCode == GoogleSignInStatusCodes.ERROR -> "An unspecified error occurred"
			exception.statusCode == GoogleSignInStatusCodes.NETWORK_ERROR -> "A network error occurred. Retrying should resolve the problem."
			else -> GoogleSignInStatusCodes.getStatusCodeString(exception.statusCode)
		}
	}

}
