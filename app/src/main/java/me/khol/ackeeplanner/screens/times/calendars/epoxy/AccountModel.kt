package me.khol.ackeeplanner.screens.times.calendars.epoxy

import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.model.calendar.Calendar
import me.khol.ackeeplanner.model.calendar.CalendarAccount
import me.khol.ackeeplanner.screens.base.epoxy.BaseEpoxyModel
import me.khol.ackeeplanner.screens.base.layout.BaseLayout
import me.khol.ackeeplanner.screens.base.layout.labelTextView
import me.khol.ackeeplanner.screens.times.calendars.epoxy.AccountModel.Layout
import me.khol.extensions.elevationCompat
import me.khol.extensions.getColor
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView

/**
 * Simplified version of [me.khol.ackeeplanner.screens.main.accounts.epoxy.AccountModel]
 *
 * Displays [name][Layout.txtAccountLabel] and [avatar][Layout.imgAccountAvatar] of a single
 * account.
 *
 * An account can be associated with multiple Calendars that are displayed
 * in [calendars][Layout.calendars] recycler view underneath the account information.
 * By clicking on a calendar view, the calendar's visibility is toggled.
 *
 * This model is used in [CalendarsDialogUI][me.khol.ackeeplanner.screens.times.calendars.CalendarsDialogUI]'s
 * [list][me.khol.ackeeplanner.screens.times.calendars.CalendarsDialogUI.list] recycler view.
 *
 *
 * Warning: There is a similar model with the same name in the [me.khol.ackeeplanner.screens.calendar.epoxy]
 * package where user chooses a calendar to log in with.
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 16.04.2018
 **/
open class AccountModel : BaseEpoxyModel<AccountModel.Layout>() {

	@EpoxyAttribute
	lateinit var account: CalendarAccount

	@EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
	lateinit var onCalendarToggled: (Calendar, Boolean) -> Unit

	override fun createViewLayout(parent: ViewGroup) = Layout(parent)

	override fun bind(layout: Layout) {
		super.bind(layout)
		layout.txtAccountLabel.text = account.name
		layout.calendarsController.calendars = account.calendars
		layout.onCalendarToggled = onCalendarToggled
	}

	@Suppress("MemberVisibilityCanBePrivate")
	class Layout(parent: ViewGroup) : BaseLayout(parent) {

		lateinit var txtAccountLabel: TextView
		lateinit var imgAccountAvatar: ImageView
		lateinit var calendars: RecyclerView

		lateinit var onCalendarToggled: (Calendar, Boolean) -> Unit

		val calendarsController: CalendarsEpoxyController = CalendarsEpoxyController(
				onCalendarToggled = { calendar, isToggled ->
					onCalendarToggled(calendar, isToggled)
				}
		)

		override fun createView(ui: AnkoContext<ViewGroup>): View {
			return ui.frameLayout {
				clipToPadding = false

				constraintLayout {
					backgroundResource = R.color.white
					verticalPadding = 8.dp
					elevationCompat = 2.dpf

					txtAccountLabel = labelTextView()

					imgAccountAvatar = imageView {
						backgroundColor = getColor(R.color.black).withAlpha(20)
					}

					calendars = recyclerView {
						layoutManager = LinearLayoutManager(ui.ctx, LinearLayoutManager.VERTICAL, false)
						itemAnimator = DefaultItemAnimator()
						addItemDecoration(HorizontalDividerItemDecoration.Builder(context)
								.margin(72.dp, 0.dp)
								.build())
						isNestedScrollingEnabled = false
						adapter = calendarsController.adapter
						clipChildren = false
					}

					constraints {

						txtAccountLabel
								.connect(VERTICAL of imgAccountAvatar)
								.connect(STARTS of parentId with 72.dp)

						imgAccountAvatar
								.connect(TOPS of parentId with 4.dp)
								.connect(STARTS of parentId with 16.dp)
								.size(40.dp, 40.dp)

						calendars
								.connect(HORIZONTAL of parentId)
								.connect(TOPS of parentId with 48.dp)
								.size(matchConstraint, wrapContent)
					}
				}.lparams(matchParent, wrapContent)

			}.apply {
				layoutParams = ViewGroup.LayoutParams(org.jetbrains.anko.matchParent, org.jetbrains.anko.wrapContent)
			}
		}

	}

}
