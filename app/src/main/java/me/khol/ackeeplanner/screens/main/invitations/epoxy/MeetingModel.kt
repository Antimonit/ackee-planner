package me.khol.ackeeplanner.screens.main.invitations.epoxy

import android.support.v7.widget.CardView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.extensions.formatRelativeToToday
import me.khol.ackeeplanner.model.api.meeting.Meeting
import me.khol.ackeeplanner.screens.base.epoxy.BaseEpoxyModel
import me.khol.ackeeplanner.screens.base.layout.BaseLayout
import me.khol.ackeeplanner.screens.base.layout.defaultTextView
import me.khol.ackeeplanner.screens.base.layout.textViewLeftDrawable
import me.khol.extensions.getDrawableAttr
import me.khol.extensions.isVisible
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 09.03.2018
 **/
@EpoxyModelClass
open class MeetingModel : BaseEpoxyModel<MeetingModel.Layout>() {

	@EpoxyAttribute
	lateinit var meeting: Meeting
	@EpoxyAttribute
	lateinit var userIds: List<Int>
	@EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
	lateinit var onCardClick: (Meeting) -> Unit

	override fun createViewLayout(parent: ViewGroup) = Layout(parent)

	override fun bind(layout: Layout) {
		super.bind(layout)
		layout.card.setOnClickListener {
			onCardClick(meeting)
		}

		layout.txtTitle.text = layout.context.getString(R.string.invitation_card_title, meeting.description)
		layout.txtLocation.text = layout.context.getString(R.string.invitation_card_location, meeting.location)
		layout.txtOwner.text = layout.context.getString(R.string.invitation_card_created_as, meeting.owner?.email)
		layout.txtTime.text = layout.context.getString(R.string.invitation_card_dates, meeting.dates.size)

		meeting.dates.firstOrNull { userIds.contains(it.user?.userId) }?.let {
			layout.txtTime.text = it.startDate.formatRelativeToToday()
		}

		val meetingUserIds = meeting.dates.filter { it.user != null }
				.map { it.user!!.userId }
		layout.imgIcon.isVisible = meetingUserIds.intersect(userIds).isNotEmpty()
	}

	override fun unbind(layout: Layout) {
		super.unbind(layout)
		layout.card.setOnClickListener(null)
	}

	class Layout(parent: ViewGroup) : BaseLayout(parent) {

		lateinit var card: CardView
		lateinit var txtTitle: TextView
		lateinit var txtOwner: TextView
		lateinit var txtTime: TextView
		lateinit var txtLocation: TextView

		lateinit var imgIcon: ImageView

		override fun createView(ui: AnkoContext<ViewGroup>): View {
			return ui.frameLayout {
				horizontalPadding = 8.dp

				card = cardView {
					cardElevation = 2.dpf
					useCompatPadding = true
					isClickable = true
					foreground = getDrawableAttr(android.R.attr.selectableItemBackground)

					constraintLayout {
						padding = 16.dp

						txtTitle = defaultTextView {
							textSize = 20f
						}

						txtOwner = textViewLeftDrawable(R.drawable.ic_perm_contact_calendar)

						txtLocation = textViewLeftDrawable(R.drawable.ic_place)

						txtTime = textViewLeftDrawable(R.drawable.ic_schedule)

						imgIcon = imageView(R.drawable.ic_check)

						constraints {
							txtTitle.connect(TOPS of parentId)
									.connect(STARTS of parentId)
									.size(matchConstraint, wrapContent)

							txtLocation
									.connect(TOP to BOTTOM of txtTitle with 8.dp)
									.connect(STARTS of parentId)

							txtOwner.connect(TOP to BOTTOM of txtLocation with 4.dp)
									.connect(STARTS of parentId)

							txtTime.connect(TOP to BOTTOM of txtOwner with 4.dp)
									.connect(STARTS of parentId)

							imgIcon.connect(TOPS of parentId)
									.connect(ENDS of parentId)
									.size(24.dp, 24.dp)
						}
					}
				}.lparams(matchParent, wrapContent)

			}.apply {
				layoutParams = ViewGroup.LayoutParams(matchParent, wrapContent)
			}
		}

	}
}
