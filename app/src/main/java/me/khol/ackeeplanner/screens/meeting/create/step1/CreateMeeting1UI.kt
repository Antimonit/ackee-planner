package me.khol.ackeeplanner.screens.meeting.create.step1

import android.support.design.widget.AppBarLayout
import android.support.v7.widget.Toolbar
import android.view.View
import android.view.ViewGroup
import android.view.ViewManager
import android.widget.FrameLayout
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMapOptions
import com.google.android.gms.maps.MapView
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.screens.meeting.ItemUI
import me.khol.ackeeplanner.screens.meeting.ProgressButtonUI
import me.khol.ackeeplanner.screens.meeting.base.BaseMeetingUI
import me.khol.ackeeplanner.screens.meeting.clickableItem
import me.khol.ackeeplanner.screens.meeting.editableItem
import me.khol.extensions.getDrawableAttr
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.navigationIconResource
import org.jetbrains.anko.appcompat.v7.toolbar
import org.jetbrains.anko.custom.ankoView
import org.jetbrains.anko.design.appBarLayout
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.support.v4.nestedScrollView

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 11.03.2018
 **/
class CreateMeeting1UI(
		parent: ViewGroup,
		private val onTitleTextChange: (String) -> Unit,
		private val onCalendarClick: () -> Unit,
		private val onTimesClick: () -> Unit,
		private val onLocationClick: () -> Unit,
		private val onContinueClick: () -> Unit
) : BaseMeetingUI(parent) {

	lateinit var toolbar: Toolbar
	lateinit var title: ItemUI
	lateinit var calendar: ItemUI
	lateinit var times: ItemUI
	lateinit var location: ItemUI
	lateinit var mapView: MapView

	lateinit var mapFrame: FrameLayout

	inline fun ViewManager.mapView(options: GoogleMapOptions? = null, init: (@AnkoViewDslMarker MapView).() -> Unit = {}): MapView {
		return ankoView({ ctx -> MapView(ctx, options) }, theme = 0) { init() }
	}

	override fun createView(ui: AnkoContext<ViewGroup>): View {
		return ui.coordinatorLayout {

			appBarLayout {
				backgroundResource = R.color.white

				toolbar = toolbar {
					fitsSystemWindows = true
					title = context.getString(R.string.create_meeting_title)
					navigationIconResource = R.drawable.ic_close
				}.lparams(matchParent, wrapContent) {
					scrollFlags = 0
				}

			}.lparams(matchParent, wrapContent)

			nestedScrollView {
				topPadding = 8.dp
				clipToPadding = false

				verticalLayout {
					title = editableItem(context.getString(R.string.create_meeting_hint_title), R.drawable.ic_description, onTitleTextChange)
					calendar = clickableItem(context.getString(R.string.create_meeting_hint_calendar), R.drawable.ic_perm_contact_calendar, onCalendarClick)
					times = clickableItem(context.getString(R.string.create_meeting_hint_times), R.drawable.ic_schedule, onTimesClick)
					location = clickableItem(context.getString(R.string.create_meeting_hint_location), R.drawable.ic_place, onLocationClick)

					mapFrame = frameLayout {
						mapView = mapView(GoogleMapOptions()
								.mapType(GoogleMap.MAP_TYPE_NORMAL)
								.liteMode(true)
								.mapToolbarEnabled(false)
								.compassEnabled(false)
								.rotateGesturesEnabled(false)
								.tiltGesturesEnabled(false)) {
							isClickable = false
						}.lparams(matchParent, matchParent)

						setOnClickListener {
							onLocationClick()
						}
						isClickable = true
						foreground = getDrawableAttr(android.R.attr.selectableItemBackground)
					}.lparams(matchParent, 120.dp)

					next = ProgressButtonUI(
							this,
							R.string.create_meeting_confirm,
							R.string.create_meeting_success,
							onContinueClick
					).apply {
						addView(view)
					}

				}.lparams(matchParent, matchParent)

			}.lparams(matchParent, matchParent) {
				behavior = AppBarLayout.ScrollingViewBehavior()
			}
		}.also {
			it.layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)
		}
	}

}
