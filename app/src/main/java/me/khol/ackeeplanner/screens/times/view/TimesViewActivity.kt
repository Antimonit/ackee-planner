package me.khol.ackeeplanner.screens.times.view

import android.Manifest
import android.annotation.SuppressLint
import android.os.Bundle
import android.support.annotation.RequiresPermission
import android.view.Menu
import io.reactivex.rxkotlin.plusAssign
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.extensions.getViewModel
import me.khol.ackeeplanner.extensions.observeOnMainThread
import me.khol.ackeeplanner.model.ColorTransformer
import me.khol.ackeeplanner.model.api.meeting.Meeting
import me.khol.ackeeplanner.screens.times.BaseTimesActivity
import me.khol.ackeeplanner.screens.times.CalendarUI
import me.khol.ackeeplanner.screens.times.OverlayEvent
import me.khol.calendar.CalendarListener
import me.khol.calendar.Event
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId

/**
 * An activity to browse a calendar without any ability to create, delete or otherwise modify
 * the contents of the calendar. This "immutable" screen is used in conjunction with
 * [MyMeetingDetailFragment][me.khol.ackeeplanner.screens.meeting.detail.MyMeetingDetailFragment].
 *
 * "Done" action button and options menu is hidden.
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 11.04.2018
 **/
class TimesViewActivity : BaseTimesActivity() {

	companion object {
		const val ARG_DATES = "dates"
	}

	override lateinit var layout: CalendarUI
	lateinit var viewModel: TimesViewViewModel
	lateinit var controller: TimesViewController

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		viewModel = getViewModel(vmFactory)

		val overlaidDates: List<Meeting.Date> = intent.extras?.getParcelableArrayList(ARG_DATES)
				?: emptyList()
		viewModel.setOverlaidDates(overlaidDates)

		controller = TimesViewController(
				onTimeClick = { _: OverlayEvent ->
					// do nothing
				}
		)

		layout = CalendarUI(
				rootView,
				controller.adapter,
				calendarListener = object : CalendarListener {
					override fun onEventClick(event: Event) {
						// do nothing
					}

					override fun onEventLongClick(event: Event) {
						// do nothing
					}

					override fun onEmptySpaceClick(dateTime: LocalDateTime) {
						// do nothing
					}

					override fun onEmptySpaceLongClick(dateTime: LocalDateTime) {
						// do nothing
					}

					override fun onDateChanged(date: LocalDate) {
						viewModel.onDateChanged(date)
						changeTitle(date)
					}
				}
		).apply {
			view // force lazy initialization
			calendar.allowInsertionOfEvents = true
			setBottomTitle(getString(R.string.detail_meeting_choose_time_slot))
			setBottomSubtitle(null)
		}
		setContentView(layout.view)

		val startDay = LocalDate.now()

		layout.calendar.numberOfDays = 1
		layout.calendar.currentDate = startDay
		viewModel.onDateChanged(startDay)

		overlaidDates.firstOrNull()?.let {
			val startDate = it.startDate.atZone(ZoneId.systemDefault()).toLocalDate()
			layout.calendar.currentDate = startDate
			viewModel.onDateChanged(startDate)
		}

		setSupportActionBar(layout.toolbar)
		supportActionBar?.apply {
			setDisplayHomeAsUpEnabled(true)
		}

		checkCalendarPermission()
	}

	@SuppressLint("MissingPermission")
	@RequiresPermission(Manifest.permission.READ_CALENDAR)
	override fun onCalendarPermissionGranted() {
		disposables += viewModel.observeState()
				.observeOnMainThread()
				.subscribe({ state: TimesViewViewModel.State ->
					when (state) {
						is TimesViewViewModel.State.Loading -> {
							layout.showProgress(true)
						}
						is TimesViewViewModel.State.Loaded -> {
							layout.showProgress(false)
							val defaultZoneId = ZoneId.systemDefault()
							val overlaidEvents = mapOverlaidEvents(state.overlaidEvents, defaultZoneId, null)
							val events = mapEvents(state.events, defaultZoneId)

							layout.calendar.events = events + overlaidEvents

							val unclaimed = state.overlaidEvents.filter { !it.isTaken }.size
							layout.setBottomSubtitle(resources.getQuantityString(
									R.plurals.detail_meeting_time_slot_available,
									unclaimed,
									unclaimed
							))
							layout.showDoneButton(false)

							controller.events = state.overlaidEvents
						}
						is TimesViewViewModel.State.Error -> {
							layout.showProgress(false)
							resolveErrorState(state.throwable)
						}
					}
				})
	}

	override fun onCreateOptionsMenu(menu: Menu): Boolean {
		super.onCreateOptionsMenu(menu)
		durationAction.isVisible = false
		menu.findItem(R.id.action_done).isVisible = false
		return true
	}

}
