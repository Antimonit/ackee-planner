package me.khol.ackeeplanner.screens.meeting.base

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import me.khol.ackeeplanner.App
import me.khol.ackeeplanner.model.api.meeting.Meeting
import me.khol.ackeeplanner.screens.base.viewmodel.ContextViewModel

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 02.05.2018
 **/
abstract class BaseMeetingViewModel(
		app: App
) : ContextViewModel(app) {

	sealed class State {
		open class Initial : State()
		open class Loading : State()
		open class Success : State()
		open class Invalid(val invalidFields: List<String>) : State()
		open class NetworkError(val throwable: Throwable) : State()
	}

	val stateSubject: Subject<State> = BehaviorSubject.createDefault<State>(State.Initial())
	fun observeState(): Observable<State> = stateSubject

}
