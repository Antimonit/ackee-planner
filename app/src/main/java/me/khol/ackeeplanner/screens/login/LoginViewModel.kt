package me.khol.ackeeplanner.screens.login

import android.content.Intent
import io.reactivex.Observable
import me.khol.ackeeplanner.App
import me.khol.ackeeplanner.model.AccountRepository
import me.khol.ackeeplanner.screens.base.viewmodel.ContextViewModel
import javax.inject.Inject

/**
 * Delegates calls to AccountRepository.
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 28.11.2017
 **/
class LoginViewModel @Inject constructor(
		app: App,
		private val accountRepository: AccountRepository
) : ContextViewModel(app) {

	fun observeLoginState(): Observable<AccountRepository.State> = accountRepository.observeLoginState()

	fun doLogIn() = accountRepository.doLogIn(null)
	fun doLogOut() = accountRepository.doLogOut()
	fun doRevoke() = accountRepository.doRevoke()

	fun onSignInResult(data: Intent?) = accountRepository.onSignInResult(data)
	fun observeSignInIntent(): Observable<Intent> = accountRepository.observeSignInIntent()

}
