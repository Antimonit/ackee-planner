package me.khol.ackeeplanner.screens.main.myMeetings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import me.khol.ackeeplanner.App
import me.khol.ackeeplanner.extensions.getViewModel
import me.khol.ackeeplanner.model.api.meeting.Meeting
import me.khol.ackeeplanner.screens.base.activity.FragmentActivity
import me.khol.ackeeplanner.screens.base.activity.startFragmentActivityForResult
import me.khol.ackeeplanner.screens.main.MainActivity
import me.khol.ackeeplanner.screens.main.base.MeetingFragment
import me.khol.ackeeplanner.screens.main.myMeetings.epoxy.MyMeetingsEpoxyController
import me.khol.ackeeplanner.screens.meeting.create.step1.CreateMeeting1Fragment
import me.khol.ackeeplanner.screens.meeting.detail.MyMeetingDetailFragment

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 09.03.2018
 **/
class MyMeetingsFragment : MeetingFragment<MyMeetingsUI, MyMeetingsViewModel, MyMeetingsEpoxyController>() {

	override lateinit var layout: MyMeetingsUI
	override lateinit var viewModel: MyMeetingsViewModel
	override lateinit var epoxyController: MyMeetingsEpoxyController

	override fun onCreate(savedInstanceState: Bundle?) {
		App.appComponent.inject(this)
		viewModel = getViewModel(vmFactory)

		epoxyController = MyMeetingsEpoxyController(
				onCardClick = { meeting: Meeting ->
					// start using parent activity, because we want to receive result there
					getActivity()?.startFragmentActivityForResult<FragmentActivity>(
							MyMeetingDetailFragment::class.java.name,
							fragmentArgs = Bundle().apply {
								putParcelable(MyMeetingDetailFragment.ARG_MEETING, meeting)
							},
							requestCode = MainActivity.RC_MEETING_DETAIL
					)
				}
		)
		super.onCreate(savedInstanceState)
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
		layout = MyMeetingsUI(
				container!!,
				eventsAdapter = epoxyController.adapter,
				onRefresh = {
					getMeetings()
				},
				onAddClick = {
					// start using parent activity, because we want to receive result there
					getActivity()?.startFragmentActivityForResult<FragmentActivity>(
							CreateMeeting1Fragment::class.java.name,
							requestCode = MainActivity.RC_CREATE_MEETING
					)
				})

		return layout.view
	}

}
