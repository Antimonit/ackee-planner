package me.khol.ackeeplanner.screens.meeting.detail

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.rxkotlin.plusAssign
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.extensions.getViewModel
import me.khol.ackeeplanner.extensions.observeOnMainThread
import me.khol.ackeeplanner.model.api.meeting.Meeting
import me.khol.ackeeplanner.model.contacts.Contact
import me.khol.ackeeplanner.screens.guests.GuestsActivity
import me.khol.ackeeplanner.screens.meeting.base.BaseMeetingFragment
import me.khol.ackeeplanner.screens.meeting.truncateMoreThanElements

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 11.04.2018
 **/
class MyMeetingDetailFragment : BaseMeetingFragment<MyMeetingDetailUI, MyMeetingDetailViewModel>() {

	companion object {
		const val ARG_MEETING = "meeting"
	}

	override lateinit var layout: MyMeetingDetailUI
	override lateinit var viewModel: MyMeetingDetailViewModel

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		viewModel = getViewModel(vmFactory)
		viewModel.setArguments(arguments!!)
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
		layout = MyMeetingDetailUI(
				container!!,
				onLocationClick = { openGoogleMaps(viewModel.observeLocation().blockingFirst()) },
				onTimesClick = { openTimesActivity(viewModel.observeTimes().blockingFirst()) },
				onGuestsClick = { openGuestsActivity(viewModel.observeGuests().firstOrError().blockingGet()) },
				onContinueClick = { viewModel.onContinue() },
				onShareClick = { shareText(viewModel.observeLinkUrl().blockingFirst()) }
		)
		layout.view

		initializeStateObservable()

		disposables += viewModel.observeTitle()
				.observeOnMainThread()
				.subscribe { title: String ->
					layout.txtTitle.text = title
				}

		disposables += viewModel.observeLocation()
				.observeOnMainThread()
				.subscribe { location: String ->
					layout.location.setText(location)
				}

		disposables += viewModel.observeTimes()
				.observeOnMainThread()
				.subscribe { times: List<Meeting.Date> ->
					val groups: Map<Boolean, List<Meeting.Date>> = times.groupBy { it.user != null }
					val acceptedSize = groups[true]?.size ?: 0

					layout.times.setText(resources.getQuantityString(R.plurals.detail_meeting_times, times.size, times.size))
					layout.times.setSubtitle(resources.getQuantityString(R.plurals.detail_meeting_accepted_times, acceptedSize, acceptedSize))
				}

		disposables += viewModel.observeOwner()
				.observeOnMainThread()
				.subscribe { optionalHost ->
					val guest = optionalHost.getNullable() ?: getString(R.string.unknown_owner)
					layout.txtOwner.text = context?.getString(R.string.detail_meeting_invited_as_account, guest)
				}

		disposables += viewModel.observeLinkUrl()
				.observeOnMainThread()
				.subscribe { linkUrl: String ->
					layout.link.setText(linkUrl)
				}

		disposables += viewModel.observeGuests()
				.observeOnMainThread()
				.subscribe { guests ->
					val mappedGuests = guests.map {
						if (it.name != null && it.name.isNotBlank()) {
							"${it.name} (${it.email})"
						} else {
							it.email
						}
					}
					layout.guests.setText(truncateMoreThanElements(mappedGuests, 5))
				}

		activity.setSupportActionBar(layout.toolbar)
		activity.supportActionBar?.apply {
			setDisplayHomeAsUpEnabled(true)
		}

		return layout.view
	}

	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		super.onActivityResult(requestCode, resultCode, data)
		if (resultCode == RESULT_OK) {
			when (requestCode) {
				RC_GUESTS -> {
					val guests: List<Contact> = data?.extras?.getParcelableArrayList(GuestsActivity.RESULT_GUESTS)
							?: emptyList()
					viewModel.setGuests(guests)
				}
			}
		}
	}

}
