package me.khol.ackeeplanner.screens.main.invitations

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import me.khol.ackeeplanner.App
import me.khol.ackeeplanner.extensions.getViewModel
import me.khol.ackeeplanner.extensions.observeOnMainThread
import me.khol.ackeeplanner.model.api.meeting.Meeting
import me.khol.ackeeplanner.screens.base.activity.FragmentActivity
import me.khol.ackeeplanner.screens.base.activity.startFragmentActivityForResult
import me.khol.ackeeplanner.screens.main.MainActivity
import me.khol.ackeeplanner.screens.main.base.MeetingFragment
import me.khol.ackeeplanner.screens.main.invitations.epoxy.InvitationsEpoxyController
import me.khol.ackeeplanner.screens.meeting.invitation.MeetingInvitationFragment

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 09.03.2018
 */
class InvitationsFragment : MeetingFragment<InvitationsUI, InvitationsViewModel, InvitationsEpoxyController>() {

	override lateinit var layout: InvitationsUI
	override lateinit var viewModel: InvitationsViewModel
	override lateinit var epoxyController: InvitationsEpoxyController

	override fun onCreate(savedInstanceState: Bundle?) {
		App.appComponent.inject(this)
		viewModel = getViewModel(vmFactory)

		epoxyController = InvitationsEpoxyController(
				onCardClick = { meeting: Meeting ->
					// start using parent activity, because we want to receive result there
					getActivity()?.startFragmentActivityForResult<FragmentActivity>(
							MeetingInvitationFragment::class.java.name,
							fragmentArgs = Bundle().apply {
								putParcelable(MeetingInvitationFragment.ARG_MEETING, meeting)
							},
							requestCode = MainActivity.RC_MEETING_INVITATION
					)
				}
		)

		viewModel.observeUserIds()
				.observeOnMainThread()
				.subscribe { userIds ->
					epoxyController.userIds = userIds
				}

		super.onCreate(savedInstanceState)
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
		layout = InvitationsUI(
				container!!,
				eventsAdapter = epoxyController.adapter,
				onRefresh = {
					getMeetings()
				})

		return layout.view
	}

}
