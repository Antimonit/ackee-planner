package me.khol.ackeeplanner.screens.times.create

import android.Manifest
import android.annotation.SuppressLint
import android.os.Bundle
import android.support.annotation.RequiresPermission
import io.reactivex.Observable
import io.reactivex.functions.Function3
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import me.khol.ackeeplanner.App
import me.khol.ackeeplanner.model.calendar.LocalCalendarRepository
import me.khol.ackeeplanner.model.event.Event
import me.khol.ackeeplanner.model.event.LocalEventRepository
import me.khol.ackeeplanner.screens.times.BaseTimesViewModel
import me.khol.ackeeplanner.screens.times.create.TimesCreateActivity.Companion.ARG_DURATION
import me.khol.ackeeplanner.screens.times.create.TimesCreateActivity.Companion.ARG_TIMES
import me.khol.ackeeplanner.screens.times.create.TimesCreateActivity.Companion.ARG_TITLE
import org.threeten.bp.Instant
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneOffset
import javax.inject.Inject

/**
 * TODO: 11. 4. 2018 david.khol: add class description
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 11.04.2018
 **/
class TimesCreateViewModel @Inject constructor(
		app: App,
		calendarRepository: LocalCalendarRepository,
		eventRepository: LocalEventRepository
) : BaseTimesViewModel(app, calendarRepository, eventRepository) {

	sealed class State {
		class Loading : State()
		class Loaded(val events: List<Event>, val overlaidTitle: String, val overlaidDuration: Int, val overlaidEvents: Set<LocalDateTime>) : State()
		class Error(val throwable: Throwable) : State()
	}

	private var overlaidTitle: String = ""
	private var overlaidDuration: Int = 60
	private var overlaidEvents: MutableSet<LocalDateTime> = mutableSetOf()

	fun setArguments(extras: Bundle) {
		overlaidTitle = extras.getString(ARG_TITLE)
		overlaidDuration = extras.getInt(ARG_DURATION)
		overlaidEvents = extras
				.getLongArray(ARG_TIMES)
				.map {
					LocalDateTime.ofInstant(Instant.ofEpochMilli(it), ZoneOffset.UTC)
				}
				.toMutableSet()

		overlaidDurationSubject.onNext(overlaidDuration)
		overlaidEventsSubject.onNext(overlaidEvents)
	}


	private val stateSubject: Subject<State> = BehaviorSubject.createDefault(State.Loading())
	private val overlaidEventsSubject: Subject<Set<LocalDateTime>> = BehaviorSubject.createDefault(overlaidEvents)
	private val overlaidDurationSubject: Subject<Int> = BehaviorSubject.createDefault(overlaidDuration)

	@SuppressLint("MissingPermission")
	@RequiresPermission(Manifest.permission.READ_CALENDAR)
	fun observeState(): Observable<State> {
		disposables += Observable.combineLatest(
				eventLoader.observeEvents(),
				overlaidDurationSubject,
				overlaidEventsSubject,
				Function3 { events: List<Event>, overlaidDuration: Int, overlaidEvents: Set<LocalDateTime> ->
					State.Loaded(events, overlaidTitle, overlaidDuration, overlaidEvents)
				})
				.subscribe({ state ->
					stateSubject.onNext(state)
				}, { t ->
					stateSubject.onNext(State.Error(t))
				})

		return stateSubject
	}

	fun observeOverlaidDuration(): Observable<Int> = overlaidDurationSubject

	fun setOverlaidDuration(duration: Int) {
		overlaidDuration = duration
		overlaidDurationSubject.onNext(overlaidDuration)
	}


	fun observeOverlaidEvents(): Observable<Set<LocalDateTime>> = overlaidEventsSubject

	fun removeOverlaidEvent(instant: LocalDateTime) {
		overlaidEvents.remove(instant)
		overlaidEventsSubject.onNext(overlaidEvents)
	}

	fun addOverlaidEvent(instant: LocalDateTime) {
		overlaidEvents.add(instant)
		overlaidEventsSubject.onNext(overlaidEvents)
	}

}
