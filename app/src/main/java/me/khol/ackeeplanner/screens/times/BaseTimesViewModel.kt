package me.khol.ackeeplanner.screens.times

import me.khol.ackeeplanner.App
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.model.api.meeting.Meeting
import me.khol.ackeeplanner.model.calendar.LocalCalendarRepository
import me.khol.ackeeplanner.model.event.LocalEventRepository
import me.khol.ackeeplanner.screens.base.viewmodel.ContextViewModel
import org.threeten.bp.LocalDate

/**
 * Super class for
 * [me.khol.ackeeplanner.screens.times.create.TimesCreateViewModel]
 * [me.khol.ackeeplanner.screens.times.select.TimesSelectViewModel]
 * and [me.khol.ackeeplanner.screens.times.view.TimesViewViewModel].
 *
 * All subclasses display events in a [CalendarView][me.khol.calendar.CalendarView], but each
 * handle the CalendarView differently.
 * The first one allows to add and edit temporary events
 * The second one just allows to select them without adding or editing.
 * The last one does not allow any editing at all.
 *
 * This class handles loading of events as it is used by all implementations.
 *
 * Created by David Khol [david@khol.me] on 9. 3. 2018.
 */
abstract class BaseTimesViewModel constructor(
		app: App,
		calendarRepository: LocalCalendarRepository,
		eventRepository: LocalEventRepository
) : ContextViewModel(app) {

	protected val eventLoader: EventLoader = EventLoader(calendarRepository, eventRepository)

	fun onDateChanged(date: LocalDate) {
		eventLoader.onDateChanged(date)
	}

	override fun onCleared() {
		super.onCleared()
		eventLoader.onCleared()
	}


	protected fun mapDatesToOverlayEvents(dates: List<Meeting.Date>): Set<OverlayEvent> {
		return dates.map { date: Meeting.Date ->
			val summary = date.user?.name ?: date.user?.email ?: ctx.getString(R.string.available)
			val color = 0
			OverlayEvent(
					date.id,
					summary,
					null,
					date.startDate,
					date.endDate,
					color,
					date.user != null,
					date.user?.name ?: date.user?.email ?: date.user?.calendarGoogleid
			)
		}.toSet()
	}

}
