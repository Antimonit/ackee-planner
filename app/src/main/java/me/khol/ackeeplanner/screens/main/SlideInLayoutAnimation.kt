package me.khol.ackeeplanner.screens.main

import android.view.animation.LayoutAnimationController

/**
 * TODO: 3. 5. 2018 david.khol: add class description
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 03.05.2018
 **/
class SlideInLayoutAnimation : LayoutAnimationController(SlideInAnimationSet(), 0.15f)
