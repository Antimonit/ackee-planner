package me.khol.ackeeplanner.screens.times.calendars.epoxy

import com.airbnb.epoxy.EpoxyController
import me.khol.ackeeplanner.model.calendar.Calendar

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 16.04.2018
 **/
class CalendarsEpoxyController(
		val onCalendarToggled: (Calendar, Boolean) -> Unit
) : EpoxyController() {

	var calendars: List<Calendar> = emptyList()
		set(value) {
			field = value
			requestModelBuild()
		}

	override fun buildModels() {
		calendars.forEach {
			calendar {
				id(it.id)
				calendar(it)
				onCalendarToggled(onCalendarToggled)
			}
		}
	}

}
