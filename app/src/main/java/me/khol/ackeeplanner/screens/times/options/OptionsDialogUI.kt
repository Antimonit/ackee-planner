package me.khol.ackeeplanner.screens.times.options

import android.content.Context
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.ViewManager
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.screens.base.layout.BaseParentlessLayout
import me.khol.extensions.getColor
import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar
import org.jetbrains.anko.*
import org.jetbrains.anko.custom.ankoView

class OptionsDialogUI(
		context: Context,
		private val duration: Int,
		private val onDurationSelected: (Int) -> Unit
) : BaseParentlessLayout(context) {

	companion object {
	    private const val SNAP_TO = 15
	}

	inline fun ViewManager.discreteSeekBar(init: (@AnkoViewDslMarker DiscreteSeekBar).() -> Unit = {}): DiscreteSeekBar {
		return ankoView({ ctx -> DiscreteSeekBar(ctx) }, theme = 0) { init() }
	}

	override fun createView(ui: AnkoContext<Context>): View {
		return ui.frameLayout {
			backgroundColor = getColor(R.color.black).withAlpha(10)

			textView("Drag the slider to change meeting's duration.") {
				padding = 16.dp
				textColorResource = R.color.textColorPrimary
				gravity = Gravity.CENTER
			}.lparams(matchParent, wrapContent) {
				gravity = Gravity.BOTTOM
			}

			discreteSeekBar {
				horizontalPadding = 32.dp
				topPadding = 64.dp

				min = 1
				max = 8
				progress = duration / SNAP_TO

				numericTransformer = object: DiscreteSeekBar.NumericTransformer() {
					override fun transform(value: Int): Int {
						return value * SNAP_TO
					}
				}

				setOnProgressChangeListener(object: DiscreteSeekBar.OnProgressChangeListener {
					override fun onProgressChanged(seekBar: DiscreteSeekBar?, value: Int, fromUser: Boolean) {
						onDurationSelected(value * SNAP_TO)
					}
					override fun onStartTrackingTouch(seekBar: DiscreteSeekBar?) {}
					override fun onStopTrackingTouch(seekBar: DiscreteSeekBar?) {}
				})

			}.lparams {
				gravity = Gravity.TOP
			}

		}.apply {
			layoutParams = ViewGroup.LayoutParams(matchParent, 144.dp)
		}
	}

}
