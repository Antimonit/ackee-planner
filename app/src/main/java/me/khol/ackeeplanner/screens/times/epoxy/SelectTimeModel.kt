package me.khol.ackeeplanner.screens.times.epoxy

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.extensions.font
import me.khol.ackeeplanner.screens.base.epoxy.BaseEpoxyModel
import me.khol.ackeeplanner.screens.base.layout.BaseLayout
import me.khol.ackeeplanner.screens.base.layout.defaultTextView
import me.khol.ackeeplanner.screens.times.OverlayEvent
import me.khol.extensions.getDrawableAttr
import me.khol.extensions.isVisible
import org.jetbrains.anko.*
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 09.03.2018
 **/
@EpoxyModelClass
open class SelectTimeModel : BaseEpoxyModel<SelectTimeModel.Layout>() {

	companion object {
		private val timeFormatter = DateTimeFormatter.ofPattern("HH:mm")
	}

	@EpoxyAttribute
	lateinit var date: OverlayEvent
	@EpoxyAttribute
	var selected: Boolean = false
	@EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
	lateinit var onTimeClick: (OverlayEvent) -> Unit

	override fun createViewLayout(parent: ViewGroup) = Layout(parent)

	override fun bind(layout: Layout) {
		super.bind(layout)
		layout.view.setOnClickListener {
			onTimeClick(date)
		}

		if (date.isTaken) {
			layout.view.backgroundDrawable = null
			layout.view.isClickable = false
			layout.txtTaken.text = layout.context.getString(R.string.claimed_by, date.takenBy)
		} else {
			layout.view.backgroundDrawable = layout.view.getDrawableAttr(android.R.attr.selectableItemBackground)
			layout.view.isClickable = true
			layout.txtTaken.text = ""
		}

		if (selected) {
			layout.imgDone.isVisible = true
			layout.txtTitle.textColorResource = R.color.black
			layout.txtTitle.font = R.font.roboto_bold
		} else if (date.isTaken) {
			layout.imgDone.isVisible = false
			layout.txtTitle.textColorResource = R.color.black_50
			layout.txtTitle.font = R.font.roboto_thin
		} else {
			layout.imgDone.isVisible = false
			layout.txtTitle.textColorResource = R.color.black
			layout.txtTitle.font = R.font.roboto
		}


		val start = LocalDateTime.ofInstant(date.start, ZoneId.systemDefault())
		val end = LocalDateTime.ofInstant(date.end, ZoneId.systemDefault())
		val text = "${timeFormatter.format(start)} - ${timeFormatter.format(end)}"

		layout.txtTitle.text = text
	}

	override fun unbind(layout: Layout) {
		super.unbind(layout)
		layout.view.setOnClickListener(null)
	}

	class Layout(parent: ViewGroup) : BaseLayout(parent) {

		lateinit var txtTitle: TextView
		lateinit var txtTaken: TextView
		lateinit var imgDone: ImageView

		override fun createView(ui: AnkoContext<ViewGroup>): View {
			return ui.constraintLayout {
				txtTitle = defaultTextView {
					textSize = 16f
				}

				txtTaken = defaultTextView {
					textSize = 14f
					font = R.font.roboto_thin
					textColor = R.color.black_50
				}

				imgDone = imageView(R.drawable.ic_check)

				constraints {
					txtTitle.connect(VERTICAL of parentId)
							.connect(STARTS of parentId with 16.dp)

					txtTaken.connect(VERTICAL of parentId)
							.connect(ENDS of parentId with 16.dp)

					imgDone.connect(VERTICAL of parentId)
							.connect(ENDS of parentId with 16.dp)
							.size(24.dp, 24.dp)
				}

			}.apply {
				layoutParams = ViewGroup.LayoutParams(org.jetbrains.anko.matchParent, 48.dp)
			}
		}

	}
}
