package me.khol.ackeeplanner.screens.main.base

import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import me.khol.ackeeplanner.screens.base.layout.BaseLayout

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 26.04.2018
 **/
abstract class MeetingUI(
        parent: ViewGroup
) : BaseLayout(parent) {

	open lateinit var eventsRecycler: RecyclerView
	open lateinit var swipeRefreshLayout: SwipeRefreshLayout

}
