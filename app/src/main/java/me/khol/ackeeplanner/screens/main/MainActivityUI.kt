package me.khol.ackeeplanner.screens.main

import android.support.design.widget.BottomNavigationView
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.screens.base.layout.BaseLayout
import me.khol.extensions.elevationCompat
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.toolbar
import org.jetbrains.anko.design.bottomNavigationView

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 05.10.2017
 **/
class MainActivityUI(
		parent: ViewGroup,
		val onNavigationItemSelected: (MenuItem) -> Boolean
) : BaseLayout(parent) {

	lateinit var toolbar: Toolbar
	lateinit var bottomNavigationView: BottomNavigationView
	lateinit var fragmentContainer: FrameLayout

	override fun createView(ui: AnkoContext<ViewGroup>): View {
		return ui.verticalLayout {
			toolbar = toolbar {
				backgroundResource = R.color.white
				elevationCompat = 4.dpf
				fitsSystemWindows = true
			}.lparams(matchParent, wrapContent)

			fragmentContainer = frameLayout {
				id = R.id.fragment_container
			}.lparams(matchParent, 0.dp, weight = 1f)

			bottomNavigationView = bottomNavigationView {
				id = R.id.bottom_navigation_view
				backgroundResource = R.color.white
				inflateMenu(R.menu.main_bottom_navigation_menu)
				setOnNavigationItemSelectedListener(onNavigationItemSelected)
			}.lparams(matchParent, wrapContent)

		}.also {
			it.layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)
		}
	}
}
