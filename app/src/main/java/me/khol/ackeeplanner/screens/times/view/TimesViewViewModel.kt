package me.khol.ackeeplanner.screens.times.view

import android.Manifest
import android.annotation.SuppressLint
import android.support.annotation.RequiresPermission
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import me.khol.ackeeplanner.App
import me.khol.ackeeplanner.model.api.meeting.Meeting
import me.khol.ackeeplanner.model.calendar.LocalCalendarRepository
import me.khol.ackeeplanner.model.event.Event
import me.khol.ackeeplanner.model.event.LocalEventRepository
import me.khol.ackeeplanner.screens.times.BaseTimesViewModel
import me.khol.ackeeplanner.screens.times.OverlayEvent
import javax.inject.Inject

/**
 * TODO: 11. 4. 2018 david.khol: add class description
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 11.04.2018
 **/
class TimesViewViewModel @Inject constructor(
		app: App,
		calendarRepository: LocalCalendarRepository,
		eventRepository: LocalEventRepository
) : BaseTimesViewModel(app, calendarRepository, eventRepository) {

	sealed class State {
		class Loading : State()
		class Loaded(val events: List<Event>, val overlaidEvents: Set<OverlayEvent>) : State()
		class Error(val throwable: Throwable) : State()
	}

	private val stateSubject: Subject<State> = BehaviorSubject.createDefault(State.Loading())
	private val overlaidEventsSubject: Subject<Set<OverlayEvent>> = BehaviorSubject.createDefault(setOf())

	private var overlayEvents: Set<OverlayEvent> = setOf()

	@SuppressLint("MissingPermission")
	@RequiresPermission(Manifest.permission.READ_CALENDAR)
	fun observeState(): Observable<State> {
		disposables += Observable.combineLatest(
				eventLoader.observeEvents(),
				overlaidEventsSubject,
				BiFunction<List<Event>, Set<OverlayEvent>, State> { events, overlaidEvents ->
					State.Loaded(events, overlaidEvents)
				})
				.subscribe({ state ->
					stateSubject.onNext(state)
				}, { t: Throwable ->
					stateSubject.onNext(State.Error(t))
				})

		return stateSubject
	}

	fun setOverlaidDates(dates: List<Meeting.Date>) {
		overlayEvents = mapDatesToOverlayEvents(dates)
		overlaidEventsSubject.onNext(overlayEvents)
	}

}
