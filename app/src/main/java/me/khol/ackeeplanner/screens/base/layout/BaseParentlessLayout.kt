package me.khol.ackeeplanner.screens.base.layout

import android.content.Context
import android.view.View
import me.khol.extensions.dp
import me.khol.extensions.dpf
import me.khol.extensions.sp
import me.khol.extensions.spf
import org.jetbrains.anko.AnkoComponent
import org.jetbrains.anko.AnkoContext

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 05.10.2017
 **/
abstract class BaseParentlessLayout(val context: Context) : AnkoComponent<Context> {

	val view: View by lazy { createView(AnkoContext.create(context)) }

	val Int.dp: Int get() = context.dp(this)
	val Int.dpf: Float get() = context.dpf(this)
	val Int.sp: Int get() = context.sp(this)
	val Int.spf: Float get() = context.spf(this)

	abstract override fun createView(ui: AnkoContext<Context>): View

}
