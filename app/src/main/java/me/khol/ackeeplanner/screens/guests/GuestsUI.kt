package me.khol.ackeeplanner.screens.guests

import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.text.InputType
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.screens.base.layout.BaseLayout
import me.khol.extensions.elevationCompat
import me.khol.extensions.getColor
import me.khol.extensions.withAlpha
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.toolbar
import org.jetbrains.anko.recyclerview.v7.recyclerView

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 11.03.2018
 **/
class GuestsUI(
		parent: ViewGroup,
		val adapter: RecyclerView.Adapter<*>
) : BaseLayout(parent) {

	lateinit var toolbar: Toolbar
	lateinit var contacts: RecyclerView
	lateinit var searchEdit: EditText

	fun getSearchQuery() = searchEdit.text.toString()

	fun clearSearchQuery() = searchEdit.setText("")

	override fun createView(ui: AnkoContext<ViewGroup>): View {
		return ui.verticalLayout {
			fitsSystemWindows = true
			backgroundColor = getColor(R.color.black).withAlpha(10)

			toolbar = toolbar {
				backgroundResource = R.color.white
				elevationCompat = 4.dpf
				fitsSystemWindows = true
				searchEdit = editText {
					backgroundDrawable = null
					hintResource = R.string.guests_toolbar_hint
					leftPadding = 0
					lines = 1
					inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
					imeOptions = EditorInfo.IME_FLAG_NO_EXTRACT_UI
				}.lparams(matchParent, wrapContent)
			}.lparams(matchParent, wrapContent)

			contacts = recyclerView {
				layoutManager = LinearLayoutManager(ui.ctx, LinearLayoutManager.VERTICAL, false)
				itemAnimator = DefaultItemAnimator()
				clipChildren = false
				adapter = this@GuestsUI.adapter
				bottomPadding = 16.dp
				clipToPadding = false
			}.lparams(matchParent, 0.dp, weight = 1f)

		}.also {
			it.layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)
		}
	}

}
