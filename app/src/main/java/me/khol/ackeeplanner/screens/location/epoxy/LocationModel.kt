package me.khol.ackeeplanner.screens.location.epoxy

import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.extensions.font
import me.khol.ackeeplanner.model.location.Location
import me.khol.ackeeplanner.screens.base.epoxy.BaseEpoxyModel
import me.khol.ackeeplanner.screens.base.layout.BaseLayout
import me.khol.ackeeplanner.screens.base.layout.defaultTextView
import me.khol.extensions.*
import org.jetbrains.anko.*


/**
 * Displays location address in title and subtitle textViews like:
 * Ackee s.r.o.
 * Karolinská 650/1, 186 00 Praha 8-Karlín, Czechia
 *
 * Or only title textView if the location does not contain address.
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 29.03.2018
 **/
open class LocationModel : BaseEpoxyModel<LocationModel.Layout>() {

	@EpoxyAttribute
	lateinit var location: Location
	@EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
	lateinit var onLocationClicked: (Location) -> Unit

	override fun createViewLayout(parent: ViewGroup) = Layout(parent)

	override fun bind(layout: Layout) {
		super.bind(layout)
		if (location.address == null) {
			layout.txtSubtitle.isVisible = false
			layout.txtTitle.text = location.name
		} else {
			layout.txtSubtitle.isVisible = true
			layout.txtTitle.text = location.name
			layout.txtSubtitle.text = location.address
		}
		layout.view.setOnClickListener {
			onLocationClicked(location)
		}
	}

	override fun unbind(layout: Layout) {
		super.unbind(layout)
		layout.view.setOnClickListener(null)
	}

	class Layout(parent: ViewGroup) : BaseLayout(parent) {

		lateinit var txtTitle: TextView
		lateinit var txtSubtitle: TextView
		lateinit var imgIcon: ImageView

		override fun createView(ui: AnkoContext<ViewGroup>): View {
			return ui.frameLayout {
				backgroundResource = R.color.white
				elevationCompat = 2.dpf
				foreground = getDrawableAttr(android.R.attr.selectableItemBackground)
				isClickable = true

				constraintLayout {

					imgIcon = imageView {
						setImageDrawable(getTintedDrawable(R.drawable.ic_place, R.color.black_50))
					}

					txtTitle = defaultTextView {
						ellipsize = TextUtils.TruncateAt.END
						textColor = getColor(R.color.textColorPrimary)
						textSize = 16f
					}

					txtSubtitle = defaultTextView {
						ellipsize = TextUtils.TruncateAt.END
						textColor = getColor(R.color.textColorSecondary)
						textSize = 14f
						font = R.font.roboto_light
					}

					constraints {
						imgIcon
								.connect(VERTICAL of parentId)
								.connect(STARTS of parentId with 16.dp)
								.size(24.dp, 24.dp)

						txtTitle
								.connect(STARTS of parentId with 72.dp)
								.connect(ENDS of parentId with 16.dp)
								.size(matchConstraint, wrapContent)

						txtSubtitle
								.connect(HORIZONTAL of txtTitle)
								.size(matchConstraint, wrapContent)

						arrayOf(txtTitle, txtSubtitle).chainPacked(
								TOP of parentId,
								BOTTOM of parentId
						)
					}
				}.lparams(matchParent, matchParent)

			}.apply {
				layoutParams = ViewGroup.LayoutParams(org.jetbrains.anko.matchParent, 64.dp)
			}
		}
	}
}
