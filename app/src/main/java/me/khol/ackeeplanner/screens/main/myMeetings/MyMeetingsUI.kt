package me.khol.ackeeplanner.screens.main.myMeetings

import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.screens.base.epoxy.VerticalSpaceItemDecoration
import me.khol.ackeeplanner.screens.main.base.MeetingUI
import me.khol.ackeeplanner.screens.main.SlideInLayoutAnimation
import org.jetbrains.anko.*
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.design.floatingActionButton
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.swipeRefreshLayout

/**
 * Created by David Khol [david@khol.me] on 9. 3. 2018.
 */
class MyMeetingsUI(
		parent: ViewGroup,
		private val eventsAdapter: RecyclerView.Adapter<*>,
		private val onRefresh: () -> Unit = {},
		private val onAddClick: () -> Unit = {}
) : MeetingUI(parent) {

	override lateinit var eventsRecycler: RecyclerView
	override lateinit var swipeRefreshLayout: SwipeRefreshLayout

	override fun createView(ui: AnkoContext<ViewGroup>): View {
		return ui.coordinatorLayout {

			swipeRefreshLayout = swipeRefreshLayout {
				setOnRefreshListener(onRefresh)
				setColorSchemeResources(R.color.colorPrimary, R.color.colorAccent)

				eventsRecycler = recyclerView {
					layoutManager = LinearLayoutManager(super.context)
					adapter = eventsAdapter
					clipToPadding = false
					verticalPadding = 16.dp
					addItemDecoration(VerticalSpaceItemDecoration(8.dp))
					layoutAnimation = SlideInLayoutAnimation()
				}
			}.lparams(matchParent, matchParent)

			floatingActionButton {
				imageResource = R.drawable.ic_add_white
				setOnClickListener {
					onAddClick()
				}
			}.lparams {
				gravity = Gravity.BOTTOM or Gravity.END
				margin = 16.dp
			}

		}.apply {
			layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)
		}
	}

}
