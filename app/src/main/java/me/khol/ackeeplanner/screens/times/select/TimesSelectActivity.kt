package me.khol.ackeeplanner.screens.times.select

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.annotation.RequiresPermission
import android.view.Menu
import io.reactivex.rxkotlin.plusAssign
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.extensions.formatRelativeToToday
import me.khol.ackeeplanner.extensions.getViewModel
import me.khol.ackeeplanner.extensions.observeOnMainThread
import me.khol.ackeeplanner.model.api.meeting.Meeting
import me.khol.ackeeplanner.screens.times.BaseTimesActivity
import me.khol.ackeeplanner.screens.times.CalendarUI
import me.khol.ackeeplanner.screens.times.OverlayEvent
import me.khol.calendar.CalendarListener
import me.khol.calendar.Event
import org.threeten.bp.*

/**
 * An activity to browse a calendar with an ability to choose one of the overlaid events.
 *
 * This screen is used in conjunction with
 * [MeetingInvitationFragment][me.khol.ackeeplanner.screens.meeting.invitation.MeetingInvitationFragment].
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 11.04.2018
 **/
class TimesSelectActivity : BaseTimesActivity() {

	companion object {
		const val ARG_DATES = "dates"
		const val ARG_SELECTED_DATE = "selected_date"
		const val RESULT_SELECTED_DATE = "selected_date"
	}

	override lateinit var layout: CalendarUI
	lateinit var viewModel: TimesSelectViewModel
	lateinit var controller: TimesSelectController

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		viewModel = getViewModel(vmFactory)

		controller = TimesSelectController(
				onTimeClick = { date: OverlayEvent ->
					viewModel.onDateClick(date)
				}
		)

		layout = CalendarUI(
				rootView,
				controller.adapter,
				calendarListener = object : CalendarListener {
					override fun onEventClick(event: Event) {
						viewModel.onEventClick(event)
					}

					override fun onEventLongClick(event: Event) {
						viewModel.onEventClick(event)
					}

					override fun onEmptySpaceClick(dateTime: LocalDateTime) {
						// do nothing
					}

					override fun onEmptySpaceLongClick(dateTime: LocalDateTime) {
						// do nothing
					}

					override fun onDateChanged(date: LocalDate) {
						viewModel.onDateChanged(date)
						changeTitle(date)
					}
				},
				onDoneClick = {
					val selectedEventId = viewModel.getSelectedEventId()

					setResult(Activity.RESULT_OK, Intent().apply {
						putExtras(Bundle().apply {
							if (selectedEventId != null) {
								putInt(RESULT_SELECTED_DATE, selectedEventId)
							}
						})
					})
					finish()
				}
		).apply {
			view // force lazy initialization
			calendar.allowInsertionOfEvents = false
			setBottomTitle(getString(R.string.invitation_meeting_choose_time_slot))
			setBottomSubtitle(resources.getQuantityString(R.plurals.invitation_meeting_time_slot_available, 0, 0))
		}
		setContentView(layout.view)

		val startDay = LocalDate.now()

		layout.calendar.numberOfDays = 1
		layout.calendar.currentDate = startDay
		viewModel.onDateChanged(startDay)

		val extras = intent.extras
		if (extras != null) {
			val overlaidDates: List<Meeting.Date> = extras.getParcelableArrayList(ARG_DATES)
					?: emptyList()
			val selectedDateId = if (extras.containsKey(ARG_SELECTED_DATE)) {
				extras.getInt(ARG_SELECTED_DATE)
			} else {
				null
			}
			viewModel.setOverlaidDates(overlaidDates, selectedDateId)

			val selected = overlaidDates

					.firstOrNull { it.id == selectedDateId }
			val first = overlaidDates
					.sortedBy { it.startDate }
					.firstOrNull { it.user == null }

			selected ?: first ?.let {
				val startDate = LocalDateTime.ofInstant(it.startDate, ZoneOffset.UTC).toLocalDate()
				layout.calendar.currentDate = startDate
				viewModel.onDateChanged(startDate)
			}
		}

		setSupportActionBar(layout.toolbar)
		supportActionBar?.apply {
			setDisplayHomeAsUpEnabled(true)
		}

		checkCalendarPermission()
	}

	@SuppressLint("MissingPermission")
	@RequiresPermission(Manifest.permission.READ_CALENDAR)
	override fun onCalendarPermissionGranted() {
		disposables += viewModel.observeState()
				.observeOnMainThread()
				.subscribe({ state: TimesSelectViewModel.State ->
					when (state) {
						is TimesSelectViewModel.State.Loading -> {
							layout.showProgress(true)
						}
						is TimesSelectViewModel.State.Loaded -> {
							layout.showProgress(false)
							val defaultZoneId = ZoneId.systemDefault()
							val overlaidEvents = mapOverlaidEvents(state.overlaidEvents, defaultZoneId, state.selectedId)
							val events = mapEvents(state.events, defaultZoneId)

							layout.calendar.events = events + overlaidEvents

							val unclaimed = state.overlaidEvents.filter { !it.isTaken }.size
							layout.setBottomSubtitle(resources.getQuantityString(
									R.plurals.invitation_meeting_time_slot_available,
									unclaimed,
									unclaimed
							))

							controller.events = state.overlaidEvents
							controller.selectedId = state.selectedId

							if (state.selectedId != null) {
								state.overlaidEvents.firstOrNull { it.id == state.selectedId }?.let {
									layout.setBottomTitle(it.start.formatRelativeToToday())
									layout.setBottomSubtitle(null)
								}
								layout.showDoneButton(true)
							} else {
								layout.showDoneButton(false)
							}
						}
						is TimesSelectViewModel.State.Error -> {
							layout.showProgress(false)
							resolveErrorState(state.throwable)
						}
					}
				})
	}

	override fun onCreateOptionsMenu(menu: Menu): Boolean {
		super.onCreateOptionsMenu(menu)
		durationAction.isVisible = false
		return true
	}

}
