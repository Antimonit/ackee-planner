package me.khol.ackeeplanner.screens.base.viewmodel

import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

/**
 * A ViewModel that automatically dispose its [disposables]
 *
 * @author David Bilik [david.bilik@ackee.cz]
 * @since 17/11/2017
 **/
abstract class BaseViewModel : ViewModel() {

	protected var disposables = CompositeDisposable()

	override fun onCleared() {
		super.onCleared()
		disposables.clear()
	}

}
