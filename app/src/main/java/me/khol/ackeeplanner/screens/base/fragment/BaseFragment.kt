package me.khol.ackeeplanner.screens.base.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import com.google.android.gms.common.GoogleApiAvailability
import io.reactivex.disposables.CompositeDisposable
import me.khol.ackeeplanner.App
import me.khol.ackeeplanner.screens.base.viewmodel.ViewModelFactory
import me.khol.ackeeplanner.screens.base.activity.BaseActivity
import javax.inject.Inject

/**
 * Base fragment
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 12. 2. 2017
 */
open class BaseFragment : Fragment() {

	val activity: BaseActivity
		get() = getActivity() as BaseActivity

	val disposables = CompositeDisposable()

	@Inject
	lateinit var vmFactory: ViewModelFactory

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		App.appComponent.inject(this)
	}

	override fun onDestroyView() {
		super.onDestroyView()
		disposables.clear()
	}

	/**
	 * Attempt to resolve a missing, out-of-date, invalid or disabled Google
	 * Play Services installation via a user dialog, if possible.
	 */
	fun acquireGooglePlayServices() {
		val apiAvailability = GoogleApiAvailability.getInstance()
		val connectionStatusCode = apiAvailability.isGooglePlayServicesAvailable(context)
		if (apiAvailability.isUserResolvableError(connectionStatusCode)) {
			showGooglePlayServicesErrorDialog(connectionStatusCode)
		}
	}

	/**
	 * Display an error dialog showing that Google Play Services is missing
	 * or out of date.
	 * @param connectionStatusCode code describing the presence (or lack of)
	 * Google Play Services on this device.
	 */
	fun showGooglePlayServicesErrorDialog(connectionStatusCode: Int) {
		GoogleApiAvailability.getInstance()
				.getErrorDialog(
						activity,
						connectionStatusCode,
						BaseActivity.REQUEST_GOOGLE_PLAY_SERVICES
				).show()
	}

}
