package me.khol.ackeeplanner.screens.meeting.invitation

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.rxkotlin.plusAssign
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.extensions.formatRelativeToToday
import me.khol.ackeeplanner.extensions.getViewModel
import me.khol.ackeeplanner.extensions.observeOnMainThread
import me.khol.ackeeplanner.model.api.meeting.Meeting
import me.khol.ackeeplanner.screens.calendar.ChooseCalendarActivity
import me.khol.ackeeplanner.screens.calendar.SimplifiedCalendar
import me.khol.ackeeplanner.screens.meeting.ProgressButtonUI
import me.khol.ackeeplanner.screens.meeting.base.BaseMeetingFragment
import me.khol.ackeeplanner.screens.times.select.TimesSelectActivity

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 11.04.2018
 **/
class MeetingInvitationFragment : BaseMeetingFragment<MeetingInvitationUI, MeetingInvitationViewModel>() {

	companion object {
		const val ARG_MEETING = "meeting"
	}

	private var canAccept: Boolean = true

	override lateinit var layout: MeetingInvitationUI
	override lateinit var viewModel: MeetingInvitationViewModel

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		viewModel = getViewModel(vmFactory)
		viewModel.setArguments(arguments!!)
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
		layout = MeetingInvitationUI(container!!,
				onCalendarClick = {
					if (canAccept) {
						openCalendarActivity(viewModel.observeCalendar().blockingFirst().getNullable(), true)
					} else {
						// don't do anything
					}
				},
				onLocationClick = {
					openGoogleMaps(viewModel.observeLocation().blockingFirst())
				},
				onTimesClick = {
					if (canAccept) {
						openTimesActivity(
								viewModel.observeDates().blockingFirst(),
								viewModel.observeSelectedTime().blockingFirst().getNullable()
						)
					} else {
						// don't do anything
					}
				},
				onContinueClick = { viewModel.onContinue() }
		)
		layout.view

		initializeStateObservable()

		disposables += viewModel.observeAcceptable()
				.observeOnMainThread()
				.subscribe { isAcceptable ->
					canAccept = isAcceptable
					if (!canAccept) {
						layout.next.setFinishedText(getString(R.string.invitation_meeting_cannot_accept_twice))
						layout.next.setState(ProgressButtonUI.State.FINISHED)
					}
				}

		disposables += viewModel.observeTitle()
				.observeOnMainThread()
				.subscribe { title: String ->
					layout.txtTitle.text = title
				}

		disposables += viewModel.observeLocation()
				.observeOnMainThread()
				.subscribe { location: String ->
					layout.location.setText(location)
				}

		disposables += viewModel.observeSelectedTime()
				.observeOnMainThread()
				.subscribe { optionalTime ->
					val time: Meeting.Date? = optionalTime.getNullable()
					time?.let {
						layout.times.setText(it.startDate.formatRelativeToToday())
					}
				}

		disposables += viewModel.observeCalendar()
				.observeOnMainThread()
				.subscribe { optionalCalendar ->
					optionalCalendar.getNullable()?.let {
						if (it.displayName != null) {
							layout.calendar.setText(it.displayName)
						} else {
							layout.calendar.setText(it.name)
						}
						layout.calendar.setSubtitle(it.email)
					}
				}

		disposables += viewModel.observeOwner()
				.observeOnMainThread()
				.subscribe { optionalHost ->
					val guest = optionalHost.getNullable() ?: getString(R.string.unknown_owner)
					layout.txtOwner.text = context?.getString(R.string.invited_by_owner, guest)
				}

		activity.setSupportActionBar(layout.toolbar)
		activity.supportActionBar?.apply {
			setDisplayHomeAsUpEnabled(true)
		}

		return layout.view
	}

	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		super.onActivityResult(requestCode, resultCode, data)
		if (resultCode == RESULT_OK) {
			when (requestCode) {
				RC_TIMES -> {
					if (data?.extras?.containsKey(TimesSelectActivity.RESULT_SELECTED_DATE) == true) {
						val selectedEventId = data.extras?.getInt(TimesSelectActivity.RESULT_SELECTED_DATE)
						viewModel.setSelectedEventId(selectedEventId)
					}
				}
				RC_CALENDAR -> {
					val calendar = data?.extras?.getParcelable<SimplifiedCalendar>(ChooseCalendarActivity.RESULT_CALENDAR)
					viewModel.setCalendar(calendar)
				}
			}
		}
	}

}
