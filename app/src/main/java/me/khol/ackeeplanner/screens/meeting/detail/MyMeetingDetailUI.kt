package me.khol.ackeeplanner.screens.meeting.detail

import android.support.design.widget.AppBarLayout
import android.support.v7.widget.Toolbar
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.screens.meeting.*
import me.khol.ackeeplanner.screens.meeting.base.BaseMeetingUI
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.navigationIconResource
import org.jetbrains.anko.appcompat.v7.toolbar
import org.jetbrains.anko.design.appBarLayout
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.support.v4.nestedScrollView

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 07.04.2018
 **/
class MyMeetingDetailUI(
		parent: ViewGroup,
		private val onLocationClick: () -> Unit,
		private val onTimesClick: () -> Unit,
		private val onGuestsClick: () -> Unit,
		private val onContinueClick: () -> Unit,
		private val onShareClick: () -> Unit
) : BaseMeetingUI(parent) {

	lateinit var toolbar: Toolbar

	lateinit var location: ItemUI
	lateinit var link: ItemUI
	lateinit var times: ItemUI
	lateinit var guests: ItemUI

	lateinit var txtTitle: TextView
	lateinit var txtOwner: TextView

	override fun createView(ui: AnkoContext<ViewGroup>): View {
		return ui.coordinatorLayout {

			appBarLayout {
				backgroundResource = R.color.white

				toolbar = toolbar {
					fitsSystemWindows = true
					title = context.getString(R.string.detail_meeting_title)
					navigationIconResource = R.drawable.ic_close
				}.lparams(matchParent, wrapContent) {
					scrollFlags = 0
				}

				txtTitle = textView {
					hintResource = R.string.detail_meeting_hint_times
					textColorResource = R.color.textColorPrimary
					textSize = 28f
					leftPadding = 72.dp
					rightPadding = 16.dp
					bottomPadding = 16.dp
				}.lparams(matchParent, wrapContent) {
					scrollFlags = 0
				}

				txtOwner = textView {
					hintResource = R.string.detail_meeting_hint_owner
					textColorResource = R.color.textColorTertiary
					textSize = 12f
					leftPadding = 72.dp
					rightPadding = 16.dp
					bottomPadding = 16.dp
				}.lparams(matchParent, wrapContent) {
					scrollFlags = 0
				}

			}.lparams(matchParent, wrapContent)

			nestedScrollView {
				topPadding = 8.dp
				clipToPadding = false

				verticalLayout {
					times = clickableItem(context.getString(R.string.detail_meeting_hint_times), R.drawable.ic_schedule, onTimesClick)
					location = clickableItem(context.getString(R.string.detail_meeting_hint_location), R.drawable.ic_place, onLocationClick)

					link = clickableItem(context.getString(R.string.detail_meeting_hint_link), R.drawable.ic_link, onShareClick)

					guests = clickableItem(context.getString(R.string.create_meeting_hint_guests), R.drawable.ic_perm_identity, onGuestsClick)

					next = ProgressButtonUI(
							this,
							R.string.create_meeting_send_invitations,
							R.string.create_meeting_success,
							onContinueClick
					).apply {
						addView(view)
					}

				}.lparams(matchParent, matchParent)

			}.lparams(matchParent, matchParent) {
				behavior = AppBarLayout.ScrollingViewBehavior()
			}
		}.also {
			it.layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)
		}
	}

}
