package me.khol.ackeeplanner.screens.meeting

import android.content.res.ColorStateList
import android.support.constraint.ConstraintLayout
import android.support.v4.widget.ImageViewCompat
import android.text.Editable
import android.text.InputType
import android.text.TextUtils
import android.text.TextWatcher
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import cz.ackee.ankoconstraintlayout._ConstraintLayout
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.screens.base.layout.BaseLayout
import me.khol.extensions.getColor
import me.khol.extensions.getDrawableAttr
import me.khol.extensions.isVisible
import org.jetbrains.anko.*

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 11.04.2018
 **/
open class ItemUI(
		parent: ViewGroup,
		protected val hint: String,
		protected val iconResource: Int
) : BaseLayout(parent) {

	open fun setText(text: String?) {
		txtDescription.text = text
	}

	open fun setSubtitle(text: String?) {
		txtSubtitle.text = text
		txtSubtitle.isVisible = text?.isNotBlank() ?: false
	}

	open lateinit var txtDescription: TextView
	open lateinit var txtSubtitle: TextView
	open lateinit var imgIcon: ImageView
	open lateinit var constraint: ConstraintLayout

	protected open fun _ConstraintLayout.titleSetup(): TextView  {
		return textView {
			hint = this@ItemUI.hint
			textSize = 16f
			textColorResource = R.color.textColorPrimary
			gravity = Gravity.CENTER_VERTICAL or Gravity.START
			ellipsize = TextUtils.TruncateAt.END
		}.lparams {
			constrainedHeight = true
		}
	}

	protected open fun _ConstraintLayout.subtitleSetup(): TextView {
		return textView {
			textColorResource = R.color.textColorSecondary
			isVisible = false
			ellipsize = TextUtils.TruncateAt.END
		}.lparams {
			constrainedHeight = true
		}
	}

	protected open fun _ConstraintLayout.iconSetup(): ImageView {
		return imageView(this@ItemUI.iconResource) {
			ImageViewCompat.setImageTintList(this, ColorStateList.valueOf(getColor(R.color.black_70)))
		}
	}

	override fun createView(ui: AnkoContext<ViewGroup>): View {
		return ui.frameLayout {
			constraint = constraintLayout {
				minHeight = 56.dp

				txtDescription = titleSetup()
				txtSubtitle = subtitleSetup()
				imgIcon = iconSetup()

				constraints {
					txtDescription
							.connect(STARTS of parentId with 72.dp)
							.connect(ENDS of parentId with 16.dp)
							.width(matchConstraint)

					txtSubtitle
							.connect(STARTS of parentId with 72.dp)
							.connect(ENDS of parentId with 16.dp)
							.width(matchConstraint)

					arrayOf(txtDescription, txtSubtitle)
							.chainPacked(TOP of parentId with 8.dp, BOTTOM of parentId with 8.dp)

					imgIcon
							.connect(TOPS of parentId with 16.dp)
							.connect(STARTS of parentId with 24.dp)
							.size(24.dp, 24.dp)
				}
			}.lparams(matchParent, matchParent)

		}.also {
			it.layoutParams = ViewGroup.MarginLayoutParams(matchParent, wrapContent).apply {
				bottomMargin = 8.dp
			}
		}
	}
}

class ClickableItemUI(
		parent: ViewGroup,
		hint: String,
		iconResource: Int,
		private val onClickListener: () -> Unit
) : ItemUI(parent, hint, iconResource) {

	override fun createView(ui: AnkoContext<ViewGroup>): View {
		return super.createView(ui).also {
			constraint.apply {
				isClickable = true
				backgroundDrawable = getDrawableAttr(android.R.attr.selectableItemBackground)
				setOnClickListener {
					this@ClickableItemUI.onClickListener()
				}
			}
		}
	}
}

class EditableItemUI(
		parent: ViewGroup,
		hint: String,
		iconResource: Int,
		private val textChangeListener: (String) -> Unit
) : ItemUI(parent, hint, iconResource) {

	private var textChangingFromOutside = false

	override fun setText(text: String?) {
		textChangingFromOutside = true
		txtDescription.setText(text, TextView.BufferType.NORMAL)
		textChangingFromOutside = false
	}

	override fun _ConstraintLayout.titleSetup(): TextView {
		return editText {
			hint = this@EditableItemUI.hint
			textSize = 16f
			textColor = getColor(R.color.textColorPrimary)
			backgroundDrawable = null
			gravity = Gravity.CENTER_VERTICAL or Gravity.START
			leftPadding = 0
			inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
			addTextChangedListener(object : TextWatcher {
				override fun afterTextChanged(s: Editable?) {
					if (!textChangingFromOutside) {
						this@EditableItemUI.textChangeListener(s.toString())
					}
				}

				override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
				override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
			})
		}.lparams {
			constrainedHeight = true
		}
	}
}

class StaticItemUI(
		parent: ViewGroup,
		hint: String,
		iconResource: Int
) : ItemUI(parent, hint, iconResource) {
	// nothing
}


fun ViewGroup.clickableItem(title: String, iconResource: Int, onClick: () -> Unit): ItemUI {
	return ClickableItemUI(this, title, iconResource, onClick).apply {
		addView(view)
	}
}

fun ViewGroup.editableItem(title: String, iconResource: Int, onTextChange: (String) -> Unit): ItemUI {
	return EditableItemUI(this, title, iconResource, onTextChange).apply {
		addView(view)
	}
}

fun ViewGroup.staticItem(title: String, iconResource: Int): ItemUI {
	return StaticItemUI(this, title, iconResource).apply {
		addView(view)
	}
}
