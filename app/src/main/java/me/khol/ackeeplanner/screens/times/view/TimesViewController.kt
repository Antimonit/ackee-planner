package me.khol.ackeeplanner.screens.times.view

import com.airbnb.epoxy.EpoxyController
import me.khol.ackeeplanner.screens.times.OverlayEvent
import me.khol.ackeeplanner.screens.times.epoxy.header
import me.khol.ackeeplanner.screens.times.epoxy.selectTime
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneOffset

/**
 * Epoxy controller for peekable view at the bottom of the calendar
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 09.04.2018
 **/
open class TimesViewController(
		private val onTimeClick: (OverlayEvent) -> Unit
) : EpoxyController() {

	var events: Set<OverlayEvent> = emptySet()
		set(value) {
			field = value
			requestModelBuild()
		}

	override fun buildModels() {
		events.groupBy { LocalDateTime.ofInstant(it.start, ZoneOffset.UTC).toLocalDate() }
				.toSortedMap()
				.forEach { date: LocalDate, events: List<OverlayEvent> ->

					header {
						id("header", date.toEpochDay())
						date(date)
					}

					events.forEach { event ->
						selectTime {
							id(event.id)
							date(event)
							selected(false)
							onTimeClick(onTimeClick)
						}
					}
				}
	}

}
