package me.khol.ackeeplanner.screens.main

import me.khol.ackeeplanner.App
import me.khol.ackeeplanner.screens.base.viewmodel.ContextViewModel
import javax.inject.Inject

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 08.03.2018
 **/
class MainViewModel @Inject constructor(
		app: App
) : ContextViewModel(app)
