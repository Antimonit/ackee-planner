package me.khol.ackeeplanner.screens.calendar.epoxy

import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.squareup.picasso.Picasso
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.ackeeplanner.CircleTransform
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.model.AccountRepository
import me.khol.ackeeplanner.model.calendar.Calendar
import me.khol.ackeeplanner.model.calendar.CalendarAccount
import me.khol.ackeeplanner.screens.base.epoxy.BaseEpoxyModel
import me.khol.ackeeplanner.screens.base.layout.BaseLayout
import me.khol.ackeeplanner.screens.base.layout.labelTextView
import me.khol.ackeeplanner.screens.calendar.epoxy.AccountModel.Layout
import me.khol.extensions.dp
import me.khol.extensions.elevationCompat
import me.khol.extensions.isVisible
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView

/**
 * Simplified version of [me.khol.ackeeplanner.screens.main.accounts.epoxy.AccountModel]
 *
 * Displays information about a single account such as account [name][Layout.txtAccountLabel]
 * and [avatar][Layout.imgAccountAvatar] and current login [progress][Layout.progressLogIn].
 *
 * An account can be associated with multiple Calendars that are displayed
 * in [calendars][Layout.calendars] recycler view underneath the account information.
 * By clicking on a calendar view, the user is logged into associated account.
 *
 * This model is used in [ChooseCalendarUI][me.khol.ackeeplanner.screens.calendar.ChooseCalendarUI]'s
 * [recycler][me.khol.ackeeplanner.screens.calendar.ChooseCalendarUI.recycler] recycler view.
 *
 *
 * Warning: There is a similar model with the same name in the [me.khol.ackeeplanner.screens.times.calendars.epoxy]
 * package where user toggles visibility of calendars.
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 13.03.2018
 **/
open class AccountModel : BaseEpoxyModel<AccountModel.Layout>() {

	@EpoxyAttribute
	lateinit var account: CalendarAccount
	@EpoxyAttribute
	lateinit var signInState: AccountRepository.State
	@EpoxyAttribute
	var selectedCalendarId: String? = null
	@EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
	lateinit var onCalendarSelected: (Calendar) -> Unit

	override fun createViewLayout(parent: ViewGroup) = Layout(parent)

	override fun bind(layout: Layout) {
		super.bind(layout)
		layout.txtAccountLabel.text = account.name
		layout.calendarsController.calendars = account.calendars
		layout.calendarsController.selectedCalendarId = selectedCalendarId
		layout.onCalendarSelected = onCalendarSelected

		layout.txtLogIn.isVisible = false
		layout.progressLogIn.isVisible = false

		Picasso.get()
				.load("https://pikmail.herokuapp.com/${account.name}?size=${layout.context.dp(40)}")
				.placeholder(layout.placeholder)
				.transform(CircleTransform)
				.into(layout.imgAccountAvatar)

		val accountLoadingState = signInState
		when (accountLoadingState) {
			is AccountRepository.State.SignedOut -> {
				// do nothing
			}
			is AccountRepository.State.SigningIn -> {
				if (accountLoadingState.accountName == account.name) {
					layout.progressLogIn.isVisible = true
				}
			}
			is AccountRepository.State.SignedIn -> {
				if (accountLoadingState.account?.email == account.name) {
					layout.txtLogIn.isVisible = true
				}
			}
		}
	}

	@Suppress("MemberVisibilityCanBePrivate")
	class Layout(parent: ViewGroup) : BaseLayout(parent) {

		lateinit var txtAccountLabel: TextView
		lateinit var imgAccountAvatar: ImageView
		lateinit var calendars: RecyclerView
		lateinit var progressLogIn: ProgressBar
		lateinit var txtLogIn: TextView

		lateinit var onCalendarSelected: (Calendar) -> Unit

		var placeholder: Drawable = ContextCompat.getDrawable(context, R.drawable.ic_account_circle)!!
				.mutate()
				.apply { alpha = 20 }

		val calendarsController: CalendarsEpoxyController = CalendarsEpoxyController(
				onCalendarSelected = { calendar ->
					onCalendarSelected(calendar)
				}
		)

		override fun createView(ui: AnkoContext<ViewGroup>): View {
			return ui.verticalLayout {
				clipToPadding = false
				clipChildren = false

				constraintLayout {
					backgroundResource = R.color.white
					verticalPadding = 8.dp
					elevationCompat = 2.dpf
					clipToPadding = false
					clipChildren = false

					txtAccountLabel = labelTextView()

					imgAccountAvatar = imageView()

					calendars = recyclerView {
						layoutManager = LinearLayoutManager(ui.ctx, LinearLayoutManager.VERTICAL, false)
						itemAnimator = DefaultItemAnimator()
						addItemDecoration(HorizontalDividerItemDecoration.Builder(context)
								.margin(72.dp, 0.dp)
								.build())
						isNestedScrollingEnabled = false
						adapter = calendarsController.adapter
						clipChildren = false
					}

					progressLogIn = progressBar()

					txtLogIn = textView("Logged in")

					constraints {

						txtAccountLabel
								.connect(VERTICAL of imgAccountAvatar)
								.connect(STARTS of parentId with 72.dp)

						imgAccountAvatar
								.connect(TOPS of parentId with 4.dp)
								.connect(STARTS of parentId with 16.dp)
								.size(40.dp, 40.dp)

						calendars
								.connect(HORIZONTAL of parentId)
								.connect(TOPS of parentId with 48.dp)
								.size(matchConstraint, wrapContent)

						progressLogIn
								.connect(VERTICAL of imgAccountAvatar)
								.connect(ENDS of parentId with 16.dp)

						txtLogIn
								.connect(VERTICAL of imgAccountAvatar)
								.connect(ENDS of parentId with 16.dp)

					}
				}.lparams(matchParent, wrapContent)

			}.apply {
				layoutParams = ViewGroup.LayoutParams(org.jetbrains.anko.matchParent, org.jetbrains.anko.wrapContent)
			}
		}

	}

}
