package me.khol.ackeeplanner.screens.times.options

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import io.reactivex.disposables.CompositeDisposable
import me.khol.ackeeplanner.screens.times.create.TimesCreateActivity

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 16.04.2018
 **/
class OptionsDialogFragment: BottomSheetDialogFragment() {

	companion object {
		const val ARG_DURATION = "duration"

	    fun newInstance(duration: Int): OptionsDialogFragment {
			return OptionsDialogFragment().apply {
				arguments = Bundle().apply {
					putInt(ARG_DURATION, duration)
				}
			}
		}
	}

	private lateinit var layout: OptionsDialogUI

	val disposables = CompositeDisposable()

	override fun onDestroyView() {
		super.onDestroyView()
		disposables.clear()
	}

	@SuppressLint("RestrictedApi")
	override fun setupDialog(dialog: Dialog, style: Int) {
		super.setupDialog(dialog, style)

		layout = OptionsDialogUI(
				context!!,
				arguments!!.getInt(ARG_DURATION),
				onDurationSelected = { duration ->
					(activity as TimesCreateActivity).viewModel.setOverlaidDuration(duration)
				}
		)
		dialog.setContentView(layout.view)
	}

}
