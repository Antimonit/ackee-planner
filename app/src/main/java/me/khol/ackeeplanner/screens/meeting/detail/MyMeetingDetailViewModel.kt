package me.khol.ackeeplanner.screens.meeting.detail

import android.os.Bundle
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.rxkotlin.withLatestFrom
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import me.khol.ackeeplanner.App
import me.khol.ackeeplanner.Optional
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.extensions.observeOnMainThread
import me.khol.ackeeplanner.model.MeetingRepository
import me.khol.ackeeplanner.model.api.meeting.Meeting
import me.khol.ackeeplanner.model.contacts.Contact
import me.khol.ackeeplanner.screens.meeting.base.BaseMeetingViewModel
import javax.inject.Inject

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 10.03.2018
 **/
class MyMeetingDetailViewModel @Inject constructor(
		app: App,
		private val meetingRepository: MeetingRepository
) : BaseMeetingViewModel(app) {

	private sealed class MeetingState {
		class Invalid(val invalidFields: List<String>) : MeetingState()
		class Valid(val apiCall: Completable) : MeetingState()
	}

	private lateinit var meeting: Meeting

	private val titleSubject: Subject<String> = BehaviorSubject.createDefault("")
	private val locationSubject: Subject<String> = BehaviorSubject.createDefault("")
	private val timesSubject: Subject<List<Meeting.Date>> = BehaviorSubject.createDefault(listOf())
	private val ownerSubject: Subject<Optional<String>> = BehaviorSubject.createDefault(Optional.empty())
	private val linkUrlSubject: Subject<String> = BehaviorSubject.create()
	private val guestsSubject: Subject<List<Contact>> = BehaviorSubject.createDefault(listOf())
	private val continueClickSubject: Subject<Unit> = PublishSubject.create()

	fun observeTitle(): Observable<String> = titleSubject
	fun observeLocation(): Observable<String> = locationSubject
	fun observeTimes(): Observable<List<Meeting.Date>> = timesSubject
	fun observeOwner(): Observable<Optional<String>> = ownerSubject
	fun observeLinkUrl(): Observable<String> = linkUrlSubject
	fun observeGuests(): Observable<List<Contact>> = guestsSubject

	fun setArguments(arguments: Bundle) {
		this.meeting = arguments.getParcelable(MyMeetingDetailFragment.ARG_MEETING)
		titleSubject.onNext(meeting.description)
		locationSubject.onNext(meeting.location)
		ownerSubject.onNext(Optional.ofNullable(meeting.owner?.email))
		timesSubject.onNext(meeting.dates)
		linkUrlSubject.onNext(ctx.getString(R.string.ackee_planner_url, meeting.id, meeting.token))
	}
	fun setGuests(guests: List<Contact>) = guestsSubject.onNext(guests)
	fun onContinue() = continueClickSubject.onNext(Unit)

	init {
		val meetingStateObservable = guestsSubject
				.map { guests ->
					val invalidFields = mutableListOf<String>()
					if (guests.isEmpty()) invalidFields += ctx.getString(R.string.error_empty_guests)

					if (invalidFields.isNotEmpty()) {
						MeetingState.Invalid(invalidFields)
					} else {
						MeetingState.Valid(
								meetingRepository.sendInvitations(
										meeting.token,
										meeting.id,
										guests.map { it.email }
								)
						)
					}
				}

		continueClickSubject.withLatestFrom(meetingStateObservable) { _, apiCall -> apiCall }
				.doOnNext {
					stateSubject.onNext(State.Loading())
				}
				.flatMapSingle { state ->
					when (state) {
						is MeetingState.Valid -> {
							state.apiCall
									.toSingle<State> {
										State.Success()
									}
									.onErrorReturn { t ->
										State.NetworkError(t)
									}
						}
						is MeetingState.Invalid -> {
							Single.just(State.Invalid(state.invalidFields))
						}
					}
				}
				.observeOnMainThread()
				.subscribe({ state ->
					stateSubject.onNext(state)
				})
	}

}
