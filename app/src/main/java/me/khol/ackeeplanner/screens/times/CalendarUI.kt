package me.khol.ackeeplanner.screens.times

import android.content.Context
import android.support.design.widget.AppBarLayout
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.ViewManager
import android.widget.Button
import android.widget.TextView
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.screens.base.layout.BaseLayout
import me.khol.calendar.CalendarListener
import me.khol.calendar.CalendarView
import me.khol.extensions.beginDelayedTransition
import me.khol.extensions.elevationCompat
import me.khol.extensions.getColor
import me.khol.extensions.getDrawableAttr
import me.khol.extensions.isVisible
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.toolbar
import org.jetbrains.anko.custom.ankoView
import org.jetbrains.anko.design.appBarLayout
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.swipeRefreshLayout

/**
 * A layout containing a [CalendarView] with a [Toolbar]
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 15.11.2017
 **/
class CalendarUI(
		parent: ViewGroup,
		private val adapter: RecyclerView.Adapter<*>,
		private val calendarListener: CalendarListener,
		private val onDoneClick: () -> Unit = {}
) : BaseLayout(parent) {

	private inline fun ViewManager.calendarView(init: (@AnkoViewDslMarker CalendarView).() -> Unit = {}): CalendarView {
		return ankoView({ ctx: Context -> CalendarView(ctx) }, theme = 0) { init() }
	}

	private lateinit var txtBottomTitle: TextView
	private lateinit var txtBottomSubtitle: TextView
	private lateinit var btnDone: Button
	private lateinit var bottomLayout: ViewGroup

	lateinit var toolbar: Toolbar
	lateinit var calendar: CalendarView
	lateinit var timesRecycler: RecyclerView
	lateinit var swipeRefreshLayout: SwipeRefreshLayout

	fun showProgress(show: Boolean) {
		bottomLayout.beginDelayedTransition()
		swipeRefreshLayout.isRefreshing = show
	}

	fun showDoneButton(show: Boolean) {
		bottomLayout.beginDelayedTransition()
		btnDone.isVisible = show
	}

	fun setBottomTitle(text: String?) {
		bottomLayout.beginDelayedTransition()
		txtBottomTitle.text = text
	}

	fun setBottomSubtitle(text: String?) {
		bottomLayout.beginDelayedTransition()
		txtBottomSubtitle.isVisible = text != null
		txtBottomSubtitle.text = text
	}

	override fun createView(ui: AnkoContext<ViewGroup>): View {
		return ui.coordinatorLayout {

			appBarLayout {
				backgroundResource = R.color.white

				toolbar = toolbar {
					fitsSystemWindows = true
					title = "Times"
				}.lparams(matchParent, wrapContent) {
					scrollFlags = 0
				}
			}.lparams(matchParent, wrapContent)

			swipeRefreshLayout = swipeRefreshLayout {
				isEnabled = false
				setColorSchemeResources(R.color.colorPrimary, R.color.colorAccent)

				frameLayout {

					timesRecycler = recyclerView {
						backgroundColor = getColor(R.color.black).withAlpha(10)
						adapter = this@CalendarUI.adapter
						layoutManager = LinearLayoutManager(context)
						itemAnimator = DefaultItemAnimator()
					}.lparams(matchParent, matchParent) {
					}

					calendar = calendarView {
						backgroundColor = getColor(R.color.black).withAlpha(10)
						calendarListener = this@CalendarUI.calendarListener
					}.lparams(matchParent, matchParent) {
					}
				}

			}.lparams(matchParent, matchParent) {
				bottomMargin = 64.dp
				behavior = AppBarLayout.ScrollingViewBehavior()
			}

			bottomLayout = constraintLayout {
				backgroundResource = R.color.white
				elevationCompat = 4.dpf

				txtBottomTitle = textView {
					textSize = 16f
					textColorResource = R.color.textColorPrimary
				}

				txtBottomSubtitle = textView {
					textSize = 14f
					textColorResource = R.color.textColorSecondary
				}

				val separator = view {
					backgroundColorResource = R.color.black_20
				}

				btnDone = button("Done") {
					backgroundDrawable = getDrawableAttr(android.R.attr.selectableItemBackground)
					isVisible = false
					lines = 1
					setOnClickListener {
						onDoneClick()
					}
				}

				constraints {
					txtBottomTitle
							.connect(STARTS of parentId with 16.dp)
							.connect(END to START of btnDone with 16.dp)
							.width(matchConstraint)

					txtBottomSubtitle
							.connect(STARTS of txtBottomTitle)
							.connect(END to START of btnDone with 16.dp)
							.width(matchConstraint)

					arrayOf(txtBottomTitle, txtBottomSubtitle).chainPacked(
							TOP of parentId,
							BOTTOM of parentId
					)

					separator
							.connect(STARTS of btnDone)
							.connect(VERTICAL of parentId with 8.dp)
							.size(1.dp, matchConstraint)

					btnDone
							.connect(ENDS of parentId)
							.connect(VERTICAL of parentId)
							.size(96.dp, matchConstraint)
				}
			}.lparams(matchParent, 64.dp) {
				gravity = Gravity.BOTTOM
			}

		}.apply {
			layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)
		}
	}
}
