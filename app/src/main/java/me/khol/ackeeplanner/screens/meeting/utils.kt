package me.khol.ackeeplanner.screens.meeting

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 01.05.2018
 **/

fun truncateMoreThanElements(list: List<Any>, maxElements: Int): String {
	return StringBuilder().apply {
		append(list.take(maxElements).joinToString("\n") { it.toString() })
		if (list.size > maxElements) {
			appendln()
			append("+${list.size - maxElements} more")
		}
	}.toString()
}
