package me.khol.ackeeplanner.screens.guests

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.view.*
import com.airbnb.epoxy.EpoxyTouchHelper
import com.jakewharton.rxbinding2.widget.textChanges
import io.reactivex.rxkotlin.plusAssign
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.extensions.*
import me.khol.ackeeplanner.model.contacts.Contact
import me.khol.ackeeplanner.screens.base.activity.BaseActivity
import me.khol.ackeeplanner.screens.guests.epoxy.EmailModel
import me.khol.ackeeplanner.screens.guests.epoxy.GuestsEpoxyController
import org.jetbrains.anko.appcompat.v7.navigationIconResource
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 11.03.2018
 **/
class GuestsActivity : BaseActivity() {

	companion object {
		private const val READ_CONTACTS_RC = 1
		private const val PERMISSION_READ_CONTACTS = Manifest.permission.READ_CONTACTS
		const val ARG_GUESTS = "guests"
		const val RESULT_GUESTS = "guests"
	}

	private lateinit var layout: GuestsUI
	private lateinit var viewModel: GuestsViewModel
	private lateinit var controller: GuestsEpoxyController

	private lateinit var doneItem: MenuItem
	private lateinit var addItem: MenuItem

	private var originalUpButton: Drawable? = null
	private var isInputMode: Boolean = false
		set(value) {
			if (field == value)
				return

			field = value
			if (field) {
				originalUpButton = layout.toolbar.navigationIcon
				layout.toolbar.navigationIconResource = R.drawable.ic_close
			} else {
				layout.toolbar.navigationIcon = originalUpButton
			}
		}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		viewModel = getViewModel(vmFactory)

		controller = GuestsEpoxyController(
				this,
				onEmailAdded = { personEmail ->
					layout.clearSearchQuery()
					viewModel.addSelectedContact(personEmail)
				},
				onEmailRemoved = { personEmail ->
					viewModel.removeSelectedContact(personEmail)
				},
				onPermissionClicked = {
					ActivityCompat.requestPermissions(this, arrayOf(PERMISSION_READ_CONTACTS), READ_CONTACTS_RC)
				},
				onDeleteRecentContactsClicked = {
					viewModel.removeAllRecentContacts()
				},
				avatarLoader = viewModel.avatarLoader
		)

		layout = GuestsUI(rootView, controller.adapter)
		setContentView(layout.view)

		EpoxyTouchHelper.initSwiping(layout.contacts)
				.leftAndRight()
				.withTarget(EmailModel::class.java)
				.andCallbacks(object : EpoxyTouchHelper.SwipeCallbacks<EmailModel>() {
					override fun isSwipeEnabledForModel(model: EmailModel): Boolean {
						return controller.query.isEmpty()
					}

					override fun onSwipeCompleted(model: EmailModel, itemView: View, position: Int, direction: Int) {
						viewModel.removeRecentContact(model.account)
					}
				})

		refreshContacts()

		val rawContactIds: List<Contact> = intent.extras?.getParcelableArrayList(ARG_GUESTS)
				?: emptyList()
		viewModel.setSelectedContactIds(rawContactIds)

		disposables += layout.searchEdit.textChanges()
				.skipInitialValue()
				.doOnNext { query ->
					doneItem.isVisible = query.isEmpty()
					addItem.isVisible = query.isNotEmpty()

					isInputMode = query.isNotEmpty()
				}
				.subscribeOnNewThread()
				.subscribe { query ->
					controller.query = query.toString()
					if (hasPermissions(PERMISSION_READ_CONTACTS)) {
						viewModel.fetchContacts(query)
					}
				}

		disposables += viewModel.observeContacts()
				.observeOnMainThread()
				.subscribe { guests ->
					controller.queriedGuests = guests
					layout.contacts.layoutManager.scrollToPosition(0)
				}

		disposables += viewModel.observeSelectedContacts()
				.observeOnMainThread()
				.subscribe { guests ->
					controller.selectedGuests = guests
					layout.contacts.layoutManager.scrollToPosition(0)
				}

		disposables += viewModel.observeRecentContacts()
				.observeOnMainThread()
				.subscribe { guests ->
					controller.recentGuests = guests
					layout.contacts.layoutManager.scrollToPosition(0)
				}

		setSupportActionBar(layout.toolbar)
		supportActionBar?.apply {
			setDisplayHomeAsUpEnabled(true)
			setDisplayShowTitleEnabled(false)
		}
	}

	override fun onNavigateUp(): Boolean {
		return if (isInputMode) {
			layout.clearSearchQuery()
			false
		} else {
			super.onNavigateUp()
		}
	}

	override fun onBackPressed() {
		if (isInputMode) {
			layout.clearSearchQuery()
		} else {
			super.onBackPressed()
		}
	}

	override fun onCreateOptionsMenu(menu: Menu): Boolean {
		menuInflater.inflate(R.menu.guests, menu)
		doneItem = menu.findItem(R.id.action_done)
		addItem = menu.findItem(R.id.action_add)
		return true
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		when (item.itemId) {
			R.id.action_done -> {
				setResult(Activity.RESULT_OK, Intent().apply {
					putExtras(Bundle().apply {
						val guests = viewModel.getSelectedContacts()
						viewModel.addRecentContacts(guests)
						putParcelableArrayList(RESULT_GUESTS, ArrayList(guests))
					})
				})
				finish()
			}
			R.id.action_add -> {
				val email = layout.getSearchQuery()
				val personEmail = Contact(null, null, email, null)
				layout.clearSearchQuery()
				viewModel.addSelectedContact(personEmail)
			}
		}
		return super.onOptionsItemSelected(item)
	}


	@AfterPermissionGranted(READ_CONTACTS_RC)
	private fun refreshContacts() {
		if (hasPermissions(PERMISSION_READ_CONTACTS)) {
			controller.hasPermission = true
			// Note: code that requires the permission is already guarded by another hasPermissions check
		} else {
			controller.hasPermission = false

			if (!ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSION_READ_CONTACTS)) {
				ActivityCompat.requestPermissions(this, arrayOf(PERMISSION_READ_CONTACTS), READ_CONTACTS_RC)
			}
		}
	}

	override fun onRequestPermissionsResult(requestCode: Int,
											permissions: Array<String>,
											grantResults: IntArray) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults)
		EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
	}

}
