package me.khol.ackeeplanner.screens.meeting.create.step2

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.rxkotlin.plusAssign
import me.khol.ackeeplanner.extensions.getViewModel
import me.khol.ackeeplanner.extensions.observeOnMainThread
import me.khol.ackeeplanner.model.contacts.Contact
import me.khol.ackeeplanner.screens.guests.GuestsActivity
import me.khol.ackeeplanner.screens.meeting.base.BaseMeetingFragment
import me.khol.ackeeplanner.screens.meeting.truncateMoreThanElements

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 11.03.2018
 **/
class CreateMeeting2Fragment : BaseMeetingFragment<CreateMeeting2UI, CreateMeeting2ViewModel>() {

	companion object {
		const val ARG_MEETING = "meeting"
	}

	override lateinit var layout: CreateMeeting2UI
	override lateinit var viewModel: CreateMeeting2ViewModel

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		viewModel = getViewModel(vmFactory)
		viewModel.setArguments(arguments!!)
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
		layout = CreateMeeting2UI(
				container!!,
				onGuestsClick = { openGuestsActivity(viewModel.observeGuests().firstOrError().blockingGet()) },
				onContinueClick = { viewModel.onContinue() },
				onShareClick = { shareText(viewModel.observeLinkUrl().blockingFirst()) }
		)
		layout.view

		initializeStateObservable()

		disposables += viewModel.observeTitle()
				.observeOnMainThread()
				.subscribe { title: String ->
					layout.txtTitle.text = title
				}

		disposables += viewModel.observeLinkUrl()
				.observeOnMainThread()
				.subscribe { linkUrl: String ->
					layout.link.setText(linkUrl)
				}

		disposables += viewModel.observeGuests()
				.observeOnMainThread()
				.subscribe { guests ->
					val mappedGuests = guests.map {
						if (it.name != null && it.name.isNotBlank()) {
							"${it.name} (${it.email})"
						} else {
							it.email
						}
					}
					layout.guests.setText(truncateMoreThanElements(mappedGuests, 5))
				}

		activity.setSupportActionBar(layout.toolbar)
		activity.supportActionBar?.apply {
			setDisplayHomeAsUpEnabled(true)
		}

		return layout.view
	}

	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		super.onActivityResult(requestCode, resultCode, data)
		if (resultCode == RESULT_OK) {
			when (requestCode) {
				RC_GUESTS -> {
					val guests: List<Contact> = data?.extras?.getParcelableArrayList(GuestsActivity.RESULT_GUESTS)
							?: emptyList()
					viewModel.setGuests(guests)
				}
			}
		}
	}

}
