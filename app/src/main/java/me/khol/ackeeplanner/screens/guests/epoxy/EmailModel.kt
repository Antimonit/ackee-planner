package me.khol.ackeeplanner.screens.guests.epoxy

import android.graphics.Color
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import com.airbnb.epoxy.EpoxyAttribute
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.extensions.font
import me.khol.ackeeplanner.model.contacts.Contact
import me.khol.ackeeplanner.screens.base.layout.defaultTextView
import me.khol.extensions.elevationCompat
import me.khol.extensions.getColor
import me.khol.extensions.getDrawableAttr
import me.khol.extensions.isVisible
import org.jetbrains.anko.*


/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 29.03.2018
 **/
open class EmailModel : BaseEmailModel<EmailModel.Layout>() {

	@EpoxyAttribute
	lateinit var query: String
	@EpoxyAttribute
	var enabled: Boolean = true
	@EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
	lateinit var onEmailAdded: (Contact) -> Unit


	private val boldStyleSpan = android.text.style.StyleSpan(android.graphics.Typeface.BOLD)

	private fun highlightSubstring(text: CharSequence, query: CharSequence): Spannable {
		val spannable = SpannableStringBuilder(text)
		val start = spannable.indexOf(query.toString(), ignoreCase = true)
		val end = start + query.length
		if (start != -1) {
			spannable.setSpan(boldStyleSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
		}
		return spannable
	}

	override fun createViewLayout(parent: ViewGroup) = Layout(parent)

	override fun bind(layout: Layout) {
		super.bind(layout)
		val name = account.name
		if (name == null) {
			layout.txtSubtitle.isVisible = false
			layout.txtTitle.text = highlightSubstring(account.email, query)
		} else {
			layout.txtSubtitle.isVisible = true
			layout.txtTitle.text = highlightSubstring(name, query)
			layout.txtSubtitle.text = highlightSubstring(account.email, query)
		}

		layout.view.isClickable = enabled
		if (enabled) {
			layout.view.setOnClickListener {
				onEmailAdded(account)
			}
			layout.txtTitle.textColor = Color.BLACK
			layout.txtSubtitle.textColor = Color.BLACK
			layout.imgAccountAvatar.alpha = 1.0f
		} else {
			layout.txtTitle.textColor = Color.GRAY
			layout.txtSubtitle.textColor = Color.GRAY
			layout.imgAccountAvatar.alpha = 0.3f
		}
	}

	override fun unbind(layout: Layout) {
		super.unbind(layout)
		layout.view.setOnClickListener(null)
	}

	class Layout(parent: ViewGroup) : BaseEmailModel.Layout(parent) {

		override fun createView(ui: AnkoContext<ViewGroup>): View {
			return ui.frameLayout {
				backgroundResource = R.color.white
				elevationCompat = 2.dpf
				foreground = getDrawableAttr(android.R.attr.selectableItemBackground)

				constraintLayout {

					txtTitle = defaultTextView {
						ellipsize = TextUtils.TruncateAt.END
						textColor = getColor(R.color.textColorPrimary)
						textSize = 16f
					}

					txtSubtitle = defaultTextView {
						ellipsize = TextUtils.TruncateAt.END
						textColor = getColor(R.color.textColorSecondary)
						textSize = 14f
						font = R.font.roboto_light
					}

					imgAccountAvatar = imageView()

					constraints {
						imgAccountAvatar
								.connect(VERTICAL of parentId)
								.connect(STARTS of parentId with 16.dp)
								.size(40.dp, 40.dp)

						txtTitle
								.connect(STARTS of parentId with 72.dp)
								.connect(ENDS of parentId with 16.dp)
								.size(matchConstraint, org.jetbrains.anko.wrapContent)

						txtSubtitle
								.connect(HORIZONTAL of txtTitle)
								.size(matchConstraint, org.jetbrains.anko.wrapContent)

						arrayOf(txtTitle, txtSubtitle).chainPacked(
								TOP of parentId,
								BOTTOM of parentId
						)
					}
				}.lparams(matchParent, matchParent)

			}.apply {
				layoutParams = ViewGroup.LayoutParams(org.jetbrains.anko.matchParent, 56.dp)
			}
		}
	}
}
