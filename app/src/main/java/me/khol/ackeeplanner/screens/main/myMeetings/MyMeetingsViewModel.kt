package me.khol.ackeeplanner.screens.main.myMeetings

import me.khol.ackeeplanner.App
import me.khol.ackeeplanner.model.MeetingRepository
import me.khol.ackeeplanner.model.api.meeting.Meeting
import me.khol.ackeeplanner.model.calendar.LocalCalendarRepository
import me.khol.ackeeplanner.model.db.dao.UserDao
import me.khol.ackeeplanner.screens.main.base.MeetingViewModel
import javax.inject.Inject

/**
 * Created by David Khol [david@khol.me] on 9. 3. 2018.
 */
class MyMeetingsViewModel @Inject constructor(
		app: App,
		userDao: UserDao,
		calendarRepository: LocalCalendarRepository,
		private val meetingRepository: MeetingRepository
) : MeetingViewModel(app, calendarRepository, userDao, meetingRepository) {

	override fun filterMeetings(userIds: List<Int>, meetings: List<Meeting>): List<Meeting> {
		return meetings.filter { meeting ->
			userIds.contains(meeting.ownerId)
		}
	}

}
