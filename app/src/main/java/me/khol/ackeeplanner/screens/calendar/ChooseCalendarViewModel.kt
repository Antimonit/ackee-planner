package me.khol.ackeeplanner.screens.calendar

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.provider.CalendarContract
import android.support.annotation.RequiresPermission
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import me.khol.ackeeplanner.App
import me.khol.ackeeplanner.extensions.observeOnMainThread
import me.khol.ackeeplanner.model.AccountRepository
import me.khol.ackeeplanner.model.calendar.Calendar
import me.khol.ackeeplanner.model.calendar.CalendarAccount
import me.khol.ackeeplanner.model.calendar.LocalCalendarRepository
import me.khol.ackeeplanner.screens.base.viewmodel.ContextViewModel
import me.khol.ackeeplanner.screens.calendar.ChooseCalendarActivity.Companion.ARG_CALENDAR
import javax.inject.Inject

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 10.03.2018
 **/
class ChooseCalendarViewModel @Inject constructor(
		app: App,
		private val calendarRepository: LocalCalendarRepository,
		private val accountRepository: AccountRepository
) : ContextViewModel(app) {

	sealed class State {
		class Loaded(val accounts: List<CalendarAccount>, val selectedCalendar: SimplifiedCalendar?) : State()
		class Error(val throwable: Throwable) : State()
	}

	private val stateSubject: Subject<State> = BehaviorSubject.create()
	private val selectedCalendarSubject: Subject<SimplifiedCalendar> = BehaviorSubject.create()
	private val finishSubject: Subject<SimplifiedCalendar> = BehaviorSubject.create()

	private var originalCalendar: SimplifiedCalendar? = null

	/**
	 * Important: Set arguments before starting to [observeState]
	 */
	fun setArguments(extras: Bundle?) {
		val calendar: SimplifiedCalendar? = extras?.getParcelable(ARG_CALENDAR)
		originalCalendar = calendar
	}

	init {
		disposables += Observable.combineLatest(
				accountRepository.observeLoginState(),
				selectedCalendarSubject,
				BiFunction { state: AccountRepository.State, selectedCalendar: SimplifiedCalendar ->
					state to selectedCalendar
				})
				.observeOnMainThread()
				.subscribe { (state, selectedCalendar) ->
					if (state is AccountRepository.State.SignedIn) {
						if ((state.user.isAnonymous && selectedCalendar.calendarId == null && state.account?.email == null) ||
							(!state.user.isAnonymous && selectedCalendar.calendarId != null && state.account?.email == selectedCalendar.email)) {
							finishSubject.onNext(selectedCalendar)
							finishSubject.onComplete()
						}
					}
				}
	}

	fun observeLoginState(): Observable<AccountRepository.State> = accountRepository.observeLoginState()
	fun observeFinish(): Observable<SimplifiedCalendar> = finishSubject

	/**
	 * Important: [setArguments] before starting to observe state
	 */
	@SuppressLint("MissingPermission")
	@RequiresPermission(Manifest.permission.READ_CALENDAR)
	fun observeState(): Observable<State> {
		// Reduce all accounts (including the special "Other calendars" account) with all calendars
		// to only real accounts (no special account) with only those calendars that the user has
		// permission to write into
		calendarRepository.observeCalendarAccounts()
				.map { accounts ->
					accounts
							.filter { it.isReal }
							.map {
								val editableCalendars = it.calendars.flatMap {
									/**
									 * 500 is the minimal access control number for which
									 * the user can modify the calendar
									 *
									 * @see CalendarContract.CalendarColumns.CAL_ACCESS_CONTRIBUTOR
									 */
									val editableAccessLevel = 500
									if (it.accessLevel >= editableAccessLevel) {
										listOf(it)
									} else {
										emptyList()
									}
								}
								it.copy(calendars = editableCalendars, isReal = false)
							}
							.filter {
								it.calendars.isNotEmpty()
							}
				}
				.subscribe({ accounts ->
					stateSubject.onNext(State.Loaded(accounts, originalCalendar))
				}, {
					stateSubject.onNext(State.Error(it))
				})

		return stateSubject
	}

	fun observeSignInIntent(): Observable<Intent> = accountRepository.observeSignInIntent()

	fun onSignInResult(data: Intent?) = accountRepository.onSignInResult(data)

	fun setAnonymousCalendar(name: String, email: String) {
		val currentSignInState = accountRepository.observeLoginState().blockingFirst()
		when (currentSignInState) {
			is AccountRepository.State.Error -> {
				accountRepository.doAnonymousLogin()
			}
			is AccountRepository.State.SignedOut -> {
				accountRepository.doAnonymousLogin()
			}
			is AccountRepository.State.SigningIn -> {
				// we are already signing in with some account, don't try to log in again
			}
			is AccountRepository.State.SignedIn -> {
				// sign the user out if he is logged in with google account and log in anonymously
				if (!currentSignInState.user.isAnonymous) {
					accountRepository.doAnonymousLogin()
				}
			}
		}

		val anonymousCalendar = SimplifiedCalendar(null, name, email, null)
		selectedCalendarSubject.onNext(anonymousCalendar)
	}

	fun setSelectedCalendar(calendar: Calendar) {
		val accountName = calendar.accountName
		val currentSignInState = accountRepository.observeLoginState().blockingFirst()
		when (currentSignInState) {
			is AccountRepository.State.Error -> {
				accountRepository.doLogIn(accountName)
			}
			is AccountRepository.State.SignedOut -> {
				accountRepository.doLogIn(accountName)
			}
			is AccountRepository.State.SigningIn -> {
				// we are already signing in with some account, don't try to log in again
			}
			is AccountRepository.State.SignedIn -> {
				// sign the user out if he is logged in with different google account
				if (currentSignInState.user.isAnonymous || currentSignInState.account?.email != accountName) {
					accountRepository.doLogIn(accountName)
				}
			}
		}

		val googleCalendar = SimplifiedCalendar(calendar.ownerAccount, null, calendar.accountName, calendar.displayName)
		selectedCalendarSubject.onNext(googleCalendar)
	}

}
