package me.khol.ackeeplanner.screens.base.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.widget.FrameLayout
import me.khol.ackeeplanner.R
import org.jetbrains.anko.matchParent

/**
 * Created by David Khol [david@khol.me] on 10. 3. 2018.
 */
open class FragmentActivity : BaseActivity() {

	companion object {
		internal const val KEY_EXTRA_FRAGMENT_NAME = "fragment_name"
		internal const val KEY_EXTRA_FRAGMENT_ARGUMENTS = "fragment_arguments"
	}

	lateinit var layout: FragmentActivityUI

	/**
	 * Returns the name of the fragment to be instantiated.
	 *
	 * Note: If you will inherit from FragmentActivity and do not provide fragment Name via intent or
	 * with overriding this attribute, exception will be thrown
	 */
	open val fragmentName: String?
		get() = intent?.getStringExtra(KEY_EXTRA_FRAGMENT_NAME)

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		layout = FragmentActivityUI(rootView)
		setContentView(layout.view, FrameLayout.LayoutParams(matchParent, matchParent))

		val fragmentName = fragmentName
				?: throw IllegalStateException("Fragment name must be provided")
		val args = intent?.getBundleExtra(KEY_EXTRA_FRAGMENT_ARGUMENTS)

		var fragment: Fragment? = supportFragmentManager.findFragmentByTag(fragmentName)
		if (fragment == null && savedInstanceState == null) {
			fragment = Fragment.instantiate(this, fragmentName, args)

			supportFragmentManager
					.beginTransaction()
					.add(R.id.fragment_container, fragment, fragment.javaClass.name)
					.commit()
		}
	}

}

/**
 * Extension to Activity that starts activity that is child of FragmentActivity and set activity properties
 */
internal inline fun <reified T : FragmentActivity> Context.startFragmentActivity(
		fragmentName: String? = null,
		fragmentArgs: Bundle? = null,
		activityBundle: Bundle? = null) {
	startActivity(getFragmentActivityIntent<T>(this, fragmentName, fragmentArgs, activityBundle))
}

internal inline fun <reified T : FragmentActivity> Fragment.startFragmentActivityForResult(
		fragmentName: String? = null,
		fragmentArgs: Bundle? = null,
		activityBundle: Bundle? = null,
		requestCode: Int) {
	val context = context
	if (context == null) {
		throw IllegalStateException("Cannot start an activity with null context.")
	} else {
		startActivityForResult(getFragmentActivityIntent<T>(context, fragmentName, fragmentArgs, activityBundle), requestCode)
	}
}

internal inline fun <reified T : FragmentActivity> Activity.startFragmentActivityForResult(
		fragmentName: String? = null,
		fragmentArgs: Bundle? = null,
		activityBundle: Bundle? = null,
		requestCode: Int) {
	startActivityForResult(getFragmentActivityIntent<T>(this, fragmentName, fragmentArgs, activityBundle), requestCode)
}

/**
 * Get intent with properties for starting of FragmentActivity
 */
internal inline fun <reified T : FragmentActivity> getFragmentActivityIntent(
		ctx: Context,
		fragmentName: String? = null,
		fragmentArgs: Bundle? = null,
		activityBundle: Bundle? = null
): Intent {
	val intent = Intent(ctx, T::class.java)
	intent.putExtra(FragmentActivity.KEY_EXTRA_FRAGMENT_NAME, fragmentName)
	intent.putExtra(FragmentActivity.KEY_EXTRA_FRAGMENT_ARGUMENTS, fragmentArgs)
	if (activityBundle != null) {
		intent.putExtras(activityBundle)
	}
	return intent
}
