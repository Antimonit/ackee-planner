package me.khol.ackeeplanner.screens.meeting

import android.support.v4.view.ViewCompat
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.screens.base.layout.BaseLayout
import me.khol.ackeeplanner.screens.meeting.ProgressButtonUI.State
import me.khol.extensions.getColorStateList
import me.khol.extensions.isVisible
import org.jetbrains.anko.*

/**
 * We can choose from three [State]s.
 * [State.NORMAL] displays a button,
 * [State.LOADING] displays a progress bar in its place and
 * [State.FINISHED] displays a static text.
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 11.04.2018
 **/
class ProgressButtonUI(
		parent: ViewGroup,
		private val buttonText: Int,
		private val finishedText: Int,
		private val onContinueClick: () -> Unit
) : BaseLayout(parent) {

	enum class State {
		NORMAL,
		LOADING,
		FINISHED
	}

	fun setButtonText(text: String) {
		button.text = text
	}
	fun setFinishedText(text: String) {
		finished.text = text
	}

	fun setState(state: State) {
		button.isVisible = false
		progress.isVisible = false
		finished.isVisible = false
		when (state) {
			State.NORMAL -> button.isVisible = true
			State.LOADING -> progress.isVisible = true
			State.FINISHED -> finished.isVisible = true
		}
	}

	private lateinit var button: Button
	private lateinit var progress: ProgressBar
	private lateinit var finished: TextView

	override fun createView(ui: AnkoContext<ViewGroup>): View {
		return ui.frameLayout {
			padding = 16.dp
			clipChildren = false

			button = button(buttonText) {
				ViewCompat.setBackgroundTintList(this, getColorStateList(R.color.white))
				setOnClickListener {
					onContinueClick()
				}
			}.lparams(matchParent, matchParent)

			progress = progressBar {
				isIndeterminate = true
			}.lparams(wrapContent, wrapContent) {
				gravity = Gravity.CENTER
			}

			finished = textView(finishedText) {
			}.lparams(wrapContent, wrapContent) {
				gravity = Gravity.CENTER
			}

			layoutParams = ViewGroup.LayoutParams(matchParent, 80.dp)
		}
	}

}
