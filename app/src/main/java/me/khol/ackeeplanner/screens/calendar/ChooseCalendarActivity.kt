package me.khol.ackeeplanner.screens.calendar

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import com.google.android.gms.common.api.ApiException
import io.reactivex.rxkotlin.plusAssign
import me.khol.ackeeplanner.api.ApiExceptionHandler
import me.khol.ackeeplanner.extensions.getViewModel
import me.khol.ackeeplanner.extensions.hasPermissions
import me.khol.ackeeplanner.extensions.observeOnMainThread
import me.khol.ackeeplanner.extensions.showSnack
import me.khol.ackeeplanner.model.AccountRepository
import me.khol.ackeeplanner.screens.base.activity.BaseActivity
import me.khol.ackeeplanner.screens.calendar.epoxy.ChooseCalendarController
import me.khol.ackeeplanner.screens.meeting.ProgressButtonUI
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 07.04.2018
 **/
class ChooseCalendarActivity : BaseActivity() {

	companion object {
		private const val REQUEST_PERMISSION_CALENDAR = 2
		private const val PERMISSION_READ_CALENDAR = Manifest.permission.READ_CALENDAR
		private const val PERMISSION_WRITE_CALENDAR = Manifest.permission.WRITE_CALENDAR

		private const val RC_SIGN_IN = 3
		const val RESULT_CALENDAR = "calendar"
		const val ARG_CALENDAR = "calendar"
		const val ARG_ANONYMOUS_ALLOWED = "anonymous_allowed"
	}

	private lateinit var layout: ChooseCalendarUI
	private lateinit var viewModel: ChooseCalendarViewModel
	private lateinit var controller: ChooseCalendarController

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		viewModel = getViewModel(vmFactory)
		viewModel.setArguments(intent.extras)

		val anonymousAllowed: Boolean? = intent.extras?.getBoolean(ARG_ANONYMOUS_ALLOWED)

		controller = ChooseCalendarController(
				this,
				onCalendarSelected = { calendar ->
					viewModel.setSelectedCalendar(calendar)
				},
				onPermissionClicked = {
					ActivityCompat.requestPermissions(this, arrayOf(PERMISSION_READ_CALENDAR, PERMISSION_WRITE_CALENDAR), REQUEST_PERMISSION_CALENDAR)
				}
		)

		disposables += viewModel.observeSignInIntent()
				.subscribe { signInIntent ->
					startActivityForResult(signInIntent, RC_SIGN_IN)
				}

		disposables += viewModel.observeLoginState()
				.observeOnMainThread()
				.subscribe { loginState ->
					controller.signInState = loginState
					when (loginState) {
						is AccountRepository.State.SignedOut -> layout.next.setState(ProgressButtonUI.State.NORMAL)
						is AccountRepository.State.SigningIn -> layout.next.setState(ProgressButtonUI.State.LOADING)
						is AccountRepository.State.SignedIn -> layout.next.setState(ProgressButtonUI.State.NORMAL)
						is AccountRepository.State.Error -> {
							layout.next.setState(ProgressButtonUI.State.NORMAL)
							if (loginState.throwable is ApiException) {
								layout.view.showSnack(ApiExceptionHandler.resolve(loginState.throwable))
							} else {
								layout.view.showSnack(loginState.throwable.toString())
							}
						}
					}
				}

		disposables += viewModel.observeFinish()
				.observeOnMainThread()
				.subscribe { calendar ->
					finishWithResult(calendar)
				}

		observeState()

		layout = ChooseCalendarUI(
				rootView,
				controller.adapter,
				anonymousAllowed ?: false,
				onAnonymousAccountClicked = { name, email ->
					viewModel.setAnonymousCalendar(name, email)
				}
		)
		setContentView(layout.view)

		setSupportActionBar(layout.toolbar)
		supportActionBar?.apply {
			setDisplayHomeAsUpEnabled(true)
		}
	}

	private fun finishWithResult(calendar: SimplifiedCalendar) {
		setResult(Activity.RESULT_OK, Intent().apply {
			putExtras(Bundle().apply {
				putParcelable(RESULT_CALENDAR, calendar)
			})
		})
		finish()
	}

	@SuppressLint("MissingPermission")
	@AfterPermissionGranted(REQUEST_PERMISSION_CALENDAR)
	private fun observeState() {
		if (hasPermissions(PERMISSION_READ_CALENDAR, PERMISSION_WRITE_CALENDAR)) {
			controller.hasPermission = true
			viewModel.observeState()
					.observeOnMainThread()
					.subscribe({ state ->
						when (state) {
							is ChooseCalendarViewModel.State.Loaded -> {
								layout.txtAnonymousName.setText(state.selectedCalendar?.name)
								layout.txtAnonymousEmail.setText(state.selectedCalendar?.email)
								controller.setAccountsAndSelectedCalendar(state.accounts, state.selectedCalendar)
							}
							is ChooseCalendarViewModel.State.Error -> {
								state.throwable.message?.let {
									layout.view.showSnack(it)
								}
							}
						}
					})
		} else {
			controller.hasPermission = false
			if (!ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSION_READ_CALENDAR) &&
					!ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSION_WRITE_CALENDAR)) {
				ActivityCompat.requestPermissions(this, arrayOf(PERMISSION_READ_CALENDAR, PERMISSION_WRITE_CALENDAR), REQUEST_PERMISSION_CALENDAR)
			}
		}
	}

	override fun onRequestPermissionsResult(requestCode: Int,
											permissions: Array<String>,
											grantResults: IntArray) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults)
		EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
	}

	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		super.onActivityResult(requestCode, resultCode, data)

		if (requestCode == RC_SIGN_IN) {
			viewModel.onSignInResult(data)
		}
	}

}
