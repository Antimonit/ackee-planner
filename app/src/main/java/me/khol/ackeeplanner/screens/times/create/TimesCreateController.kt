package me.khol.ackeeplanner.screens.times.create

import com.airbnb.epoxy.EpoxyController
import me.khol.ackeeplanner.screens.times.epoxy.createTime
import me.khol.ackeeplanner.screens.times.epoxy.header
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneOffset

/**
 * Epoxy controller for peekable view at the bottom of the calendar
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 09.04.2018
 **/
open class TimesCreateController(
		private val onTimeClick: (LocalDateTime) -> Unit
) : EpoxyController() {

	var events: Set<LocalDateTime> = emptySet()
		set(value) {
			field = value
			requestModelBuild()
		}

	override fun buildModels() {
		events.groupBy { it.toLocalDate() }
				.toSortedMap()
				.forEach { date: LocalDate, events ->

					header {
						id("header", date.toEpochDay())
						date(date)
					}

					events.sorted().forEach { event ->
						createTime {
							id(event.toInstant(ZoneOffset.UTC).toEpochMilli())
							time(event)
							onTimeClick(onTimeClick)
						}
					}
				}
	}

}
