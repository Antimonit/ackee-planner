package me.khol.ackeeplanner.screens.main.invitations.epoxy

import me.khol.ackeeplanner.model.api.meeting.Meeting
import me.khol.ackeeplanner.screens.main.base.epoxy.MeetingEpoxyController

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 06.04.2018
 **/
class InvitationsEpoxyController(
		private val onCardClick: (Meeting) -> Unit
) : MeetingEpoxyController() {

	var userIds: List<Int> = emptyList()
	// When userIds change, meetings change too -> don't trigger model rebuild here and rebuild
	// only when meetings change

	override fun buildModels() {
		if (meetings.isEmpty()) {
			empty {
				id("empty")
			}
		}

		meetings.forEach {
			meeting {
				id(it.id)
				meeting(it)
				userIds(userIds)
				onCardClick(onCardClick)
			}
		}
	}

}
