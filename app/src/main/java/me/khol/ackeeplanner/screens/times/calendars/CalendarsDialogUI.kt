package me.khol.ackeeplanner.screens.times.calendars

import android.content.Context
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.screens.base.epoxy.VerticalSpaceItemDecoration
import me.khol.ackeeplanner.screens.base.layout.BaseParentlessLayout
import me.khol.extensions.getColor
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.withAlpha

class CalendarsDialogUI(
		context: Context,
		private val accountsAdapter: RecyclerView.Adapter<*>
) : BaseParentlessLayout(context) {

	lateinit var list: RecyclerView

	override fun createView(ui: AnkoContext<Context>): View {
		return ui.recyclerView {
			backgroundColor = getColor(R.color.black).withAlpha(10)
			layoutManager = LinearLayoutManager(ui.ctx, LinearLayoutManager.VERTICAL, false)
			itemAnimator = DefaultItemAnimator()
			adapter = accountsAdapter
			clipChildren = false
			clipToPadding = false

			addItemDecoration(VerticalSpaceItemDecoration(16.dp))
		}.apply {
			list = this
			layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)
		}
	}

}
