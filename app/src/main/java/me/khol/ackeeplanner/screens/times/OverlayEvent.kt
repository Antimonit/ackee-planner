package me.khol.ackeeplanner.screens.times

import org.threeten.bp.Instant

/**
 * An event that didn't come from the calendar itself, but from user input or from ackee endpoint
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 12.04.2018
 **/
data class OverlayEvent(
		var id: Int,
		val summary: String,
		val location: String?,
		val start: Instant,
		val end: Instant,
		val displayColor: Int,
		val isTaken: Boolean,
		val takenBy: String?
)
