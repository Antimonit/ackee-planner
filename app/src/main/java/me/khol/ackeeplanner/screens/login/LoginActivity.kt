package me.khol.ackeeplanner.screens.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.google.android.gms.common.api.ApiException
import io.reactivex.disposables.Disposable
import me.khol.ackeeplanner.App
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.api.ApiExceptionHandler
import me.khol.ackeeplanner.extensions.getViewModel
import me.khol.ackeeplanner.extensions.observeOnMainThread
import me.khol.ackeeplanner.model.AccountRepository
import me.khol.ackeeplanner.screens.base.activity.BaseActivity
import me.khol.ackeeplanner.screens.main.MainActivity
import timber.log.Timber

/**
 * A login screen that is not used in the production version of the application. Sole purpose of
 * this activity is to inspect and debug sign in process.
 *
 * Displays a button to log in when user is logged out. Displays two buttons when logged in, one to
 * log out, the other one to revoke access. Revoking access removes the access to Google data
 * permission from the backend.
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 28.11.2017
 **/
class LoginActivity : BaseActivity() {

	companion object {
		private const val RC_SIGN_IN = 2
	}

	private lateinit var layout: LoginActivityUI
	private lateinit var viewModel: LoginViewModel

	private lateinit var loginObservable: Disposable
	private lateinit var intentObservable: Disposable

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		App.appComponent.inject(this)
		viewModel = getViewModel(vmFactory)

		layout = LoginActivityUI(
				rootView,
				onLogInClicked = { viewModel.doLogIn() },
				onLogOutClicked = { viewModel.doLogOut() },
				onRevokeClicked = { viewModel.doRevoke() },
				onContinueClicked = {
					startActivity(Intent(this, MainActivity::class.java))
				})

		setContentView(layout.view)
	}


	override fun onStart() {
		super.onStart()

		intentObservable = viewModel.observeSignInIntent()
				.subscribe { signInIntent: Intent ->
					startActivityForResult(signInIntent, RC_SIGN_IN)
				}

		loginObservable = viewModel.observeLoginState()
				.observeOnMainThread()
				.subscribe { loginState ->
					layout.groupLogOut.visibility = View.INVISIBLE
					layout.btnLogIn.visibility = View.INVISIBLE
					layout.progressLogIn.visibility = View.INVISIBLE
					layout.txtAccount.text = ""

					when (loginState) {
						is AccountRepository.State.SignedOut -> {
							layout.btnLogIn.visibility = View.VISIBLE
							layout.txtLoginState.text = getString(R.string.login_signed_out)
						}
						is AccountRepository.State.SigningIn -> {
							layout.progressLogIn.visibility = View.VISIBLE
							layout.txtLoginState.text = getString(R.string.login_signing_in)
						}
						is AccountRepository.State.SignedIn -> {
							layout.groupLogOut.visibility = View.VISIBLE
							layout.txtLoginState.text = getString(R.string.login_signed_in)
							val accountInfo = loginState.account?.let { "${loginState.account.displayName}, ${loginState.account.email}" }
									?: "anonymous"
							layout.txtAccount.text = getString(R.string.login_logged_in_as, accountInfo, loginState.user.id.toString(), loginState.user.email)
						}
						is AccountRepository.State.Error -> {
							layout.txtLoginState.text = getString(R.string.login_sign_in_error)
							layout.btnLogIn.visibility = View.VISIBLE

							if (loginState.throwable is ApiException) {
								layout.txtAccount.text = ApiExceptionHandler.resolve(loginState.throwable)
								Timber.w("signInResult: failed | code=${loginState.throwable.statusCode} | ${layout.txtAccount.text}")
							} else {
								layout.txtAccount.text = loginState.throwable.message
								Timber.w("signInResult: failed")
							}
						}
					}
				}
	}

	override fun onStop() {
		super.onStop()
		intentObservable.dispose()
		loginObservable.dispose()
	}

	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		super.onActivityResult(requestCode, resultCode, data)

		if (requestCode == RC_SIGN_IN) {
			viewModel.onSignInResult(data)
		}
	}

}
