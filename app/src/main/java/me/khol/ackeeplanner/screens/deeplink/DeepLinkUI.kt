package me.khol.ackeeplanner.screens.deeplink

import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.screens.base.layout.BaseLayout
import me.khol.extensions.getColor
import me.khol.extensions.withAlpha
import org.jetbrains.anko.*

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 11.03.2018
 **/
class DeepLinkUI(
		parent: ViewGroup
) : BaseLayout(parent) {

	lateinit var txtMessage: TextView
	lateinit var txtSubMessage: TextView
	lateinit var progress: ProgressBar

	override fun createView(ui: AnkoContext<ViewGroup>): View {
		return ui.constraintLayout {
			fitsSystemWindows = true
			backgroundColor = getColor(R.color.black).withAlpha(10)

			txtMessage = textView("We are retrieving your meeting.") {
				textSize = 16f
				textColorResource = R.color.textColorPrimary
				gravity = Gravity.CENTER
			}
			txtSubMessage = textView("This might take a moment.") {
				textSize = 12f
				textColorResource = R.color.textColorTertiary
				gravity = Gravity.CENTER
			}

			progress = progressBar {
				isIndeterminate = true
				padding = 16.dp
			}

			constraints {
				txtMessage
						.connect(HORIZONTAL of parentId with 16.dp)
						.size(matchConstraint, wrapContent)

				txtSubMessage
						.connect(HORIZONTAL of parentId with 16.dp)
						.size(matchConstraint, wrapContent)

				progress.connect(HORIZONTAL of parentId with 16.dp)
						.size(wrapContent, wrapContent)

				arrayOf(txtMessage, progress, txtSubMessage)
						.chainPacked(TOP of parentId, BOTTOM of parentId)
			}

		}.also {
			it.layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)
		}
	}

}
