package me.khol.ackeeplanner.screens.deeplink

import io.reactivex.Observable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import me.khol.ackeeplanner.App
import me.khol.ackeeplanner.extensions.subscribeOnIO
import me.khol.ackeeplanner.model.AccountRepository
import me.khol.ackeeplanner.model.MeetingRepository
import me.khol.ackeeplanner.model.api.meeting.Meeting
import me.khol.ackeeplanner.screens.base.viewmodel.ContextViewModel
import javax.inject.Inject

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 10.03.2018
 **/
class DeepLinkViewModel @Inject constructor(
		app: App,
		private val accountRepository: AccountRepository,
		private val meetingRepository: MeetingRepository
) : ContextViewModel(app) {

	sealed class State {
		open class Loading : State()
		open class Success(val meeting: Meeting) : State()
		open class Error(val throwable: Throwable) : State()
	}

	private val stateSubject: Subject<State> = BehaviorSubject.createDefault(State.Loading())
	fun observeState(): Observable<State> = stateSubject

	fun addMeeting(token: String, id: Int) {
		val loginState: AccountRepository.State = accountRepository.observeLoginState().blockingFirst()

		if (loginState is AccountRepository.State.SignedOut ||
				loginState is AccountRepository.State.Error) {
			accountRepository.doAnonymousLogin()
		}

		disposables += accountRepository.observeLoginState()
				.ofType(AccountRepository.State.SignedIn::class.java)
				.firstOrError()
				.flatMap {
					meetingRepository.meetingDetail(token, id)
				}
				.subscribeOnIO()
				.subscribe({ meeting ->
					stateSubject.onNext(State.Success(meeting))
				}, { t ->
					stateSubject.onNext(State.Error(t))
				})
	}

}
