package me.khol.ackeeplanner.screens.location

import com.google.android.gms.location.places.AutocompletePrediction
import io.reactivex.Observable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import me.khol.ackeeplanner.App
import me.khol.ackeeplanner.model.location.Location
import me.khol.ackeeplanner.model.LocationRepository
import me.khol.ackeeplanner.screens.base.viewmodel.ContextViewModel
import javax.inject.Inject

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 10.03.2018
 **/
class LocationViewModel @Inject constructor(
		app: App,
		private val locationRepository: LocationRepository
) : ContextViewModel(app) {

	private val errorSubject = BehaviorSubject.create<Throwable>()
	private val locationPredictionsSubject = BehaviorSubject.createDefault<List<Location>>(listOf())
	private val locationSubject = BehaviorSubject.create<Location>()
	private val locationQuerySubject = BehaviorSubject.create<String>()
	private val recentLocationsSubject: Subject<List<Location>> = BehaviorSubject.create()

	fun observeErrors(): Observable<Throwable> = errorSubject
	fun observeLocationPredictions(): Observable<List<Location>> = locationPredictionsSubject
	fun observeLocation(): Observable<Location> = locationSubject
	fun observeRecentLocations(): Observable<List<Location>> = recentLocationsSubject

	fun fetchLocationPredictions(query: String) {
		locationQuerySubject.onNext(query)
	}

	init {
		disposables += locationQuerySubject
				.flatMapMaybe(locationRepository::fetchLocationPredictions)
				.map { it: List<AutocompletePrediction> ->
					it.map {
						Location(
								it.placeId,
								it.getPrimaryText(null).toString(),
								it.getSecondaryText(null).toString(),
								null,
								null
						)
					}
				}
				.subscribe({
					locationPredictionsSubject.onNext(it)
				}, { t ->
					t.printStackTrace()
				})

		disposables += locationSubject
				.subscribe(::addRecentLocation)

		disposables += locationRepository
				.observeRecentLocations()
				.subscribe({ it ->
					recentLocationsSubject.onNext(it)
				})
	}

	fun onLocationClicked(location: Location) {
		if (location.id == null) {
			// location is user defined location
			locationSubject.onNext(location)
		} else {
			// location is prediction from auto-complete
			// fetch better information about it (latLng and latLngBounds)
			disposables += locationRepository.fetchLocationFromPlaceId(location.id)
					.subscribe({ fetchedLocation ->
						locationSubject.onNext(fetchedLocation)
					}, { t ->
						t.printStackTrace()
						errorSubject.onNext(t)
					})
		}
	}

	fun onDoneClicked(query: String) {
		locationSubject.onNext(Location(null, query, null, null, null))
	}

	fun addRecentLocation(location: Location) {
		if (location.name.isNotEmpty()) {
			disposables += locationRepository.addRecentLocation(location).subscribe()
		}
	}

	fun removeRecentLocation(location: Location) {
		disposables += locationRepository.removeRecentLocation(location).subscribe()
	}

	fun removeAllRecentLocations() {
		disposables += locationRepository.removeAllRecentLocations().subscribe()
	}

}
