package me.khol.ackeeplanner.screens.main.myMeetings.epoxy

import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import com.airbnb.epoxy.EpoxyModelClass
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.screens.base.epoxy.BaseEpoxyModel
import me.khol.ackeeplanner.screens.base.layout.BaseLayout
import me.khol.extensions.getColor
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.textColor
import org.jetbrains.anko.textView
import org.jetbrains.anko.withAlpha


/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 09.03.2018
 **/
@EpoxyModelClass
open class EmptyModel : BaseEpoxyModel<EmptyModel.Layout>() {

	override fun createViewLayout(parent: ViewGroup) = Layout(parent)

	class Layout(parent: ViewGroup) : BaseLayout(parent) {

		override fun createView(ui: AnkoContext<ViewGroup>): View {
			return ui.constraintLayout {

				val text1 = textView(R.string.my_meetings_empty_title) {
					textSize = 18f
					textColor = getColor(R.color.black_50)
					gravity = Gravity.CENTER_HORIZONTAL
				}

				val text2 = textView(R.string.my_meetings_empty_subtitle) {
					textSize = 12f
					textColor = getColor(R.color.black).withAlpha(50)
				}

				constraints {
					text1.connect(
							HORIZONTAL of parentId with 24.dp,
							TOPS of parentId with 56.dp
					)
					text2.connect(
							HORIZONTAL of parentId with 40.dp,
							TOP to BOTTOM of text1 with 8.dp
					)
				}

			}.apply {
				layoutParams = ViewGroup.LayoutParams(org.jetbrains.anko.matchParent, org.jetbrains.anko.matchParent)
			}
		}
	}
}
