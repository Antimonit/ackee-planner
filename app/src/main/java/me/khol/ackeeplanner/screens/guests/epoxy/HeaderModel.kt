package me.khol.ackeeplanner.screens.guests.epoxy

import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.screens.base.epoxy.BaseEpoxyModel
import me.khol.ackeeplanner.screens.base.layout.BaseLayout
import me.khol.ackeeplanner.screens.base.layout.labelTextView
import me.khol.extensions.getDrawableAttr
import me.khol.extensions.getTintedDrawable
import org.jetbrains.anko.*

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 29.03.2018
 **/
@EpoxyModelClass
open class HeaderModel : BaseEpoxyModel<HeaderModel.Layout>() {

	@EpoxyAttribute
	lateinit var headerText: String
	@EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
	var onClearClicked: (() -> Unit)? = null

	override fun createViewLayout(parent: ViewGroup) = Layout(parent)

	override fun bind(layout: Layout) {
		super.bind(layout)
		layout.txtHeader.text = headerText
		layout.btnClear.visibility = if (onClearClicked == null) {
			View.GONE
		} else {
			View.VISIBLE
		}
		layout.btnClear.setOnClickListener {
			onClearClicked?.invoke()
		}
	}

	override fun unbind(layout: Layout) {
		super.unbind(layout)
		layout.btnClear.setOnClickListener(null)
	}

	class Layout(parent: ViewGroup) : BaseLayout(parent) {

		lateinit var txtHeader: TextView
		lateinit var btnClear: ImageButton

		override fun createView(ui: AnkoContext<ViewGroup>): View {
			return ui.frameLayout {
				topPadding = 8.dp

				txtHeader = labelTextView {
					horizontalPadding = 16.dp
				}.lparams(wrapContent, wrapContent) {
					gravity = Gravity.CENTER_VERTICAL or Gravity.START
				}

				btnClear = imageButton {
					setImageDrawable(getTintedDrawable(R.drawable.ic_clear_all, R.color.black_50))
					isClickable = true
					horizontalPadding = 16.dp
					verticalPadding = 8.dp
					backgroundDrawable = getDrawableAttr(android.R.attr.selectableItemBackgroundBorderless)
//					gravity = Gravity.CENTER
				}.lparams(64.dp, 32.dp) {
					gravity = Gravity.CENTER_VERTICAL or Gravity.END
				}

				layoutParams = ViewGroup.LayoutParams(org.jetbrains.anko.matchParent, 48.dp)
			}
		}
	}
}
