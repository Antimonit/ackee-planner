package me.khol.ackeeplanner.screens.calendar.epoxy

import android.content.res.ColorStateList
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.OvalShape
import android.support.v4.view.ViewCompat
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.model.ColorTransformer
import me.khol.ackeeplanner.model.calendar.Calendar
import me.khol.ackeeplanner.screens.base.epoxy.BaseEpoxyModel
import me.khol.ackeeplanner.screens.base.layout.BaseLayout
import me.khol.extensions.getColor
import me.khol.extensions.getDrawableAttr
import me.khol.extensions.getTintedDrawable
import org.jetbrains.anko.*


/**
 * Displays a clickable view with a name of one of the user's calendars.
 *
 * This model is used in [AccountModel]'s [AccountModel.Layout.calendars] recycler view.
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 14.04.2018
 **/
open class CalendarModel : BaseEpoxyModel<CalendarModel.Layout>() {

	@EpoxyAttribute
	var selected: Boolean = false
	@EpoxyAttribute
	lateinit var calendar: Calendar

	@EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
	lateinit var onCalendarSelected: (Calendar) -> Unit

	override fun createViewLayout(parent: ViewGroup) = Layout(parent)

	override fun bind(layout: Layout) {
		super.bind(layout)
		layout.txtCalendarName.text = if (calendar.isPrimary) "Events" else calendar.displayName
		layout.view.setOnClickListener {
			onCalendarSelected(calendar)
		}
		layout.imgSelected.visibility = if (selected) View.VISIBLE else View.GONE
//		layout.imgColor.backgroundColor = ColorTransformer.getDisplayColor(calendar.color)

		val transformedColor = ColorTransformer.getDisplayColor(calendar.color)
		ViewCompat.setBackgroundTintList(layout.imgColor, ColorStateList.valueOf(transformedColor))
	}

	override fun unbind(layout: Layout) {
		super.unbind(layout)
		layout.view.setOnClickListener(null)
	}

	@Suppress("MemberVisibilityCanBePrivate")
	class Layout(parent: ViewGroup) : BaseLayout(parent) {

		lateinit var imgColor: View
		lateinit var txtCalendarName: TextView
		lateinit var imgSelected: ImageView

		override fun createView(ui: AnkoContext<ViewGroup>): View {
			return ui.frameLayout {
				isClickable = true
				foreground = getDrawableAttr(android.R.attr.selectableItemBackground)

				constraintLayout {

					imgColor = view {
						backgroundDrawable = ShapeDrawable(OvalShape())
					}

					txtCalendarName = textView {
						textColor = getColor(R.color.textColorPrimary)
						textSize = 16f
						ellipsize = TextUtils.TruncateAt.END
					}

					imgSelected = imageView {
						setImageDrawable(getTintedDrawable(R.drawable.ic_check, R.color.black_50))
					}

					constraints {
						imgColor.connect(STARTS of parentId with 28.dp)
								.connect(VERTICAL of parentId)
								.size(16.dp, 16.dp)

						txtCalendarName
								.connect(VERTICAL of parentId)
								.connect(STARTS of parentId with 72.dp)
								.connect(ENDS of parentId)
								.size(matchConstraint, wrapContent)

						imgSelected
								.connect(VERTICAL of txtCalendarName)
								.connect(ENDS of parentId with 16.dp)

					}
				}.lparams(matchParent, 48.dp)

			}.apply {
				layoutParams = ViewGroup.LayoutParams(org.jetbrains.anko.matchParent, org.jetbrains.anko.wrapContent)
			}
		}

	}

}
