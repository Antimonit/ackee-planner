package me.khol.ackeeplanner.screens.main

import android.view.animation.*

/**
 * TODO: 3. 5. 2018 david.khol: add class description
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 03.05.2018
 **/
class SlideInAnimationSet : AnimationSet(true) {

    init {
        duration = 500

        addAnimation(AlphaAnimation(0.0f, 1.0f).apply {
            interpolator = AccelerateDecelerateInterpolator()
        })
        addAnimation(TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.0f
        ).apply {
            interpolator = AccelerateDecelerateInterpolator()
        })
    }

}
