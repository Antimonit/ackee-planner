package me.khol.ackeeplanner.screens.times

import android.Manifest
import android.support.annotation.ColorInt
import android.view.Menu
import android.view.MenuItem
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.extensions.hasPermissions
import me.khol.ackeeplanner.extensions.requestPermissions
import me.khol.ackeeplanner.extensions.showSnack
import me.khol.ackeeplanner.model.ColorTransformer
import me.khol.ackeeplanner.model.event.EventStatus
import me.khol.ackeeplanner.screens.base.activity.BaseActivity
import me.khol.ackeeplanner.screens.times.calendars.CalendarsDialogFragment
import me.khol.ackeeplanner.screens.times.options.OptionsDialogFragment
import me.khol.calendar.Event
import me.khol.extensions.isVisible
import org.threeten.bp.LocalDate
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions

/**
 * Created by David Khol [david@khol.me] on 4. 2. 2018.
 */
abstract class BaseTimesActivity : BaseActivity() {

	companion object {
		const val READ_CALENDAR_RC = 1

		val MONTH_PATTERN: DateTimeFormatter = DateTimeFormatter.ofPattern("MMMM")
		val MONTH_YEAR_PATTERN: DateTimeFormatter = DateTimeFormatter.ofPattern("MMM YYYY")
	}

	protected open lateinit var layout: CalendarUI

	protected abstract fun onCalendarPermissionGranted()

	@AfterPermissionGranted(READ_CALENDAR_RC)
	fun checkCalendarPermission() {
		if (hasPermissions(Manifest.permission.READ_CALENDAR)) {
			onCalendarPermissionGranted()
		} else {
			requestPermissions(
					getString(R.string.times_missing_permission),
					READ_CALENDAR_RC,
					Manifest.permission.READ_CALENDAR)
		}
	}

	override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults)
		EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
	}

	protected fun mapEvents(events: List<me.khol.ackeeplanner.model.event.Event>, defaultZoneId: ZoneId): List<Event> {
		return events.map {
			// Event timezone is already accounted for in the timestamp.
			// Don't use meeting.timezone and use the local timezone instead
			Event(
					"${it.eventId}|${it.instanceId}|${it.calendarId}",
					it.summary,
					it.location,
					it.start.atZone(defaultZoneId),
					it.end.atZone(defaultZoneId),
					it.allDay,
					ColorTransformer.getDisplayColor(it.displayColor),
					when (it.status) {
						EventStatus.CONFIRMED -> Event.Status.CONFIRMED
						EventStatus.CANCELLED -> Event.Status.CANCELLED
						EventStatus.TENTATIVE -> Event.Status.TENTATIVE
					},
					layer = 0
			)
		}
	}

	@ColorInt
	protected fun overlaidColor(isTaken: Boolean): Int {
		return when {
			isTaken -> ColorTransformer.GRAY
			else -> ColorTransformer.RED
		}
	}

	protected fun overlaidStatus(isSelected: Boolean): Event.Status {
		return when (isSelected) {
			true -> Event.Status.TENTATIVE
			else -> Event.Status.CONFIRMED
		}
	}

	protected fun mapOverlaidEvents(overlaidEvents: Set<OverlayEvent>, defaultZoneId: ZoneId, selectedId: Int?): List<Event> {
		return overlaidEvents.map {
			Event(it.id.toString(),
					it.summary,
					it.location,
					it.start.atZone(defaultZoneId),
					it.end.atZone(defaultZoneId),
					false,
					overlaidColor(it.isTaken),
					overlaidStatus(selectedId == it.id),
					1)
		}
	}

	protected fun resolveErrorState(t: Throwable) {
		when (t) {
			is GooglePlayServicesAvailabilityIOException -> showGooglePlayServicesErrorDialog(t.connectionStatusCode)
			is UserRecoverableAuthIOException -> startActivityForResult(t.intent, BaseActivity.RC_AUTHORIZATION)
			else -> {
				t.printStackTrace()
				layout.view.showSnack(getString(R.string.error_occurred, t))
			}
		}
	}

	protected fun changeTitle(date: LocalDate) {
		val timeFormatter = if (LocalDate.now().year == date.year) {
			MONTH_PATTERN
		} else {
			MONTH_YEAR_PATTERN
		}
		layout.toolbar.title = date.format(timeFormatter)
	}


	protected open fun getCurrentDuration(): Int {
		return 60
	}

	private var isCalendarViewEnabled = true
	private var isWeekViewEnabled = false

	lateinit var dayViewAction: MenuItem
	lateinit var weekViewAction: MenuItem
	lateinit var calendarFormAction: MenuItem
	lateinit var listFormAction: MenuItem
	lateinit var calendarFilterAction: MenuItem
	lateinit var durationAction: MenuItem

	private fun updateActionItemVisibility() {
		calendarFormAction.isVisible = isCalendarViewEnabled == false
		listFormAction.isVisible = isCalendarViewEnabled == true
		dayViewAction.isVisible = (isWeekViewEnabled == true) && isCalendarViewEnabled
		weekViewAction.isVisible = (isWeekViewEnabled == false) && isCalendarViewEnabled
	}

	private fun updateCalendarVisibility() {
		layout.timesRecycler.isVisible = isCalendarViewEnabled == false
		layout.calendar.isVisible = isCalendarViewEnabled == true
	}

	override fun onCreateOptionsMenu(menu: Menu): Boolean {
		super.onCreateOptionsMenu(menu)
		menuInflater.inflate(R.menu.times, menu)
		dayViewAction = menu.findItem(R.id.action_day_view)
		weekViewAction = menu.findItem(R.id.action_week_view)
		calendarFormAction = menu.findItem(R.id.action_form_calendar)
		listFormAction = menu.findItem(R.id.action_form_text)
		calendarFilterAction = menu.findItem(R.id.action_filter_calendars)
		durationAction = menu.findItem(R.id.action_event_duration)
		updateActionItemVisibility()
		updateCalendarVisibility()
		return true
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		when (item.itemId) {
//			R.id.action_today -> layout.calendar.currentDate = LocalDate.now()
			R.id.action_day_view -> {
				layout.calendar.numberOfDays = 1
				item.isChecked = true
				isWeekViewEnabled = false
			}
			R.id.action_week_view -> {
				layout.calendar.numberOfDays = 7
				item.isChecked = true
				isWeekViewEnabled = true
			}
			R.id.action_form_calendar -> {
				isCalendarViewEnabled = true
				updateCalendarVisibility()
			}
			R.id.action_form_text -> {
				isCalendarViewEnabled = false
				updateCalendarVisibility()
			}
			R.id.action_filter_calendars -> {
				val bottomSheetDialogFragment = CalendarsDialogFragment()
				bottomSheetDialogFragment.show(supportFragmentManager, "calendars")
			}
			R.id.action_event_duration -> {
				val bottomSheetDialogFragment = OptionsDialogFragment.newInstance(getCurrentDuration())
				bottomSheetDialogFragment.show(supportFragmentManager, "options")
			}
		}
		updateActionItemVisibility()
		return super.onOptionsItemSelected(item)
	}

}
