package me.khol.ackeeplanner.screens.base.epoxy

import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.screens.base.layout.BaseLayout
import me.khol.extensions.elevationCompat
import me.khol.extensions.getColor
import me.khol.extensions.getDrawableAttr
import org.jetbrains.anko.*

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 13.03.2018
 **/
open class MissingPermissionModel : BaseEpoxyModel<MissingPermissionModel.Layout>() {

	@EpoxyAttribute
	lateinit var title: String
	@EpoxyAttribute
	lateinit var description: String
	@EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
	lateinit var onClicked: () -> Unit

	override fun createViewLayout(parent: ViewGroup) = Layout(parent)

	override fun bind(layout: Layout) {
		super.bind(layout)
		layout.view.setOnClickListener { _ ->
			onClicked()
		}
		layout.txtTitle.text = title
		layout.txtDescription.text = description
	}

	override fun unbind(layout: Layout) {
		super.unbind(layout)
		layout.view.setOnClickListener(null)
	}

	class Layout(parent: ViewGroup) : BaseLayout(parent) {

		lateinit var txtTitle: TextView
		lateinit var txtDescription: TextView

		override fun createView(ui: AnkoContext<ViewGroup>): View {
			return ui.frameLayout {
				backgroundResource = R.color.white
				elevationCompat = 2.dpf
				isClickable = true
				foreground = getDrawableAttr(android.R.attr.selectableItemBackground)

				constraintLayout {
					padding = 16.dp

					txtTitle = textView {
						textColor = getColor(R.color.textColorPrimary)
						textSize = 18f
						gravity = Gravity.CENTER
					}

					txtDescription = textView {
						textColor = getColor(R.color.textColorSecondary)
						textSize = 12f
						gravity = Gravity.CENTER
					}

					constraints {
						txtTitle.connect(HORIZONTAL of parentId)
								.connect(TOPS of parentId)
								.size(matchConstraint, org.jetbrains.anko.wrapContent)

						txtDescription.connect(HORIZONTAL of parentId)
								.connect(TOP to BOTTOM of txtTitle with 16.dp)
					}
				}.lparams(matchParent, matchParent)

			}.apply {
				layoutParams = ViewGroup.MarginLayoutParams(org.jetbrains.anko.matchParent, org.jetbrains.anko.wrapContent)
						.apply { topMargin = 16.dp }
			}
		}

	}

}
