package me.khol.ackeeplanner.screens.dialogs

import android.support.v4.app.DialogFragment
import me.khol.ackeeplanner.R

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 06.04.2018
 **/
abstract class MeetingConfirmedDialog : DialogWithImageHeader() {
	companion object {
		fun getInstance(): DialogFragment {
			return DialogWithImageHeader.getInstance(
					R.string.meeting_accepted_title,
					R.string.meeting_accepted_description,
					R.drawable.airplane,
					R.string.meeting_accepted_confirmation
			)
		}
	}
}
