package me.khol.ackeeplanner.screens.base.activity

import android.view.View
import android.view.ViewGroup
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.matchParent

/**
 * Created by David Khol [david@khol.me] on 10. 3. 2018.
 */
class FragmentActivityUI(parent: ViewGroup) : BaseLayout(parent) {

	override fun createView(ui: AnkoContext<ViewGroup>): View {
		return ui.frameLayout {
			fitsSystemWindows = true
			id = R.id.fragment_container
		}.apply {
			layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)
		}
	}

}
