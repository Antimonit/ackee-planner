package me.khol.ackeeplanner.screens.location.epoxy

import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import com.airbnb.epoxy.EpoxyModelClass
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.screens.base.epoxy.BaseEpoxyModel
import me.khol.ackeeplanner.screens.base.layout.BaseLayout
import me.khol.extensions.getTintedDrawable
import org.jetbrains.anko.*


/**
 * Shows "Powered by Google" logo. It is required to display the logo when using Google places
 * suggestions without a Google map.
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 29.03.2018
 **/
@EpoxyModelClass
open class PoweredByGoogleModel : BaseEpoxyModel<PoweredByGoogleModel.Layout>() {

	override fun createViewLayout(parent: ViewGroup) = Layout(parent)

	class Layout(parent: ViewGroup) : BaseLayout(parent) {

		override fun createView(ui: AnkoContext<ViewGroup>): View {
			return ui.frameLayout {
				verticalPadding = 8.dp

				imageView {
					//imageResource = R.drawable.powered_by_google_light
					//imageResource = R.drawable.powered_by_google_dark
					image = getTintedDrawable(R.drawable.powered_by_google_dark, R.color.black_50)
				}.lparams(wrapContent, wrapContent) {
					leftMargin = 72.dp
					gravity = Gravity.START
				}

			}.apply {
				layoutParams = ViewGroup.LayoutParams(matchParent, wrapContent)
			}
		}
	}
}
