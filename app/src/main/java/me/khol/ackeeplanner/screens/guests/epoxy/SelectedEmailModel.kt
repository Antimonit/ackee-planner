package me.khol.ackeeplanner.screens.guests.epoxy

import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import com.airbnb.epoxy.EpoxyAttribute
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.extensions.font
import me.khol.ackeeplanner.model.contacts.Contact
import me.khol.ackeeplanner.screens.base.layout.defaultTextView
import me.khol.extensions.elevationCompat
import me.khol.extensions.getColor
import me.khol.extensions.getDrawableAttr
import me.khol.extensions.isVisible
import org.jetbrains.anko.*

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 29.03.2018
 **/
open class SelectedEmailModel : BaseEmailModel<SelectedEmailModel.Layout>() {

	@EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
	lateinit var onEmailRemoved: (Contact) -> Unit

	override fun createViewLayout(parent: ViewGroup) = Layout(parent)

	override fun bind(layout: Layout) {
		super.bind(layout)
		val name = account.name
		if (name == null) {
			layout.txtSubtitle.isVisible = false
			layout.txtTitle.text = account.email
		} else {
			layout.txtSubtitle.isVisible = true
			layout.txtTitle.text = name
			layout.txtSubtitle.text = account.email
		}
		layout.btnDelete.setOnClickListener {
			onEmailRemoved(account)
		}
	}

	override fun unbind(layout: Layout) {
		super.unbind(layout)
		layout.btnDelete.setOnClickListener(null)
	}

	class Layout(parent: ViewGroup) : BaseEmailModel.Layout(parent) {

		lateinit var btnDelete: ImageButton

		override fun createView(ui: AnkoContext<ViewGroup>): View {
			return ui.constraintLayout {
				backgroundResource = R.color.white
				elevationCompat = 2.dpf

				txtTitle = defaultTextView {
					ellipsize = TextUtils.TruncateAt.END
					textColor = getColor(R.color.textColorPrimary)
					textSize = 16f
				}

				txtSubtitle = defaultTextView {
					ellipsize = TextUtils.TruncateAt.END
					textColor = getColor(R.color.textColorSecondary)
					textSize = 14f
					font = R.font.roboto_light
				}

				imgAccountAvatar = imageView()

				btnDelete = imageButton(R.drawable.ic_close) {
					backgroundDrawable = getDrawableAttr(android.R.attr.selectableItemBackgroundBorderless)
				}

				constraints {
					imgAccountAvatar
							.connect(VERTICAL of parentId)
							.connect(STARTS of parentId with 16.dp)
							.size(40.dp, 40.dp)

					btnDelete
							.connect(VERTICAL of parentId)
							.connect(ENDS of parentId with 16.dp)
							.size(32.dp, 32.dp)

					txtTitle
							.connect(STARTS of parentId with 72.dp)
							.connect(ENDS of parentId with 72.dp)
							.size(matchConstraint, wrapContent)

					txtSubtitle
							.connect(HORIZONTAL of txtTitle)
							.size(matchConstraint, wrapContent)

					arrayOf(txtTitle, txtSubtitle).chainPacked(
							TOP of parentId,
							BOTTOM of parentId
					)
				}
			}.apply {
				layoutParams = ViewGroup.LayoutParams(org.jetbrains.anko.matchParent, 56.dp)
			}
		}
	}
}
