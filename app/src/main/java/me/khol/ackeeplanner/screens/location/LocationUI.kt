package me.khol.ackeeplanner.screens.location

import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.screens.base.layout.BaseLayout
import me.khol.extensions.elevationCompat
import me.khol.extensions.getColor
import me.khol.extensions.withAlpha
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.toolbar
import org.jetbrains.anko.recyclerview.v7.recyclerView

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 14.04.2018
 **/
class LocationUI(
		parent: ViewGroup,
		val adapter: RecyclerView.Adapter<*>
) : BaseLayout(parent) {

	lateinit var toolbar: Toolbar
	lateinit var addresses: RecyclerView
	lateinit var searchEdit: EditText

	fun getSearchQuery() = searchEdit.text.toString()

	fun clearSearchQuery() = searchEdit.setText("")

	override fun createView(ui: AnkoContext<ViewGroup>): View {
		return ui.verticalLayout {
			fitsSystemWindows = true
			backgroundColor = getColor(R.color.black).withAlpha(10)

			toolbar = toolbar {
				backgroundResource = R.color.white
				elevationCompat = 4.dpf
				fitsSystemWindows = true
				searchEdit = editText {
					backgroundDrawable = null
					hintResource = R.string.location_toolbar_hint
					leftPadding = 0
					lines = 1
					imeOptions = EditorInfo.IME_FLAG_NO_EXTRACT_UI
				}.lparams(matchParent, wrapContent)
			}.lparams(matchParent, wrapContent)

			addresses = recyclerView {
				layoutManager = LinearLayoutManager(ui.ctx, LinearLayoutManager.VERTICAL, false)
				itemAnimator = DefaultItemAnimator()
				clipChildren = false
				adapter = this@LocationUI.adapter
			}.lparams(matchParent, 0.dp, weight = 1f)

		}.also {
			it.layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)
		}
	}

}
