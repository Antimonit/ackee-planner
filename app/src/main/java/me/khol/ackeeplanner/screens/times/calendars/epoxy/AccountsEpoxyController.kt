package me.khol.ackeeplanner.screens.times.calendars.epoxy

import android.content.Context
import com.airbnb.epoxy.EpoxyController
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.model.calendar.Calendar
import me.khol.ackeeplanner.model.calendar.CalendarAccount
import me.khol.ackeeplanner.screens.base.epoxy.missingPermission

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 16.04.2018
 **/
class AccountsEpoxyController(
		private val context: Context,
		private val onCalendarToggled: (Calendar, Boolean) -> Unit,
		private val onPermissionClicked: () -> Unit
) : EpoxyController() {

	var hasPermission = false
		set(value) {
			field = value
			requestModelBuild()
		}

	var accounts: List<CalendarAccount> = emptyList()
		set(value) {
			field = value
			requestModelBuild()
		}

	override fun buildModels() {
		if (!hasPermission) {
			missingPermission {
				id("missing_permission")
				title(context.getString(R.string.permission_missing_permission))
				description(context.getString(R.string.permission_calendar_rationale))
				onClicked(onPermissionClicked)
			}
		}
		accounts.forEach { account ->
			account {
				id(account.name)
				account(account)
				onCalendarToggled(onCalendarToggled)
			}
		}
	}

}
