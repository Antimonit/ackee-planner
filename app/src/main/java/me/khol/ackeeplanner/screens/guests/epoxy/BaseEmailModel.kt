package me.khol.ackeeplanner.screens.guests.epoxy

import android.graphics.Bitmap
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import io.reactivex.disposables.Disposable
import me.khol.ackeeplanner.CircleTransform
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.extensions.observeOnMainThread
import me.khol.ackeeplanner.extensions.safeDispose
import me.khol.ackeeplanner.model.contacts.AvatarLoader
import me.khol.ackeeplanner.model.contacts.Contact
import me.khol.ackeeplanner.screens.base.epoxy.BaseEpoxyModel
import me.khol.ackeeplanner.screens.base.layout.BaseLayout
import me.khol.extensions.getTintedDrawable
import org.jetbrains.anko.imageBitmap

/**
 * Super class for [EmailModel] and [SelectedEmailModel].
 * Both models show some name, email and avatar of specified contact.
 * Loading of avatars is little bit more involved and so it is handled only once, here.
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 05.04.2018
 **/
abstract class BaseEmailModel<T : BaseEmailModel.Layout> : BaseEpoxyModel<T>() {

	@EpoxyAttribute
	lateinit var account: Contact
	@EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
	lateinit var avatarLoader: AvatarLoader

	private var avatarLoaderDisposable: Disposable? = null

	override fun bind(layout: T) {
		super.bind(layout)

		avatarLoaderDisposable.safeDispose()
		avatarLoaderDisposable = avatarLoader.loadAvatar(account.photoId)
				.observeOnMainThread()
				.subscribe(
						/* onSuccess, successfully retrieved avatar */
						{ avatar: Bitmap? ->
							layout.imgAccountAvatar.imageBitmap = CircleTransform.transform(avatar!!)
						},
						/* onError, something broke, we probably didn't have required permission, sad panda */
						{ t: Throwable ->
							t.printStackTrace()
							layout.imgAccountAvatar.setImageDrawable(layout.imgAccountAvatar.getTintedDrawable(R.drawable.ic_account_circle, R.color.black_50))
						},
						/* onComplete, avatar couldn't be retrieved, probably because we loaded custom contact */
						{
							layout.imgAccountAvatar.setImageDrawable(layout.imgAccountAvatar.getTintedDrawable(R.drawable.ic_account_circle, R.color.black_50))
						}
				)
	}

	override fun unbind(layout: T) {
		super.unbind(layout)
		avatarLoaderDisposable.safeDispose()
	}

	abstract class Layout(parent: ViewGroup) : BaseLayout(parent) {

		lateinit var txtTitle: TextView
		lateinit var txtSubtitle: TextView
		lateinit var imgAccountAvatar: ImageView

	}

}
