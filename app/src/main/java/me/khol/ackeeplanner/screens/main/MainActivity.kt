package me.khol.ackeeplanner.screens.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import me.khol.ackeeplanner.App
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.extensions.getViewModel
import me.khol.ackeeplanner.model.api.meeting.Meeting
import me.khol.ackeeplanner.screens.base.activity.BaseActivity
import me.khol.ackeeplanner.screens.base.activity.FragmentActivity
import me.khol.ackeeplanner.screens.base.activity.startFragmentActivityForResult
import me.khol.ackeeplanner.screens.dialogs.MeetingConfirmedDialog
import me.khol.ackeeplanner.screens.dialogs.MeetingCreatedDialog
import me.khol.ackeeplanner.screens.main.invitations.InvitationsFragment
import me.khol.ackeeplanner.screens.main.myMeetings.MyMeetingsFragment
import me.khol.ackeeplanner.screens.meeting.invitation.MeetingInvitationFragment

/**
 * Main activity that is run when starting the app.
 *
 * Contains a bottom navigation bar with 2 entries - My Meetings and Invitations.
 *
 * Indirectly handles deep links. Deep link is first handled by [DeepLinkActivity]
 * [me.khol.ackeeplanner.screens.deeplink.DeepLinkActivity] that downloads deeplinked content
 * while displaying a loading progress bar. When the loading is finished, the downloaded event is
 * passed to this Activity utilizing android:launchMode="singleTask" specified in Android manifest.
 * Instead of creating a new instance of the activity, the intent is delivered through [onNewIntent].
 */
class MainActivity : BaseActivity() {

	companion object {
		const val ARG_MEETING = "meeting"

		const val RC_CREATE_MEETING = 2001
		const val RC_MEETING_DETAIL = 2002
		const val RC_MEETING_INVITATION = 2003
	}

	private lateinit var layout: MainActivityUI
	private lateinit var viewModel: MainViewModel

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		App.appComponent.inject(this)
		viewModel = getViewModel(vmFactory)

		layout = MainActivityUI(
				rootView,
				onNavigationItemSelected = { item ->
					val fragmentToShow = when (item.itemId) {
						R.id.nav_my_meetings -> MyMeetingsFragment()
						R.id.nav_invitations -> InvitationsFragment()
						else -> throw IllegalStateException("Item with id ${item.itemId} does not exist")
					}

					supportFragmentManager
							.beginTransaction()
							.replace(R.id.fragment_container, fragmentToShow)
							.show(fragmentToShow)
							.commit()

					true
				})

		setContentView(layout.view)

		setSupportActionBar(layout.toolbar)

		onNewIntent(intent)
	}

	override fun onNewIntent(intent: Intent) {
		super.onNewIntent(intent)

		val meeting: Meeting? = intent.extras?.getParcelable(ARG_MEETING)

		if (meeting == null) {
			layout.bottomNavigationView.selectedItemId = R.id.nav_my_meetings
		} else {
			layout.bottomNavigationView.selectedItemId = R.id.nav_invitations

			startFragmentActivityForResult<FragmentActivity>(
					MeetingInvitationFragment::class.java.name,
					fragmentArgs = Bundle().apply {
						putParcelable(MeetingInvitationFragment.ARG_MEETING, meeting)
					},
					requestCode = RC_MEETING_INVITATION
			)
		}
	}

	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		super.onActivityResult(requestCode, resultCode, data)

		when (requestCode) {
			RC_CREATE_MEETING -> {
				if (resultCode == Activity.RESULT_OK) {
					MeetingCreatedDialog.getInstance().show(supportFragmentManager, MeetingCreatedDialog::class.java.name)
				}
			}
			RC_MEETING_DETAIL -> {
				if (resultCode == Activity.RESULT_OK) {
					MeetingCreatedDialog.getInstance().show(supportFragmentManager, MeetingCreatedDialog::class.java.name)
				}
			}
			RC_MEETING_INVITATION -> {
				if (resultCode == Activity.RESULT_OK) {
					MeetingConfirmedDialog.getInstance().show(supportFragmentManager, MeetingConfirmedDialog::class.java.name)
				}
			}
		}
	}

}
