package me.khol.ackeeplanner.screens.times.select

import android.Manifest
import android.annotation.SuppressLint
import android.support.annotation.RequiresPermission
import io.reactivex.Observable
import io.reactivex.functions.Function3
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import me.khol.ackeeplanner.App
import me.khol.ackeeplanner.Optional
import me.khol.ackeeplanner.model.api.meeting.Meeting
import me.khol.ackeeplanner.model.calendar.LocalCalendarRepository
import me.khol.ackeeplanner.model.event.Event
import me.khol.ackeeplanner.model.event.LocalEventRepository
import me.khol.ackeeplanner.screens.times.BaseTimesViewModel
import me.khol.ackeeplanner.screens.times.OverlayEvent
import javax.inject.Inject

/**
 * TODO: 11. 4. 2018 david.khol: add class description
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 11.04.2018
 **/
class TimesSelectViewModel @Inject constructor(
		app: App,
		calendarRepository: LocalCalendarRepository,
		eventRepository: LocalEventRepository
) : BaseTimesViewModel(app, calendarRepository, eventRepository) {

	sealed class State {
		class Loading : State()
		class Loaded(val events: List<Event>, val overlaidEvents: Set<OverlayEvent>, val selectedId: Int?) : State()
		class Error(val throwable: Throwable) : State()
	}

	private val stateSubject: Subject<State> = BehaviorSubject.createDefault(State.Loading())
	private val overlaidEventsSubject: Subject<Set<OverlayEvent>> = BehaviorSubject.createDefault(setOf())
	private val selectedEventIdSubject: Subject<Optional<Int>> = BehaviorSubject.createDefault(Optional.empty())

	private var overlayEvents: Set<OverlayEvent> = setOf()

	@SuppressLint("MissingPermission")
	@RequiresPermission(Manifest.permission.READ_CALENDAR)
	fun observeState(): Observable<State> {
		disposables += Observable.combineLatest(
				eventLoader.observeEvents(),
				overlaidEventsSubject,
				selectedEventIdSubject,
				Function3<List<Event>, Set<OverlayEvent>, Optional<Int>, State> { events, overlaidEvents, selectedId ->
					State.Loaded(events, overlaidEvents, selectedId.getNullable())
				})
				.subscribe({ state ->
					stateSubject.onNext(state)
				}, { t: Throwable ->
					stateSubject.onNext(State.Error(t))
				})

		return stateSubject
	}

	fun getSelectedEventId(): Int? = selectedEventIdSubject.blockingFirst().getNullable()

	fun setOverlaidDates(dates: List<Meeting.Date>, selectedId: Int?) {
		overlayEvents = mapDatesToOverlayEvents(dates)
		selectedEventIdSubject.onNext(Optional.ofNullable(selectedId))
		overlaidEventsSubject.onNext(overlayEvents)
	}

	fun onEventClick(event: me.khol.calendar.Event) {
		if (overlayEvents.filter { !it.isTaken }.map { it.id }.contains(event.id.toInt())) {
			selectedEventIdSubject.onNext(Optional.ofNullable(event.id.toInt()))
		}
	}

	fun onDateClick(event: OverlayEvent) {
		if (!event.isTaken) {
			selectedEventIdSubject.onNext(Optional.ofNullable(event.id))
		}
	}

}
