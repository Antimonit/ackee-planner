package me.khol.ackeeplanner.screens.meeting.invitation

import android.os.Bundle
import io.reactivex.BackpressureStrategy
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import me.khol.ackeeplanner.App
import me.khol.ackeeplanner.Optional
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.extensions.observeOnMainThread
import me.khol.ackeeplanner.extensions.subscribeOnIO
import me.khol.ackeeplanner.model.MeetingRepository
import me.khol.ackeeplanner.model.api.meeting.Meeting
import me.khol.ackeeplanner.model.calendar.LocalCalendarRepository
import me.khol.ackeeplanner.model.db.dao.UserDao
import me.khol.ackeeplanner.model.toUser
import me.khol.ackeeplanner.screens.calendar.SimplifiedCalendar
import me.khol.ackeeplanner.screens.meeting.base.BaseMeetingViewModel
import javax.inject.Inject

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 10.03.2018
 **/
class MeetingInvitationViewModel @Inject constructor(
		app: App,
		private val meetingRepository: MeetingRepository,
		private val calendarRepository: LocalCalendarRepository,
		private val userDao: UserDao
) : BaseMeetingViewModel(app) {

	private sealed class MeetingState {
		class Invalid(val invalidFields: List<String>): MeetingState()
		class Valid(val apiCall: Single<Meeting>): MeetingState()
	}

	private lateinit var meeting: Meeting

	private val titleSubject: Subject<String> = BehaviorSubject.createDefault("")
	private val locationSubject: Subject<String> = BehaviorSubject.createDefault("")
	private val datesSubject: Subject<List<Meeting.Date>> = BehaviorSubject.createDefault(listOf())
	private val ownerSubject: Subject<Optional<String>> = BehaviorSubject.createDefault(Optional.empty())
	private val calendarSubject: Subject<Optional<SimplifiedCalendar>> = BehaviorSubject.createDefault(Optional.empty())
	private val selectedTimeSubject: Subject<Optional<Meeting.Date>> = BehaviorSubject.createDefault(Optional.empty())
	private val continueClickSubject: Subject<Unit> = PublishSubject.create()
	private val acceptableSubject: Subject<Boolean> = BehaviorSubject.createDefault(true)

	fun observeTitle(): Observable<String> = titleSubject
	fun observeLocation(): Observable<String> = locationSubject
	fun observeDates(): Observable<List<Meeting.Date>> = datesSubject
	fun observeOwner(): Observable<Optional<String>> = ownerSubject
	fun observeCalendar(): Observable<Optional<SimplifiedCalendar>> = calendarSubject
	fun observeSelectedTime(): Observable<Optional<Meeting.Date>> = selectedTimeSubject

	/**
	 * Observe whether the user may accept this meeting. If the user previously accepted the meeting
	 * with one of his accounts, don't allow him to accept it again.
	 */
	fun observeAcceptable(): Observable<Boolean> = acceptableSubject

	fun setArguments(arguments: Bundle) {
		this.meeting = arguments.getParcelable(MeetingInvitationFragment.ARG_MEETING)
		titleSubject.onNext(meeting.description)
		locationSubject.onNext(meeting.location)
		ownerSubject.onNext(Optional.ofNullable(meeting.owner?.email))
		calendarSubject.onNext(Optional.empty())
		selectedTimeSubject.onNext(Optional.empty())
		datesSubject.onNext(meeting.dates)

		// finds out whether the user already accepted the meeting or not
		// TODO: rewrite this spaghetti
		disposables += calendarRepository
				.observeCalendarAccountsSafe()
				.subscribeOnIO()
				.toFlowable(BackpressureStrategy.LATEST)
				.switchMap { accounts ->
					val accountNames = accounts.map { account ->
						account.name
					}.toTypedArray()

					userDao.getUsersByEmailFlowable(*accountNames)
							.map { dbUsers ->
								dbUsers.map { it.toUser() }
							}
				}
				.map { users ->
					val userIds = users.map { it.id.toInt() }
					val acceptedDate: Meeting.Date? = meeting.dates.firstOrNull { date ->
						userIds.contains(date.user?.userId)
					}

					acceptedDate?.let {
						val user = users.first { it.id.toInt() == acceptedDate.user?.userId }
						val email = user.email
						val calendarId = acceptedDate.user?.calendarGoogleid
//						val calendarName: String? = null // don't know how to retrieve it here
						val acceptedCalendar = SimplifiedCalendar(calendarId, email, "", null)
						calendarSubject.onNext(Optional.of(acceptedCalendar))
						selectedTimeSubject.onNext(Optional.of(acceptedDate))
					}

					acceptedDate == null
				}
				.subscribe { isNotClaimedByUser ->
					acceptableSubject.onNext(isNotClaimedByUser)
				}
	}
	fun setCalendar(calendar: SimplifiedCalendar?) {
		calendarSubject.onNext(Optional.ofNullable(calendar))
	}
	fun setSelectedEventId(selectedEventId: Int?) {
		val date: Meeting.Date? = meeting.dates.find { it.id == selectedEventId }
		selectedTimeSubject.onNext(Optional.ofNullable(date))
	}
	fun onContinue() = continueClickSubject.onNext(Unit)

	init {
		val meetingStateObservable = Observable.combineLatest(
				calendarSubject,
				selectedTimeSubject,
				BiFunction { optionalCalendar: Optional<SimplifiedCalendar>,
							 optionalSelectedDate: Optional<Meeting.Date> ->

					val calendar = optionalCalendar.getNullable()
					val date = optionalSelectedDate.getNullable()

					val invalidFields = mutableListOf<String>()
					if (calendar == null) invalidFields += ctx.getString(R.string.error_empty_calendar)
					if (date == null) invalidFields += ctx.getString(R.string.error_empty_choose_times)

					if (invalidFields.isNotEmpty()) {
						MeetingState.Invalid(invalidFields)
					} else {
						MeetingState.Valid(
								meetingRepository.confirmMeeting(
										calendar!!.calendarId,
										calendar!!.name,
										calendar!!.email,
										meeting.token,
										meeting.id,
										date!!.id
								)
						)
					}
				})

		disposables += continueClickSubject.withLatestFrom(
				meetingStateObservable,
				BiFunction { _: Unit, state: MeetingState ->
					state
				}
		)
				.doOnNext {
					stateSubject.onNext(State.Loading())
				}
				.flatMapSingle { state ->
					when (state) {
						is MeetingState.Valid -> {
							state.apiCall
									.map<State> { _ ->
										State.Success()
									}
									.onErrorReturn { t ->
										State.NetworkError(t)
									}
						}
						is MeetingState.Invalid -> {
							Single.just(State.Invalid(state.invalidFields))
						}
					}
				}
				.observeOnMainThread()
				.subscribe({ state ->
					stateSubject.onNext(state)
				})
	}

}
