package me.khol.ackeeplanner.screens.times.calendars.epoxy

import android.content.res.ColorStateList
import android.support.v4.widget.CompoundButtonCompat
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.ackeeplanner.R
import me.khol.extensions.*
import me.khol.ackeeplanner.model.ColorTransformer
import me.khol.ackeeplanner.model.calendar.Calendar
import me.khol.ackeeplanner.screens.base.epoxy.BaseEpoxyModel
import me.khol.ackeeplanner.screens.base.layout.BaseLayout
import org.jetbrains.anko.*

/**
 * Displays a checkable view with a name of one of the user's calendars.
 *
 * This model is used in [AccountModel]'s [AccountModel.Layout.calendars] recycler view.
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 16.04.2018
 **/
open class CalendarModel : BaseEpoxyModel<CalendarModel.Layout>() {

	@EpoxyAttribute
	lateinit var calendar: Calendar

	@EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
	lateinit var onCalendarToggled: (Calendar, Boolean) -> Unit

	override fun createViewLayout(parent: ViewGroup) = Layout(parent)

	override fun bind(layout: Layout) {
		super.bind(layout)

		layout.txtCalendarName.text = if (calendar.isPrimary) "Events" else calendar.displayName
		layout.checkBox.isChecked = calendar.isVisible
		layout.view.setOnClickListener {
			layout.checkBox.performClick()
		}
		layout.checkBox.setOnCheckedChangeListener { _, isChecked ->
			onCalendarToggled(calendar, isChecked)
		}
		val transformedColor = ColorTransformer.getDisplayColor(calendar.color)
		CompoundButtonCompat.setButtonTintList(layout.checkBox, ColorStateList.valueOf(transformedColor))
	}

	override fun unbind(layout: Layout) {
		super.unbind(layout)

		layout.view.setOnClickListener(null)
		layout.checkBox.setOnClickListener(null)
	}

	@Suppress("MemberVisibilityCanBePrivate")
	class Layout(parent: ViewGroup) : BaseLayout(parent) {

		lateinit var txtCalendarName: TextView
		lateinit var checkBox: CheckBox

		override fun createView(ui: AnkoContext<ViewGroup>): View {
			return ui.constraintLayout {
				isClickable = true
				backgroundDrawable = getDrawableAttr(android.R.attr.selectableItemBackground)

				txtCalendarName = textView {
					textColor = getColor(R.color.textColorPrimary)
					textSize = 16f
					ellipsize = TextUtils.TruncateAt.END
				}

				checkBox = checkBox {
					isClickable = false
				}

				constraints {
					txtCalendarName
							.connect(VERTICAL of parentId)
							.connect(STARTS of parentId with 72.dp)
							.connect(END to START of checkBox)
							.size(matchConstraint, wrapContent)

					checkBox
							.connect(VERTICAL of parentId)
							.connect(ENDS of parentId)
							.size(48.dp, 48.dp)
				}
			}.apply {
				layoutParams = ViewGroup.LayoutParams(org.jetbrains.anko.matchParent, 48.dp)
			}
		}

	}

}
