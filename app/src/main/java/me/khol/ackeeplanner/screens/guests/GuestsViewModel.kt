package me.khol.ackeeplanner.screens.guests

import android.Manifest
import android.annotation.SuppressLint
import io.reactivex.Observable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import me.khol.ackeeplanner.App
import me.khol.ackeeplanner.extensions.hasPermissions
import me.khol.ackeeplanner.model.ContactsRepository
import me.khol.ackeeplanner.model.contacts.Contact
import me.khol.ackeeplanner.screens.base.viewmodel.ContextViewModel
import javax.inject.Inject

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 10.03.2018
 **/
class GuestsViewModel @Inject constructor(
		app: App,
		private val contactsRepository: ContactsRepository
) : ContextViewModel(app) {

	private val selectedContactList: MutableList<Contact> = mutableListOf()

	private val contactsSubject: Subject<List<Contact>> = BehaviorSubject.create()
	private val selectedContactsSubject: Subject<List<Contact>> = BehaviorSubject.create()
	private val recentContactsSubject: Subject<List<Contact>> = BehaviorSubject.create()

	val avatarLoader = contactsRepository.getAvatarLoader()

	fun observeContacts(): Observable<List<Contact>> = contactsSubject
	fun observeSelectedContacts(): Observable<List<Contact>> = selectedContactsSubject
	fun observeRecentContacts(): Observable<List<Contact>> = recentContactsSubject

	fun fetchContacts(query: CharSequence) {
		contactsRepository.getContacts(query)
				.subscribe({ it ->
					contactsSubject.onNext(it)
				})
	}

	init {
		disposables += contactsRepository.observeRecentContacts()
				.subscribe({ it ->
					recentContactsSubject.onNext(it)
				})
	}

	fun addSelectedContact(contact: Contact) {
		if (!selectedContactList.contains(contact)) {
			selectedContactList.add(contact)
			selectedContactsSubject.onNext(selectedContactList.toList())
		}
	}

	fun addRecentContacts(contacts: List<Contact>) {
		contactsRepository.addRecentContact(contacts).subscribe()
	}

	fun removeRecentContact(contact: Contact) {
		contactsRepository.removeRecentContacts(contact).subscribe()
	}

	fun removeAllRecentContacts() {
		contactsRepository.removeAllRecentContacts().subscribe()
	}

	@SuppressLint("MissingPermission")
	fun setSelectedContactIds(contacts: List<Contact>) {
		// If we have permission to read contacts, query for all real contacts and update their
		// info. It is possible someone's name or email changed when the app was not running
		if (ctx.hasPermissions(Manifest.permission.READ_CONTACTS)) {
			val customContacts = contacts.filter { it.rawContactId == null }
			val realContacts = contacts.filter { it.rawContactId != null }
			val realRawContactIds = realContacts.map { it.rawContactId!! }

			contactsRepository.getContactsByIds(realRawContactIds)
					.subscribe { it ->
						selectedContactList.clear()
						selectedContactList.addAll(customContacts)
						selectedContactList.addAll(it)
						selectedContactsSubject.onNext(selectedContactList.toList())
					}
		} else {
			selectedContactList.clear()
			selectedContactList.addAll(contacts)
			selectedContactsSubject.onNext(selectedContactList.toList())
		}
	}

	fun removeSelectedContact(contact: Contact) {
		selectedContactList.remove(contact)
		selectedContactsSubject.onNext(selectedContactList.toList())
	}

	fun getSelectedContacts(): List<Contact> {
		return selectedContactList.toList()
	}

}
