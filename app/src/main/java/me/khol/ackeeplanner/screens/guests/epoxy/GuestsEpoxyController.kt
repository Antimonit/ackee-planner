package me.khol.ackeeplanner.screens.guests.epoxy

import android.content.Context
import com.airbnb.epoxy.EpoxyController
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.model.contacts.AvatarLoader
import me.khol.ackeeplanner.model.contacts.Contact
import me.khol.ackeeplanner.screens.base.epoxy.missingPermission

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 29.03.2018
 **/
class GuestsEpoxyController(
		private val context: Context,
		private val onEmailAdded: (Contact) -> Unit,
		private val onEmailRemoved: (Contact) -> Unit,
		private val onPermissionClicked: () -> Unit,
		private val onDeleteRecentContactsClicked: () -> Unit,
		private val avatarLoader: AvatarLoader
) : EpoxyController() {

	var hasPermission = false
		set(value) {
			field = value
			requestModelBuild()
		}
	var queriedGuests: List<Contact> = listOf()
		set(value) {
			field = value
			requestModelBuild()
		}
	var selectedGuests: List<Contact> = listOf()
		set(value) {
			field = value
			requestModelBuild()
		}
	var recentGuests: List<Contact> = listOf()
		set(value) {
			field = value
			requestModelBuild()
		}
	var query: String = ""

	override fun buildModels() {
		if (!hasPermission) {
			missingPermission {
				id("missing_permission")
				title(context.getString(R.string.permission_missing_permission_optional))
				description(context.getString(R.string.permission_contacts_rationale))
				onClicked(onPermissionClicked)
			}
		}
		if (query.isEmpty()) {
			if (selectedGuests.isNotEmpty()) {
				header {
					id("invited_header")
					headerText(context.getString(R.string.guests_header_invited))
				}
				selectedGuests.forEach {
					selectedEmail {
						id("invited", it.rawContactId ?: it.email)
						account(it)
						avatarLoader(avatarLoader)
						onEmailRemoved(onEmailRemoved)
					}
				}
			}
			if (recentGuests.isNotEmpty()) {
				header {
					id("recent_header")
					headerText(context.getString(R.string.guests_header_recent))
					onClearClicked(onDeleteRecentContactsClicked)
				}
				recentGuests.forEach {
					email {
						id("recent", it.rawContactId ?: it.email)
						query(query)
						account(it)
						enabled(!selectedGuests.contains(it))
						avatarLoader(avatarLoader)
						onEmailAdded(onEmailAdded)
					}
				}
			}
		} else {
			queriedGuests.forEach {
				email {
					id("queried", it.rawContactId)
					query(query)
					account(it)
					enabled(!selectedGuests.contains(it))
					avatarLoader(avatarLoader)
					onEmailAdded(onEmailAdded)
				}
			}
		}
	}

}
