package me.khol.ackeeplanner.screens.times.create

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.annotation.RequiresPermission
import io.reactivex.rxkotlin.plusAssign
import me.khol.ackeeplanner.App
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.extensions.getViewModel
import me.khol.ackeeplanner.extensions.observeOnMainThread
import me.khol.ackeeplanner.model.ColorTransformer
import me.khol.ackeeplanner.screens.times.BaseTimesActivity
import me.khol.ackeeplanner.screens.times.CalendarUI
import me.khol.calendar.CalendarListener
import me.khol.calendar.Event
import org.threeten.bp.*
import org.threeten.bp.temporal.ChronoUnit

/**
 * An activity to browse a calendar with an ability to add and remove overlaid events.
 *
 * This screen is used in conjunction with
 * [CreateMeeting1Fragment][me.khol.ackeeplanner.screens.meeting.create.step1.CreateMeeting1Fragment].
 *
 * Created by David Khol [david@khol.me] on 4. 2. 2018.
 */
class TimesCreateActivity : BaseTimesActivity() {

	companion object {
		const val ARG_TITLE = "title"
		const val ARG_TIMES = "times"
		const val ARG_DURATION = "duration"
		const val RESULT_TIMES = "times"
		const val RESULT_DURATION = "duration"
	}

	override lateinit var layout: CalendarUI
	lateinit var viewModel: TimesCreateViewModel
	lateinit var controller: TimesCreateController

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		App.appComponent.inject(this)
		viewModel = getViewModel(vmFactory)
		viewModel.setArguments(intent.extras)

		controller = TimesCreateController(
				onTimeClick = { instant ->
					viewModel.removeOverlaidEvent(instant)
				}
		)

		layout = CalendarUI(
				rootView,
				controller.adapter,
				calendarListener = object : CalendarListener {
					override fun onEventClick(event: Event) {
						viewModel.removeOverlaidEvent(event.start.toLocalDateTime())
					}

					override fun onEventLongClick(event: Event) {
						viewModel.removeOverlaidEvent(event.start.toLocalDateTime())
					}

					override fun onEmptySpaceClick(dateTime: LocalDateTime) {
						// snap to 15 minutes intervals
						val snappedDateTime = dateTime.withMinute(dateTime.minute / 15 * 15)
						viewModel.addOverlaidEvent(snappedDateTime)
					}

					override fun onEmptySpaceLongClick(dateTime: LocalDateTime) {
						// snap to 15 minutes intervals
						val snappedDateTime = dateTime.withMinute(dateTime.minute / 15 * 15)
						viewModel.addOverlaidEvent(snappedDateTime)
					}

					override fun onDateChanged(date: LocalDate) {
						viewModel.onDateChanged(date)
						changeTitle(date)
					}
				},
				onDoneClick = {
					setResult(Activity.RESULT_OK, Intent().apply {
						putExtras(Bundle().apply {
							val times = viewModel.observeOverlaidEvents()
									.blockingFirst()
									.map { it.toInstant(ZoneOffset.UTC).toEpochMilli() }
									.toLongArray()
							val duration = viewModel.observeOverlaidDuration()
									.blockingFirst()
							putLongArray(RESULT_TIMES, times)
							putInt(RESULT_DURATION, duration)
						})
					})
					finish()
				}
		).apply {
			view // force lazy initialization
			calendar.allowInsertionOfEvents = true
			setBottomTitle(getString(R.string.create_time_slots))
			setBottomSubtitle(resources.getQuantityString(R.plurals.create_meeting_time_slot_created, 0, 0))
		}
		setContentView(layout.view)

		val startDay = LocalDate.now()

		layout.calendar.numberOfDays = 1
		layout.calendar.currentDate = startDay
		viewModel.onDateChanged(startDay)

		intent.extras
				.getLongArray(ARG_TIMES)
				.sorted()
				.map { Instant.ofEpochMilli(it) }
				.firstOrNull()?.let {
					val startDate = LocalDateTime.ofInstant(it, ZoneOffset.UTC).toLocalDate()
					layout.calendar.currentDate = startDate
					viewModel.onDateChanged(startDate)
				}

		setSupportActionBar(layout.toolbar)
		supportActionBar?.apply {
			setDisplayHomeAsUpEnabled(true)
		}

		checkCalendarPermission()
	}

	@SuppressLint("MissingPermission")
	@RequiresPermission(Manifest.permission.READ_CALENDAR)
	override fun onCalendarPermissionGranted() {
		disposables += viewModel.observeState()
				.observeOnMainThread()
				.subscribe({ state: TimesCreateViewModel.State ->
					when (state) {
						is TimesCreateViewModel.State.Loading -> {
							layout.showProgress(true)
						}
						is TimesCreateViewModel.State.Loaded -> {
							layout.showProgress(false)
							val defaultZoneId = ZoneId.systemDefault()

							val name = state.overlaidTitle
							val duration = state.overlaidDuration

							val overlaidEvents = state.overlaidEvents.map {
								Event(it.toString(),
										name,
										null,
										it.atZone(defaultZoneId),
										it.plus(duration.toLong(), ChronoUnit.MINUTES).atZone(defaultZoneId),
										false,
										overlaidColor(false),
										overlaidStatus(false),
										1)
							}
							val events = mapEvents(state.events, defaultZoneId)

							layout.calendar.events = events + overlaidEvents
							layout.calendar.eventDuration = duration

							val size = state.overlaidEvents.size
							layout.setBottomSubtitle(resources.getQuantityString(R.plurals.create_meeting_time_slot_created, size, size))
							layout.showDoneButton(size > 0)

							controller.events = state.overlaidEvents
						}
						is TimesCreateViewModel.State.Error -> {
							layout.showProgress(false)
							resolveErrorState(state.throwable)
						}
					}
				})
	}

    override fun getCurrentDuration(): Int {
        return viewModel.observeOverlaidDuration().blockingFirst()
    }

}
