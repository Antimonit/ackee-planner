package me.khol.ackeeplanner.screens.times.calendars

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import me.khol.ackeeplanner.App
import me.khol.ackeeplanner.extensions.getViewModel
import me.khol.ackeeplanner.extensions.hasPermissions
import me.khol.ackeeplanner.extensions.observeOnMainThread
import me.khol.ackeeplanner.screens.base.viewmodel.ViewModelFactory
import me.khol.ackeeplanner.screens.times.calendars.epoxy.AccountsEpoxyController
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import javax.inject.Inject

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 16.04.2018
 **/
class CalendarsDialogFragment : BottomSheetDialogFragment() {

	companion object {
		private const val REQUEST_PERMISSION_CALENDAR = 2
		private const val PERMISSION_READ_CALENDAR = Manifest.permission.READ_CALENDAR
		private const val PERMISSION_WRITE_CALENDAR = Manifest.permission.WRITE_CALENDAR
	}

	private lateinit var layout: CalendarsDialogUI
	private lateinit var viewModel: CalendarsDialogViewModel
	private lateinit var controller: AccountsEpoxyController

	val disposables = CompositeDisposable()

	@Inject
	lateinit var vmFactory: ViewModelFactory

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		App.appComponent.inject(this)
		viewModel = getViewModel(vmFactory)

		controller = AccountsEpoxyController(
				context!!,
				onCalendarToggled = { calendar, isToggled ->
					@SuppressLint("MissingPermission")
					if (context!!.hasPermissions(PERMISSION_WRITE_CALENDAR)) {
						viewModel.calendarToggled(calendar, isToggled)
					}
				},
				onPermissionClicked = {
					requestPermissions(arrayOf(PERMISSION_READ_CALENDAR, PERMISSION_WRITE_CALENDAR), REQUEST_PERMISSION_CALENDAR)
				}
		)

		observeAccounts()
	}

	override fun onDestroyView() {
		super.onDestroyView()
		disposables.clear()
	}

	@SuppressLint("RestrictedApi")
	override fun setupDialog(dialog: Dialog, style: Int) {
		super.setupDialog(dialog, style)

		layout = CalendarsDialogUI(context!!, controller.adapter)
		dialog.setContentView(layout.view)
	}

	@AfterPermissionGranted(REQUEST_PERMISSION_CALENDAR)
	private fun observeAccounts() {
		if (context!!.hasPermissions(PERMISSION_READ_CALENDAR, PERMISSION_WRITE_CALENDAR)) {
			controller.hasPermission = true
			@SuppressLint("MissingPermission")
			disposables += viewModel.observeAccounts()
					.observeOnMainThread()
					.subscribe { accounts ->
						controller.accounts = accounts
					}
		} else {
			controller.hasPermission = false
			if (!shouldShowRequestPermissionRationale(PERMISSION_READ_CALENDAR) &&
					!shouldShowRequestPermissionRationale(PERMISSION_WRITE_CALENDAR)) {
				requestPermissions(arrayOf(PERMISSION_READ_CALENDAR, PERMISSION_WRITE_CALENDAR), REQUEST_PERMISSION_CALENDAR)
			}
		}
	}

	override fun onRequestPermissionsResult(requestCode: Int,
											permissions: Array<String>,
											grantResults: IntArray) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults)
		EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
	}

}
