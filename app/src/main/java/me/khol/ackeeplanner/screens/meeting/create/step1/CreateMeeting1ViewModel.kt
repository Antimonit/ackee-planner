package me.khol.ackeeplanner.screens.meeting.create.step1

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.Function5
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.withLatestFrom
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import me.khol.ackeeplanner.App
import me.khol.ackeeplanner.Optional
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.extensions.formatInstant
import me.khol.ackeeplanner.extensions.observeOnMainThread
import me.khol.ackeeplanner.model.LocationRepository
import me.khol.ackeeplanner.model.MeetingRepository
import me.khol.ackeeplanner.model.api.meeting.Meeting
import me.khol.ackeeplanner.model.location.Location
import me.khol.ackeeplanner.screens.calendar.SimplifiedCalendar
import me.khol.ackeeplanner.screens.meeting.base.BaseMeetingViewModel
import org.threeten.bp.Instant
import javax.inject.Inject

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 10.03.2018
 **/
class CreateMeeting1ViewModel @Inject constructor(
		app: App,
		private val meetingRepository: MeetingRepository,
		locationRepository: LocationRepository
) : BaseMeetingViewModel(app) {

	open class Success(val meeting: Meeting) : State.Success()

	private sealed class MeetingState {
		class Invalid(val invalidFields: List<String>) : MeetingState()
		class Valid(val apiCall: Single<Meeting>) : MeetingState()
	}

	private val titleSubject: Subject<String> = BehaviorSubject.createDefault("")
	private val calendarSubject: Subject<Optional<SimplifiedCalendar>> = BehaviorSubject.createDefault(Optional.empty())
	private val timesSubject: Subject<List<Instant>> = BehaviorSubject.createDefault(listOf())
	private val durationSubject: Subject<Int> = BehaviorSubject.createDefault(60)
	private val locationSubject: Subject<Optional<Location>> = BehaviorSubject.create()
	private val continueClickSubject: Subject<Unit> = PublishSubject.create()

	// Fragment has no need of observing the title, because it changes the title directly without
	// any extra screens
	@Suppress("unused")
	fun observeTitle(): Observable<String> = titleSubject
	fun observeCalendar(): Observable<Optional<SimplifiedCalendar>> = calendarSubject
	fun observeTimes(): Observable<List<Instant>> = timesSubject
	fun observeDuration(): Observable<Int> = durationSubject
	fun observeLocation(): Observable<Optional<Location>> = locationSubject

	fun setTitle(title: String) = titleSubject.onNext(title)
	fun setCalendar(calendar: SimplifiedCalendar?) = calendarSubject.onNext(Optional.ofNullable(calendar))
	fun setTimes(times: List<Instant>) = timesSubject.onNext(times)
	fun setDuration(duration: Int) = durationSubject.onNext(duration)
	fun setLocation(place: Location?) = locationSubject.onNext(Optional.ofNullable(place))

	fun onContinue() = continueClickSubject.onNext(Unit)

	init {
		disposables += locationRepository.fetchAckeeLocation()
				.subscribe({ ackeeLocation ->
					locationSubject.onNext(Optional.ofNullable(ackeeLocation))
				})

		val meetingStateObservable = Observable.combineLatest(
				titleSubject,
				calendarSubject,
				timesSubject,
				durationSubject,
				locationSubject,
				Function5 { title: String,
							optionalCalendar: Optional<SimplifiedCalendar>,
							times: List<Instant>,
							duration: Int,
							optionalLocation: Optional<Location> ->

					val calendar = optionalCalendar.getNullable()
					val location = optionalLocation.getNullable()

					val invalidFields = mutableListOf<String>()
					if (title.isBlank()) invalidFields += ctx.getString(R.string.error_empty_title)
					if (calendar == null) invalidFields += ctx.getString(R.string.error_empty_calendar)
					if (times.isEmpty()) invalidFields += ctx.getString(R.string.error_empty_define_times)
					if (location == null) invalidFields += ctx.getString(R.string.error_empty_location)

					if (invalidFields.isNotEmpty()) {
						MeetingState.Invalid(invalidFields)
					} else {
						MeetingState.Valid(
								meetingRepository.createMeeting(
										"oneToMany",    // TODO: What does it mean?
										title,
										duration,
										calendar!!.calendarId!!,
										location!!.name,
										times.map { formatInstant(it) },
										emptyList()
								)
						)
					}
				})


		disposables += continueClickSubject.withLatestFrom(meetingStateObservable) { _, state -> state }
				.doOnNext {
					stateSubject.onNext(State.Loading())
				}
				.flatMapSingle { state ->
					when (state) {
						is MeetingState.Valid -> {
							state.apiCall
									.map<State> { meeting ->
										Success(meeting)
									}
									.onErrorReturn { t ->
										State.NetworkError(t)
									}
						}
						is MeetingState.Invalid -> {
							Single.just(State.Invalid(state.invalidFields))
						}
					}
				}
				.observeOnMainThread()
				.subscribe({ state ->
					stateSubject.onNext(state)
				})
	}

}
