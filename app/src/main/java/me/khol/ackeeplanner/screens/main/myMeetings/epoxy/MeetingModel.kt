package me.khol.ackeeplanner.screens.main.myMeetings.epoxy

import android.support.v7.widget.CardView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.model.api.meeting.Meeting
import me.khol.ackeeplanner.screens.base.epoxy.BaseEpoxyModel
import me.khol.ackeeplanner.screens.base.layout.BaseLayout
import me.khol.ackeeplanner.screens.base.layout.defaultTextView
import me.khol.ackeeplanner.screens.base.layout.textViewLeftDrawable
import me.khol.extensions.getDrawableAttr
import me.khol.extensions.isVisible
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 09.03.2018
 **/
@EpoxyModelClass
open class MeetingModel : BaseEpoxyModel<MeetingModel.Layout>() {

	@EpoxyAttribute
	lateinit var meeting: Meeting
	@EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
	lateinit var onCardClick: (Meeting) -> Unit

	override fun createViewLayout(parent: ViewGroup) = Layout(parent)

	override fun bind(layout: Layout) {
		super.bind(layout)
		layout.card.setOnClickListener {
			onCardClick(meeting)
		}
		val resources = layout.context.resources

		layout.txtTitle.text = resources.getString(R.string.my_meeting_card_title, meeting.description)
		layout.txtLocation.text = resources.getString(R.string.my_meeting_card_location, meeting.location)
		layout.txtTime.text = resources.getString(R.string.my_meeting_card_dates, meeting.dates.size)

		val groups: Map<Boolean, List<Meeting.Date>> = meeting.dates.groupBy { it.user != null }
		val acceptedSize = groups[true]?.size ?: 0
		layout.txtAccepted.isVisible = acceptedSize > 0
		layout.txtAccepted.text = resources.getQuantityString(R.plurals.detail_meeting_accepted_times, acceptedSize, acceptedSize)
	}

	override fun unbind(layout: Layout) {
		super.unbind(layout)
		layout.card.setOnClickListener(null)
	}

	class Layout(parent: ViewGroup) : BaseLayout(parent) {

		lateinit var card: CardView
		lateinit var txtTitle: TextView
		lateinit var txtTime: TextView
		lateinit var txtLocation: TextView
		lateinit var txtAccepted: TextView

		override fun createView(ui: AnkoContext<ViewGroup>): View {
			return ui.frameLayout {
				horizontalPadding = 8.dp

				card = cardView {
					cardElevation = 2.dpf
					useCompatPadding = true
					isClickable = true
					foreground = getDrawableAttr(android.R.attr.selectableItemBackground)

					constraintLayout {
						padding = 16.dp

						txtTitle = defaultTextView {
							textSize = 20f
						}

						txtLocation = textViewLeftDrawable(R.drawable.ic_place)

						txtTime = textViewLeftDrawable(R.drawable.ic_schedule)

						txtAccepted = textViewLeftDrawable(R.drawable.ic_done)

						constraints {
							txtTitle.connect(TOPS of parentId)
									.connect(STARTS of parentId)
									.size(matchConstraint, wrapContent)

							txtLocation
									.connect(TOP to BOTTOM of txtTitle with 8.dp)
									.connect(STARTS of parentId)

							txtTime.connect(TOP to BOTTOM of txtLocation with 4.dp)
									.connect(STARTS of parentId)

							txtAccepted.connect(TOP to BOTTOM of txtTime with 4.dp)
									.connect(STARTS of parentId)
						}
					}
				}.lparams(matchParent, wrapContent)

			}.apply {
				layoutParams = ViewGroup.LayoutParams(matchParent, wrapContent)
			}
		}

	}
}
