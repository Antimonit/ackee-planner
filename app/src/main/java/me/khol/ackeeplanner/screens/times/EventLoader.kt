package me.khol.ackeeplanner.screens.times

import android.Manifest
import android.annotation.SuppressLint
import android.support.annotation.RequiresPermission
import me.khol.extensions.toLocalDateTime
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import me.khol.ackeeplanner.extensions.safeDispose
import me.khol.ackeeplanner.model.calendar.Calendar
import me.khol.ackeeplanner.model.calendar.LocalCalendarRepository
import me.khol.ackeeplanner.model.event.Event
import me.khol.ackeeplanner.model.event.LocalEventRepository
import org.threeten.bp.LocalDate
import org.threeten.bp.ZoneOffset

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 09.04.2018
 **/
class EventLoader(
		private val calendarRepository: LocalCalendarRepository,
		private val eventRepository: LocalEventRepository
) : DateChangedListener {

	private val dateSubject: Subject<LocalDate> = BehaviorSubject.create()
	private var disposable: Disposable? = null

	@SuppressLint("MissingPermission")
	@RequiresPermission(Manifest.permission.READ_CALENDAR)
	fun observeEvents(): Observable<List<Event>> {
		val dateObservable = dateSubject
				.distinctUntilChanged { date: LocalDate ->
					date.month
				}

		val visibleCalendarIdsObservable = calendarRepository.observeVisibleCalendars()
				.map { calendars: List<Calendar> ->
					calendars
							.map { it.id }
							.toLongArray()
				}

		disposable = Observable.combineLatest(
				dateObservable,
				visibleCalendarIdsObservable,
				BiFunction { date: LocalDate, calendarIds: LongArray ->
					date to calendarIds
				})
				.subscribe { (date, calendarIds) ->
					val start = date.withDayOfMonth(1).minusWeeks(2)
					val end = start.plusMonths(2)

					eventRepository.fetchInstances(
							start.toLocalDateTime().atZone(ZoneOffset.systemDefault()).toInstant(),
							end.toLocalDateTime().atZone(ZoneOffset.systemDefault()).toInstant(),
							*calendarIds
					)
				}

		return eventRepository.observeEvents()
	}

	override fun onDateChanged(date: LocalDate) {
		dateSubject.onNext(date)
	}

	fun onCleared() {
		disposable.safeDispose()
	}

}
