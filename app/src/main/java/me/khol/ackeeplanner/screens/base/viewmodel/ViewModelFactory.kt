package me.khol.ackeeplanner.screens.base.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import javax.inject.Inject
import javax.inject.Provider

/**
 * Factory for providing general ViewModels
 *
 * @author David Bilik [david.bilik@ackee.cz]
 * @since 10/11/2017
 **/
class ViewModelFactory @Inject constructor(
		private val creators: MutableMap<Class<out ViewModel>, Provider<ViewModel>>
) : ViewModelProvider.Factory {

	@Suppress("UNCHECKED_CAST")
	override fun <T : ViewModel> create(modelClass: Class<T>): T {
		var creator: Provider<out ViewModel>? = creators[modelClass]
		if (creator == null) {
			for ((key, value) in creators) {
				if (modelClass.isAssignableFrom(key)) {
					creator = value
					break
				}
			}
		}

		if (creator == null) {
			throw IllegalArgumentException("Unknown model class $modelClass")
		}

		try {
			return creator.get() as T
		} catch (e: Exception) {
			throw RuntimeException(e)
		}
	}
}
