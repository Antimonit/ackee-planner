package me.khol.ackeeplanner.screens.calendar.epoxy

import android.content.Context
import com.airbnb.epoxy.EpoxyController
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.model.AccountRepository
import me.khol.ackeeplanner.model.calendar.Calendar
import me.khol.ackeeplanner.model.calendar.CalendarAccount
import me.khol.ackeeplanner.screens.base.epoxy.missingPermission
import me.khol.ackeeplanner.screens.calendar.SimplifiedCalendar

/**
 * Epoxy controller for peekable view at the bottom of the calendar
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 09.04.2018
 **/
open class ChooseCalendarController(
		private val context: Context,
		private val onCalendarSelected: (Calendar) -> Unit,
		private val onPermissionClicked: () -> Unit
) : EpoxyController() {

	var hasPermission = false
		set(value) {
			field = value
			requestModelBuild()
		}

	var signInState: AccountRepository.State = AccountRepository.State.SignedOut()
		set(value) {
			field = value
			requestModelBuild()
		}

	private var accounts: List<CalendarAccount> = emptyList()
	private var selectedCalendar: SimplifiedCalendar? = null

	fun setAccountsAndSelectedCalendar(accounts: List<CalendarAccount>, selectedCalendar: SimplifiedCalendar?) {
		this.accounts = accounts
		this.selectedCalendar = selectedCalendar
		requestModelBuild()
	}

	override fun buildModels() {
		if (!hasPermission) {
			missingPermission {
				id("missing_permission")
				title(context.getString(R.string.permission_missing_permission))
				description(context.getString(R.string.permission_calendar_rationale))
				onClicked(onPermissionClicked)
			}
		}
		accounts.forEach { account ->
			account {
				id(account.name)
				account(account)
				signInState(signInState)
				selectedCalendarId(selectedCalendar?.calendarId)
				onCalendarSelected(onCalendarSelected)
			}
		}
	}

}
