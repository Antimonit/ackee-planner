package me.khol.ackeeplanner.screens.calendar

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * TODO: Separate signed in and anonymous users
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 17.04.2018
 **/
@Parcelize
data class SimplifiedCalendar(
		val calendarId: String?,
		val name: String?,
		val email: String,
		val displayName: String?
) : Parcelable
