package me.khol.ackeeplanner.screens.base.epoxy

/**
 * Created by David Khol [david@khol.me] on 17. 12. 2017.
 */

import android.view.View
import android.view.ViewGroup
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelWithView
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.screens.base.layout.BaseLayout

/**
 * Epoxy model with [BaseLayout] implementation
 *
 * @author David Bilik
 * @since 24/04/17
 **/
abstract class BaseEpoxyModel<T : BaseLayout> : EpoxyModelWithView<View>() {

	@EpoxyAttribute
	var spanSize: Int = 1

	override fun getSpanSize(totalSpanCount: Int, position: Int, itemCount: Int): Int {
		return spanSize
	}

	abstract fun createViewLayout(parent: ViewGroup): T

	final override fun buildView(parent: ViewGroup): View {
		val layout: T = createViewLayout(parent)
		val view = layout.view
		view.setTag(R.id.epoxy_view_layout, layout)
		return view
	}

	final override fun bind(view: View) {
		super.bind(view)
		@Suppress("UNCHECKED_CAST")
		bind(view.getTag(R.id.epoxy_view_layout) as T)
	}

	open fun bind(layout: T) {

	}

	override fun unbind(view: View) {
		super.unbind(view)
		@Suppress("UNCHECKED_CAST")
		unbind(view.getTag(R.id.epoxy_view_layout) as T)
	}

	open fun unbind(layout: T) {

	}

}
