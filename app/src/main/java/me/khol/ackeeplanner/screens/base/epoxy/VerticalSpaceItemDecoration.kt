package me.khol.ackeeplanner.screens.base.epoxy

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View


/**
 * Adds a vertical space of size [verticalSpaceHeight] between each item of the recycler view.
 * Does not add the space after the last item.
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 23.04.2018
 **/
class VerticalSpaceItemDecoration(private val verticalSpaceHeight: Int) : RecyclerView.ItemDecoration() {

	override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
		if (parent.getChildAdapterPosition(view) != parent.adapter.itemCount - 1) {
			outRect.bottom = verticalSpaceHeight
		}
	}

}
