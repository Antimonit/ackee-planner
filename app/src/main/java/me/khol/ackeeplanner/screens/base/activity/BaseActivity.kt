package me.khol.ackeeplanner.screens.base.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.ViewGroup
import com.google.android.gms.common.GoogleApiAvailability
import io.reactivex.disposables.CompositeDisposable
import me.khol.ackeeplanner.App
import me.khol.ackeeplanner.screens.base.viewmodel.ViewModelFactory
import javax.inject.Inject

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 15.11.2017
 **/
abstract class BaseActivity : AppCompatActivity() {

	companion object {
		const val REQUEST_GOOGLE_PLAY_SERVICES = 9002
		const val RC_AUTHORIZATION = 1001
	}

	val disposables = CompositeDisposable()
	val rootView: ViewGroup by lazy { findViewById<ViewGroup>(android.R.id.content) }

	@Inject
	lateinit var vmFactory: ViewModelFactory

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		App.appComponent.inject(this)
	}

	override fun onDestroy() {
		super.onDestroy()
		disposables.clear()
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		if (item.itemId == android.R.id.home) {
			onBackPressed()
			return true
		}
		return super.onOptionsItemSelected(item)
	}

	/**
	 * Attempt to resolve a missing, out-of-date, invalid or disabled Google
	 * Play Services installation via a user dialog, if possible.
	 */
	fun acquireGooglePlayServices() {
		val apiAvailability = GoogleApiAvailability.getInstance()
		val connectionStatusCode = apiAvailability.isGooglePlayServicesAvailable(this)
		if (apiAvailability.isUserResolvableError(connectionStatusCode)) {
			showGooglePlayServicesErrorDialog(connectionStatusCode)
		}
	}

	/**
	 * Display an error dialog showing that Google Play Services is missing
	 * or out of date.
	 * @param connectionStatusCode code describing the presence (or lack of)
	 * Google Play Services on this device.
	 */
	fun showGooglePlayServicesErrorDialog(connectionStatusCode: Int) {
		GoogleApiAvailability.getInstance()
				.getErrorDialog(
						this,
						connectionStatusCode,
						REQUEST_GOOGLE_PLAY_SERVICES
				).show()
	}

}
