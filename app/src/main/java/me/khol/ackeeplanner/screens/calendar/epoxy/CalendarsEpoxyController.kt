package me.khol.ackeeplanner.screens.calendar.epoxy

import com.airbnb.epoxy.EpoxyController
import me.khol.ackeeplanner.model.calendar.Calendar

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 14.04.2018
 **/
class CalendarsEpoxyController(
		val onCalendarSelected: (Calendar) -> Unit
) : EpoxyController() {

	var calendars: List<Calendar> = emptyList()
		set(value) {
			field = value
			requestModelBuild()
		}

	var selectedCalendarId: String? = null
		set(value) {
			field = value
			requestModelBuild()
		}

	override fun buildModels() {
		calendars.forEach {
			calendar {
				id(it.id)
				calendar(it)
				selected(it.ownerAccount == selectedCalendarId)
				onCalendarSelected(onCalendarSelected)
			}
		}
	}

}
