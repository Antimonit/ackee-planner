package me.khol.ackeeplanner.screens.main.myMeetings.epoxy

import me.khol.ackeeplanner.model.api.meeting.Meeting
import me.khol.ackeeplanner.screens.main.base.epoxy.MeetingEpoxyController

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 09.03.2018
 **/
class MyMeetingsEpoxyController(
		private val onCardClick: (Meeting) -> Unit
) : MeetingEpoxyController() {

	override fun buildModels() {
		if (meetings.isEmpty()) {
			empty {
				id("empty")
			}
		}

		meetings.forEach {
			meeting {
				id(it.id)
				meeting(it)
				onCardClick(onCardClick)
			}
		}
	}

}
