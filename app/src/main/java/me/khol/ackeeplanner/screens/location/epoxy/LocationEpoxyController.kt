package me.khol.ackeeplanner.screens.location.epoxy

import android.content.Context
import com.airbnb.epoxy.EpoxyController
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.model.location.Location
import me.khol.ackeeplanner.screens.guests.epoxy.header

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 29.03.2018
 **/
class LocationEpoxyController(
		private val context: Context,
		private val onLocationClicked: (Location) -> Unit,
		private val onDeleteRecentLocationsClicked: () -> Unit
) : EpoxyController() {

	var predictions: List<Location> = emptyList()
		set(value) {
			field = value
			requestModelBuild()
		}

	var recentLocations: List<Location> = emptyList()
		set(value) {
			field = value
			requestModelBuild()
		}

	var query: String = ""

	override fun buildModels() {
		if (query.isEmpty()) {
			if (recentLocations.isNotEmpty()) {
				header {
					id("recent_header")
					headerText(context.getString(R.string.guests_header_recent))
					onClearClicked(onDeleteRecentLocationsClicked)
				}
				recentLocations.forEach {
					location {
						id("recent", it.name, it.address ?: "")
						location(it)
						onLocationClicked(onLocationClicked)
					}
				}
			}
		} else {
			predictions.forEach {
				location {
					id("prediction", it.id)
					location(it)
					onLocationClicked(onLocationClicked)
				}
			}
			poweredByGoogle {
				id("poweredByGoogle")
			}
		}
	}

}
