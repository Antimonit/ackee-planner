package me.khol.ackeeplanner.screens.times.epoxy

import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.extensions.font
import me.khol.ackeeplanner.screens.base.epoxy.BaseEpoxyModel
import me.khol.ackeeplanner.screens.base.layout.BaseLayout
import me.khol.ackeeplanner.screens.base.layout.defaultTextView
import me.khol.extensions.getColor
import org.jetbrains.anko.*
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 09.03.2018
 **/
@EpoxyModelClass
open class HeaderModel : BaseEpoxyModel<HeaderModel.Layout>() {

	companion object {
		private val sameYearFormatter = DateTimeFormatter.ofPattern("MMMM dd EEEE")
		private val differentYearFormatter = DateTimeFormatter.ofPattern("YYYY MMMM dd EEEE")
	}

	@EpoxyAttribute
	lateinit var date: LocalDate

	override fun createViewLayout(parent: ViewGroup) = Layout(parent)

	override fun bind(layout: Layout) {
		super.bind(layout)

		layout.txtTitle.text = if (LocalDate.now().year == date.year) {
			sameYearFormatter.format(date)
		} else {
			differentYearFormatter.format(date)
		}
	}

	class Layout(parent: ViewGroup) : BaseLayout(parent) {

		lateinit var txtTitle: TextView

		override fun createView(ui: AnkoContext<ViewGroup>): View {
			return ui.frameLayout {

				view {
					backgroundColor = getColor(R.color.black).withAlpha(20)
				}.lparams(matchParent, 1.dp)

				txtTitle = defaultTextView {
					textSize = 14f
					font = R.font.roboto_medium
					textColor = getColor(R.color.black_70)
					gravity = Gravity.CENTER_VERTICAL
					leftPadding = 16.dp
					topPadding = 4.dp
				}.lparams(matchParent, matchParent)

			}.apply {
				layoutParams = ViewGroup.LayoutParams(matchParent, 40.dp)
			}
		}

	}
}
