package me.khol.ackeeplanner.screens.location

import android.app.Activity
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.airbnb.epoxy.EpoxyTouchHelper
import com.jakewharton.rxbinding2.widget.textChanges
import io.reactivex.rxkotlin.plusAssign
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.extensions.getViewModel
import me.khol.ackeeplanner.extensions.observeOnMainThread
import me.khol.ackeeplanner.extensions.showSnack
import me.khol.ackeeplanner.extensions.subscribeOnNewThread
import me.khol.ackeeplanner.model.location.Location
import me.khol.ackeeplanner.screens.base.activity.BaseActivity
import me.khol.ackeeplanner.screens.location.epoxy.LocationEpoxyController
import me.khol.ackeeplanner.screens.location.epoxy.LocationModel
import org.jetbrains.anko.appcompat.v7.navigationIconResource
import java.util.concurrent.TimeUnit

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 11.03.2018
 **/
class LocationActivity : BaseActivity() {

	companion object {
		const val RESULT_LOCATION = "location"
	}

	private lateinit var layout: LocationUI
	private lateinit var viewModel: LocationViewModel
	private lateinit var controller: LocationEpoxyController

	private lateinit var doneItem: MenuItem

	private var originalUpButton: Drawable? = null
	private var isInputMode: Boolean = false
		set(value) {
			if (field == value)
				return

			field = value
			if (field) {
				originalUpButton = layout.toolbar.navigationIcon
				layout.toolbar.navigationIconResource = R.drawable.ic_close
			} else {
				layout.toolbar.navigationIcon = originalUpButton
			}
		}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		viewModel = getViewModel(vmFactory)

		controller = LocationEpoxyController(
				this,
				onLocationClicked = { location: Location ->
					viewModel.onLocationClicked(location)
				},
				onDeleteRecentLocationsClicked = {
					viewModel.removeAllRecentLocations()
				}
		)

		layout = LocationUI(rootView, controller.adapter)
		setContentView(layout.view)

		EpoxyTouchHelper.initSwiping(layout.addresses)
				.leftAndRight()
				.withTarget(LocationModel::class.java)
				.andCallbacks(object : EpoxyTouchHelper.SwipeCallbacks<LocationModel>() {
					override fun onSwipeCompleted(model: LocationModel, itemView: View, position: Int, direction: Int) {
						viewModel.removeRecentLocation(model.location)
					}
				})

		disposables += layout.searchEdit.textChanges()
				.skipInitialValue()
				.doOnNext { query ->
					doneItem.isVisible = query.isNotEmpty()

					isInputMode = query.isNotEmpty()
				}
				.debounce(300, TimeUnit.MILLISECONDS)
				.subscribeOnNewThread()
				.subscribe { query: CharSequence ->
					controller.query = query.toString()
					viewModel.fetchLocationPredictions(query.toString())
				}

		disposables += viewModel.observeLocationPredictions()
				.observeOnMainThread()
				.subscribe({ predictions ->
					controller.predictions = predictions
					layout.addresses.layoutManager.scrollToPosition(0)
				})

		disposables += viewModel.observeLocation()
				.observeOnMainThread()
				.subscribe({ location ->
					setResult(Activity.RESULT_OK, Intent().apply {
						putExtras(Bundle().apply {
							putParcelable(RESULT_LOCATION, location)
						})
					})
					finish()
				})

		disposables += viewModel.observeRecentLocations()
				.observeOnMainThread()
				.subscribe({ recentLocations ->
					controller.recentLocations = recentLocations
					layout.addresses.layoutManager.scrollToPosition(0)
				}, { t ->
					t.printStackTrace()
				})

		disposables += viewModel.observeErrors()
				.observeOnMainThread()
				.subscribe({ error ->
					error.printStackTrace()
					layout.view.showSnack(error.message ?: "Undefined error has occurred.")
				})

		setSupportActionBar(layout.toolbar)
		supportActionBar?.apply {
			setDisplayHomeAsUpEnabled(true)
			setDisplayShowTitleEnabled(false)
		}
	}

	override fun onNavigateUp(): Boolean {
		return if (isInputMode) {
			layout.clearSearchQuery()
			false
		} else {
			super.onNavigateUp()
		}
	}

	override fun onBackPressed() {
		if (isInputMode) {
			layout.clearSearchQuery()
		} else {
			super.onBackPressed()
		}
	}

	override fun onCreateOptionsMenu(menu: Menu): Boolean {
		menuInflater.inflate(R.menu.location, menu)
		doneItem = menu.findItem(R.id.action_done)
		return true
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		when (item.itemId) {
			R.id.action_done -> {
				val query = layout.getSearchQuery()
				viewModel.onDoneClicked(query)
			}
		}
		return super.onOptionsItemSelected(item)
	}

}
