package me.khol.ackeeplanner.screens.dialogs

import android.app.Dialog
import android.os.Bundle
import android.support.annotation.DrawableRes
import android.support.annotation.StringRes
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.view.Gravity
import android.widget.FrameLayout
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.extensions.font
import me.khol.extensions.dp
import me.khol.extensions.getColor
import org.jetbrains.anko.*
import org.jetbrains.anko.support.v4.UI

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 06.04.2018
 **/
open class DialogWithImageHeader : DialogFragment() {

	companion object {
		const val ARG_TITLE = "title"
		const val ARG_MESSAGE = "message"
		const val ARG_HEADER_IMAGE = "header_image"
		const val ARG_CONFIRMATION_TEXT = "confirmation_text"

		fun getInstance(
				@StringRes titleRes: Int,
				@StringRes messageRes: Int,
				@DrawableRes headerRes: Int,
				@StringRes confirmationTextRes: Int
		): DialogFragment {
			return DialogWithImageHeader().apply {
				arguments = Bundle().apply {
					putInt(ARG_TITLE, titleRes)
					putInt(ARG_MESSAGE, messageRes)
					putInt(ARG_HEADER_IMAGE, headerRes)
					putInt(ARG_CONFIRMATION_TEXT, confirmationTextRes)
				}
			}
		}
	}

	override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
		val titleRes = arguments!!.getInt(ARG_TITLE)
		val messageRes = arguments!!.getInt(ARG_MESSAGE)
		val headerImageRes = arguments!!.getInt(ARG_HEADER_IMAGE)
		val confirmationTextRes = arguments!!.getInt(ARG_CONFIRMATION_TEXT)

		val customTitle: AnkoContext<Fragment> = UI {
			frameLayout {
				backgroundResource = headerImageRes
				textView(titleRes) {
					textSize = 24f
					font = R.font.roboto
					textColorResource = R.color.white
					leftPadding = ctx.dp(24)
					bottomPadding = ctx.dp(20)
					topPadding = ctx.dp(20)
				}.lparams(matchParent, wrapContent) {
					gravity = Gravity.BOTTOM
				}

			}.apply {
				layoutParams = FrameLayout.LayoutParams(matchParent, ctx.dp(144))
			}
		}

		val customView = UI {
			textView(messageRes) {
				textSize = 14f
				textColorResource = R.color.textColorPrimary
				font = R.font.roboto_light
				horizontalPadding = ctx.dp(24)
				topPadding = ctx.dp(24)
				bottomPadding = ctx.dp(8)
			}
		}

		return AlertDialog.Builder(context!!)
				.setCustomTitle(customTitle.view)
				.setView(customView.view)
				.setPositiveButton(confirmationTextRes, null)
				.create()
	}

	override fun onResume() {
		super.onResume()
		(dialog as AlertDialog).apply {
			getButton(AlertDialog.BUTTON_POSITIVE).apply {
				font = R.font.roboto_light
				textColor = getColor(R.color.black)
			}
		}
	}

}
