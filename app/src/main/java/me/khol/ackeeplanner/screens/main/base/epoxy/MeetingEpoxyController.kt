package me.khol.ackeeplanner.screens.main.base.epoxy

import com.airbnb.epoxy.EpoxyController
import com.airbnb.epoxy.TypedEpoxyController
import me.khol.ackeeplanner.model.api.meeting.Meeting

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 26.04.2018
 **/
abstract class MeetingEpoxyController : EpoxyController() {

	var meetings: List<Meeting> = emptyList()
		set(value) {
			field = value
			requestModelBuild()
		}

}
