package me.khol.ackeeplanner.screens.meeting.base

import android.view.ViewGroup
import me.khol.ackeeplanner.screens.base.layout.BaseLayout
import me.khol.ackeeplanner.screens.meeting.ProgressButtonUI

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 02.05.2018
 **/
abstract class BaseMeetingUI(
		parent: ViewGroup
) : BaseLayout(parent) {

	lateinit var next: ProgressButtonUI

}
