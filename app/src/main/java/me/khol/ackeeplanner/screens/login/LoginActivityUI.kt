package me.khol.ackeeplanner.screens.login

import android.support.constraint.Group
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import cz.ackee.ankoconstraintlayout.constraintLayout
import cz.ackee.ankoconstraintlayout.group
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.screens.base.layout.BaseLayout
import me.khol.extensions.getColor
import org.jetbrains.anko.*

/**
 * Displays a button to log in when user is logged out. Displays two buttons when logged in, one to
 * log out, the other one to revoke access. Revoking access removes the access to Google data
 * permission from the backend.
 *
 * Continue button is used to escape from this screen.
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 28.11.2017
 **/
class LoginActivityUI(
		parent: ViewGroup,
		val onLogInClicked: () -> Unit = {},
		val onLogOutClicked: () -> Unit = {},
		val onRevokeClicked: () -> Unit = {},
		val onContinueClicked: () -> Unit = {}
) : BaseLayout(parent) {

	lateinit var txtAccount: TextView
	lateinit var txtLoginState: TextView

	lateinit var progressLogIn: ProgressBar
	lateinit var btnLogIn: Button
	lateinit var btnLogOut: Button
	lateinit var btnRevoke: Button
	lateinit var btnContinue: Button

	lateinit var groupLogOut: Group

	override fun createView(ui: AnkoContext<ViewGroup>): View {
		return ui.constraintLayout {

			padding = 16.dp

			progressLogIn = progressBar()

			txtLoginState = textView("Login state") {
				textColor = getColor(R.color.textColorPrimary)
			}

			txtAccount = textView("CalendarAccount") {
				textColor = getColor(R.color.textColorSecondary)
			}

			btnLogIn = button("Log in") {
				setOnClickListener {
					onLogInClicked()
				}
			}

			btnLogOut = button("Log out") {
				setOnClickListener {
					onLogOutClicked()
				}
			}

			btnRevoke = button("Revoke") {
				setOnClickListener {
					onRevokeClicked()
				}
			}

			btnContinue = button("Continue") {
				setOnClickListener {
					onContinueClicked()
				}
			}

			groupLogOut = group(btnLogOut, btnRevoke)
			groupLogOut.visibility = View.INVISIBLE

			constraints {
				txtLoginState.width(matchConstraint)
				txtLoginState.connect(HORIZONTAL of parentId)
				txtLoginState.connect(BOTTOM to TOP of txtAccount)

				txtAccount.width(matchConstraint)
				txtAccount.connect(HORIZONTAL of parentId)
				txtAccount.connect(BOTTOM to TOP of btnLogIn)

				btnContinue.connect(HORIZONTAL of parentId)
				btnContinue.connect(BOTTOMS of parentId)
				btnContinue.width(matchConstraint)

				btnLogIn.connect(HORIZONTAL of parentId)
				btnLogIn.connect(BOTTOM to TOP of btnContinue with 8.dp)
				btnLogIn.width(matchConstraint)

				progressLogIn.connect(ALL of btnLogIn)

				arrayOf(btnLogOut, btnRevoke)
						.chainSpread(START of btnLogIn, END of btnLogIn, floatArrayOf(1f, 1f))
						.forEach { view ->
							view.width(matchConstraint)
							view.connect(VERTICAL of btnLogIn)
						}
			}
		}
	}
}
