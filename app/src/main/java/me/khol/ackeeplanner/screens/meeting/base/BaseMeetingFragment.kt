package me.khol.ackeeplanner.screens.meeting.base

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import io.reactivex.rxkotlin.plusAssign
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.extensions.observeOnMainThread
import me.khol.ackeeplanner.extensions.showSnack
import me.khol.ackeeplanner.model.api.meeting.Meeting
import me.khol.ackeeplanner.model.contacts.Contact
import me.khol.ackeeplanner.screens.base.fragment.BaseFragment
import me.khol.ackeeplanner.screens.calendar.ChooseCalendarActivity
import me.khol.ackeeplanner.screens.calendar.SimplifiedCalendar
import me.khol.ackeeplanner.screens.guests.GuestsActivity
import me.khol.ackeeplanner.screens.location.LocationActivity
import me.khol.ackeeplanner.screens.meeting.ProgressButtonUI
import me.khol.ackeeplanner.screens.meeting.create.step1.CreateMeeting1Fragment
import me.khol.ackeeplanner.screens.times.create.TimesCreateActivity
import me.khol.ackeeplanner.screens.times.select.TimesSelectActivity
import me.khol.ackeeplanner.screens.times.view.TimesViewActivity
import org.threeten.bp.Instant
import retrofit2.HttpException

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 02.05.2018
 **/
open class BaseMeetingFragment<UI : BaseMeetingUI, VM : BaseMeetingViewModel> : BaseFragment() {

	companion object {
		const val ARG_MEETING = "meeting"

		const val RC_CALENDAR = 1
		const val RC_TIMES = 2
		const val RC_LOCATION = 5
		const val RC_GUESTS = 6
	}

	open lateinit var layout: UI
	open lateinit var viewModel: VM

	open fun onStateInitial(state: BaseMeetingViewModel.State.Initial) {
	}

	open fun onStateLoading(state: BaseMeetingViewModel.State.Loading) {
	}

	open fun onStateInvalid(state: BaseMeetingViewModel.State.Invalid) {
		layout.view.showSnack(state.invalidFields.joinToString("\n"))
	}

	open fun onStateSuccess(state: BaseMeetingViewModel.State.Success) {
		activity.setResult(RESULT_OK)
		activity.finish()
	}

	open fun onStateNetworkError(state: BaseMeetingViewModel.State.NetworkError) {
		if (state.throwable is HttpException) {
			state.throwable.code()
			view?.showSnack(state.throwable.message(), Snackbar.LENGTH_LONG)
		} else {
			view?.showSnack(getString(R.string.unknown_error), Snackbar.LENGTH_LONG)
		}
	}

	fun initializeStateObservable() {
		disposables += viewModel.observeState()
				.observeOnMainThread()
				.subscribe { state: BaseMeetingViewModel.State ->
					when (state) {
						is BaseMeetingViewModel.State.Initial -> {
							layout.next.setState(ProgressButtonUI.State.NORMAL)
							onStateInitial(state)
						}
						is BaseMeetingViewModel.State.Loading -> {
							layout.next.setState(ProgressButtonUI.State.LOADING)
							onStateLoading(state)
						}
						is BaseMeetingViewModel.State.Invalid -> {
							layout.next.setState(ProgressButtonUI.State.NORMAL)
							onStateInvalid(state)
						}
						is BaseMeetingViewModel.State.Success -> {
							layout.next.setState(ProgressButtonUI.State.FINISHED)
							onStateSuccess(state)
						}
						is BaseMeetingViewModel.State.NetworkError -> {
							layout.next.setState(ProgressButtonUI.State.NORMAL)
							state.throwable.printStackTrace()
							onStateNetworkError(state)
						}
					}
				}
	}


	protected fun openCalendarActivity(calendar: SimplifiedCalendar?, anonymousAllowed: Boolean) {
		val intent = Intent(context, ChooseCalendarActivity::class.java).apply {
			putExtra(ChooseCalendarActivity.ARG_CALENDAR, calendar)
			putExtra(ChooseCalendarActivity.ARG_ANONYMOUS_ALLOWED, anonymousAllowed)
		}
		startActivityForResult(intent, RC_CALENDAR)
	}

	protected fun openLocationActivity() {
		val intent = Intent(context, LocationActivity::class.java)
		startActivityForResult(intent, RC_LOCATION)
	}

	protected fun openGoogleMaps(location: String) {
		if (location.isNotBlank()) {
			val gmmIntentUri = Uri.parse("geo:0,0?q=$location")
			val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri).apply {
				`package` = "com.google.android.apps.maps"
			}
			startActivity(mapIntent)
		}
	}

	protected fun openTimesActivity(times: List<Meeting.Date>) {
		val intent = Intent(context, TimesViewActivity::class.java).apply {
			putParcelableArrayListExtra(TimesViewActivity.ARG_DATES, ArrayList(times))
		}
		startActivity(intent)
	}

	protected fun openTimesActivity(times: List<Meeting.Date>, selectedTime: Meeting.Date?) {
		val intent = Intent(context, TimesSelectActivity::class.java).apply {
			putParcelableArrayListExtra(TimesSelectActivity.ARG_DATES, ArrayList(times))
			selectedTime?.let {
				putExtra(TimesSelectActivity.ARG_SELECTED_DATE, it.id)
			}
		}
		startActivityForResult(intent, RC_TIMES)
	}

	protected fun openTimesActivity(instants: List<Instant>, title: String, duration: Int) {
		val times = instants.map { it.toEpochMilli() }.toLongArray()
		val intent = Intent(context, TimesCreateActivity::class.java).apply {
			putExtra(TimesCreateActivity.ARG_TITLE, title)
			putExtra(TimesCreateActivity.ARG_TIMES, times)
			putExtra(TimesCreateActivity.ARG_DURATION, duration)
		}
		startActivityForResult(intent, RC_TIMES)
	}

	protected fun openGuestsActivity(guests: List<Contact>) {
		val intent = Intent(context, GuestsActivity::class.java).apply {
			putExtras(Bundle().apply {
				putParcelableArrayList(GuestsActivity.ARG_GUESTS, ArrayList(guests))
			})
		}
		startActivityForResult(intent, RC_GUESTS)
	}

	fun shareText(text: String) {
		startActivity(Intent().apply {
			action = Intent.ACTION_SEND
			type = "text/plain"
			putExtra(Intent.EXTRA_TEXT, text)
		})
	}

}
