package me.khol.ackeeplanner.screens.times.calendars

import android.Manifest
import android.annotation.SuppressLint
import android.support.annotation.RequiresPermission
import io.reactivex.Observable
import me.khol.ackeeplanner.model.calendar.LocalCalendarRepository
import me.khol.ackeeplanner.model.calendar.Calendar
import me.khol.ackeeplanner.model.calendar.CalendarAccount
import me.khol.ackeeplanner.screens.base.viewmodel.BaseViewModel
import javax.inject.Inject

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 16.04.2018
 **/
class CalendarsDialogViewModel @Inject constructor(
		private val calendarAccountRepository: LocalCalendarRepository
) : BaseViewModel() {

	@SuppressLint("MissingPermission")
	@RequiresPermission(Manifest.permission.READ_CALENDAR)
	fun observeAccounts(): Observable<List<CalendarAccount>> {
		return calendarAccountRepository.observeCalendarAccounts()
	}

	@SuppressLint("MissingPermission")
	@RequiresPermission(Manifest.permission.WRITE_CALENDAR)
	fun calendarToggled(calendar: Calendar, toggled: Boolean) {
		calendarAccountRepository.updateVisibilityOfCalendar(calendar.id, toggled)
				.subscribe()
	}

}
