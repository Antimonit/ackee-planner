package me.khol.ackeeplanner.screens.deeplink

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.extensions.getViewModel
import me.khol.ackeeplanner.extensions.observeOnMainThread
import me.khol.ackeeplanner.extensions.showSnackWithAction
import me.khol.ackeeplanner.screens.base.activity.BaseActivity
import me.khol.ackeeplanner.screens.main.MainActivity
import me.khol.extensions.isVisible
import timber.log.Timber

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 11.03.2018
 **/
class DeepLinkActivity : BaseActivity() {

	private lateinit var layout: DeepLinkUI
	private lateinit var viewModel: DeepLinkViewModel


	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		viewModel = getViewModel(vmFactory)

		layout = DeepLinkUI(rootView)
		setContentView(layout.view)

		onNewIntent(intent)

		viewModel.observeState()
				.observeOnMainThread()
				.subscribe({ state ->
					when (state) {
						is DeepLinkViewModel.State.Loading -> {
							layout.progress.isVisible = true
						}
						is DeepLinkViewModel.State.Success -> {
							layout.progress.isVisible = false
							finish()
							startActivity(Intent(this, MainActivity::class.java).apply {
								putExtra(MainActivity.ARG_MEETING, state.meeting)
							})
						}
						is DeepLinkViewModel.State.Error -> {
							layout.progress.isVisible = false
							val message = state.throwable.message ?: getString(R.string.error_occurred)
							layout.view.showSnackWithAction(message, "Retry") {
								onNewIntent(intent)
							}
						}
					}
				})
	}

	override fun onNewIntent(intent: Intent) {
		val action = intent.action
		val dataUri: Uri? = intent.data
		if (Intent.ACTION_VIEW == action && dataUri != null) {
			val segments = dataUri.pathSegments
			if (segments.size >= 3) {
				val token = segments[segments.size - 1]
				val id = segments[segments.size - 2].toIntOrNull()
				val go = segments[segments.size - 3]
				Timber.i("Segments: %s, %s, %s", go, id, token)
				if (id != null) {
					viewModel.addMeeting(token, id)
					return
				}
			}
		}

		// received invalid uri, just open main activity
		finish()
		startActivity(Intent(this, MainActivity::class.java))
	}
}
