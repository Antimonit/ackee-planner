package me.khol.ackeeplanner.screens.meeting.create.step1

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import io.reactivex.rxkotlin.plusAssign
import me.khol.ackeeplanner.extensions.formatRelativeToToday
import me.khol.ackeeplanner.extensions.getViewModel
import me.khol.ackeeplanner.extensions.observeOnMainThread
import me.khol.ackeeplanner.model.location.Location
import me.khol.ackeeplanner.screens.base.activity.FragmentActivity
import me.khol.ackeeplanner.screens.base.activity.startFragmentActivityForResult
import me.khol.ackeeplanner.screens.calendar.ChooseCalendarActivity
import me.khol.ackeeplanner.screens.calendar.SimplifiedCalendar
import me.khol.ackeeplanner.screens.location.LocationActivity
import me.khol.ackeeplanner.screens.meeting.base.BaseMeetingFragment
import me.khol.ackeeplanner.screens.meeting.base.BaseMeetingViewModel
import me.khol.ackeeplanner.screens.meeting.create.step2.CreateMeeting2Fragment
import me.khol.ackeeplanner.screens.meeting.truncateMoreThanElements
import me.khol.ackeeplanner.screens.times.create.TimesCreateActivity
import me.khol.extensions.isVisible
import org.threeten.bp.Instant

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 11.03.2018
 **/
class CreateMeeting1Fragment : BaseMeetingFragment<CreateMeeting1UI, CreateMeeting1ViewModel>() {

	companion object {
		private const val RC_STEP_2 = 7
	}

	override lateinit var layout: CreateMeeting1UI
	override lateinit var viewModel: CreateMeeting1ViewModel

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		viewModel = getViewModel(vmFactory)
	}

	override fun onStateSuccess(state: BaseMeetingViewModel.State.Success) {
		val successState = state as CreateMeeting1ViewModel.Success

		activity.finish()
		startFragmentActivityForResult<FragmentActivity>(
				CreateMeeting2Fragment::class.java.name,
				fragmentArgs = Bundle().apply {
					putParcelable(CreateMeeting2Fragment.ARG_MEETING, successState.meeting)
				},
				requestCode = RC_STEP_2
		)
	}


	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
		layout = CreateMeeting1UI(
				container!!,
				onTitleTextChange = { viewModel.setTitle(it) },
				onCalendarClick = { openCalendarActivity(viewModel.observeCalendar().blockingFirst().getNullable(), false) },
				onTimesClick = {
					super.openTimesActivity(
							viewModel.observeTimes().blockingFirst(),
							viewModel.observeTitle().blockingFirst(),
							viewModel.observeDuration().blockingFirst()
					)
				},
				onLocationClick = { openLocationActivity() },
				onContinueClick = { viewModel.onContinue() }
		)
		layout.view

		layout.mapView.onCreate(savedInstanceState)

		initializeStateObservable()

		disposables += viewModel.observeCalendar()
				.observeOnMainThread()
				.subscribe { optionalCalendar ->
					optionalCalendar.getNullable()?.let {
						if (it.displayName != null) {
							layout.calendar.setText(it.displayName)
						} else {
							layout.calendar.setText(it.name)
						}
						layout.calendar.setSubtitle(it.email)
					}
				}

		disposables += viewModel.observeTimes()
				.observeOnMainThread()
				.subscribe { times ->
					val mappedTimes = times
							.sorted()
							.map { it.formatRelativeToToday() }
					layout.times.setText(truncateMoreThanElements(mappedTimes, 5))
				}

		disposables += viewModel.observeLocation()
				.observeOnMainThread()
				.subscribe { optionalLocation ->
					val location = optionalLocation.getNullable()
					layout.location.setText(location?.name)
					layout.location.setSubtitle(location?.address)
					layout.mapView.getMapAsync { map ->
						map.setOnMapClickListener { openLocationActivity() }
						map.setOnMapLongClickListener(null)
						if (location?.viewport == null) {
							layout.mapFrame.isVisible = false
						} else {
							layout.mapFrame.isVisible = true
							map.moveCamera(CameraUpdateFactory.newLatLngBounds(location.viewport, 0))
						}
					}
				}

		activity.setSupportActionBar(layout.toolbar)
		activity.supportActionBar?.apply {
			setDisplayHomeAsUpEnabled(true)
		}

		return layout.view
	}

	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		super.onActivityResult(requestCode, resultCode, data)
		if (resultCode == RESULT_OK) {
			when (requestCode) {
				RC_CALENDAR -> {
					val calendar = data?.extras?.getParcelable<SimplifiedCalendar>(ChooseCalendarActivity.RESULT_CALENDAR)
					viewModel.setCalendar(calendar)
				}
				RC_LOCATION -> {
					val location: Location? = data?.extras?.getParcelable(LocationActivity.RESULT_LOCATION)
					viewModel.setLocation(location)
				}
				RC_TIMES -> {
					data?.extras?.let {
						if (it.containsKey(TimesCreateActivity.RESULT_TIMES)) {
							val times = it.getLongArray(TimesCreateActivity.RESULT_TIMES)
							viewModel.setTimes(times.map { Instant.ofEpochMilli(it) })
						}
						if (it.containsKey(TimesCreateActivity.RESULT_DURATION)) {
							val duration = it.getInt(TimesCreateActivity.RESULT_DURATION)
							viewModel.setDuration(duration)
						}
					}
				}
				RC_STEP_2 -> {
					activity.setResult(RESULT_OK)
					activity.finish()
				}
			}
		}
	}

}
