package me.khol.ackeeplanner.screens.base.viewmodel

import android.arch.lifecycle.AndroidViewModel
import io.reactivex.disposables.CompositeDisposable
import me.khol.ackeeplanner.App

/**
 * A ViewModel with context that automatically disposes of its [disposables]
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 08.03.2018
 **/
abstract class ContextViewModel(app: App) : AndroidViewModel(app) {

	/**
	 * Automatically release all disposables when this view model is no longer needed
	 */
	val disposables = CompositeDisposable()

	override fun onCleared() {
		super.onCleared()
		disposables.clear()
	}

	/**
	 * Short syntax for getApplication()
	 */
	val ctx: App
		get() = getApplication()

}
