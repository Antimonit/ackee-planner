package me.khol.ackeeplanner.screens.main.invitations

import me.khol.ackeeplanner.App
import me.khol.ackeeplanner.model.MeetingRepository
import me.khol.ackeeplanner.model.api.meeting.Meeting
import me.khol.ackeeplanner.model.calendar.LocalCalendarRepository
import me.khol.ackeeplanner.model.db.dao.UserDao
import me.khol.ackeeplanner.screens.main.base.MeetingViewModel
import javax.inject.Inject

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 09.03.2018
 **/
class InvitationsViewModel @Inject constructor(
        app: App,
        userDao: UserDao,
        calendarRepository: LocalCalendarRepository,
        meetingRepository: MeetingRepository
) : MeetingViewModel(app, calendarRepository, userDao, meetingRepository) {

    override fun filterMeetings(userIds: List<Int>, meetings: List<Meeting>): List<Meeting> {
        return meetings.filterNot { meeting ->
            userIds.contains(meeting.ownerId)
        }
    }

}
