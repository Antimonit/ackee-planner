package me.khol.ackeeplanner.screens.main.base

import android.Manifest
import android.annotation.SuppressLint
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.processors.FlowableProcessor
import io.reactivex.processors.PublishProcessor
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import me.khol.ackeeplanner.App
import me.khol.ackeeplanner.extensions.subscribeOnIO
import me.khol.ackeeplanner.model.MeetingRepository
import me.khol.ackeeplanner.model.api.meeting.Meeting
import me.khol.ackeeplanner.model.calendar.LocalCalendarRepository
import me.khol.ackeeplanner.model.db.dao.UserDao
import me.khol.ackeeplanner.model.toUser
import me.khol.ackeeplanner.screens.base.viewmodel.ContextViewModel

/**
 * TODO: 26. 4. 2018 david.khol: add class description
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 26.04.2018
 **/
abstract class MeetingViewModel constructor(
        app: App,
        private val calendarRepository: LocalCalendarRepository,
        private val userDao: UserDao,
        private val meetingRepository: MeetingRepository
) : ContextViewModel(app) {

    sealed class State {
        class Initial : State()
        class Loading : State()
        class Loaded(val meetings: List<Meeting>) : State()
        class Error(val throwable: Throwable) : State()
    }

    private val stateSubject: Subject<State> = BehaviorSubject.createDefault(State.Initial())
    fun observeState(): Observable<State> = stateSubject

	private val reloadProcessor: FlowableProcessor<Unit> = PublishProcessor.create()
	fun getMeetings() = reloadProcessor.onNext(Unit)

    abstract fun filterMeetings(userIds: List<Int>, meetings: List<Meeting>): List<Meeting>

	private val userIdsFlowable: Flowable<List<Int>>
	fun observeUserIds(): Flowable<List<Int>> = userIdsFlowable

    init {
		userIdsFlowable = calendarRepository
				.observeCalendarAccountsSafe()
				.subscribeOnIO()
				.toFlowable(BackpressureStrategy.LATEST)
				.switchMap { accounts ->
					val accountNames = accounts.map { account ->
						account.name
					}.toTypedArray()

					userDao.getUsersByEmailFlowable(*accountNames)
							.subscribeOnIO()
							.map { dbUsers ->
								dbUsers.map { it.toUser() }
							}
				}
				.map {
					it.map { it.id.toInt() }
				}

		disposables += Flowable.combineLatest(
				userIdsFlowable,
				reloadProcessor,
				BiFunction { calendars: List<Int>, _: Unit ->
					calendars
				}
		)
				.doOnNext {
					stateSubject.onNext(State.Loading())
				}
				.flatMap { userIds: List<Int> ->
					meetingRepository.getMeetings()
							.map { meetings ->
								filterMeetings(userIds, meetings)
							}
				}
				.subscribe({ meetings ->
					stateSubject.onNext(State.Loaded(meetings))
				}, { throwable ->
					stateSubject.onNext(State.Error(throwable))
				})


    }


}
