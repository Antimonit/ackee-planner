package me.khol.ackeeplanner.screens.base.layout

import android.support.v4.widget.TextViewCompat
import android.view.ViewManager
import android.widget.TextView
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.extensions.font
import me.khol.extensions.dp
import me.khol.extensions.getColor
import me.khol.extensions.getTintedDrawable
import org.jetbrains.anko.textColor
import org.jetbrains.anko.textView

/**
 * TODO: 13. 3. 2018 david.khol: add class description
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 13.03.2018
 **/

fun ViewManager.defaultTextView(text: CharSequence = "", init: TextView.() -> Unit = {}): TextView {
	return textView {
		this.text = text
		font = R.font.roboto
		textColor = getColor(R.color.textColorPrimary)
		init()
	}
}

fun ViewManager.textViewLeftDrawable(drawableRes: Int, text: CharSequence = "", init: TextView.() -> Unit = {}): TextView {
	return textView {
		this.text = text
		font = R.font.roboto
		textColor = getColor(R.color.textColorSecondary)

		val drawable = getTintedDrawable(drawableRes, R.color.black_70)?.apply {
			setBounds(0, 0, context.dp(16), context.dp(16))
		}
		TextViewCompat.setCompoundDrawablesRelative(this, drawable, null, null, null)
		compoundDrawablePadding = context.dp(8)

		init()
	}
}

fun TextView.labelTextViewStyle() {
	font = R.font.roboto_medium
	textSize = 14f
	textColor = getColor(R.color.textColorPrimary)
}

fun ViewManager.labelTextView(text: CharSequence = "", init: TextView.() -> Unit = {}): TextView {
	return textView {
		labelTextViewStyle()
		this.text = text
		init()
	}
}
