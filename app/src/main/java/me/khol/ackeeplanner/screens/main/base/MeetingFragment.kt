package me.khol.ackeeplanner.screens.main.base

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException
import io.reactivex.rxkotlin.plusAssign
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.extensions.*
import me.khol.ackeeplanner.screens.base.activity.BaseActivity
import me.khol.ackeeplanner.screens.base.fragment.BaseFragment
import me.khol.ackeeplanner.screens.main.base.epoxy.MeetingEpoxyController
import pub.devrel.easypermissions.EasyPermissions

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 26.04.2018
 **/
abstract class MeetingFragment<UI: MeetingUI, ViewModel: MeetingViewModel, Controller: MeetingEpoxyController> : BaseFragment() {

    open lateinit var layout: UI
    open lateinit var viewModel: ViewModel
    open lateinit var epoxyController: Controller

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        disposables += viewModel.observeState()
                .observeOnMainThread()
                .subscribe { state: MeetingViewModel.State ->
                    fun showProgress(show: Boolean) {
                        layout.swipeRefreshLayout.isRefreshing = show
                    }
                    when (state) {
                        is MeetingViewModel.State.Initial -> {
                            showProgress(false)
                        }
                        is MeetingViewModel.State.Loading -> {
                            showProgress(true)
                        }
                        is MeetingViewModel.State.Loaded -> {
                            showProgress(false)
                            epoxyController.meetings = state.meetings
							layout.eventsRecycler.scheduleLayoutAnimation()
                            layout.eventsRecycler.invalidate()
                        }
                        is MeetingViewModel.State.Error -> {
                            showProgress(false)
                            val t = state.throwable
                            when (t) {
                                is GooglePlayServicesAvailabilityIOException -> showGooglePlayServicesErrorDialog(t.connectionStatusCode)
                                is UserRecoverableAuthIOException -> startActivityForResult(t.intent, BaseActivity.RC_AUTHORIZATION)
                                else -> {
                                    t.printStackTrace()
                                    view?.showSnack(getString(R.string.error_occurred, t.message))
                                }
                            }
                        }
                    }
                }
    }

    override fun onStart() {
        super.onStart()
        getMeetings()
    }

    protected fun getMeetings() {
        if (!context!!.isGooglePlayServicesAvailable()) {
            acquireGooglePlayServices()
            return
        }

        viewModel.getMeetings()

        if (!context!!.isDeviceOnline()) {
            layout.view.displayNoInternetSnack()
            return
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            BaseActivity.REQUEST_GOOGLE_PLAY_SERVICES -> {
                if (resultCode == Activity.RESULT_OK) {
                    getMeetings()
                } else {
                    view?.showSnack(getString(R.string.error_gps_required))
                }
            }
            BaseActivity.RC_AUTHORIZATION -> {
                if (resultCode == Activity.RESULT_OK) {
                    getMeetings()
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }


}
