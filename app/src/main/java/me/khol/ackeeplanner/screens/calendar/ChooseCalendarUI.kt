package me.khol.ackeeplanner.screens.calendar

import android.support.design.widget.TextInputLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SimpleItemAnimator
import android.support.v7.widget.Toolbar
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import cz.ackee.ankoconstraintlayout._ConstraintSet
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.ackeeplanner.R
import me.khol.ackeeplanner.screens.base.epoxy.VerticalSpaceItemDecoration
import me.khol.ackeeplanner.screens.base.layout.BaseLayout
import me.khol.ackeeplanner.screens.base.layout.defaultTextView
import me.khol.ackeeplanner.screens.meeting.ProgressButtonUI
import me.khol.extensions.beginDelayedTransition
import me.khol.extensions.elevationCompat
import me.khol.extensions.getColor
import me.khol.extensions.getDrawableAttr
import me.khol.extensions.withAlpha
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.titleResource
import org.jetbrains.anko.appcompat.v7.toolbar
import org.jetbrains.anko.design.appBarLayout
import org.jetbrains.anko.design.textInputEditText
import org.jetbrains.anko.design.textInputLayout
import org.jetbrains.anko.recyclerview.v7.recyclerView

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 07.04.2018
 **/
class ChooseCalendarUI(
		parent: ViewGroup,
		private val adapter: RecyclerView.Adapter<*>,
		private val anonymousAllowed: Boolean,
		private val onAnonymousAccountClicked: (String, String) -> Unit
) : BaseLayout(parent) {

	lateinit var toolbar: Toolbar
	private lateinit var recycler: View
	private lateinit var anonymousLogIn: View

	private var anonymousLogInToggled = false

	private lateinit var calendarConstraints: _ConstraintSet
	private lateinit var anonymousConstraints: _ConstraintSet

	lateinit var txtInputAnonymousName: TextInputLayout
	lateinit var txtInputAnonymousEmail: TextInputLayout
	lateinit var txtAnonymousName: EditText
	lateinit var txtAnonymousEmail: EditText
//	private lateinit var btnAnonymousAccept: Button

	lateinit var next: ProgressButtonUI


	override fun createView(ui: AnkoContext<ViewGroup>): View {
		return ui.constraintLayout {
			fitsSystemWindows = true
			backgroundColor = getColor(R.color.black).withAlpha(10)

			val appBar = appBarLayout {
				backgroundResource = R.color.white

				toolbar = toolbar {
					fitsSystemWindows = true
					titleResource = R.string.choose_calendar_toolbar_title
				}.lparams(org.jetbrains.anko.matchParent, org.jetbrains.anko.wrapContent) {
					scrollFlags = 0
				}
			}

			recycler = recyclerView {
				bottomPadding = 16.dp
				clipToPadding = false
				clipChildren = false

				layoutManager = LinearLayoutManager(context)
				adapter = this@ChooseCalendarUI.adapter

				(itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
				itemAnimator.changeDuration = 0

				addItemDecoration(VerticalSpaceItemDecoration(16.dp))
			}

			val toggleButton = frameLayout {
				isClickable = true
				foreground = getDrawableAttr(android.R.attr.selectableItemBackground)
				backgroundResource = R.color.white
				elevationCompat = 4.dpf

				val txtToggle = defaultTextView {
					textResource = R.string.choose_calendar_log_in_anonymous
					textSize = 16f
				}.lparams(org.jetbrains.anko.wrapContent, org.jetbrains.anko.wrapContent) {
					gravity = Gravity.CENTER
				}

				setOnClickListener {
					this@constraintLayout.beginDelayedTransition()
					if (anonymousLogInToggled) {
						txtToggle.setText(R.string.choose_calendar_log_in_anonymous)
						calendarConstraints
					} else {
						txtToggle.setText(R.string.choose_calendar_log_in_google)
						anonymousConstraints
					}.applyTo(this@constraintLayout)
					anonymousLogInToggled = !anonymousLogInToggled
				}

				visibility = if (anonymousAllowed) View.VISIBLE else View.GONE
			}

			anonymousLogIn = constraintLayout {
				verticalPadding = 16.dp
				isClickable = true
				backgroundResource = R.color.white

				txtInputAnonymousName = textInputLayout {
					id = R.id.choose_calendar_anonymous_name
					hint = "Name"
					txtAnonymousName = textInputEditText()
				}
				txtInputAnonymousEmail = textInputLayout {
					id = R.id.choose_calendar_anonymous_email
					hint = "Email"
					txtAnonymousEmail = textInputEditText()
				}

				next = ProgressButtonUI(
						this,
						R.string.detail_meeting_confirm,
						R.string.detail_meeting_success,
						onContinueClick = {
							onAnonymousAccountClicked(
									txtAnonymousName.text.toString(),
									txtAnonymousEmail.text.toString()
							)
						}
				).apply {
					addView(view)
				}


				constraints {
					txtInputAnonymousName.connect(
							HORIZONTAL of parentId with 16.dp,
							TOPS of parentId
					).size(matchConstraint, wrapContent)

					txtInputAnonymousEmail.connect(
							HORIZONTAL of parentId with 16.dp,
							TOP to BOTTOM of txtInputAnonymousName
					).size(matchConstraint, wrapContent)

					next.view.connect(
							STARTS of txtInputAnonymousName,
							TOP to BOTTOM of txtInputAnonymousEmail
					).size(wrapContent, wrapContent)
				}

				visibility = if (anonymousAllowed) View.VISIBLE else View.GONE
			}

			constraints {
				appBar.connect(TOPS of parentId)
						.connect(HORIZONTAL of parentId)
						.size(matchConstraint, wrapContent)

				recycler.connect(TOP to BOTTOM of appBar)
						.connect(BOTTOMS of parentId with if (anonymousAllowed) 64.dp else 0)
						.connect(HORIZONTAL of parentId)
						.size(matchConstraint, matchConstraint)

				anonymousLogIn
						.connect(TOP to BOTTOM of toggleButton)
						.connect(BOTTOMS of parentId)
						.connect(HORIZONTAL of parentId)
						.size(matchConstraint, matchConstraint)

				toggleButton
						.connect(HORIZONTAL of parentId)
						.size(matchConstraint, 64.dp)
			}

			calendarConstraints = prepareConstraints {
				toggleButton
						.clear(TOP)
						.connect(BOTTOMS of parentId)
			}

			anonymousConstraints = prepareConstraints {
				toggleButton
						.clear(BOTTOM)
						.connect(TOP to BOTTOM of appBar)
			}

			calendarConstraints.applyTo(this@constraintLayout)

		}.also {
			it.layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)
		}
	}

}

