package me.khol.ackeeplanner.screens.times

import org.threeten.bp.LocalDate

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 09.04.2018
 **/
interface DateChangedListener {

	fun onDateChanged(date: LocalDate)

}
