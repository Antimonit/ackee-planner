package me.khol.ackeeplanner.screens.times

import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.CoordinatorLayout
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.ackeeplanner.R
import me.khol.extensions.elevationCompat
import me.khol.extensions.getColor
import me.khol.ackeeplanner.screens.base.layout.BaseLayout
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 10.04.2018
 **/

class PeekableUI(
		parent: ViewGroup,
		private val adapter: RecyclerView.Adapter<*>
) : BaseLayout(parent) {

	companion object {
		const val PEEKABLE_HEIGHT = 80
	}

	private lateinit var peekableHeader: View
	private lateinit var peekableRecycler: RecyclerView

	private lateinit var peekableTitle: TextView
	private lateinit var peekableSubtitle: TextView

	private lateinit var peekableBehavior: BottomSheetBehavior<View>

	var title: CharSequence
		set(value) {
			peekableTitle.text = value
		}
		get() = peekableTitle.text

	var subtitle: CharSequence
		set(value) {
			peekableSubtitle.text = value
		}
		get() = peekableSubtitle.text


	override fun createView(ui: AnkoContext<ViewGroup>): View {
		return ui.verticalLayout {
			backgroundResource = R.color.white
			elevationCompat = 4.dpf

			peekableHeader = constraintLayout {
				backgroundResource = R.color.white
				elevationCompat = 2.dpf

				setOnClickListener {
					peekableBehavior.state = when (peekableBehavior.state) {
						BottomSheetBehavior.STATE_COLLAPSED -> BottomSheetBehavior.STATE_EXPANDED
						BottomSheetBehavior.STATE_EXPANDED -> BottomSheetBehavior.STATE_COLLAPSED
						else -> peekableBehavior.state
					}
				}

				peekableTitle = textView {
					textSize = 16f
					textColorResource = R.color.textColorPrimary
				}
				peekableSubtitle = textView {
					textSize = 14f
					textColorResource = R.color.textColorSecondary
				}

				constraints {
					peekableTitle
							.connect(STARTS of parentId with 16.dp)
							.connect(TOPS of parentId with 16.dp)

					peekableSubtitle
							.connect(STARTS of peekableTitle)
							.connect(TOP to BOTTOM of peekableTitle with 4.dp)
				}
			}.lparams(matchParent, PEEKABLE_HEIGHT.dp)

			peekableRecycler = recyclerView {
				backgroundColor = getColor(R.color.black).withAlpha(10)
				adapter = this@PeekableUI.adapter
				layoutManager = LinearLayoutManager(context)
				itemAnimator = DefaultItemAnimator()
			}.lparams(matchParent, 240.dp)

			layoutParams = CoordinatorLayout.LayoutParams(matchParent, wrapContent).apply {
				behavior = BottomSheetBehavior<View>()
			}

			peekableBehavior = BottomSheetBehavior.from(this)
			peekableBehavior.peekHeight = PEEKABLE_HEIGHT.dp
			peekableBehavior.isHideable = false
			peekableBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
				override fun onStateChanged(bottomSheet: View, newState: Int) {}
				override fun onSlide(bottomSheet: View, slideOffset: Float) {}
			})
		}
	}

}
