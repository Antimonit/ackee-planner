package me.khol.ackeeplanner.screens.times.epoxy

import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import me.khol.ackeeplanner.screens.base.epoxy.BaseEpoxyModel
import me.khol.ackeeplanner.screens.base.layout.BaseLayout
import me.khol.ackeeplanner.screens.base.layout.defaultTextView
import me.khol.extensions.getDrawableAttr
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.horizontalPadding
import org.jetbrains.anko.matchParent
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 09.03.2018
 **/
@EpoxyModelClass
open class CreateTimeModel : BaseEpoxyModel<CreateTimeModel.Layout>() {

	companion object {
		private val timeFormatter = DateTimeFormatter.ofPattern("HH:mm")
	}

	@EpoxyAttribute
	lateinit var time: LocalDateTime
	@EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
	lateinit var onTimeClick: (LocalDateTime) -> Unit

	override fun createViewLayout(parent: ViewGroup) = Layout(parent)

	override fun bind(layout: Layout) {
		super.bind(layout)
		layout.view.setOnClickListener {
			onTimeClick(time)
		}

		val start = time.toLocalTime()
		val end = start.plusHours(1)
		val text = "${timeFormatter.format(start)} - ${timeFormatter.format(end)}"

		layout.txtTitle.text = text
	}

	override fun unbind(layout: Layout) {
		super.unbind(layout)
		layout.view.setOnClickListener(null)
	}

	class Layout(parent: ViewGroup) : BaseLayout(parent) {

		lateinit var txtTitle: TextView

		override fun createView(ui: AnkoContext<ViewGroup>): View {
			return ui.frameLayout {
				foreground = getDrawableAttr(android.R.attr.selectableItemBackground)
				isClickable = true

				txtTitle = defaultTextView {
					textSize = 16f
					gravity = Gravity.CENTER_VERTICAL
					horizontalPadding = 16.dp
				}.lparams(matchParent, matchParent)

			}.apply {
				layoutParams = ViewGroup.LayoutParams(matchParent, 48.dp)
			}
		}

	}
}
