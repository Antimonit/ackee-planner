@file:Suppress("unused")

package me.khol.ackeeplanner

/**
 * A container object which may or may not contain a non-null value.
 * If a value is present, `isPresent()` will return `true` and
 * `get()` will return the value.
 *
 *
 * Additional methods that depend on the presence or absence of a contained
 * value are provided, such as [orElse()][.orElse]
 * (return a default value if value not present) and
 * [ifPresent()][.ifPresent] (execute a block
 * of code if the value is present).
 *
 * @since 1.8
 */
class Optional<T> {

	/**
	 * If non-null, the value; if null, indicates no value is present
	 */
	private val value: T?

	/**
	 * Return `true` if there is a value present, otherwise `false`.
	 *
	 * @return `true` if there is a value present, otherwise `false`
	 */
	val isPresent: Boolean
		get() = value != null

	/**
	 * Constructs an empty instance.
	 *
	 * @implNote Generally only one empty instance, [Optional.EMPTY],
	 * should exist per VM.
	 */
	private constructor() {
		this.value = null
	}

	/**
	 * Constructs an instance with the value present.
	 *
	 * @param value the non-null value to be present
	 * @throws NullPointerException if value is null
	 */
	private constructor(value: T) {
		if (value == null)
			throw NullPointerException()
		this.value = value
	}

	fun get(): T {
		if (value == null) {
			throw NoSuchElementException("No value present")
		}
		return value
	}

	fun getNullable(): T? {
		return value
	}

	fun orElse(other: T): T {
		return value ?: other
	}


	/**
	 * Indicates whether some other object is "equal to" this Optional. The
	 * other object is considered equal if:
	 *
	 *  * it is also an `Optional` and;
	 *  * both instances have no value present or;
	 *  * the present values are "equal to" each other via `equals()`.
	 *
	 *
	 * @param other an object to be tested for equality
	 * @return {code true} if the other object is "equal to" this object
	 * otherwise `false`
	 */
	override fun equals(other: Any?): Boolean {
		if (this === other) {
			return true
		}

		if (other !is Optional<*>) {
			return false
		}

		return value == other.value
	}

	/**
	 * Returns a non-empty string representation of this Optional suitable for
	 * debugging. The exact presentation format is unspecified and may vary
	 * between implementations and versions.
	 *
	 * @implSpec If a value is present the result must include its string
	 * representation in the result. Empty and present Optionals must be
	 * unambiguously differentiable.
	 *
	 * @return the string representation of this instance
	 */
	override fun toString(): String {
		return if (value != null)
			"Optional[$value]"
		else
			"Optional.empty"
	}

	override fun hashCode(): Int {
		return value?.hashCode() ?: 0
	}

	companion object {
		/**
		 * Common instance for `empty()`.
		 */
		private val EMPTY = Optional<Any>()

		/**
		 * Returns an empty `Optional` instance.  No value is present for this
		 * Optional.
		 *
		 * @apiNote Though it may be tempting to do so, avoid testing if an object
		 * is empty by comparing with `==` against instances returned by
		 * `Option.empty()`. There is no guarantee that it is a singleton.
		 * Instead, use [.isPresent].
		 *
		 * @param <T> Type of the non-existent value
		 * @return an empty `Optional`
		</T> */
		fun <T> empty(): Optional<T> {
			@Suppress("UNCHECKED_CAST")
			return EMPTY as Optional<T>
		}

		/**
		 * Returns an `Optional` with the specified present non-null value.
		 *
		 * @param <T> the class of the value
		 * @param value the value to be present, which must be non-null
		 * @return an `Optional` with the value present
		 * @throws NullPointerException if value is null
		</T> */
		fun <T> of(value: T): Optional<T> {
			return Optional(value)
		}

		/**
		 * Returns an `Optional` describing the specified value, if non-null,
		 * otherwise returns an empty `Optional`.
		 *
		 * @param <T> the class of the value
		 * @param value the possibly-null value to describe
		 * @return an `Optional` with a present value if the specified value
		 * is non-null, otherwise an empty `Optional`
		</T> */
		fun <T> ofNullable(value: T?): Optional<T> {
			return if (value == null) empty() else of(value)
		}
	}
}
