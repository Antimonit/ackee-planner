package me.khol.ackeeplanner.model.api.meeting

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import me.khol.ackeeplanner.model.api.User
import me.khol.ackeeplanner.model.db.meeting.DBMeeting
import me.khol.ackeeplanner.model.db.meeting.DBMeetingDate
import me.khol.ackeeplanner.model.db.meeting.DBMeetingDateUser
import org.threeten.bp.Instant

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 03.04.2018
 **/
@Parcelize
data class Meeting(
		val id: Int,
		val ownerId: Int,
		val token: String,
		val description: String,
		val durationMinutes: Int,
		val type: String,
		val isTaken: Int,
		val calendarGoogleid: String,
		val location: String,
		val dates: List<Meeting.Date>,
		val owner: User?,
		val canUserConfirm: Boolean?
) : Parcelable {

	@Parcelize
	data class Date(
			val id: Int,
			val meetingId: Int,
			val startDate: Instant,
			val endDate: Instant,
			val user: Meeting.Date.User?
	) : Parcelable {

		@Parcelize
		data class User(
				val id: Int,
				val userId: Int,
				val meetingsHasDateId: Int,
				val meetingId: Int,
				val calendarGoogleid: String?,
				val email: String?,
				val name: String?
		) : Parcelable

	}

}
