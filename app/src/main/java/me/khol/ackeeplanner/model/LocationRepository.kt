package me.khol.ackeeplanner.model

import android.content.Context
import com.google.android.gms.location.places.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.tasks.Task
import io.reactivex.*
import me.khol.ackeeplanner.extensions.subscribeOnNewThread
import me.khol.ackeeplanner.model.db.dao.LocationDao
import me.khol.ackeeplanner.model.db.location.DBRecentCustomLocation
import me.khol.ackeeplanner.model.db.location.DBRecentLocation
import me.khol.ackeeplanner.model.location.Location
import timber.log.Timber
import java.lang.Exception
import javax.inject.Inject

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 13.04.2018
 **/

interface LocationRepository {
	fun fetchAckeeLocation(): Maybe<Location>
	fun fetchLocationPredictions(query: String): Maybe<List<AutocompletePrediction>>
	fun fetchLocationFromPlaceId(placeId: String): Maybe<Location>
	fun addRecentLocation(location: Location): Completable
	fun removeRecentLocation(location: Location): Completable
	fun observeRecentLocations(): Flowable<List<Location>>
	fun removeAllRecentLocations(): Completable
}

class LocationRepositoryImpl @Inject constructor(
		private val context: Context,
		private val locationDao: LocationDao
) : LocationRepository {

	companion object {
		const val ackeePlaceId = "ChIJQyLcrDCVC0cR1qdIMJ-HRbQ"

		val defaultLocation = Location(
				ackeePlaceId,
				"Ackee s. r. o.",
				"Karolinská 650/1, 186 00 Praha 8-Karlín, Czechia",
				LatLng(50.094367, 14.4434423),
				LatLngBounds(LatLng(50.093082669708494, 14.442709419708496), LatLng(50.09578063029149, 14.4454073802915))
		)

		private val autocompleteFilter = AutocompleteFilter.Builder().setCountry("CZ").build()
	}

	override fun fetchAckeeLocation(): Maybe<Location> {
		return Maybe.create<Location> { subscriber ->
			Places.getGeoDataClient(context)
					.getPlaceById(ackeePlaceId)
					.addOnSuccessListener { placeBuffer: PlaceBufferResponse ->
						val place = placeBuffer.elementAt(0)
						val ackeeLocation = Location(
								place.id,
								place.name.toString(),
								place.address.toString(),
								place.latLng,
								place.viewport
						)
						subscriber.onSuccess(ackeeLocation)
						placeBuffer.release()
					}
					.addOnCanceledListener {
						Timber.e("Loading of Ackee location was cancelled. Using default hardcoded values instead.")
						subscriber.onSuccess(defaultLocation)
					}
					.addOnFailureListener { _: Exception ->
						Timber.e("Loading of Ackee location failed. Using default hardcoded values instead.")
						subscriber.onSuccess(defaultLocation)
					}
		}
	}

	override fun fetchLocationFromPlaceId(placeId: String): Maybe<Location> {
		return Maybe.create<Location> { emitter: MaybeEmitter<Location> ->
			// We cannot show a custom error to the user when GPS are out of date, because
			// this function does not trigger any callback listeners :(
			Places.getGeoDataClient(context)
					.getPlaceById(placeId)
					.addOnSuccessListener { placeBuffer: PlaceBufferResponse ->
						val place = placeBuffer.elementAt(0)
						val ackeeLocation = Location(
								place.id,
								place.name.toString(),
								place.address.toString(),
								place.latLng,
								place.viewport
						)
						emitter.onSuccess(ackeeLocation)
						placeBuffer.release()
					}
					.addOnCanceledListener {
						emitter.onComplete()
					}
					.addOnFailureListener { ex: Exception ->
						emitter.onError(ex)
					}
		}
	}

	override fun fetchLocationPredictions(query: String): Maybe<List<AutocompletePrediction>> {
		return Maybe.create<List<AutocompletePrediction>> { subscriber: MaybeEmitter<List<AutocompletePrediction>> ->
			Places.getGeoDataClient(context)
					.getAutocompletePredictions(query, null, autocompleteFilter)
					.addOnCompleteListener { it: Task<AutocompletePredictionBufferResponse> ->
						val buffer: AutocompletePredictionBufferResponse = it.result

						val predictions: List<AutocompletePrediction> = buffer.map {
							it.freeze()
						}
						buffer.release()

						subscriber.onSuccess(predictions)
					}
					.addOnCanceledListener {
						subscriber.onComplete()
					}
					.addOnFailureListener { ex: Exception ->
						subscriber.onError(ex)
					}
		}
	}


	override fun addRecentLocation(location: Location): Completable {
		return Completable.fromCallable {
			if (location.id == null || location.address == null || location.latLng == null) {
				locationDao.insert(location.toRecentCustomLocation())
			} else {
				locationDao.insert(location.toRecentLocation())
			}
		}.subscribeOnNewThread()
	}

	override fun removeRecentLocation(location: Location): Completable {
		return Completable.fromCallable {
			if (location.id == null || location.address == null || location.latLng == null) {
				locationDao.delete(location.toRecentCustomLocation())
			} else {
				locationDao.delete(location.toRecentLocation())
			}
		}.subscribeOnNewThread()
	}

	override fun observeRecentLocations(): Flowable<List<Location>> {
		return locationDao.getCombinedRecentLocations(4)
	}

	override fun removeAllRecentLocations(): Completable {
		return Completable.fromCallable {
			locationDao.deleteAllRecentLocations()
			locationDao.deleteAllRecentCustomLocations()
		}.subscribeOnNewThread()
	}
}
