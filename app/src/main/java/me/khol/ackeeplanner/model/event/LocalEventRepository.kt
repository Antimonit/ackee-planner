package me.khol.ackeeplanner.model.event

import android.Manifest
import android.annotation.SuppressLint
import android.content.ContentUris
import android.content.Context
import android.database.ContentObserver
import android.os.Handler
import android.provider.CalendarContract
import android.support.annotation.RequiresPermission
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import me.khol.ackeeplanner.extensions.subscribeOnNewThread
import org.threeten.bp.Instant
import timber.log.Timber
import java.util.concurrent.Callable
import javax.inject.Inject

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 07.03.2018
 **/
interface LocalEventRepository {
	@RequiresPermission(Manifest.permission.READ_CALENDAR)
	fun observeEvents(): Observable<List<Event>>

	@RequiresPermission(Manifest.permission.READ_CALENDAR)
	fun fetchInstances(start: Instant, end: Instant, vararg calendarIds: Long)
}

class LocalEventRepositoryImpl @Inject constructor(
		private val context: Context
) : LocalEventRepository {

	private val eventsSubject: Subject<List<Event>> = BehaviorSubject.create()

	private val instancesUri = CalendarContract.Instances.CONTENT_URI

	@RequiresPermission(Manifest.permission.READ_CALENDAR)
	private var lastCallable: Callable<List<Event>>? = null

	private var isRegistered = false

	@RequiresPermission(Manifest.permission.READ_CALENDAR)
	private val contentObserver = object : ContentObserver(Handler()) {
		@SuppressLint("MissingPermission")
		override fun onChange(selfChange: Boolean) {
			fetchInstances(lastCallable)
		}
	}

	@RequiresPermission(Manifest.permission.READ_CALENDAR)
	private fun registerContentObserver() {
		if (!isRegistered) {
			context.contentResolver.registerContentObserver(instancesUri, true, contentObserver)
			isRegistered = true
		}
	}

	@SuppressLint("MissingPermission")
	@RequiresPermission(Manifest.permission.READ_CALENDAR)
	override fun observeEvents(): Observable<List<Event>> {
		registerContentObserver()
		return eventsSubject
	}

	@SuppressLint("MissingPermission")
	@RequiresPermission(Manifest.permission.READ_CALENDAR)
	override fun fetchInstances(start: Instant, end: Instant, vararg calendarIds: Long) {
		lastCallable = Callable { getInstances(start, end, *calendarIds) }

		fetchInstances(lastCallable)
	}

	@RequiresPermission(Manifest.permission.READ_CALENDAR)
	private fun fetchInstances(callable: Callable<List<Event>>?) {
		if (callable == null)
			return

		Single.fromCallable(callable)
				.subscribeOnNewThread()
				.subscribe({ events ->
					eventsSubject.onNext(events)
				}, {
					Timber.e(it)
				})
	}

	@SuppressLint("MissingPermission")
	@RequiresPermission(Manifest.permission.READ_CALENDAR)
	private fun getInstances(start: Instant, end: Instant, vararg calendarIds: Long): List<Event> {
		val projection = arrayOf(
				CalendarContract.Instances._ID,
				CalendarContract.Instances.EVENT_ID,
				CalendarContract.Instances.TITLE,
				CalendarContract.Instances.DESCRIPTION,
				CalendarContract.Instances.EVENT_LOCATION,
				CalendarContract.Instances.DTSTART,
				CalendarContract.Instances.DTEND,
				CalendarContract.Instances.BEGIN,
				CalendarContract.Instances.END,
				CalendarContract.Instances.EVENT_TIMEZONE,
				CalendarContract.Instances.ALL_DAY,
				CalendarContract.Instances.STATUS,
				CalendarContract.Instances.EVENT_COLOR,
				CalendarContract.Instances.EVENT_COLOR_KEY,
				CalendarContract.Instances.DISPLAY_COLOR,
				CalendarContract.Instances.CALENDAR_ID
		)

		val uri = instancesUri.buildUpon().apply {
			ContentUris.appendId(this, start.toEpochMilli())
			ContentUris.appendId(this, end.toEpochMilli())
		}.build()

		val selection = """
            | ${CalendarContract.Instances.CALENDAR_ID} IN (${calendarIds.joinToString()})
        """.trimMargin()

		val selectionArgs = arrayOf<String>()

		val events: MutableList<Event> = mutableListOf()
		context.contentResolver.query(
				uri,
				projection,
				selection,
				selectionArgs,
				null
		)?.use { cursor ->
			while (cursor.moveToNext()) {
				val instanceId = cursor.getLong(cursor.getColumnIndexOrThrow(CalendarContract.Instances._ID))
				val eventId = cursor.getLong(cursor.getColumnIndexOrThrow(CalendarContract.Instances.EVENT_ID))
				val title = cursor.getString(cursor.getColumnIndex(CalendarContract.Instances.TITLE))
				val description = cursor.getString(cursor.getColumnIndex(CalendarContract.Instances.DESCRIPTION))
				val location = cursor.getString(cursor.getColumnIndex(CalendarContract.Instances.EVENT_LOCATION))
				val dtStart = cursor.getLong(cursor.getColumnIndex(CalendarContract.Instances.DTSTART))
				val dtEnd = cursor.getLong(cursor.getColumnIndex(CalendarContract.Instances.DTEND))
				val startMilli = cursor.getLong(cursor.getColumnIndex(CalendarContract.Instances.BEGIN)) // actual starting time of a recurrent meeting
				val endMilli = cursor.getLong(cursor.getColumnIndex(CalendarContract.Instances.END)) // actual ending time of a recurrent meeting
				val timeZone = cursor.getString(cursor.getColumnIndex(CalendarContract.Instances.EVENT_TIMEZONE))
				val allDay = cursor.getInt(cursor.getColumnIndex(CalendarContract.Instances.ALL_DAY)) == 1
				val status = cursor.getInt(cursor.getColumnIndex(CalendarContract.Instances.STATUS))
				val eventColor = cursor.getInt(cursor.getColumnIndex(CalendarContract.Instances.EVENT_COLOR))
				val eventColorKey = cursor.getString(cursor.getColumnIndex(CalendarContract.Instances.EVENT_COLOR_KEY))
				val displayColor = cursor.getInt(cursor.getColumnIndex(CalendarContract.Instances.DISPLAY_COLOR))
				val calendarId = cursor.getInt(cursor.getColumnIndex(CalendarContract.Events.CALENDAR_ID))

				val statusEnum = when (status) {
					CalendarContract.Instances.STATUS_TENTATIVE -> EventStatus.TENTATIVE
					CalendarContract.Instances.STATUS_CONFIRMED -> EventStatus.CONFIRMED
					CalendarContract.Instances.STATUS_CANCELED -> EventStatus.CANCELLED
					else -> throw IllegalArgumentException("Status must be one of (tentative, confirmed, canceled)")
				}

				val start = Instant.ofEpochMilli(startMilli)
				val end = Instant.ofEpochMilli(endMilli)

				events.add(Event(eventId, instanceId, calendarId, title, description, location, start, end, timeZone, allDay, statusEnum, eventColor, eventColorKey, displayColor))
			}
		}
		return events
	}

}
