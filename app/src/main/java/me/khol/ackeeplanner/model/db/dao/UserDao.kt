package me.khol.ackeeplanner.model.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Flowable
import io.reactivex.Single
import me.khol.ackeeplanner.model.db.meeting.DBUser

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 17.04.2018
 **/
@Dao
interface UserDao {

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	fun insertUser(item: DBUser)

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	fun insertUsers(list: List<DBUser>)

	@Query("SELECT * FROM user WHERE id = :userId")
	fun getUserSingle(userId: Int): Single<DBUser>

	@Query("SELECT * FROM user WHERE email IN(:emails)")
	fun getUsersByEmailFlowable(vararg emails: String): Flowable<List<DBUser>>

}
