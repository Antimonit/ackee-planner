package me.khol.ackeeplanner.model.db.meeting

import android.arch.persistence.room.*
import me.khol.ackeeplanner.model.api.User

@Entity(
		tableName = "user"
)
data class DBUser(
		@PrimaryKey
		val id: Long,
		val email: String,
		val googleId: String?,
		val enabled: Int,
		val anonymous: Int,
		val havingGoogleRefreshToken: Boolean,
		val havingGoogleAccessToken: Boolean
)
