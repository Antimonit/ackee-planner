package me.khol.ackeeplanner.model.calendar

/**
 * An account with a [name] and its [calendars].
 * If the account not an actual account (such as in case of "Other" calendars - holidays,
 * birthdays, etc.) the [isReal] is set to false.
 */
data class CalendarAccount(
		val name: String,
		val calendars: List<Calendar> = listOf(),
		val isReal: Boolean
)
