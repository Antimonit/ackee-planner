package me.khol.ackeeplanner.model.api

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 25.11.2017
 **/
data class CalendarEntity(
		val id: String,
		val summary: String,
		val description: String?,
		val location: String?,
		val backgroundColor: String,
		val foregroundColor: String,
		val accessRole: String
)
