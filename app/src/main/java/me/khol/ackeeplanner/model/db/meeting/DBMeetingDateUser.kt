package me.khol.ackeeplanner.model.db.meeting

import android.arch.persistence.room.*
import me.khol.ackeeplanner.model.api.meeting.Meeting

@Entity(
		tableName = "meeting_date_user"
)
data class DBMeetingDateUser(
		@PrimaryKey
		val id: Int,
		val userId: Int,
		val meetingsHasDateId: Int,
		val meetingId: Int,
		val calendarGoogleid: String?,
		val email: String?,
		val name: String?
)
