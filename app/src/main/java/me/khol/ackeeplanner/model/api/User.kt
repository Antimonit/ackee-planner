package me.khol.ackeeplanner.model.api

import android.os.Parcelable
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

/**
 * Information about a user of Ackee Planner
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 25.11.2017
 **/
@Parcelize
data class User(
		val id: Long,
		val email: String,
		val googleId: String?,
		val enabled: Int,
		val anonymous: Int,
		val havingGoogleRefreshToken: Boolean,
		val havingGoogleAccessToken: Boolean
) : Parcelable {

	@IgnoredOnParcel
	val isAnonymous: Boolean
		get() = anonymous == 1

}
