package me.khol.ackeeplanner.model.db.location

import android.arch.persistence.room.Entity
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import org.threeten.bp.Instant

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 15.04.2018
 **/
@Entity(tableName = "recent_location", primaryKeys = ["id"])
class DBRecentLocation(
		val id: String,
		val name: String,
		val address: String,
		val latLng: LatLng,
		val viewport: LatLngBounds?,
		val lastTimeUsed: Long = Instant.now().toEpochMilli()
)
