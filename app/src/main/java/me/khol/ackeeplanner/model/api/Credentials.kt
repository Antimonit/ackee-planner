package me.khol.ackeeplanner.model.api

/**
 * OAuth credentials
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 25.11.2017
 **/
data class Credentials(
		val accessToken: String,
		val refreshToken: String?
)
