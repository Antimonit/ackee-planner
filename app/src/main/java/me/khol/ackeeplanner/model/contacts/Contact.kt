package me.khol.ackeeplanner.model.contacts

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import me.khol.ackeeplanner.model.db.contact.DBRecentContact
import me.khol.ackeeplanner.model.db.contact.DBRecentCustomContact

@Parcelize
data class Contact(
		val rawContactId: String?,
		val name: String?,
		val email: String,
		val photoId: String?
) : Parcelable {

	fun toRecentCustomContact(): DBRecentCustomContact {
		return DBRecentCustomContact(email)
	}

	fun toRecentContact(): DBRecentContact {
		return DBRecentContact(rawContactId!!, name!!, email, photoId)
	}

}
