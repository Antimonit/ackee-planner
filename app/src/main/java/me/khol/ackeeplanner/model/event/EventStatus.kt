package me.khol.ackeeplanner.model.event

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 02.04.2018
 **/
enum class EventStatus {
	CONFIRMED,
	TENTATIVE,
	CANCELLED
}
