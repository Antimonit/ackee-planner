package me.khol.ackeeplanner.model.event

import org.threeten.bp.Instant

/**
 * Event retrieved from local calendar provider or from remote google endpoint
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 31.03.2018
 **/
data class Event(
		var eventId: Long,
		var instanceId: Long,
		var calendarId: Int,
		val summary: String,
		val description: String?,
		val location: String?,
		val start: Instant,
		val end: Instant,
		val timeZone: String,
		val allDay: Boolean,
		val status: EventStatus,
		val color: Int,
		val colorKey: String?,
		val displayColor: Int
)
