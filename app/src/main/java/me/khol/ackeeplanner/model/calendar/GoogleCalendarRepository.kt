package me.khol.ackeeplanner.model.calendar

import com.google.api.client.util.DateTime
import com.google.api.services.calendar.model.*
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import me.khol.ackeeplanner.SharedPreferencesInteractor
import me.khol.ackeeplanner.api.GoogleCalendarInteractorImpl
import me.khol.ackeeplanner.extensions.*
import timber.log.Timber
import javax.inject.Inject

/**
 * Repository for retrieving **calendar** related data from remote Google servers.
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 18.03.2018
 **/
interface GoogleCalendarRepository {
	fun fetchEvents(accountName: String, vararg calendarIds: String)
	fun fetchCalendarList(accountName: String)
	fun fetchColors(accountName: String)

	fun observeEvents(): Observable<List<Event>>
	fun observeCalendarList(): Observable<List<CalendarListEntry>>
	fun observeColors(): Observable<Colors>
}

class GoogleCalendarRepositoryImpl @Inject constructor(
		private val preferences: SharedPreferencesInteractor,
		private val googleCalendar: GoogleCalendarInteractorImpl
) : GoogleCalendarRepository {

	init {
		if (!preferences.hasCalendarColorsSyncTimestamp) {
			fetchColors("david@khol.me")
		}
	}

	private val eventsSubject: Subject<List<Event>> = BehaviorSubject.create()
	private val calendarListSubject: Subject<List<CalendarListEntry>> = BehaviorSubject.create()
	private val colorsSubject: Subject<Colors> = BehaviorSubject.create()

	override fun observeEvents(): Observable<List<Event>> = eventsSubject
	override fun observeCalendarList(): Observable<List<CalendarListEntry>> = calendarListSubject
	override fun observeColors(): Observable<Colors> = colorsSubject

	override fun fetchCalendarList(accountName: String) {
		Single.fromCallable {
			googleCalendar.getCalendarList(accountName)
		}.subscribeOnNewThread()
				.map { list ->
					list.items
				}
				.subscribe({ calendarList ->
					Timber.d("$calendarList")
					calendarListSubject.onNext(calendarList)
				}, { throwable ->
					Timber.d(throwable)
				})
	}


	private fun getGoogleColors(accountName: String): Single<Colors> {
		return Single.fromCallable {
			googleCalendar.getColors(accountName)
		}.subscribeOnNewThread()
//                .map { colors ->
//                    val eventColors = colors.meeting.entries.map { (key, color) ->
//                        DBColor(key, DBColor.Type.Event, color.background, color.foreground)
//                    }
//                    val calendarColors = colors.calendar.entries.map { (key, color) ->
//                        DBColor(key, DBColor.Type.Calendar, color.background, color.foreground)
//                    }
//                    eventColors + calendarColors
//                }
	}

	override fun fetchColors(accountName: String) {
		val googleColors: Single<Colors> = getGoogleColors(accountName)

		googleColors
				.observeOn(Schedulers.computation())
//				.onErrorReturn { e: Throwable ->
//					if (e is GoogleJsonResponseException) {
//						listOf()
//					} else {
//						throw e
//					}
//				}
				.subscribe({ colors: Colors ->
					colorsSubject.onNext(colors)
				})
	}


	/**
	 * Retrieves calendar meetings from remote Google host
	 */
	private fun getGoogleEvents(year: Int, month: Int, accountName: String, calendarId: String): Single<List<Event>> {
		return Single.fromCallable {
			googleCalendar.getEvents(year, month, accountName, calendarId)
		}.subscribeOnNewThread()
				.map { it.items }
				.map { googleEvents ->
					googleEvents
							.filter { it.start.dateTime != null && it.end.dateTime != null }
				}
	}

	/**
	 * Retrieves meetings from the Google servers.
	 */
	override fun fetchEvents(accountName: String, vararg calendarIds: String) {
		Observable.fromArray(*calendarIds)
				.flatMapSingle { calendarId ->
					getGoogleEvents(2017, 10, accountName, calendarId).subscribeOnIO()
				}
				.toList()
				.map { it -> it.flatten() }
				.subscribe { events ->
					val sortedEvents = events.sortedBy { it.start.dateTime.value }
					eventsSubject.onNext(sortedEvents)
				}
	}

	private fun createLocalEvent() {
		val event = Event()
				.apply {
					summary = "Ackee planner"
					location = "Ackee s. r. o."
					description = "A chance to hear more about Google's developer products."
					start = EventDateTime().apply {
						dateTime = DateTime("2017-11-18T09:00:00-07:00")
						timeZone = "America/Los_Angeles"
					}
					end = EventDateTime().apply {
						dateTime = DateTime("2017-11-18T17:00:00-07:00")
						timeZone = "America/Los_Angeles"
					}
					recurrence = listOf("RRULE:FREQ=DAILY;COUNT=2")
					attendees = listOf(EventAttendee().setEmail("david@khol.me"), EventAttendee().setEmail("david.khol@ackee.cz"))
					reminders = Event.Reminders()
							.setUseDefault(false)
							.setOverrides(listOf(
									EventReminder().setMethod("email").setMinutes(24 * 60),
									EventReminder().setMethod("popup").setMinutes(10))
							)
				}

		val calendarId = "primary"

		googleCalendar.createEvent(calendarId, event)
	}

}
