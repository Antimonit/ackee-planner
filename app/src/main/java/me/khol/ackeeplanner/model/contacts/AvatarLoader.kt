package me.khol.ackeeplanner.model.contacts

import android.Manifest
import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.support.annotation.RequiresPermission
import io.reactivex.Maybe
import me.khol.ackeeplanner.extensions.subscribeOnIO
import me.khol.ackeeplanner.model.ContactsRepository

/**
 * Utility class for asynchronous loading of avatars for contacts from local calendar database
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 05.04.2018
 */
class AvatarLoader(private val repository: ContactsRepository) {

	private val avatarCache: MutableMap<String, Bitmap?> = hashMapOf()

	@SuppressLint("MissingPermission")
	@RequiresPermission(Manifest.permission.READ_CONTACTS)
	fun loadAvatar(photoId: String?): Maybe<Bitmap> {
		return Maybe.create<Bitmap> {
			if (photoId == null) {
				it.onComplete()
				return@create
			}

			val image = if (avatarCache.containsKey(photoId)) {
				avatarCache[photoId]
			} else {
				repository.queryContactImage(photoId).also {
					avatarCache[photoId] = it?.copy(it.config, false)
				}
			}

			if (image != null) {
				it.onSuccess(image.copy(image.config, false))
			} else {
				it.onComplete()
			}
		}.subscribeOnIO()
	}

}
