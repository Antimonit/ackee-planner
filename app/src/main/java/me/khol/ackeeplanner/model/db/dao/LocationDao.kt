package me.khol.ackeeplanner.model.db.dao

import android.arch.persistence.room.*
import io.reactivex.Flowable
import me.khol.ackeeplanner.model.location.Location
import me.khol.ackeeplanner.model.db.location.DBRecentCustomLocation
import me.khol.ackeeplanner.model.db.location.DBRecentLocation

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 05.04.2018
 **/
@Dao
interface LocationDao {

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	fun insert(item: DBRecentLocation)

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	fun insert(item: DBRecentCustomLocation)

	@Delete
	fun delete(location: DBRecentLocation)

	@Delete
	fun delete(location: DBRecentCustomLocation)

	@Query("DELETE FROM recent_location")
	fun deleteAllRecentLocations()

	@Query("DELETE FROM recent_custom_location")
	fun deleteAllRecentCustomLocations()

	@Query("""
		SELECT
			id,
			name,
			address,
			latLng,
			viewport
		FROM (
				SELECT
					id,
					name,
					address,
					latLng,
					viewport,
					lastTimeUsed
				FROM recent_location
			UNION
				SELECT
					null as id,
					name,
					null as address,
					null as latLng,
					null as viewport,
					lastTimeUsed
				FROM recent_custom_location
			ORDER BY lastTimeUsed DESC
		)
		LIMIT :limit
		""")
	fun getCombinedRecentLocations(limit: Int): Flowable<List<Location>>

}
