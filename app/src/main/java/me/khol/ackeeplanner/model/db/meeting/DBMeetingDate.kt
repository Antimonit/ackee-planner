package me.khol.ackeeplanner.model.db.meeting

import android.arch.persistence.room.*
import me.khol.ackeeplanner.model.api.meeting.Meeting
import org.threeten.bp.Instant

@Entity(
		tableName = "meeting_date"
)
data class DBMeetingDate(
		@PrimaryKey
		val id: Int,
		val meetingId: Int,
		val startDate: Instant,
		val endDate: Instant,
		val userId: Int?
)
