package me.khol.ackeeplanner.model.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Flowable
import io.reactivex.Single
import me.khol.ackeeplanner.model.db.meeting.DBMeeting
import me.khol.ackeeplanner.model.db.meeting.DBMeetingDate
import me.khol.ackeeplanner.model.db.meeting.DBMeetingDateUser
import me.khol.ackeeplanner.model.db.meeting.DBUser

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 05.04.2018
 **/
@Dao
interface MeetingDao {

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	fun insertMeeting(item: DBMeeting)

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	fun insertMeetings(list: List<DBMeeting>)

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	fun insertMeetingDate(item: DBMeetingDate)

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	fun insertMeetingDates(list: List<DBMeetingDate>)

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	fun insertMeetingDateUser(item: DBMeetingDateUser)

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	fun insertMeetingDateUsers(list: List<DBMeetingDateUser>)

	@Query("SELECT * FROM meeting")
	fun getAllMeetings(): Flowable<List<DBMeeting>>

	@Query("SELECT * FROM meeting_date WHERE meetingId = :meetingId")
	fun getAllMeetingDatesSingle(meetingId: Int): Single<List<DBMeetingDate>>

	@Query("SELECT * FROM meeting_date_user WHERE id = :id")
	fun getMeetingUserSingle(id: Int): Single<DBMeetingDateUser>

}
