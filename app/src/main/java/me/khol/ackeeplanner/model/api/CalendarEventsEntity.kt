package me.khol.ackeeplanner.model.api

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 25.11.2017
 **/
data class CalendarEventsEntity(
		var calendar: CalendarEntity,
		var events: List<EventEntity>
)
