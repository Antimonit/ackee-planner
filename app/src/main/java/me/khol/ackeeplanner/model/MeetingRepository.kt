package me.khol.ackeeplanner.model

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import me.khol.ackeeplanner.Optional
import me.khol.ackeeplanner.api.ApiInteractor
import me.khol.ackeeplanner.extensions.subscribeOnIO
import me.khol.ackeeplanner.extensions.subscribeOnNewThread
import me.khol.ackeeplanner.model.api.User
import me.khol.ackeeplanner.model.api.meeting.Meeting
import me.khol.ackeeplanner.model.db.meeting.DBMeeting
import me.khol.ackeeplanner.model.db.meeting.DBMeetingDate
import me.khol.ackeeplanner.model.db.meeting.DBMeetingDateUser
import me.khol.ackeeplanner.model.db.dao.MeetingDao
import me.khol.ackeeplanner.model.db.dao.UserDao
import javax.inject.Inject

/**
 * A repository for retrieving **Meetings**. Meetings are bound to the
 * domain of this app and are in no way related to google meetings.
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 18.03.2018
 **/

interface MeetingRepository {

	fun getMeetings(): Flowable<List<Meeting>>

	fun createMeeting(type: String, description: String, durationMinutes: Int, calendarGoogleId: String, location: String, dates: List<String>, emails: List<String>): Single<Meeting>

	fun meetingDetail(meetingToken: String, meetingId: Int): Single<Meeting>

	fun confirmMeeting(calendarGoogleId: String?, name: String?, email: String?, meetingToken: String, meetingId: Int, dateId: Int): Single<Meeting>

	fun sendInvitations(meetingToken: String, meetingId: Int, emails: List<String>): Completable

}

class MeetingRepositoryImpl @Inject constructor(
		private val meetingDao: MeetingDao,
		private val userDao: UserDao,
		private val apiInteractor: ApiInteractor
) : MeetingRepository {

	/** Saving to DB */
	private fun saveUser(user: User) {
		userDao.insertUser(user.toDBUser())
	}

	private fun saveMeetingDateUsers(users: List<Meeting.Date.User>) {
		meetingDao.insertMeetingDateUsers(users.map { it.toDBMeetingDateUser() })
	}

	private fun saveDates(dates: List<Meeting.Date>) {
		meetingDao.insertMeetingDates(dates.map { it.toDBMeetingDate() })
		saveMeetingDateUsers(dates.mapNotNull { it.user })
	}

	private fun saveMeeting(meeting: Meeting) {
		meetingDao.insertMeeting(meeting.toDBMeeting())
		saveDates(meeting.dates)
		if (meeting.owner != null) {
			saveUser(meeting.owner)
		}
	}

	private fun saveMeetingSingle(meeting: Meeting): Single<Meeting> {
		return Completable
				.fromCallable {
					saveMeeting(meeting)
				}
				.subscribeOnNewThread()
				.toSingleDefault(meeting)
	}

	/** Retrieval from DB */
	private fun getUser(userId: Int): Single<User> {
		return userDao.getUserSingle(userId)
				.map { it.toUser() }
	}

	private fun getMeetingDateUser(userId: Int?): Single<Optional<Meeting.Date.User>> {
		return if (userId != null) {
			meetingDao.getMeetingUserSingle(userId)
					.map { Optional.of(it) }
		} else {
			Single.just(Optional.empty())
		}
				.map { it: Optional<DBMeetingDateUser> ->
					val nullableUser = it.getNullable()?.toUser()
					Optional.ofNullable(nullableUser)
				}
	}

	private fun getDates(meetingId: Int): Single<List<Meeting.Date>> {
		return meetingDao.getAllMeetingDatesSingle(meetingId)
				.flatMap { dates: List<DBMeetingDate> ->
					Flowable.fromIterable(dates)
							.flatMapSingle { date: DBMeetingDate ->
								getMeetingDateUser(date.userId)
										.map { user: Optional<Meeting.Date.User> ->
											date.toDate(user.getNullable())
										}
							}
							.toList()
				}
	}

	private fun getMeeting(meeting: DBMeeting): Single<Meeting> {
		return Single.zip(
				getDates(meeting.id),
				getUser(meeting.ownerId),
				BiFunction { dates: List<Meeting.Date>, user: User ->
					meeting.toMeeting(dates, user)
				}
		)
	}

	override fun getMeetings(): Flowable<List<Meeting>> {
		return meetingDao.getAllMeetings()
				.concatMapEager { meetings: List<DBMeeting> ->
					Flowable.fromIterable(meetings)
							.flatMapSingle { meeting: DBMeeting ->
								getMeeting(meeting)
							}
							.toList()
							.toFlowable()
				}
				.subscribeOnIO()
	}

	/** API calls */
	override fun createMeeting(type: String, description: String, durationMinutes: Int, calendarGoogleId: String, location: String, dates: List<String>, emails: List<String>): Single<Meeting> {
		return apiInteractor.createMeeting(type, description, durationMinutes, calendarGoogleId, location, dates, emails)
				.subscribeOnIO()
				.flatMap(::saveMeetingSingle)
	}

	override fun meetingDetail(meetingToken: String, meetingId: Int): Single<Meeting> {
		return apiInteractor.meetingDetail(meetingId, meetingToken)
				.subscribeOnIO()
				.flatMap(::saveMeetingSingle)
	}

	override fun confirmMeeting(calendarGoogleId: String?, name: String?, email: String?, meetingToken: String, meetingId: Int, dateId: Int): Single<Meeting> {
		return apiInteractor.confirmMeeting(calendarGoogleId, name, email, meetingId, dateId, meetingToken)
				.subscribeOnIO()
				.flatMap(::saveMeetingSingle)
	}

	override fun sendInvitations(meetingToken: String, meetingId: Int, emails: List<String>): Completable {
		return apiInteractor.sendInvitations(emails, meetingId, meetingToken)
				.subscribeOnIO()
	}

}
