package me.khol.ackeeplanner.model

import me.khol.ackeeplanner.model.api.User
import me.khol.ackeeplanner.model.api.meeting.Meeting
import me.khol.ackeeplanner.model.db.meeting.DBMeeting
import me.khol.ackeeplanner.model.db.meeting.DBMeetingDate
import me.khol.ackeeplanner.model.db.meeting.DBMeetingDateUser
import me.khol.ackeeplanner.model.db.meeting.DBUser

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 23.04.2018
 **/

fun Meeting.toDBMeeting(): DBMeeting {
	return DBMeeting(
			id,
			ownerId,
			token,
			description,
			durationMinutes,
			type,
			isTaken,
			calendarGoogleid,
			location,
			canUserConfirm
	)
}

fun Meeting.Date.toDBMeetingDate(): DBMeetingDate {
	return DBMeetingDate(
			id,
			meetingId,
			startDate,
			endDate,
			user?.id
	)
}

fun Meeting.Date.User.toDBMeetingDateUser(): DBMeetingDateUser {
	return DBMeetingDateUser(
			id,
			userId,
			meetingsHasDateId,
			meetingId,
			calendarGoogleid,
			email,
			name
	)
}

fun User.toDBUser(): DBUser {
	return DBUser(
			id,
			email,
			googleId,
			enabled,
			anonymous,
			havingGoogleRefreshToken,
			havingGoogleAccessToken
	)
}

fun DBUser.toUser(): User {
	return User(
			id,
			email,
			googleId,
			enabled,
			anonymous,
			havingGoogleRefreshToken,
			havingGoogleAccessToken
	)
}

fun DBMeeting.toMeeting(dates: List<Meeting.Date>, user: User?): Meeting {
	return Meeting(
			id,
			ownerId,
			token,
			description,
			durationMinutes,
			type,
			isTaken,
			calendarGoogleid,
			location,
			dates,
			user,
			canUserConfirm
	)
}

fun DBMeetingDate.toDate(user: Meeting.Date.User?): Meeting.Date {
	return Meeting.Date(
			id,
			meetingId,
			startDate,
			endDate,
			user
	)
}

fun DBMeetingDateUser.toUser(): Meeting.Date.User {
	return Meeting.Date.User(
			id,
			userId,
			meetingsHasDateId,
			meetingId,
			calendarGoogleid,
			email,
			name
	)
}
