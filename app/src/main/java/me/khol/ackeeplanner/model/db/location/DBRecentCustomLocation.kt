package me.khol.ackeeplanner.model.db.location

import android.arch.persistence.room.Entity
import org.threeten.bp.Instant

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 15.04.2018
 **/
@Entity(tableName = "recent_custom_location", primaryKeys = ["name"])
class DBRecentCustomLocation(
		val name: String,
		val lastTimeUsed: Long = Instant.now().toEpochMilli()
)
