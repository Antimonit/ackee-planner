package me.khol.ackeeplanner.model.db.meeting

import android.arch.persistence.room.*
import me.khol.ackeeplanner.model.api.meeting.Meeting

@Entity(
		tableName = "meeting"
)
data class DBMeeting(
		@PrimaryKey
		var id: Int,
		var ownerId: Int,
		var token: String,
		var description: String,
		var durationMinutes: Int,
		var type: String,
		var isTaken: Int,
		var calendarGoogleid: String,
		var location: String,
		val canUserConfirm: Boolean?
)
