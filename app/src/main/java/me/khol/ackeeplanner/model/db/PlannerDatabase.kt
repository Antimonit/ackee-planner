package me.khol.ackeeplanner.model.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverter
import android.arch.persistence.room.TypeConverters
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import me.khol.ackeeplanner.model.db.contact.DBRecentContact
import me.khol.ackeeplanner.model.db.contact.DBRecentCustomContact
import me.khol.ackeeplanner.model.db.dao.*
import me.khol.ackeeplanner.model.db.location.DBRecentCustomLocation
import me.khol.ackeeplanner.model.db.location.DBRecentLocation
import me.khol.ackeeplanner.model.db.meeting.DBMeeting
import me.khol.ackeeplanner.model.db.meeting.DBMeetingDate
import me.khol.ackeeplanner.model.db.meeting.DBMeetingDateUser
import me.khol.ackeeplanner.model.db.meeting.DBUser
import org.threeten.bp.Instant

/**
 * Room Database description for providing DAOs
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 18.03.2018
 **/
@Database(
		entities = [
			DBRecentContact::class,
			DBRecentCustomContact::class,
			DBUser::class,
			DBMeeting::class,
			DBMeetingDate::class,
			DBMeetingDateUser::class,
			DBRecentLocation::class,
			DBRecentCustomLocation::class
		],
		version = 15
)
@TypeConverters(
		PlannerDatabase.InstantConverter::class,
		PlannerDatabase.LatLngConverter::class,
		PlannerDatabase.LatLngBoundsConverter::class
)
abstract class PlannerDatabase : RoomDatabase() {

	abstract fun contactDao(): ContactDao

	abstract fun meetingDao(): MeetingDao

	abstract fun locationDao(): LocationDao

	abstract fun userDao(): UserDao

	class InstantConverter {
		@TypeConverter
		fun stringToInstant(time: Long): Instant {
			return Instant.ofEpochMilli(time)
		}

		@TypeConverter
		fun instantToString(time: Instant): Long {
			return time.toEpochMilli()
		}
	}

	class LatLngConverter {
		@TypeConverter
		fun stringToLatLng(location: String?): LatLng? {
			if (location == null)
				return null
			val split: List<String> = location.split("|")
			return LatLng(split[0].toDouble(), split[1].toDouble())
		}

		@TypeConverter
		fun latLngToString(latLng: LatLng?): String? {
			if (latLng == null)
				return null
			return listOf(latLng.latitude, latLng.longitude)
					.joinToString("|")
		}
	}

	class LatLngBoundsConverter {
		@TypeConverter
		fun stringToLatLngBounds(location: String?): LatLngBounds? {
			if (location == null)
				return null
			val split: List<String> = location.split("|")
			val sw = LatLng(split[0].toDouble(), split[1].toDouble())
			val ne = LatLng(split[2].toDouble(), split[3].toDouble())
			return LatLngBounds(sw, ne)
		}

		@TypeConverter
		fun latLngBoundstToString(latLngBounds: LatLngBounds?): String? {
			if (latLngBounds == null)
				return null
			return listOf(
					latLngBounds.southwest.latitude,
					latLngBounds.southwest.longitude,
					latLngBounds.northeast.latitude,
					latLngBounds.northeast.longitude)
					.joinToString("|")
		}
	}
}
