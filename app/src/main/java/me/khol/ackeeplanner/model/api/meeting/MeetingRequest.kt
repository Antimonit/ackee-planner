package me.khol.ackeeplanner.model.api.meeting

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 03.04.2018
 **/
class MeetingRequest(
		val meeting: Meeting,
		val dates: List<String>,
		val emails: List<String>
) {

	class Meeting(
			val type: String,
			val description: String,
			val durationMinutes: Int,
			val calendarGoogleid: String,
			val location: String
	)
}
