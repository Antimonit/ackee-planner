package me.khol.ackeeplanner.model.api

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 25.11.2017
 **/
data class RegistrationEntity(
		val user: User,
		val credentials: Credentials
)
