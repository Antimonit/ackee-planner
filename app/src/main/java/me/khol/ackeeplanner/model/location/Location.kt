package me.khol.ackeeplanner.model.location

import android.os.Parcelable
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import kotlinx.android.parcel.Parcelize
import me.khol.ackeeplanner.model.db.location.DBRecentCustomLocation
import me.khol.ackeeplanner.model.db.location.DBRecentLocation

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 04.04.2018
 **/
@Parcelize
data class Location(
		val id: String?,
		val name: String,
		val address: String?,
		val latLng: LatLng?,
		val viewport: LatLngBounds?
) : Parcelable {

	fun toRecentCustomLocation(): DBRecentCustomLocation {
		return DBRecentCustomLocation(name)
	}

	fun toRecentLocation(): DBRecentLocation {
		return DBRecentLocation(id!!, name, address!!, latLng!!, viewport)
	}

}
