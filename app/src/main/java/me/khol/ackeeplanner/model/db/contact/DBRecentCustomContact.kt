package me.khol.ackeeplanner.model.db.contact

import android.arch.persistence.room.Entity
import org.threeten.bp.Instant

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 05.04.2018
 **/
@Entity(tableName = "recent_custom_contact", primaryKeys = ["email"])
data class DBRecentCustomContact(
		val email: String,
		val lastTimeUsed: Long = Instant.now().toEpochMilli()
)
