package me.khol.ackeeplanner.model.api.meeting

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 27.04.2018
 **/
class EmailsRequest(
		val emails: List<String>
)
