package me.khol.ackeeplanner.model.calendar

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.provider.CalendarContract
import android.support.annotation.RequiresPermission
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import me.khol.ackeeplanner.extensions.subscribeOnNewThread
import timber.log.Timber
import javax.inject.Inject
import android.content.ContentUris
import android.content.ContentValues
import android.database.ContentObserver
import android.os.Handler
import io.reactivex.Completable
import me.khol.ackeeplanner.extensions.hasPermissions
import me.khol.ackeeplanner.extensions.subscribeOnIO


/**
 * Repository for retrieving **calendar** related data.
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 18.03.2018
 **/
interface LocalCalendarRepository {
	fun observeCalendarAccountsSafe(): Observable<List<CalendarAccount>>

	@RequiresPermission(Manifest.permission.READ_CALENDAR)
	fun observeVisibleCalendars(): Observable<List<Calendar>>

	@RequiresPermission(Manifest.permission.READ_CALENDAR)
	fun observeCalendarList(): Observable<List<Calendar>>

	@RequiresPermission(Manifest.permission.READ_CALENDAR)
	fun observeCalendarAccounts(): Observable<List<CalendarAccount>>

	@RequiresPermission(Manifest.permission.WRITE_CALENDAR)
	fun updateVisibilityOfCalendar(calendarId: Long, visible: Boolean): Completable
}

class LocalCalendarRepositoryImpl @Inject constructor(
		val context: Context
) : LocalCalendarRepository {

	private val calendarsSubject: Subject<List<Calendar>> = BehaviorSubject.create()
	private val accountsSubject: Subject<List<CalendarAccount>> = BehaviorSubject.create()
	private val visibleCalendarsSubject: Subject<List<Calendar>> = BehaviorSubject.create()

	private val calendarUri = CalendarContract.Calendars.CONTENT_URI

	private var isRegistered = false
	private val contentObserver = object : ContentObserver(Handler()) {
		@SuppressLint("MissingPermission")
		override fun onChange(selfChange: Boolean) {
			fetchAccounts()
		}
	}

	private fun registerContentObserver() {
		if (!isRegistered) {
			context.contentResolver.registerContentObserver(calendarUri, true, contentObserver)
			isRegistered = true
		}
	}

	init {
		calendarsSubject
				.map { calendarList ->
					calendarList.filter { it.isVisible }
				}
				.distinctUntilChanged()
				.subscribe {
					visibleCalendarsSubject.onNext(it)
				}

		calendarsSubject
				.map { calendars ->
					// Separate calendars into calendars specific to certain account and calendars
					// that can be in multiple accounts, such as holidays
					val normalAndOtherCalendars: Map<Boolean, List<Calendar>> = calendars.groupBy({
						it.ownerAccount.endsWith("@group.v.calendar.google.com")
					}, {
						it
					})

					// en.czech#holiday@group.v.calendar.google.com
					val holidayRegex = Regex("""(.*)\.(.*)#(holiday@group.v.calendar.google.com)""")

					// Find all holiday calendars and separate them based on the country name
					// ignoring the localization
					// group[0] is the whole regex
					// group[1] is localization (en / cs / kr etc.)
					// group[2] is country name (us / czech / korean etc.)
					// group[3] is "holiday@group.v.calendar.google.com"
					val otherCalendars = (normalAndOtherCalendars[true] ?: emptyList())
							.distinctBy {
								val name = it.ownerAccount
								val matches: MatchResult? = holidayRegex.matchEntire(name)
								if (matches != null) {
									"${matches.groups[2]}#${matches.groups[3]}"
								} else {
									name
								}
							}
					// Create a fake account with holiday calendars
					val otherAccount = CalendarAccount("Other", otherCalendars, false)

					// Group by normal calendars by account name
					val normalCalendars = normalAndOtherCalendars[false] ?: emptyList()
					val normalAccounts = normalCalendars
							.groupBy({ it.accountName }, { it })
							.map { CalendarAccount(it.key, it.value, true) }

					normalAccounts + otherAccount
				}.subscribe {
					accountsSubject.onNext(it)
				}
	}

	@SuppressLint("MissingPermission")
	override fun observeCalendarAccountsSafe(): Observable<List<CalendarAccount>> {
		// TODO: Monitor permissions in a reactive manner
		// Currently relies on the screen being reloaded when user return to the screen.

		return if (context.hasPermissions(Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR)) {
			observeCalendarAccounts()
					.distinctUntilChanged()
		} else {
			Observable.just(emptyList())
		}.subscribeOnIO()
	}

	@SuppressLint("MissingPermission")
	@RequiresPermission(Manifest.permission.READ_CALENDAR)
	override fun observeCalendarAccounts(): Observable<List<CalendarAccount>> {
		registerContentObserver()
		fetchAccounts()
		return accountsSubject
	}

	/**
	 * Observes a list of calendars that are set to be visible on the device
	 */
	@SuppressLint("MissingPermission")
	@RequiresPermission(Manifest.permission.READ_CALENDAR)
	override fun observeVisibleCalendars(): Observable<List<Calendar>> {
		registerContentObserver()
		fetchAccounts()
		return visibleCalendarsSubject
	}

	/**
	 * Observes a list of all calendars (both visible and invisible) on the device
	 */
	@SuppressLint("MissingPermission")
	@RequiresPermission(Manifest.permission.READ_CALENDAR)
	override fun observeCalendarList(): Observable<List<Calendar>> {
		registerContentObserver()
		fetchAccounts()
		return calendarsSubject
	}


	@SuppressLint("MissingPermission")
	@RequiresPermission(Manifest.permission.READ_CALENDAR)
	private fun fetchAccounts() {
		Single.fromCallable { getCalendars() }
				.subscribeOnNewThread()
				.subscribe({ calendarList ->
					calendarsSubject.onNext(calendarList)
				}, {
					Timber.e(it)
				})
	}

	@SuppressLint("MissingPermission")
	@RequiresPermission(Manifest.permission.READ_CALENDAR)
	private fun getCalendars(selection: String? = null, selectionArgs: Array<String>? = null): List<Calendar> {
		val projection = arrayOf(
				CalendarContract.Calendars._ID,
				CalendarContract.Calendars.ACCOUNT_NAME,
				CalendarContract.Calendars.ACCOUNT_TYPE,
				CalendarContract.Calendars.OWNER_ACCOUNT,
				CalendarContract.Calendars.CALENDAR_DISPLAY_NAME,
				CalendarContract.Calendars.CALENDAR_LOCATION,
				CalendarContract.Calendars.CALENDAR_TIME_ZONE,
				CalendarContract.Calendars.VISIBLE,
				CalendarContract.Calendars.CALENDAR_COLOR,
				CalendarContract.Calendars.CALENDAR_COLOR_KEY,
				CalendarContract.Calendars.CALENDAR_ACCESS_LEVEL
		)

		val calendars = mutableListOf<Calendar>()
		context.contentResolver.query(calendarUri, projection, selection, selectionArgs, null)
				?.use { cursor ->
					while (cursor.moveToNext()) {
						val calendarId: Long = cursor.getLong(cursor.getColumnIndex(CalendarContract.Calendars._ID))
						val accountName: String = cursor.getString(cursor.getColumnIndex(CalendarContract.Calendars.ACCOUNT_NAME))
						val accountType: String = cursor.getString(cursor.getColumnIndex(CalendarContract.Calendars.ACCOUNT_TYPE))
						val ownerAccount: String = cursor.getString(cursor.getColumnIndex(CalendarContract.Calendars.OWNER_ACCOUNT))
						val calendarDisplayName: String? = cursor.getString(cursor.getColumnIndex(CalendarContract.Calendars.CALENDAR_DISPLAY_NAME))
						val calendarLocation: String? = cursor.getString(cursor.getColumnIndex(CalendarContract.Calendars.CALENDAR_LOCATION))
						val timeZone: String? = cursor.getString(cursor.getColumnIndex(CalendarContract.Calendars.CALENDAR_TIME_ZONE))
						val isVisible: Boolean = cursor.getInt(cursor.getColumnIndex(CalendarContract.Calendars.VISIBLE)) == 1
						val calendarColor: Int = cursor.getInt(cursor.getColumnIndex(CalendarContract.Calendars.CALENDAR_COLOR))
						val calendarColorKey: String? = cursor.getString(cursor.getColumnIndex(CalendarContract.Calendars.CALENDAR_COLOR_KEY))
						val accessLevel: Int = cursor.getInt(cursor.getColumnIndex(CalendarContract.Calendars.CALENDAR_ACCESS_LEVEL))

						val calendar = Calendar(
								calendarId,
								accountName,
								accountType,
								ownerAccount,
								calendarDisplayName,
								calendarLocation,
								timeZone,
								isVisible,
								calendarColor,
								calendarColorKey,
								accessLevel
						)
						calendars.add(calendar)
					}
				}
		return calendars
	}

	/**
	 * Changes the visibility of the calendar in the database
	 */
	@SuppressLint("MissingPermission")
	@RequiresPermission(Manifest.permission.WRITE_CALENDAR)
	override fun updateVisibilityOfCalendar(calendarId: Long, visible: Boolean): Completable {
		return Completable.fromAction {
			val values = ContentValues().apply {
				put(CalendarContract.Calendars.VISIBLE, visible)
			}
			val updateUri = ContentUris.withAppendedId(calendarUri, calendarId)
			context.contentResolver.update(updateUri, values, null, null)
		}.subscribeOnIO()
	}

}
