package me.khol.ackeeplanner.model.api

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 25.11.2017
 **/
data class EventEntity(
		var start: DateTime,
		var end: DateTime,
		var summary: String
) {

	data class DateTime(
			var dateTime: String,
			var timeZone: String?
	)
}
