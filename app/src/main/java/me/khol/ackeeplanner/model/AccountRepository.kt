package me.khol.ackeeplanner.model

import android.content.Context
import android.content.Intent
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.tasks.Task
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import me.khol.ackeeplanner.Optional
import me.khol.ackeeplanner.SharedPreferencesInteractor
import me.khol.ackeeplanner.api.ApiInteractor
import me.khol.ackeeplanner.extensions.getGoogleSignInClient
import me.khol.ackeeplanner.extensions.subscribeOnNewThread
import me.khol.ackeeplanner.model.api.User
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Handles everything related to signing in.
 *
 * [AccountRepository.State] holds information about global sign-in process of Google sign-in
 * and Ackee sign-in combined.
 *
 * In order to register to Ackee backend, one must also log in with Google account.
 * [RxGoogleLogin] class handles [sign-in][AccountRepositoryImpl.doLogIn]
 * (including [silent sign-in][AccountRepositoryImpl.doSilentLogIn]) process to Google servers.
 * The user can also decide to [doAnonymousLogin()][AccountRepositoryImpl.doAnonymousLogin]
 * without logging in with Google account.
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 12.03.2018
 **/
interface AccountRepository {

	/**
	 * Holds information about state of global sign-in process (google and Ackee combined)
	 */
	sealed class State {
		class SignedOut : State()
		/**
		 * @param accountName A Google account name (email) the user is currently trying to log in with
		 */
		class SigningIn(val accountName: String?) : State()
		/**
		 * @param account A Google account the user is currently logged in with
		 * @param user Currently logged in Ackee user
		 */
		class SignedIn(val account: GoogleSignInAccount?, val user: User) : State()
		/**
		 * @param throwable Error, duh...
		 */
		class Error(val throwable: Throwable) : State()
	}

	fun observeLoginState(): Observable<State>

	fun doAnonymousLogin()
	fun doSilentLogIn(silentAccount: GoogleSignInAccount)
	fun doLogIn(name: String?)
	fun doLogOut()
	fun doRevoke()

	fun onSignInResult(data: Intent?)
	fun observeSignInIntent(): Observable<Intent>
}

@Singleton
class AccountRepositoryImpl @Inject constructor(
		context: Context,
		private val apiInteractor: ApiInteractor,
		private val preferences: SharedPreferencesInteractor,
		private val credential: GoogleAccountCredential
) : AccountRepository {

	/**
	 * Holds information about current sign-in process to Ackee backend
	 */
	private sealed class AckeeState {
		class SignedOut : AckeeState()
		class SigningIn : AckeeState()
		/**
		 * @param user Currently logged in Ackee user
		 */
		class SignedIn(val user: User) : AckeeState()
		/**
		 * @param throwable Error, duh...
		 */
		class Error(val throwable: Throwable) : AckeeState()
	}

	private val googleLogin: RxGoogleLogin = RxGoogleLogin(context)

	private val ackeeLoginStateSubject: Subject<AckeeState>
	private val globalLoginStateSubject: Subject<AccountRepository.State>

	override fun observeLoginState(): Observable<AccountRepository.State> = globalLoginStateSubject

	init {
		ackeeLoginStateSubject = BehaviorSubject.createDefault(AckeeState.SignedOut())
		globalLoginStateSubject = BehaviorSubject.createDefault(AccountRepository.State.SignedOut())

		googleLogin.observeState()
				.subscribe { state ->
					when (state) {
						is RxGoogleLogin.GoogleState.SigningOut -> {
							globalLoginStateSubject.onNext(AccountRepository.State.SignedOut())
						}
						is RxGoogleLogin.GoogleState.SignedOut -> {
							globalLoginStateSubject.onNext(AccountRepository.State.SignedOut())
						}
						is RxGoogleLogin.GoogleState.SigningIn -> {
							globalLoginStateSubject.onNext(AccountRepository.State.SigningIn(state.accountName))
						}
						is RxGoogleLogin.GoogleState.SignedIn -> {
							// let the observable below handle the sign-in to Ackee backend
						}
						is RxGoogleLogin.GoogleState.Error -> {
							globalLoginStateSubject.onNext(AccountRepository.State.Error(state.throwable))
						}
					}
				}

		googleLogin.observeSignedInState()
				.filter { it.account.serverAuthCode != null }
				.map { it.account.serverAuthCode }
				.switchMapSingle {
					ackeeLoginStateSubject.onNext(AckeeState.SigningIn())
					apiInteractor.loginWithGoogleAuthCode(it)
				}
				.doOnNext { result ->
					ackeeLoginStateSubject.onNext(AckeeState.SignedIn(result.user))
				}
				.doOnError { t ->
					ackeeLoginStateSubject.onNext(AckeeState.Error(t))
				}
				.retry()
				.subscribe()

		ackeeLoginStateSubject
				.subscribe { state ->
					when (state) {
						is AckeeState.SignedIn -> {
							preferences.ackeeUserId = state.user.id
							preferences.selectedAccountName = state.user.email
						}
						is AckeeState.SigningIn -> {
							globalLoginStateSubject.onNext(AccountRepository.State.SigningIn(null))
						}
						is AckeeState.SignedOut -> {
							preferences.ackeeUserId = null
							preferences.selectedAccountName = null

							globalLoginStateSubject.onNext(AccountRepository.State.SignedOut())
						}
						is AckeeState.Error -> {
							preferences.ackeeUserId = null
							preferences.selectedAccountName = null

							globalLoginStateSubject.onNext(AccountRepository.State.Error(state.throwable))
						}
					}
				}

		/**
		 * When we successfully log in with ackee account (linked with google account or anonymously)
		 * change global state to SignedIn. Combine it with the google account we are currently
		 * signed in with or a null, if we are not signed in with google account (because we logged
		 * in anonymously)
		 */
		ackeeLoginStateSubject
				.ofType(AckeeState.SignedIn::class.java)
				.map { it.user }
				.withLatestFrom(
						googleLogin.observeState()
								.map {
									val account = if (it is RxGoogleLogin.GoogleState.SignedIn) {
										it.account
									} else null
									Optional.ofNullable(account)
								},
						BiFunction { user: User, google: Optional<GoogleSignInAccount> ->
							val account = google.getNullable()
							AccountRepository.State.SignedIn(account, user)
						}
				)
				.subscribe { state ->
					globalLoginStateSubject.onNext(state)
				}
	}

	override fun doSilentLogIn(silentAccount: GoogleSignInAccount) {
		googleLogin.silentSignIn(silentAccount)
				.subscribe({
					/**
					 * Sign in succeeded, just ignore it because this is already handled
					 * with [googleLogin.observeState()][RxGoogleLogin.observeState]
					 */
				}, {
					/**
					 * Silent sign in failed, just ignore it because errors are already handled
					 * with [googleLogin.observeState()][RxGoogleLogin.observeState]
					 */
				})
	}

	override fun doAnonymousLogin() {
		googleLogin.signOut()
				.onErrorComplete()
				.subscribe({
					ackeeLoginStateSubject.onNext(AckeeState.SigningIn())
					apiInteractor.loginAnonymous()
							.subscribeOnNewThread()
							.subscribe({ result ->
								ackeeLoginStateSubject.onNext(AckeeState.SignedIn(result.user))
							}, { t ->
								ackeeLoginStateSubject.onNext(AckeeState.Error(t))
							})
				}, {
					/**
					 * Sign out failed, just ignore it because errors are already handled
					 * with [googleLogin.observeState()][RxGoogleLogin.observeState]
					 */
				})
	}

	override fun doLogIn(name: String?) {
		googleLogin.signIn(name)
				.subscribe({
					/**
					 * Sign in succeeded, just ignore it because this is already handled
					 * with [googleLogin.observeState()][RxGoogleLogin.observeState]
					 */
				}, {
					/**
					 * Sign in failed, just ignore it because errors are already handled
					 * with [googleLogin.observeState()][RxGoogleLogin.observeState]
					 */
				})
	}

	override fun doLogOut() {
		googleLogin.signOut()
				.onErrorComplete()
				.subscribe({
					apiInteractor.logout()
							.subscribe({
								ackeeLoginStateSubject.onNext(AckeeState.SignedOut())
							}, { t ->
								ackeeLoginStateSubject.onNext(AckeeState.Error(t))
							})
				}, {
					/**
					 * Sign out failed, just ignore it because errors are already handled
					 * with [googleLogin.observeState()][RxGoogleLogin.observeState]
					 */
				})
	}

	override fun doRevoke() {
		googleLogin.revokeAccess()
				.onErrorComplete()
				.subscribe({
					apiInteractor.logout()
							.subscribe({
								ackeeLoginStateSubject.onNext(AckeeState.SignedOut())
							}, { t ->
								ackeeLoginStateSubject.onNext(AckeeState.Error(t))
							})
				}, {
					/**
					 * Revoke access failed, just ignore it because errors are already handled
					 * with [googleLogin.observeState()][RxGoogleLogin.observeState]
					 */
				})
	}

	override fun observeSignInIntent() = googleLogin.observeSignInIntent()

	override fun onSignInResult(data: Intent?) = googleLogin.onSignInResult(data)

}



class RxGoogleLogin(private val context: Context) {

	/**
	 * Holds information about current sign-in process to Google servers
	 */
	sealed class GoogleState {
		class SigningOut : GoogleState()
		class SignedOut : GoogleState()
		/**
		 * @param accountName A Google account name (email) the user is currently trying to log in with
		 */
		class SigningIn(val accountName: String?) : GoogleState()
		/**
		 * @param account A Google account the user is currently logged in with
		 */
		class SignedIn(val account: GoogleSignInAccount) : GoogleState()
		/**
		 * @param throwable Error, duh...
		 */
		class Error(val throwable: Throwable) : GoogleState()
	}

	/**
	 * Remember what name we used to create [googleSignInClient]
	 */
	private var currentGoogleSignInClientName: String? = null
	private var googleSignInClient: GoogleSignInClient = context.getGoogleSignInClient()

	private val googleLoginStateSubject: Subject<GoogleState> = BehaviorSubject.createDefault(GoogleState.SignedOut())
	fun observeState(): Observable<GoogleState> = googleLoginStateSubject
	fun observeSignedInState(): Observable<GoogleState.SignedIn> = observeState().ofType(GoogleState.SignedIn::class.java)

	/**
	 * Because we need access to activity to sign the user in, notify the activity via
	 * [observeSignInIntent()][observeSignInIntent] observable.
	 * After the activity receives the result, it should notify us via [onSignInResult]
	 */
	private val signInIntentSubject: Subject<Intent> = PublishSubject.create()
	fun observeSignInIntent(): Observable<Intent> = signInIntentSubject

	private val signInResultSubject: Subject<Task<GoogleSignInAccount>> = PublishSubject.create()
	fun onSignInResult(data: Intent?) {
		val completedTask = GoogleSignIn.getSignedInAccountFromIntent(data)
		signInResultSubject.onNext(completedTask)
	}

	/**
	 * Transforms [GoogleSignInClient]'s sign in process into RxJava [Single].
	 * Because we can only [startActivityForResult()][android.app.Activity.startActivityForResult]
	 * from within an activity, we emit [signInIntent][GoogleSignInClient.getSignInIntent] to
	 * [signInIntentSubject] and expect to receive a result through [signInResultSubject].
	 * You have to call [onSignInResult] otherwise returned Single will never complete.
	 *
	 * The only emission of the returned Single is a logged in account or an exception.
	 */
	private fun rxSignIn(): Single<GoogleSignInAccount> {
		return Single.create { e ->
			signInResultSubject
					.firstOrError()
					.subscribe({ completedTask: Task<GoogleSignInAccount> ->
						if (completedTask.isSuccessful) {
							e.onSuccess(completedTask.result)
						} else {
							e.onError(completedTask.exception!!)
						}
					}, { t ->
						e.onError(t)
					})

			signInIntentSubject.onNext(googleSignInClient.signInIntent)
		}
	}

	/**
	 * Transforms [GoogleSignInClient.silentSignIn] into RxJava [Single].
	 * The only emission of the returned Single is a logged in account or an exception.
	 */
	private fun rxSilentSignIn(): Single<GoogleSignInAccount> {
		return Single.create { e ->
			googleSignInClient.silentSignIn()
					.addOnFailureListener { exception ->
						e.onError(exception)
					}
					.addOnSuccessListener { result: GoogleSignInAccount ->
						e.onSuccess(result)
					}
					.addOnCanceledListener {
						e.onError(Exception("Sign in cancelled"))
					}
		}
	}

	/**
	 * Transforms [GoogleSignInClient.signOut] into RxJava [Completable]
	 */
	private fun rxSignOut(): Completable {
		return Completable.create { e ->
			googleSignInClient.signOut()
					.addOnFailureListener { exception ->
						e.onError(exception)
					}
					.addOnSuccessListener {
						e.onComplete()
					}
					.addOnCanceledListener {
						e.onError(Exception("Sign out cancelled"))
					}
		}
	}

	/**
	 * Transforms [GoogleSignInClient.revokeAccess] into RxJava [Completable]
	 */
	private fun rxRevokeAccess(): Completable {
		return Completable.create { e ->
			googleSignInClient.revokeAccess()
					.addOnFailureListener { exception ->
						e.onError(exception)
					}
					.addOnSuccessListener { _: Void? ->
						e.onComplete()
					}
					.addOnCanceledListener {
						e.onError(Exception("Revoke access cancelled"))
					}
		}
	}


	private fun rxChangeSignInClient(accountName: String?): Completable {
		return if (currentGoogleSignInClientName != accountName) {
			currentGoogleSignInClientName = accountName
			signOut()
					.doOnComplete {
						googleSignInClient = if (accountName == null) {
							context.getGoogleSignInClient()
						} else {
							context.getGoogleSignInClient(accountName)
						}
					}
		} else {
			Completable.complete()
		}
	}

	fun signIn(name: String?): Single<GoogleSignInAccount> {
		return Single.create { e ->
			rxChangeSignInClient(name)
					.subscribe({
						googleLoginStateSubject.onNext(GoogleState.SigningIn(name))

						rxSignIn()
								.subscribe({ account ->
									googleLoginStateSubject.onNext(GoogleState.SignedIn(account))
									e.onSuccess(account)
								}, { t ->
									googleLoginStateSubject.onNext(GoogleState.Error(t))
									e.onError(t)
								})
					}, { t ->
						googleLoginStateSubject.onNext(GoogleState.Error(t))
						e.onError(t)
					})
		}
	}

	fun silentSignIn(silentAccount: GoogleSignInAccount): Single<GoogleSignInAccount> {
		return Single.create { e ->
			rxChangeSignInClient(silentAccount.email)
					.subscribe({
						googleLoginStateSubject.onNext(GoogleState.SigningIn(silentAccount.email))

						rxSilentSignIn()
								.subscribe({ account ->
									googleLoginStateSubject.onNext(GoogleState.SignedIn(account))
									e.onSuccess(account)
								}, { t ->
									googleLoginStateSubject.onNext(GoogleState.Error(t))
									e.onError(t)
								})
					}, { t ->
						googleLoginStateSubject.onNext(GoogleState.Error(t))
						e.onError(t)
					})
		}
	}

	fun signOut(): Completable {
		return Completable.create { e ->
			googleLoginStateSubject.onNext(GoogleState.SigningOut())

			rxSignOut()
					.subscribe({
						googleLoginStateSubject.onNext(GoogleState.SignedOut())
						e.onComplete()
					}, { t ->
						googleLoginStateSubject.onNext(GoogleState.Error(t))
						e.onError(t)
					})
		}
	}

	fun revokeAccess(): Completable {
		return Completable.create { e ->
			googleLoginStateSubject.onNext(GoogleState.SigningOut())

			rxRevokeAccess()
					.subscribe({
						googleLoginStateSubject.onNext(GoogleState.SignedOut())
						e.onComplete()
					}, { t ->
						googleLoginStateSubject.onNext(GoogleState.Error(t))
						e.onError(t)
					})
		}
	}

}
