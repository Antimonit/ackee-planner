package me.khol.ackeeplanner.model.api.meeting

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 03.04.2018
 **/
class ConfirmMeetingRequest(
		val calendarGoogleid: String?,
		val name: String?,
		val email: String?
)
