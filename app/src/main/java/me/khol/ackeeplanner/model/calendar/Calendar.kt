package me.khol.ackeeplanner.model.calendar

import android.os.Parcelable
import android.support.annotation.ColorInt
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Calendar(
		val id: Long,
		val accountName: String,
		val accountType: String,
		val ownerAccount: String,
		val displayName: String?,
		val calendarLocation: String?,
		val timeZone: String?,
		val isVisible: Boolean,
		@ColorInt
		val color: Int,
		val colorKey: String?,
		val accessLevel: Int
) : Parcelable {

	val isPrimary: Boolean
		get() = accountName == ownerAccount

}
