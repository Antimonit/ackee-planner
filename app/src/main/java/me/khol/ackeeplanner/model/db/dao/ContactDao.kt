package me.khol.ackeeplanner.model.db.dao

import android.arch.persistence.room.*
import io.reactivex.Flowable
import me.khol.ackeeplanner.model.contacts.Contact
import me.khol.ackeeplanner.model.db.contact.DBRecentContact
import me.khol.ackeeplanner.model.db.contact.DBRecentCustomContact

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 05.04.2018
 **/
@Dao
interface ContactDao {

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	fun insertContacts(item: List<DBRecentContact>)

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	fun insertCustomContacts(item: List<DBRecentCustomContact>)

	@Delete
	fun delete(item: DBRecentContact)

	@Delete
	fun delete(item: DBRecentCustomContact)

	@Query("DELETE FROM recent_contact")
	fun deleteAllRecentContacts()

	@Query("DELETE FROM recent_custom_contact")
	fun deleteAllRecentCustomContacts()

	@Query("""
		SELECT
			rawContactId,
			name,
			email,
			photoId
		FROM (
				SELECT
					rawContactId,
					name,
					email,
					photoId,
					lastTimeUsed
				FROM recent_contact
			UNION
				SELECT
					null as rawContactId,
					null as name,
					email,
					null as photoId,
					lastTimeUsed
				FROM recent_custom_contact
			ORDER BY lastTimeUsed DESC
		)
		LIMIT :limit
		""")
	fun getCombinedRecentContacts(limit: Int): Flowable<List<Contact>>

}
