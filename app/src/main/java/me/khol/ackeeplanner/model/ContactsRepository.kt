package me.khol.ackeeplanner.model

import android.Manifest
import android.annotation.SuppressLint
import android.content.ContentResolver
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.provider.ContactsContract
import android.support.annotation.RequiresPermission
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import me.khol.ackeeplanner.extensions.subscribeOnNewThread
import me.khol.ackeeplanner.model.contacts.AvatarLoader
import me.khol.ackeeplanner.model.contacts.Contact
import me.khol.ackeeplanner.model.db.contact.DBRecentContact
import me.khol.ackeeplanner.model.db.contact.DBRecentCustomContact
import me.khol.ackeeplanner.model.db.dao.ContactDao
import javax.inject.Inject


/**
 * TODO: 29. 3. 2018 david.khol: add class description
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 29.03.2018
 **/
interface ContactsRepository {

	fun getContacts(query: CharSequence): Single<List<Contact>>

	@RequiresPermission(Manifest.permission.READ_CONTACTS)
	fun getContactsByIds(rawContactIds: List<String>): Single<List<Contact>>

	fun observeRecentContacts(): Flowable<List<Contact>>

	fun addRecentContact(contacts: List<Contact>): Completable

	fun removeRecentContacts(contact: Contact): Completable

	fun removeAllRecentContacts(): Completable

	@RequiresPermission(Manifest.permission.READ_CONTACTS)
	fun queryContactImage(imageDataRow: String?): Bitmap?

	fun getAvatarLoader(): AvatarLoader
}


class ContactsRepositoryImpl @Inject constructor(
		private val contentResolver: ContentResolver,
		private val contactDao: ContactDao
) : ContactsRepository {

	private val avatarLoader = AvatarLoader(this)

	override fun getAvatarLoader() = avatarLoader

	@SuppressLint("MissingPermission")
	@RequiresPermission(Manifest.permission.READ_CONTACTS)
	override fun getContactsByIds(rawContactIds: List<String>): Single<List<Contact>> {
		val filter = """
			${ContactsContract.RawContacts._ID} IN (${rawContactIds.joinToString { "'$it'" }})
		""".trimMargin()

		return getFilteredContacts(filter)
	}

	@SuppressLint("MissingPermission")
	@RequiresPermission(Manifest.permission.READ_CONTACTS)
	override fun getContacts(query: CharSequence): Single<List<Contact>> {
		val filter = """
			${ContactsContract.CommonDataKinds.Email.DATA} NOT LIKE ''
			| AND (
				| ${ContactsContract.Contacts.DISPLAY_NAME} LIKE '%$query%'
				| OR
				| ${ContactsContract.CommonDataKinds.Email.DATA} LIKE '%$query%'
			| )
		""".trimMargin()

		return getFilteredContacts(filter)
	}

	override fun addRecentContact(contacts: List<Contact>): Completable {
		return Completable.fromCallable {
			contacts
					.groupBy({ contact ->
						contact.rawContactId == null || contact.name == null
					})
					.forEach { isCustom: Boolean, contacts: List<Contact> ->
						if (isCustom) {
							contactDao.insertCustomContacts(contacts.map { it.toRecentCustomContact() })
						} else {
							contactDao.insertContacts(contacts.map { it.toRecentContact() })
						}
					}
		}.subscribeOnNewThread()
	}

	override fun removeRecentContacts(contact: Contact): Completable {
		return Completable.fromCallable {
			if (contact.rawContactId == null || contact.name == null) {
				contactDao.delete(contact.toRecentCustomContact())
			} else {
				contactDao.delete(contact.toRecentContact())
			}
		}.subscribeOnNewThread()
	}

	override fun removeAllRecentContacts(): Completable {
		return Completable.fromCallable {
			contactDao.deleteAllRecentContacts()
			contactDao.deleteAllRecentCustomContacts()
		}.subscribeOnNewThread()
	}

	override fun observeRecentContacts(): Flowable<List<Contact>> {
		return contactDao.getCombinedRecentContacts(4)
	}


	@SuppressLint("MissingPermission")
	@RequiresPermission(Manifest.permission.READ_CONTACTS)
	private fun getFilteredContacts(filter: String): Single<List<Contact>> {
		val projection = arrayOf(
				ContactsContract.RawContacts._ID,
				ContactsContract.Contacts.DISPLAY_NAME,
				ContactsContract.Contacts.PHOTO_ID,
				ContactsContract.CommonDataKinds.Email.DATA,
				ContactsContract.CommonDataKinds.Photo.CONTACT_ID
		)

		val order = """CASE WHEN
            | ${ContactsContract.Contacts.DISPLAY_NAME} NOT LIKE '%@%' THEN 1 ELSE 2 END,
            | ${ContactsContract.Contacts.DISPLAY_NAME},
            | ${ContactsContract.CommonDataKinds.Email.DATA}
            | COLLATE NOCASE
		""".trimMargin()

		return Single.fromCallable {

			val personEmails = mutableListOf<Contact>()
			val emails = mutableSetOf<String>()

			contentResolver.query(
					ContactsContract.CommonDataKinds.Email.CONTENT_URI,
					projection,
					filter,
					null,
					order
			)?.use { cursor ->
				if (cursor.moveToFirst()) {
					do {
						val rawContactId = cursor.getString(0)
						val name = cursor.getString(1)
						val photoId = cursor.getString(2)
						val email = cursor.getString(3)

						val personEmail = Contact(rawContactId, name, email, photoId)

						if (emails.add(email.toLowerCase())) {
							personEmails.add(personEmail)
						}
					} while (cursor.moveToNext())
				}
			}

			personEmails
		}
	}

	@SuppressLint("MissingPermission")
	@RequiresPermission(Manifest.permission.READ_CONTACTS)
	override fun queryContactImage(imageDataRow: String?): Bitmap? {
		if (imageDataRow == null) {
			return null
		}

		contentResolver.query(
				ContactsContract.Data.CONTENT_URI,
				arrayOf(ContactsContract.CommonDataKinds.Photo.PHOTO),
				ContactsContract.Data._ID + "=?",
				arrayOf(imageDataRow),
				null
		)?.use { cursor ->
			if (cursor.moveToFirst()) {
				val imageBytes = cursor.getBlob(0)
				if (imageBytes != null) {
					return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
				}
			}
		}

		return null
	}

}
