package me.khol.ackeeplanner.constants

/**
 * Network constants.
 *
 * @author Dušan Jenčík [dusan.jencik@ackee.cz]
 * @since 25.07.17
 **/
object NetworkConstants {
	const val API_URL = "https://ackeeplanner-api.ack.ee/api/v1/"

	/**
	 * Header flags.
	 */
	object Header {
		const val X_VERSION = "X-Version"
		const val USER_AGENT = "User-Agent"
		const val X_DEVICE_ID = "X-Device-Id"
	}
}
