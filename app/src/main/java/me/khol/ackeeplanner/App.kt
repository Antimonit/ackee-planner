package me.khol.ackeeplanner

import android.support.multidex.MultiDexApplication
import com.facebook.stetho.Stetho
import com.jakewharton.threetenabp.AndroidThreeTen
import com.tspoon.traceur.Traceur
import me.khol.ackeeplanner.di.*
import timber.log.Timber
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 13.11.2017
 **/
class App : MultiDexApplication() {

	companion object {
		@JvmStatic
		lateinit var instance: App
		@JvmStatic
		lateinit var appComponent: AppComponent
	}

	override fun onCreate() {
		super.onCreate()

		Fabric.with(this, Crashlytics())

		AndroidThreeTen.init(this)

		Timber.plant(Timber.DebugTree())

		Stetho.initializeWithDefaults(this)

//        Traceur.enableLogging()

		instance = this

		appComponent = DaggerAppComponent.builder()
				.appModule(AppModule(this))
				.persistenceModule(PersistenceModule(this))
				.build()
	}

}
