package me.khol.ackeeplanner.extensions

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity

/**
 * Extensions for Activities and Fragments for easier retrieval of ViewModels
 *
 * Register ViewModels in [me.khol.ackeeplanner.di.ViewModelModule]
 *
 * @author David Bilik [david.bilik@ackee.cz]
 * @since 10/11/2017
 **/

inline fun <reified T : ViewModel> Fragment.getViewModel(factory: ViewModelProvider.Factory): T {
	return ViewModelProviders.of(this, factory).get(T::class.java)
}

inline fun <reified T : ViewModel> FragmentActivity.getViewModel(factory: ViewModelProvider.Factory): T {
	return ViewModelProviders.of(this, factory).get(T::class.java)
}
