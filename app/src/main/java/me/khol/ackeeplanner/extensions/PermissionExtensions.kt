package me.khol.ackeeplanner.extensions

import android.app.Activity
import android.content.Context
import android.support.v4.app.Fragment
import pub.devrel.easypermissions.EasyPermissions

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 28.11.2017
 **/

/**
 * Asks for a permission when called for the first time. Doesn't show dialog again if user has
 * declined the permission before.
 * To show dialog again, call Activity.requestPermissions().
 */
fun Context.hasPermissions(vararg perms: String) = EasyPermissions.hasPermissions(this, *perms)

fun Activity.requestPermissions(rationale: String, requestCode: Int, vararg perms: String) =
		EasyPermissions.requestPermissions(this, rationale, android.R.string.ok, android.R.string.cancel, requestCode, *perms)

fun Fragment.requestEasyPermissions(rationale: String, requestCode: Int, vararg perms: String) =
		EasyPermissions.requestPermissions(this, rationale, android.R.string.ok, android.R.string.cancel, requestCode, *perms)
