package me.khol.ackeeplanner.extensions

import android.content.Context
import android.net.ConnectivityManager
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability

/**
 * Helper functions related to Google Play Services
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 05.10.2017
 **/


/**
 * Checks whether the device currently has a network connection.
 * @return true if the device has a network connection, false otherwise.
 */
fun Context.isDeviceOnline(): Boolean {
	val connMgr = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
	val networkInfo = connMgr.activeNetworkInfo
	return networkInfo != null && networkInfo.isConnected
}

/**
 * Check that Google Play services APK is installed and up to date.
 * @return true if Google Play Services is available and up to
 * date on this device; false otherwise.
 */
fun Context.isGooglePlayServicesAvailable(): Boolean {
	val apiAvailability = GoogleApiAvailability.getInstance()
	val connectionStatusCode = apiAvailability.isGooglePlayServicesAvailable(this)
	return connectionStatusCode == ConnectionResult.SUCCESS
}
