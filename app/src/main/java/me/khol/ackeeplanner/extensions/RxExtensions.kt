package me.khol.ackeeplanner.extensions

import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Extensions for easy subscription on different threads.
 *
 * Created by David Khol [david@khol.me] on 10. 3. 2018.
 */

/**
 * Safely dispose = if not null and not already disposed
 */
fun Disposable?.safeDispose() {
	if (this != null && !this.isDisposed) {
		dispose()
	}
}

fun <T> Flowable<T>.observeOnMainThread(): Flowable<T> = observeOn(AndroidSchedulers.mainThread())
fun <T> Flowable<T>.subscribeOnIO(): Flowable<T> = subscribeOn(Schedulers.io())
fun <T> Flowable<T>.subscribeOnNewThread(): Flowable<T> = subscribeOn(Schedulers.newThread())
fun <T> Flowable<T>.subscribeOnComputation(): Flowable<T> = subscribeOn(Schedulers.computation())
fun <T> Observable<T>.observeOnMainThread(): Observable<T> = observeOn(AndroidSchedulers.mainThread())
fun <T> Observable<T>.subscribeOnIO(): Observable<T> = subscribeOn(Schedulers.io())
fun <T> Observable<T>.subscribeOnNewThread(): Observable<T> = subscribeOn(Schedulers.newThread())
fun <T> Observable<T>.subscribeOnComputation(): Observable<T> = subscribeOn(Schedulers.computation())
fun Completable.observeOnMainThread(): Completable = observeOn(AndroidSchedulers.mainThread())
fun Completable.subscribeOnIO(): Completable = subscribeOn(Schedulers.io())
fun Completable.subscribeOnNewThread(): Completable = subscribeOn(Schedulers.newThread())
fun Completable.subscribeOnComputation(): Completable = subscribeOn(Schedulers.computation())
fun <T> Maybe<T>.observeOnMainThread(): Maybe<T> = observeOn(AndroidSchedulers.mainThread())
fun <T> Maybe<T>.subscribeOnIO(): Maybe<T> = subscribeOn(Schedulers.io())
fun <T> Maybe<T>.subscribeOnNewThread(): Maybe<T> = subscribeOn(Schedulers.newThread())
fun <T> Maybe<T>.subscribeOnComputation(): Maybe<T> = subscribeOn(Schedulers.computation())
fun <T> Single<T>.observeOnMainThread(): Single<T> = observeOn(AndroidSchedulers.mainThread())
fun <T> Single<T>.subscribeOnIO(): Single<T> = subscribeOn(Schedulers.io())
fun <T> Single<T>.subscribeOnNewThread(): Single<T> = subscribeOn(Schedulers.newThread())
fun <T> Single<T>.subscribeOnComputation(): Single<T> = subscribeOn(Schedulers.computation())
