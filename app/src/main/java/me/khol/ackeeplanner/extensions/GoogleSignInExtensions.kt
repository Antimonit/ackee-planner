package me.khol.ackeeplanner.extensions

import android.app.Activity
import android.content.Context
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.Scopes
import com.google.android.gms.common.api.Scope
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.util.ExponentialBackOff
import com.google.api.services.calendar.CalendarScopes

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 04.12.2017
 **/

object Permission {
	var CONTACTS_READ = "https://www.googleapis.com/auth/contacts.readonly"
	var CALENDAR = CalendarScopes.CALENDAR
	var CALENDAR_READ = CalendarScopes.CALENDAR_READONLY
	var EMAIL = Scopes.EMAIL
}

object MyScope {
	val CONTACTS_READ = Scope(Permission.CONTACTS_READ)
	val CALENDAR = Scope(Permission.CALENDAR)
	val CALENDAR_READ = Scope(Permission.CALENDAR_READ)
	val EMAIL = Scope(Permission.EMAIL)
}


fun GoogleSignInAccount?.hasGooglePermissions(vararg scopes: Scope): Boolean {
	return GoogleSignIn.hasPermissions(this, *scopes)
}

fun Context.getLastSignedInAccount(): GoogleSignInAccount? {
	return GoogleSignIn.getLastSignedInAccount(this)
}

fun Activity.requestGooglePermissions(requestCode: Int, vararg scopes: Scope) {
	GoogleSignIn.requestPermissions(
			this,
			requestCode,
			getLastSignedInAccount(),
			*scopes)
}

fun Context.credentialUsingOAuth2(vararg permissions: String): GoogleAccountCredential {
	return GoogleAccountCredential
			.usingOAuth2(this, permissions.toList())
			.setBackOff(ExponentialBackOff())
}


private const val SERVER_CLIENT_ID = "107415524478-mijd5oduolfe7ee4bdubmig7r5mjrrq0.apps.googleusercontent.com"

fun Context.getGoogleSignInClient(): GoogleSignInClient {
	val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
			.requestScopes(MyScope.CALENDAR_READ)
			.requestServerAuthCode(SERVER_CLIENT_ID)
			.requestIdToken(SERVER_CLIENT_ID)
			.requestEmail()
			.build()

	return GoogleSignIn.getClient(this, gso)
}

fun Context.getGoogleSignInClient(accountName: String): GoogleSignInClient {
	val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
			.requestScopes(MyScope.CALENDAR_READ)
			.requestServerAuthCode(SERVER_CLIENT_ID)
			.requestIdToken(SERVER_CLIENT_ID)
			.requestEmail()
			.setAccountName(accountName)
			.build()

	return GoogleSignIn.getClient(this, gso)
}
