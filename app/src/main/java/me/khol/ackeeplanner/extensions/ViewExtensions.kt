package me.khol.ackeeplanner.extensions

import android.content.Context
import android.graphics.Typeface
import android.support.design.widget.Snackbar
import android.support.v4.content.res.ResourcesCompat
import android.view.View
import android.widget.TextView
import android.widget.Toast
import java.lang.IllegalStateException
import java.util.*

/**
 * Snack related extensions
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 28.11.2017
 **/


private val typefaceMap: WeakHashMap<Context, MutableMap<Int, Typeface?>> = WeakHashMap()

var TextView.font: Int
	get() {
		throw IllegalStateException()
	}
	set(value) {
		var contextMap = typefaceMap[context]
		if (contextMap == null) {
			contextMap = HashMap()
			typefaceMap[context] = contextMap
		}

		var font = contextMap[value]
		if (font == null) {
			font = ResourcesCompat.getFont(context, value)
			contextMap[value] = font
		}

		this.typeface = font
	}


fun View.showSnack(text: CharSequence, duration: Int = Snackbar.LENGTH_LONG) {
	Snackbar.make(this, text, duration).show()
}

fun View.showSnack(resId: Int, duration: Int = Snackbar.LENGTH_LONG) {
	Snackbar.make(this, resId, duration).show()
}

fun Context.showToast(text: CharSequence, duration: Int = Toast.LENGTH_LONG) {
	Toast.makeText(this, text, duration).show()
}

fun Context.showToast(resId: Int, duration: Int = Toast.LENGTH_LONG) {
	Toast.makeText(this, resId, duration).show()
}

fun View.displayNoInternetSnack() {
	showSnackWithAction("No internet connection.", "Close") {
		dismiss()
	}
}

fun View.showSnackWithAction(messageText: String, actionText: String, duration: Int = Snackbar.LENGTH_INDEFINITE, action: Snackbar.(View) -> Unit) {
	Snackbar.make(this, messageText, duration).apply {
		setAction(actionText) { action(it) }
	}.show()
}
