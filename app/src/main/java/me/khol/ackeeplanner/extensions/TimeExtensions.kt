package me.khol.ackeeplanner.extensions

import org.threeten.bp.*
import org.threeten.bp.format.DateTimeFormatter

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 09.04.2018
 **/

@Deprecated("Use DateTimeFormatter.ISO_INSTANT instead")
private val dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ")

fun parseInstant(date: String): Instant {
	return Instant.parse(date)
}

fun formatInstant(date: Instant): String {
	return DateTimeFormatter.ISO_INSTANT.format(date)
}

fun parseZonedDateTime(date: String): ZonedDateTime {
	return ZonedDateTime.parse(date)
}

private const val TODAY = "'Today'"
private const val TOMORROW = "'Tomorrow'"
private const val HOURS_MINUTES = "H:mm"
private val formatToday = DateTimeFormatter.ofPattern("$TODAY $HOURS_MINUTES")
private val formatTomorrow = DateTimeFormatter.ofPattern("$TOMORROW $HOURS_MINUTES")
private val formatAnotherYear = DateTimeFormatter.ofPattern("yyyy MMMM dd, EEEE $HOURS_MINUTES")
private val formatOther = DateTimeFormatter.ofPattern("MMMM dd, EEEE $HOURS_MINUTES")

fun Instant.formatRelativeToToday(): String {
	val now = LocalDateTime.now(ZoneOffset.systemDefault())
	val then = LocalDateTime.ofInstant(this, ZoneOffset.systemDefault())
	val period = Period.between(now.toLocalDate(), then.toLocalDate())
	return when {
		period.days == 0 -> formatToday
		period.days == 1 -> formatTomorrow
		now.year != then.year -> formatAnotherYear
		else -> formatOther
	}.format(then)
}
