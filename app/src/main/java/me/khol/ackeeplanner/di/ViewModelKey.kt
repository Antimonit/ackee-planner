package me.khol.ackeeplanner.di

import android.arch.lifecycle.ViewModel
import dagger.MapKey
import kotlin.reflect.KClass

/**
 * Key describing ViewModel stuff
 *
 * @author David Bilik [david.bilik@ackee.cz]
 * @since 10/11/2017
 **/
@MustBeDocumented
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
@MapKey
internal annotation class ViewModelKey(val value: KClass<out ViewModel>)
