package me.khol.ackeeplanner.di

import android.content.ContentResolver
import android.content.Context
import dagger.Module
import dagger.Provides
import me.khol.ackeeplanner.App
import me.khol.ackeeplanner.oauth.OAuthManager
import javax.inject.Singleton

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 13.11.2017
 **/
@Module(includes = [
	GoogleModule::class,
	RemoteModule::class,
	ViewModelModule::class,
	PersistenceModule::class,
	RepositoryModule::class
])
class AppModule(private val app: App) {

	@Provides
	@Singleton
	fun provideApp(): App = app

	@Provides
	@Singleton
	fun provideContext(): Context = app

	@Provides
	@Singleton
	fun provideContentResolver(): ContentResolver = app.contentResolver

	@Provides
	@Singleton
	fun provideOauthManager(ctx: Context) = OAuthManager(ctx)

}
