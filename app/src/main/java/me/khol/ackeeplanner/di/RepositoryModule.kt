package me.khol.ackeeplanner.di

import dagger.Binds
import dagger.Module
import me.khol.ackeeplanner.model.*
import me.khol.ackeeplanner.model.calendar.*
import me.khol.ackeeplanner.model.ContactsRepository
import me.khol.ackeeplanner.model.ContactsRepositoryImpl
import me.khol.ackeeplanner.model.AccountRepository
import me.khol.ackeeplanner.model.AccountRepositoryImpl
import me.khol.ackeeplanner.model.event.LocalEventRepository
import me.khol.ackeeplanner.model.event.LocalEventRepositoryImpl

/**
 * Module for providing repositories
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 7.3.2018
 **/
@Module
abstract class RepositoryModule {

	@Binds
	abstract fun provideEventRepository(repo: LocalEventRepositoryImpl): LocalEventRepository

	@Binds
	abstract fun provideAccountRepository(repo: AccountRepositoryImpl): AccountRepository

	@Binds
	abstract fun provideGoogleCalendarRepository(repo: GoogleCalendarRepositoryImpl): GoogleCalendarRepository

	@Binds
	abstract fun provideLocalCalendarRepository(repo: LocalCalendarRepositoryImpl): LocalCalendarRepository

	@Binds
	abstract fun providePlannerRepository(repo: MeetingRepositoryImpl): MeetingRepository

	@Binds
	abstract fun provideContactsRepository(repo: ContactsRepositoryImpl): ContactsRepository

	@Binds
	abstract fun provideLocationRepository(repo: LocationRepositoryImpl): LocationRepository

}
