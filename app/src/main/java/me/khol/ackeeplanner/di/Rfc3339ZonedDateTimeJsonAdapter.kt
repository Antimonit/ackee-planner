package me.khol.ackeeplanner.di

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.Rfc3339DateJsonAdapter

import org.threeten.bp.Instant
import org.threeten.bp.ZoneOffset
import org.threeten.bp.ZonedDateTime

import java.io.IOException
import java.util.Date


/**
 * Formats dates using [RFC 3339](https://www.ietf.org/rfc/rfc3339.txt), which is
 * formatted like `2015-09-26T18:23:50.250Z`.
 */
class Rfc3339ZonedDateTimeJsonAdapter : JsonAdapter<ZonedDateTime>() {

	private val dateJsonAdapter = Rfc3339DateJsonAdapter()

	@Synchronized
	@Throws(IOException::class)
	override fun fromJson(reader: JsonReader): ZonedDateTime? {
		val date = dateJsonAdapter.fromJson(reader) ?: return null
		return Instant.ofEpochMilli(date.time).atOffset(ZoneOffset.UTC).toZonedDateTime()
	}

	@Synchronized
	@Throws(IOException::class)
	override fun toJson(writer: JsonWriter, value: ZonedDateTime?) {
		val date = if (value == null) {
			null
		} else {
			Date(value.toInstant().toEpochMilli())
		}
		dateJsonAdapter.toJson(writer, date)
	}
}
