package me.khol.ackeeplanner.di

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import dagger.Module
import dagger.Provides
import me.khol.ackeeplanner.SharedPreferencesInteractor
import me.khol.ackeeplanner.model.LocationRepositoryImpl
import me.khol.ackeeplanner.model.db.PlannerDatabase
import me.khol.ackeeplanner.model.db.location.DBRecentLocation
import org.jetbrains.anko.coroutines.experimental.bg
import javax.inject.Singleton

/**
 * Dagger module for providing persistence related dependencies
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 7.3.2018
 **/
@Module
open class PersistenceModule(ctx: Context) {

	private val callback = object : RoomDatabase.Callback() {
		override fun onCreate(db: SupportSQLiteDatabase) {
			bg {
				val location = LocationRepositoryImpl.defaultLocation
				room.locationDao().insert(DBRecentLocation(
						location.id!!,
						location.name,
						location.address!!,
						location.latLng!!,
						location.viewport!!
				))
			}
		}
	}

	private val room: PlannerDatabase = Room
			.databaseBuilder(ctx, PlannerDatabase::class.java, "planner.db")
			.addCallback(callback)
			.fallbackToDestructiveMigration()
			.build()

	@Provides
	@Singleton
	fun provideSharedPreferencesInteractor(context: Context) = SharedPreferencesInteractor(context)

	@Provides
	@Singleton
	fun provideContactDao() = room.contactDao()

	@Provides
	@Singleton
	fun provideMeetingDao() = room.meetingDao()

	@Provides
	@Singleton
	fun provideLocationDao() = room.locationDao()

	@Provides
	@Singleton
	fun provideUserDao() = room.userDao()

}
