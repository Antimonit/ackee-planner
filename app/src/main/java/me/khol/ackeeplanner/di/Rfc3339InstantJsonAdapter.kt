package me.khol.ackeeplanner.di

import com.squareup.moshi.*

import org.threeten.bp.Instant
import org.threeten.bp.ZoneOffset
import org.threeten.bp.ZonedDateTime

import java.io.IOException
import java.util.Date


/**
 * Formats dates using [RFC 3339](https://www.ietf.org/rfc/rfc3339.txt), which is
 * formatted like `2015-09-26T18:23:50.250Z`.
 */
class Rfc3339InstantJsonAdapter : JsonAdapter<Instant>() {

	private val dateJsonAdapter = Rfc3339DateJsonAdapter()

	@Synchronized
	@Throws(IOException::class)
	override fun fromJson(reader: JsonReader): Instant? {
		val date = dateJsonAdapter.fromJson(reader) ?: return null
		return Instant.ofEpochMilli(date.time)
	}

	@Synchronized
	@Throws(IOException::class)
	override fun toJson(writer: JsonWriter, value: Instant?) {
		val date = if (value == null) {
			null
		} else {
			Date(value.toEpochMilli())
		}
		dateJsonAdapter.toJson(writer, date)
	}
}
