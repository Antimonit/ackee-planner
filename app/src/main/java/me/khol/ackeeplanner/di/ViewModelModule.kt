package me.khol.ackeeplanner.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import me.khol.ackeeplanner.screens.base.viewmodel.ViewModelFactory
import me.khol.ackeeplanner.screens.times.create.TimesCreateViewModel
import me.khol.ackeeplanner.screens.times.select.TimesSelectViewModel
import me.khol.ackeeplanner.screens.calendar.ChooseCalendarViewModel
import me.khol.ackeeplanner.screens.deeplink.DeepLinkViewModel
import me.khol.ackeeplanner.screens.meeting.create.step1.CreateMeeting1ViewModel
import me.khol.ackeeplanner.screens.guests.GuestsViewModel
import me.khol.ackeeplanner.screens.location.LocationViewModel
import me.khol.ackeeplanner.screens.login.LoginViewModel
import me.khol.ackeeplanner.screens.main.MainViewModel
import me.khol.ackeeplanner.screens.main.invitations.InvitationsViewModel
import me.khol.ackeeplanner.screens.main.myMeetings.MyMeetingsViewModel
import me.khol.ackeeplanner.screens.meeting.create.step2.CreateMeeting2ViewModel
import me.khol.ackeeplanner.screens.meeting.detail.MyMeetingDetailViewModel
import me.khol.ackeeplanner.screens.meeting.invitation.MeetingInvitationViewModel
import me.khol.ackeeplanner.screens.times.calendars.CalendarsDialogViewModel
import me.khol.ackeeplanner.screens.times.view.TimesViewViewModel


/**
 * Dagger Module for providing general ViewModels and ViewModel factory
 *
 * @author David Bilik [david.bilik@ackee.cz]
 * @since 10/11/2017
 **/
@Suppress("unused")
@Module
abstract class ViewModelModule {

	@Binds
	@IntoMap
	@ViewModelKey(LoginViewModel::class)
	abstract fun bindLoginViewModel(vm: LoginViewModel): ViewModel

	@Binds
	@IntoMap
	@ViewModelKey(MainViewModel::class)
	abstract fun bindMainViewModel(vm: MainViewModel): ViewModel

	@Binds
	@IntoMap
	@ViewModelKey(MyMeetingsViewModel::class)
	abstract fun bindMyMeetingsViewModel(vm: MyMeetingsViewModel): ViewModel

	@Binds
	@IntoMap
	@ViewModelKey(InvitationsViewModel::class)
	abstract fun bindInvitationsViewModel(vm: InvitationsViewModel): ViewModel

	@Binds
	@IntoMap
	@ViewModelKey(CreateMeeting1ViewModel::class)
	abstract fun bindCreateMeetingViewModel(vm: CreateMeeting1ViewModel): ViewModel

	@Binds
	@IntoMap
	@ViewModelKey(CreateMeeting2ViewModel::class)
	abstract fun bindCreateMeeting2ViewModel(vm: CreateMeeting2ViewModel): ViewModel

	@Binds
	@IntoMap
	@ViewModelKey(GuestsViewModel::class)
	abstract fun bindGuestsViewModel(vm: GuestsViewModel): ViewModel

	@Binds
	@IntoMap
	@ViewModelKey(LocationViewModel::class)
	abstract fun bindLocationViewModel(vm: LocationViewModel): ViewModel

	@Binds
	@IntoMap
	@ViewModelKey(ChooseCalendarViewModel::class)
	abstract fun bindChooseCalendarViewModel(vm: ChooseCalendarViewModel): ViewModel

	@Binds
	@IntoMap
	@ViewModelKey(MyMeetingDetailViewModel::class)
	abstract fun bindMeetingDetailViewModel(vm: MyMeetingDetailViewModel): ViewModel

	@Binds
	@IntoMap
	@ViewModelKey(MeetingInvitationViewModel::class)
	abstract fun bindMeetingInvitationViewModel(vm: MeetingInvitationViewModel): ViewModel

	@Binds
	@IntoMap
	@ViewModelKey(TimesCreateViewModel::class)
	abstract fun bindCreateCalendarViewModel(vm: TimesCreateViewModel): ViewModel

	@Binds
	@IntoMap
	@ViewModelKey(TimesSelectViewModel::class)
	abstract fun bindSelectCalendarViewModel(vm: TimesSelectViewModel): ViewModel

	@Binds
	@IntoMap
	@ViewModelKey(TimesViewViewModel::class)
	abstract fun bindViewCalendarViewModel(vm: TimesViewViewModel): ViewModel

	@Binds
	@IntoMap
	@ViewModelKey(CalendarsDialogViewModel::class)
	abstract fun bindCalendarsDialogViewModel(vm: CalendarsDialogViewModel): ViewModel

	@Binds
	@IntoMap
	@ViewModelKey(DeepLinkViewModel::class)
	abstract fun bindDeepLinkViewModel(vm: DeepLinkViewModel): ViewModel

	@Binds
	abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

}
