package me.khol.ackeeplanner.di

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.squareup.moshi.Moshi
import com.squareup.moshi.Rfc3339DateJsonAdapter
import dagger.Module
import dagger.Provides
import me.khol.ackeeplanner.api.ApiDescription
import me.khol.ackeeplanner.api.ApiInteractor
import me.khol.ackeeplanner.api.ApiInteractorImpl
import me.khol.ackeeplanner.constants.NetworkConstants
import me.khol.ackeeplanner.model.db.dao.UserDao
import me.khol.ackeeplanner.oauth.OAuthInterceptor
import me.khol.ackeeplanner.oauth.OAuthManager
import me.khol.extensions.isDebug
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.threeten.bp.Instant
import org.threeten.bp.ZonedDateTime
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 13.11.2017
 **/
@Module
class RemoteModule {

	@Provides
	@Singleton
	fun provideApiInteractor(apiDescription: ApiDescription, userDao: UserDao, manager: OAuthManager): ApiInteractor = ApiInteractorImpl(apiDescription, userDao, manager)

	@Provides
	@Singleton
	fun provideApiService(client: OkHttpClient, moshi: Moshi): ApiDescription {
		return Retrofit.Builder()
				.baseUrl(NetworkConstants.API_URL)
				.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
				.addConverterFactory(MoshiConverterFactory.create(moshi))
				.client(client)
				.build()
				.create(ApiDescription::class.java)
	}

	@Provides
	@Singleton
	fun provideMoshi(): Moshi {
		return Moshi.Builder()
				.add(Date::class.java, Rfc3339DateJsonAdapter())
				.add(ZonedDateTime::class.java, Rfc3339ZonedDateTimeJsonAdapter())
				.add(Instant::class.java, Rfc3339InstantJsonAdapter())
				.build()
	}

	@Provides
	@Singleton
	@Named("logging_interceptor")
	fun provideLoggingInterceptor(): Interceptor {
		return HttpLoggingInterceptor()
				.setLevel(if (isDebug) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE)
	}

//    @Provides
//    @Singleton
//    @Named("header_interceptor")
//    fun provideHeaderInterceptor(@Named("device_id") deviceId: String,
//                                 @Named("user_agent") userAgent: String): Interceptor {
//        return HeaderInterceptor(deviceId, userAgent)
//    }

	@Provides
	@Singleton
	@Named("oauth_interceptor")
	fun provideOAuthInterceptor(manager: OAuthManager): Interceptor {
		return OAuthInterceptor(manager)
	}


	@Provides
	@Singleton
	fun provideOkHttpClient(@Named("logging_interceptor") loggingInterceptor: Interceptor,
//                            @Named("header_interceptor") headerInterceptor: Interceptor,
							@Named("oauth_interceptor") oAuthInterceptor: Interceptor
	): OkHttpClient {
		return OkHttpClient.Builder()
				.connectTimeout(30, TimeUnit.SECONDS)
				.writeTimeout(30, TimeUnit.SECONDS)
				.readTimeout(30, TimeUnit.SECONDS)
//                .addNetworkInterceptor(loggingInterceptor)
				.addInterceptor(loggingInterceptor)
				.addNetworkInterceptor(StethoInterceptor())
//                .addNetworkInterceptor(headerInterceptor)
				.addNetworkInterceptor(oAuthInterceptor)
				.build()
	}
}
