package me.khol.ackeeplanner.di

import android.content.Context
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import dagger.Module
import dagger.Provides
import me.khol.ackeeplanner.api.GoogleCalendarInteractor
import me.khol.ackeeplanner.api.GoogleCalendarInteractorImpl
import me.khol.ackeeplanner.extensions.Permission
import me.khol.ackeeplanner.extensions.credentialUsingOAuth2
import javax.inject.Named
import javax.inject.Singleton


/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 22.11.2017
 **/
@Module
class GoogleModule {

	@Provides
	@Singleton
	fun provideGoogleAccountCredential(context: Context): GoogleAccountCredential {
		return context.credentialUsingOAuth2(Permission.CALENDAR, Permission.CALENDAR_READ)
	}

	@Provides
	@Singleton
	@Named("contacts_credential")
	fun provideContactsGoogleAccountCredential(context: Context): GoogleAccountCredential {
		return context.credentialUsingOAuth2(Permission.CONTACTS_READ)
	}

	@Provides
	@Singleton
	fun provideGoogleCalendarInteractor(credential: GoogleAccountCredential): GoogleCalendarInteractor = GoogleCalendarInteractorImpl(credential)

}
