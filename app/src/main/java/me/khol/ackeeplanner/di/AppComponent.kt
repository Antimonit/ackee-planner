package me.khol.ackeeplanner.di

import dagger.Component
import me.khol.ackeeplanner.screens.base.activity.BaseActivity
import me.khol.ackeeplanner.screens.base.fragment.BaseFragment
import me.khol.ackeeplanner.screens.times.BaseTimesActivity
import me.khol.ackeeplanner.screens.meeting.create.step1.CreateMeeting1Fragment
import me.khol.ackeeplanner.screens.login.LoginActivity
import me.khol.ackeeplanner.screens.main.MainActivity
import me.khol.ackeeplanner.screens.main.invitations.InvitationsFragment
import me.khol.ackeeplanner.screens.main.myMeetings.MyMeetingsFragment
import me.khol.ackeeplanner.screens.times.calendars.CalendarsDialogFragment
import javax.inject.Singleton

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 13.11.2017
 **/
@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

	fun inject(baseActivity: BaseActivity)

	fun inject(activity: MainActivity)

	fun inject(activity: LoginActivity)

	fun inject(activity: BaseTimesActivity)


	fun inject(baseFragment: BaseFragment)

	fun inject(fragment: MyMeetingsFragment)

	fun inject(fragment: InvitationsFragment)

	fun inject(fragment: CreateMeeting1Fragment)

	fun inject(fragment: CalendarsDialogFragment)

}
