package me.khol.ackeeplanner.oauth

import android.content.Context
import me.khol.extensions.clear
import me.khol.extensions.getStringPref
import me.khol.extensions.setStringPref
import me.khol.extensions.transaction
import me.khol.ackeeplanner.model.api.Credentials

/**
 * Manager for dealing with Oauth2 authorization
 * Created by David Bilik[david.bilik@ackee.cz] on {03/03/16}
 */
class OAuthManager(ctx: Context) {

	companion object {
		private const val SP_NAME = "oauth"
		private const val ACCESS_TOKEN_KEY = "access_token"
		private const val REFRESH_TOKEN_KEY = "refresh_token"
	}

	private val sp = ctx.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE)

	fun clearCredentials() = sp.clear()

	var accessToken: String?
		get() = sp.getStringPref(ACCESS_TOKEN_KEY)
		set(value) {
			sp.setStringPref(ACCESS_TOKEN_KEY, value)
		}

	var refreshToken: String?
		get() = sp.getStringPref(REFRESH_TOKEN_KEY)
		set(value) {
			sp.setStringPref(REFRESH_TOKEN_KEY, value)
		}

	fun saveCredentials(credentials: Credentials) {
		sp.transaction {
			putString(ACCESS_TOKEN_KEY, credentials.accessToken)
			if (credentials.refreshToken != null) {
				putString(REFRESH_TOKEN_KEY, credentials.refreshToken)
			}
		}
	}

}
