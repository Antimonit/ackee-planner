package me.khol.ackeeplanner.oauth

import android.text.TextUtils

import java.io.IOException

import okhttp3.Interceptor
import okhttp3.Response

/**
 * Interceptor for oauth2 authorization
 * Created by David Bilik[david.bilik@ackee.cz] on {03/03/16}
 */
class OAuthInterceptor(private val oAuthManager: OAuthManager) : Interceptor {

	@Throws(IOException::class)
	override fun intercept(chain: Interceptor.Chain): Response {
		val originalRequest = chain.request()
		val builder = originalRequest.newBuilder()

		val accessToken = oAuthManager.accessToken
		if (!TextUtils.isEmpty(accessToken)) {
			builder.addHeader("Authorization", "Bearer $accessToken")
		}

		return chain.proceed(builder.build())
	}

}
