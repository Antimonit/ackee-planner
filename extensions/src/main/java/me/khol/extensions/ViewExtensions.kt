package me.khol.extensions

import android.annotation.TargetApi
import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.os.Build
import android.support.annotation.AttrRes
import android.support.annotation.ColorInt
import android.support.annotation.ColorRes
import android.support.annotation.DrawableRes
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.support.v4.graphics.drawable.DrawableCompat
import android.support.v4.view.ViewCompat
import android.transition.TransitionManager
import android.view.View
import android.view.ViewGroup

/**
 * TODO: 28. 11. 2017 david.khol: add class description
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 28.11.2017
 **/

var View.elevationCompat: Float
	get() {
		return ViewCompat.getElevation(this)
	}
	set(value) {
		ViewCompat.setElevation(this, value)
	}

fun View.getColorAttr(@AttrRes attr: Int): Int {
	val ta = context.obtainStyledAttributes(intArrayOf(attr))
	val value = ta.getColor(0, 0)
	ta.recycle()
	return value
}

fun View.getColorStateListAttr(@AttrRes attr: Int): ColorStateList? {
	val ta = context.obtainStyledAttributes(intArrayOf(attr))
	val value = ta.getColorStateList(0)
	ta.recycle()
	return value
}

fun View.getDrawableAttr(@AttrRes attr: Int): Drawable? {
	val ta = context.obtainStyledAttributes(intArrayOf(attr))
	val value = ta.getDrawable(0)
	ta.recycle()
	return value
}

fun View.getDimensionAttr(@AttrRes attr: Int): Int {
	val ta = context.obtainStyledAttributes(intArrayOf(attr))
	val value = ta.getDimensionPixelSize(0, 0)
	ta.recycle()
	return value
}


@ColorInt
fun View.getColor(@ColorRes colorId: Int): Int {
	return ContextCompat.getColor(context, colorId)
}

fun View.getColorStateList(@ColorRes colorId: Int): ColorStateList? {
	return ContextCompat.getColorStateList(context, colorId)
}

fun View.getDrawable(@DrawableRes drawableId: Int): Drawable? {
	return ContextCompat.getDrawable(context, drawableId)
}

fun View.getTintedDrawable(@DrawableRes drawableId: Int, @ColorRes colorId: Int): Drawable? {
	val tint: Int = ContextCompat.getColor(context, colorId)
	val drawable: Drawable? = ContextCompat.getDrawable(context, drawableId)
	if (drawable != null) {
		drawable.mutate()
		DrawableCompat.setTint(drawable, tint)
	}
	return drawable
}

var View.foregroundResource: Int
	get() {
		throw UnsupportedOperationException("No get")
	}
	@TargetApi(Build.VERSION_CODES.M)
	set(@DrawableRes value) {
		foreground = ResourcesCompat.getDrawable(resources, value, null)
	}

var View.isVisible: Boolean
	get() = visibility == View.VISIBLE
	set(value) {
		visibility = if (value) View.VISIBLE else View.GONE
	}

fun ViewGroup.beginDelayedTransition() {
	if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
		TransitionManager.beginDelayedTransition(this)
	}
}
