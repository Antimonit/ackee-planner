package me.khol.extensions

/**
 * @author David Khol [david.khol@ackee.cz]
 * @since 27.03.2018
 **/

fun Int.withAlpha(alpha: Int): Int {
	return this.transparent or (alpha shl 24)
}

val Int.transparent: Int
	get() = this and 0xFFFFFF

val Int.opaque: Int
	get() = this or (0xFF shl 24)
