package me.khol.extensions

import android.content.Context

/**
 * Extensions for easy transformation of DP to pixels
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 16.03.2018
 **/

fun Context.dp(value: Int): Int = dpf(value).toInt()
fun Context.dp(value: Float): Int = dpf(value).toInt()

fun Context.dpf(value: Int): Float = value * resources.displayMetrics.density
fun Context.dpf(value: Float): Float = value * resources.displayMetrics.density

fun Context.sp(value: Int): Int = spf(value).toInt()
fun Context.sp(value: Float): Int = spf(value).toInt()

fun Context.spf(value: Int): Float = value * resources.displayMetrics.scaledDensity
fun Context.spf(value: Float): Float = value * resources.displayMetrics.scaledDensity
