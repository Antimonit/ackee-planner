package me.khol.extensions

import android.os.Build
import me.khol.extensions.BuildConfig


/**
 * Extensions that do not fit any other description
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 14. 8. 2017
 **/

/**
 * Get class's TAG from anywhere
 */
inline val <reified T> T.TAG: String
	get() = T::class.java.simpleName


/**
 * Global property indicating if application is debuggable
 */
val isDebug = BuildConfig.DEBUG


fun targetApi(targetApi: Int): Boolean {
	return Build.VERSION.SDK_INT >= targetApi
}
