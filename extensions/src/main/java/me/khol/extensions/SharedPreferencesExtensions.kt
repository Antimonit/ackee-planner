package me.khol.extensions

import android.content.SharedPreferences

/**
 * Extensions for easier manipulation of [SharedPreferences]
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 27.11.2017
 **/

object UNDEFINED {
	const val INT = -1
	const val FLOAT = -1f
	const val LONG = -1L
	const val BOOL = false
}

fun SharedPreferences.clear() = transaction { clear() }

fun SharedPreferences.setIntPref(pref: String, data: Int) = transaction { putInt(pref, data) }
fun SharedPreferences.setLongPref(pref: String, data: Long) = transaction { putLong(pref, data) }
fun SharedPreferences.setFloatPref(pref: String, data: Float) = transaction { putFloat(pref, data) }
fun SharedPreferences.setBoolPref(pref: String, data: Boolean) = transaction { putBoolean(pref, data) }
fun SharedPreferences.setStringPref(pref: String, data: String?) = transaction { putString(pref, data) }

fun SharedPreferences.getIntPref(pref: String, defaultValue: Int = UNDEFINED.INT): Int = getInt(pref, defaultValue)
fun SharedPreferences.getLongPref(pref: String, defaultValue: Long = UNDEFINED.LONG): Long = getLong(pref, defaultValue)
fun SharedPreferences.getFloatPref(pref: String, defaultValue: Float = UNDEFINED.FLOAT): Float = getFloat(pref, defaultValue)
fun SharedPreferences.getBoolPref(pref: String, defaultValue: Boolean = UNDEFINED.BOOL): Boolean = getBoolean(pref, defaultValue)
fun SharedPreferences.getStringPref(pref: String, defaultValue: String? = null): String? = getString(pref, defaultValue)

fun SharedPreferences.removePref(pref: String) = transaction { remove(pref) }

fun SharedPreferences.transaction(f: SharedPreferences.Editor.() -> Unit) {
	edit().apply {
		f()
		apply()
	}
}
