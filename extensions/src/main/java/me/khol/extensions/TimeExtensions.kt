package me.khol.extensions

import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.LocalTime
import org.threeten.bp.ZoneOffset
import org.threeten.bp.temporal.ChronoField

/**
 * Extensions related to Java 8 new Time Api.
 *
 * Uses back-ported Java 6 version.
 *
 * @author David Khol [david.khol@ackee.cz]
 * @since 19.03.2018
 **/

private val EPOCH = LocalDateTime.ofEpochSecond(0, 0, ZoneOffset.UTC)

fun LocalDateTime.daysSinceEpoch(): Long {
	return getLong(ChronoField.EPOCH_DAY)
}

fun LocalDate.toLocalDateTime(): LocalDateTime {
	return LocalDateTime.of(this, LocalTime.MIDNIGHT)
}

